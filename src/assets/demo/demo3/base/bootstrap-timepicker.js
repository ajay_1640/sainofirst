function getCurrentTime(date) {
    var hours = date.getHours(),
        minutes = date.getMinutes(),
        ampm = hours >= 12 ? 'pm' : 'am';

  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;

  return hours + ':' + minutes + ' ' + ampm;
}
var BootstrapTimepicker=
{
    init:function(){
        $("#m_timepicker_1,#m_timepicker_8").timepicker(
            {'timeFormat': 'h:i A',minuteStep:5,'minTime': getCurrentTime(new Date())}),
            $("#m_timepicker_10_modal,#m_timepicker_11_modal,#m_timepicker_4").timepicker(
                {'timeFormat':'HH:mm:ss',minuteStep:5,'minTime': "",hideMinutes:1,showMinutes:1})
            ,
            $("#bulksms_schedule").timepicker(
                {'timeFormat':'HH:mm:ss',minuteStep:5,'minTime':  getCurrentTime(new Date()),hideMinutes:1,showMinutes:1})
            ,
            $("#customize_schedule").timepicker(
                {'timeFormat':'HH:mm:ss',minuteStep:5,'minTime':  getCurrentTime(new Date()),hideMinutes:1,showMinutes:1})
            ,
            $("#m_timepicker_2, #m_timepicker_2_modal").timepicker(
                {minuteStep:1,defaultTime:"",showSeconds:!0,showMeridian:!1,snapToStep:!0})
                ,$("#m_timepicker_3, #m_timepicker_3_modal").timepicker({defaultTime:"11:45:20 AM",minuteStep:1,showSeconds:!0,showMeridian:!0})
                ,$("#m_timepicker_4, #m_timepicker_4_modal").timepicker({defaultTime:"10:30:20 AM",minuteStep:1,showSeconds:!0,showMeridian:!0}),
                $("#m_timepicker_1_validate, #m_timepicker_2_validate, #m_timepicker_3_validate").timepicker({minuteStep:1,showSeconds:!0,showMeridian:!1,snapToStep:!0})}};
                
                
                jQuery(document).ready(function(){BootstrapTimepicker.init()});