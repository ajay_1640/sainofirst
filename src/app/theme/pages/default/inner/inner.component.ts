import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import swal from 'sweetalert2';
declare let moment;

declare let selectpicket;
declare let $;

@Component({
    selector: "app-inner",
    templateUrl: "./inner.component.html",
    styleUrls: ['./inner.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class InnerComponent implements OnInit, AfterViewInit {
    agent_list = [{
        name: 'agent name',
        email_id: 'agent emailid',
        login_access: 'login access',
        status: 'status',
        username: 'username'

    }
    ];
    p = 1;
    calllogs_p = 1;
    name = "";
    access = false;
    status = 0;
    email = "";
    number = "";
    userid = "";
    password = "";
    list = ['Inactive', 'Active'];
    fromDate = '';
    toDate = '';
    dateRange;
    ar: any;
    fromDate_2 = '';
    toDate_2 = '';
    dateRange_2;
    ar_2: any;
    call_searchText = "";
    agentnames = "";
    numbers = "";
    extension = "";
    constructor(private _script: ScriptLoaderService) {

    }
    ngOnInit() {
        this._script.loadScripts('app-inner',
            ['assets/app/js/dashboard.js']);
        $('.selectpicker').selectpicker({
            style: 'btn-info',
            size: 4
        });

        //   $('#mySelect').on('loaded.bs.select', function (e) {
        //     // do something...
        //     console.log("select loaded");
        //   });
        $('#add_agent').on('shown.bs.modal', function(e) {
            $('#select1').selectpicker('refresh');

        })
        $(function() {

            var start = moment();
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
        $(function() {

            var start_2 = moment();
            var end_2 = moment();

            function cb(start, end) {
                $('#reportrange_2 span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            }

            $('#reportrange_2').daterangepicker({
                startDate_2: start_2,
                endDate_2: end_2,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start_2, end_2);

        });
        this.dateRange_2 = $('#reportrange span').text();
        this.ar_2 = this.dateRange.split(' - ');
        this.fromDate_2 = this.ar[0];
        this.toDate_2 = this.ar[1];

        this.dateRange = $('#reportrange span').text();
        this.ar = this.dateRange.split(' - ');
        this.fromDate = this.ar[0];
        this.toDate = this.ar[1];



    }
    ngAfterViewInit() {

        this._script.loadScripts('app-inner',
            ['assets/demo/demo3/base/bootstrap-select.js']);
    }
    access_changed() {
        this.access = !this.access;
        // $('.selectpicker').selectpicker('refresh');
    }
    add_agent_clicked() {
        this.name = "";
        this.status = 0;
        this.access = false;
        this.email = "";
        this.number = "";
        this.userid = "";
        this.password = "";

    }
    new_agent() {

    }
    saveagents() {

    }
    edit_agent_clicked(agent_index) {
        //  console.log(this.agent_list[agent_index]);

    }


}