import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortlinkComponent } from './pricing.component';

import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { FormsModule } from "@angular/forms";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ShortlinkComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, NgxPaginationModule, AmChartsModule, FormsModule, Ng2SearchPipeModule
    ],
    exports: [RouterModule],
    declarations: [ShortlinkComponent]

})

export class ShortlinkModule { }
