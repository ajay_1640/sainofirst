import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../app.service';
import { UserService } from '../../../../auth/_services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Helpers } from '../../../../helpers';
import { DomSanitizer } from '@angular/platform-browser';


declare let toastr,moment;
declare let $;
@Component({
    selector: 'app-pricing',
    templateUrl: './pricing.component.html',
    styleUrls:['./pri.component.css']
})
export class ShortlinkComponent implements OnInit {
    chart1: any;
    show_loading = false;
    showvisits = false;
    show_calendar = false;
    show_calendar1 = false;
    showgraph = false;
    pricing_p = 1;
    calendar_range = "";
    calendar_range1 = "";
    p_report=1;
    searchText = "";
    shortlink_title = "";
    shortlink_description="";
    shortlink_url = "";
    shortlink_tokenAlias="";
    shortlink_token = "";
    shortlink_webhookurl = "";
    shortlink_trackmble =true;
    shortlink_title_edit = "";
    shortlink_description_edit="";
    shortlink_url_edit = "";
    shortlink_tokenAlias_edit="";
    shortlink_token_edit = "";
    shortlink_webhookurl_edit = "";
    shortlink_trackmble_edit =true;
    shortlink_track_edit=false;
    searchshortlink = "";
    chart: any;
    shortUrlData=[];
    shorturlpagination=1;
    visitactivity=[];
    visit_p=1;
    shorturl:any;
    totalVisits=1;
    downloadReportActive=false;
    chart1Loader = false;
    shortlinkDownloadReport=[];
    downloadReport=[];
    shortlinkName="";
    loading=false;
    from_date="";
    to_date="";
    from_date1="";
    to_date1="";
    editShorturl:any={}
    intervalStartDate:any=0;
    timeIntervalData=false;
    intervalEndDate:any=0;
    constructor(private _appService: AppService, private AmCharts: AmChartsService,private _domSanitizer:DomSanitizer, private _script: ScriptLoaderService, private _cookieService: CookieService, private _userService: UserService) { }
    NineDayData = [];

    ngOnInit() {
        this.getShortLinks();
        this.numberEncodeAndDecode()
        // this.ninedayData();
        this._script.loadScripts('app-pricing',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-pricing',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        (<any>$('#sandbox-container1 .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        this._script.loadScripts('app-pricing',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

    }
    getShortLinks() {
        this.show_loading=true;
        this._appService.get_method(this._userService.shortLink).subscribe(
            data => {
                this.shortUrlData=[];
                if(data.status){
                    this.shortUrlData=data.data;
                    console.log(data.data)
                    for(let i=0;i<this.shortUrlData.length;i++){
                        this.shortUrlData[i]['date_created']=moment.unix(this.shortUrlData[i]['date_created']).format("DD/MM/YYYY hh:mm:ss A");
                        console.log(this.shortUrlData[i]['date_created']);
                    }
                    this.show_loading=false;
                }
                else{
                    this.show_loading=false;
                    toastr.error("<h5>"+data.message+"<h5>");
                }
            },

            error => {
                toastr.error("<h5>Server Error: "+error+"<h5>");
            }

        );
    }
    createNewShortlink(){
       if(this.validateaddShortLinkValues()){
           toastr.success("Shortlink is ok");
           let data={
            "title":this.shortlink_title,
            "description":this.shortlink_description ,
            "long_url": this.shortlink_url, 
            "custom_alias":this.shortlink_token,
            "webhook_url": this.shortlink_webhookurl,        
            "mobile_tracking_enabled": this.shortlink_trackmble
           };
            this._appService.post_method(this._userService.shortLink,JSON.stringify(data)).subscribe(
                data=>{
                    if(data.status){
                        this.getShortLinks()
                        return toastr.success("<h5>"+data.message+"<h5>");
                        
                    }
                    else{
                        return toastr.error("<h5>"+data.message+"<h5>");
                    }
                },
                error=>{return toastr.error("<h5>Server Error: "+error+"<h5>")}
            );
       }
       
    }
    validateaddShortLinkValues(){
        if(!this.validateShortLinkTitle()){return false;}
        else if(!this.validateShortLinkUrl()){return false;}
        else if(!this.validateShortLinkDescription()){return false;}
        else if(!this.validateShortLinkToken()){return false;}
        else if(!this.validateShortLinkWebHookUrl()){return false;}
        else{ return true;}

    }
    validateShortLinkTitle(){
        if(this.shortlink_title==""){
            toastr.warning("<h5>Title For Shortlink is Required<h5>");
            return false;
        }
        else{return true;}
    }
    validateShortLinkDescription(){
        if(this.shortlink_description==""){
            toastr.warning("<h5>Description For Shortlink is Required<h5>");
            return false;
        }
        else{return true;}
    }
    validateShortLinkUrl(){
        if(this.shortlink_url==""){
            toastr.warning("<h5>Url For Shortlink is Required<h5>");
            return false;
        }
        else{return true; }
    }
    validateShortLinkToken(){
        if(this.shortlink_token==""){
            toastr.warning("<h5>Token For Shortlink is Required<h5>");
            return false;
        }else{return true;}

    }
    validateShortLinkWebHookUrl(){
        if(this.shortlink_webhookurl==""){
            toastr.warning("<h5>WebHook Url For Shortlink is Required<h5>");
            return false;
        }else{return true;}
    }
    openCreateShortlink() {
        $('#branding_modal').modal('show');
    }
    showVisits(shorturl) {
        this.show_loading=true;
        this.timeIntervalData=false;
        this.visit_p=1;
        this.visitactivity=[]
        this.shorturl=shorturl;
        this.showvisits = true;
        this.calendar_range = "";
        this.show_calendar = false;
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
        this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/analytics").subscribe(
            data=>{
                if(data.status){
                    this.totalVisits=data.data['urlDetail']['total_clicks'];
                }
            },
            error=>{}
        );
        this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/allClicks/0/10").subscribe(
            data=>{
               if(data.status){

                   this.visitactivity=data.data;
                   for(let i=0;i<this.visitactivity.length;i++){
                    this.visitactivity[i]['referer']=this.visitactivity[i]['referer'].toUpperCase();
                    this.visitactivity[i]['country']=this.visitactivity[i]['country'].toUpperCase();
                    this.visitactivity[i]['click_date']=moment.unix(this.visitactivity[i]['click_date']).format("DD/MM/YYYY hh:mm:ss A");
                    let snd= this.visitactivity[i]['mobile_code'];

                                let map = {
                                    "y":"/",
                                    "z":"-"
                                }
                                let tgt = snd.split("");
                                tgt.forEach((g,idx) => {
                                    if(map.hasOwnProperty(g)){
                                        g = map[g];
                                    }
                                    let t=g.charCodeAt()
                                    if(t<127){
                                        tgt[idx]=t-33;
                                    }
                                    else{
                                        tgt[idx]=t-34-65;
                                    }
                                })
                                let phonenumber=""
                                for(let k=tgt.length-1;k>=0;k--)
                                {
                                    if(tgt[k]<10 && (k!=tgt.length-1))
                                        phonenumber +="0"+tgt[k];
                                    else
                                        phonenumber+=tgt[k];
                                    if(k==0){
                                        this.visitactivity[i]['mobile_code']=phonenumber
                                    }
                                }
                }
               }
               else{
                   toastr.error("<h5>"+data.message+"<h5>");
               }
               this.show_loading=false;
            },
            error=>{
                this.show_loading=false;
                toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );
    }
    numberEncodeAndDecode(){
        //total code
//encryption code

let phone = 9133903612;
let forbidden = {
    "/":"y",
 	"-":"z"
}
let arr = [];
while(phone>0){
    if((phone%100+33)<127)
    arr.push(String.fromCharCode((phone%100+33)))
    else
    arr.push(String.fromCharCode((phone%100+34+65)))
    phone=Math.floor(phone/100)
}
console.log(arr)
arr.forEach((g,idx)=>{
    let t=g+"";
    if(forbidden.hasOwnProperty(t)){
        arr[idx]=forbidden[t]
    }
    else {
        arr[idx]=t
    }
})
let snd = arr.join("")
console.log("encoded phone no",snd);


//decryption codel
let map = {
    "y":"/",
    "z":"-"
}
let tgt:any = snd.split("");
console.log(tgt)
tgt.forEach((g,idx) => {
	console.log(g,map.hasOwnProperty(g));
    if(map.hasOwnProperty(g)){
        g = map[g];
    }
    let t=g.charCodeAt(idx)
    if(t<127){
        tgt[idx]=t-33;
    }
    else{
        tgt[idx]=t-34-65;
    }
})
console.log(tgt);
let phonenumber=""
for(let i=tgt.length-1;i>=0;i--)
{
	if(tgt[i]<10 && i!=tgt.length-1)
		phonenumber +="0"+tgt[i];
	else
        phonenumber+=tgt[i];
    console.log("hii",i,phonenumber)
}

console.log("decrypted number",phonenumber)
    }
    visitPaginationChanged(pageno){
        this.show_loading=true;
        this.visit_p=pageno;
        this.visitactivity=[];
        if(this.timeIntervalData){
            this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/allClicks/"+((pageno-1)*10)+"/"+10+"/"+this.intervalStartDate+"/"+this.intervalEndDate).subscribe(
                data=>{
                   if(data.status){
                       this.visitactivity=data.data;
                       for(let i=0;i<this.visitactivity.length;i++){
                           this.visitactivity[i]['referer']=this.visitactivity[i]['referer'].toUpperCase();
                           this.visitactivity[i]['country']=this.visitactivity[i]['country'].toUpperCase();
                           this.visitactivity[i]['click_date']=moment.unix(this.visitactivity[i]['click_date']).format("DD/MM/YYYY hh:mm:ss A");  
                           let snd= this.visitactivity[i]['mobile_code'];

                           let map = {
                               "y":"/",
                               "z":"-"
                           }
                           let tgt = snd.split("");
                           tgt.forEach((g,idx) => {
                               if(map.hasOwnProperty(g)){
                                   g = map[g];
                               }
                               let t=g.charCodeAt()
                               if(t<127){
                                   tgt[idx]=t-33;
                               }
                               else{
                                   tgt[idx]=t-34-65;
                               }
                           })
                           let phonenumber=""
                           for(let k=tgt.length-1;k>=0;k--)
                           {
                               if(tgt[k]<10 && (k!=tgt.length-1))
                                   phonenumber +="0"+tgt[k];
                               else
                                   phonenumber+=tgt[k];
                               if(k==0){
                                   this.visitactivity[i]['mobile_code']=phonenumber
                               }
                           }                         
                       }                       
                   }
                   else{
                    toastr.error("<h5>"+data.message+"<h5>");
                    }
                    this.show_loading=false;
                },
                error=>{
                    this.show_loading=false;
                    toastr.error("<h5>Server Error: "+error+"<h5>");
                }
            );
        }
        else{
            this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/allClicks/"+((pageno-1)*10)+"/"+10).subscribe(
                data=>{
                   if(data.status){
                       console.log(data)
                       this.visitactivity=data.data;
                       for(let i=0;i<this.visitactivity.length;i++){
                           this.visitactivity[i]['referer']=this.visitactivity[i]['referer'].toUpperCase();
                           this.visitactivity[i]['country']=this.visitactivity[i]['country'].toUpperCase();
                           this.visitactivity[i]['click_date']=moment.unix(this.visitactivity[i]['click_date']).format("DD/MM/YYYY hh:mm:ss A");
                           let snd= this.visitactivity[i]['mobile_code'];

                           let map = {
                               "y":"/",
                               "z":"-"
                           }
                           let tgt = snd.split("");
                           tgt.forEach((g,idx) => {
                               if(map.hasOwnProperty(g)){
                                   g = map[g];
                               }
                               let t=g.charCodeAt()
                               if(t<127){
                                   tgt[idx]=t-33;
                               }
                               else{
                                   tgt[idx]=t-34-65;
                               }
                           })
                           let phonenumber=""
                           for(let k=tgt.length-1;k>=0;k--)
                           {
                               if(tgt[k]<10 && (k!=tgt.length-1))
                                   phonenumber +="0"+tgt[k];
                               else
                                   phonenumber+=tgt[k];
                               if(k==0){
                                   this.visitactivity[i]['mobile_code']=phonenumber
                               }
                           }
                           
                       }
                       
                   }
                   else{
                    toastr.error("<h5>"+data.message+"<h5>");
                    }
                    this.show_loading=false;
                },
                error=>{
                    this.show_loading=false;
                    toastr.error("<h5>Server Error: "+error+"<h5>");
                }
            );
        }
        
    }
    download(url){
        
    }
    downloadAllClickData(){
        this._appService.getFileMethod(this._userService.shortLink+this.shorturl.id+"/download").subscribe(
            data=>{
                console.log(data)
                var url=window.URL.createObjectURL(data);
                var a = $("<a style='display: none;'/>");
                a.attr("href",url);
                a.attr("download","VisitData.xls");
                $("body").append(a);
                a[0].click();
                window.URL.revokeObjectURL(url);
                a.remove();             
            },
            error=>{
                return ;
               
            }
        );  
    }
    downloadFile(id,key){
      var anchor=document.createElement('a');
      anchor.href=this._userService.DownloadShorlinkReports+id+"/"+key;
      anchor.download="ShortlinkReport.xls";
      anchor.click();
      anchor.remove();      
      }
    currentshortlist(shorturl) {
        $('#editshortLink').modal('show');
        this.editShorturl=shorturl;
        console.log(this.editShorturl);
        this.shortlink_title_edit=this.editShorturl['title'];
        if(this.editShorturl['mobile_tracking_enabled']==1 || this.editShorturl['mobile_tracking_enabled']=='1'){
            this.shortlink_trackmble_edit=true;
        }
        else{ this.shortlink_trackmble_edit=false;}
        if(this.editShorturl['active']==1 || this.editShorturl['active']=='1'){
            this.shortlink_track_edit=true;
        }
        else{ this.shortlink_track_edit=false;}
        this.shortlink_description_edit=this.editShorturl['description'];
       
    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }
    rangeChanged1(){
        if (this.calendar_range1 == "custom") {
            $('#m_datepicker_11')[0].value = "";
            $('#m_datepicker_21')[0].value = "";
            this.show_calendar1 = true;
        }
        else {
            this.show_calendar1 = false;
        }
    }
   
    closeVisitpage() {
        this.showvisits = false;
    }
    navigateToDownloadReport(){
        setTimeout(()=>{
            $('.selectpicker').selectpicker('refresh')
        },10)
        this.downloadReportActive=true;
        this.shortlinkDownloadReport=[];
        this.downloadReport=[];
        this.shortlinkName="";
        this.calendar_range1="";
        this.show_calendar1=false;
        this._appService.get_method(this._userService.shortLink).subscribe(
            data=>{
                if(data.status){
                    this.shortlinkDownloadReport=data.data;                                        
                }
                else{
                    toastr.error("<h5>"+data.message+"<h5>");
                }
                setTimeout(()=>{
                    $('.selectpicker').selectpicker('refresh')
                },10)
                },
            error=>{
                toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );

    }
    navigateToShortLinks(){
        this.downloadReportActive=false;
    }
    shortlinkChanged(){
        this.loading=true;
        this.downloadReport=[];
        this._appService.get_method(this._userService.shortLink+"report/"+this.shortlinkName).subscribe(
            data=>{
                if(data.status){
                    this.downloadReport=data.data;
                    for(let i=0;i<this.downloadReport.length;i++){
                        if(this.downloadReport[i]['report_from']!=null){
                            this.downloadReport[i]['report_from']=moment.unix(this.downloadReport[i]['report_from']).format("DD/MM/YYYY hh:mm:ss A");
                        }
                        if(this.downloadReport[i]['report_to']!=null){
                            this.downloadReport[i]['report_to']=moment.unix(this.downloadReport[i]['report_to']).format("DD/MM/YYYY hh:mm:ss A");

                        }
                        if(this.downloadReport[i]['requested_at']!=null){
                            this.downloadReport[i]['requested_at']=moment.unix(this.downloadReport[i]['requested_at']).format("DD/MM/YYYY hh:mm:ss A");
                        }
                        if(this.downloadReport[i]['prepared_at']!=null){
                        this.downloadReport[i]['prepared_at']=moment.unix(this.downloadReport[i]['prepared_at']).format("DD/MM/YYYY hh:mm:ss A");
                         }
                    }
                }
                else{
                    toastr.error("<h5>"+data.message+"<h5>");

                }
                this.loading=false;

            },
            error=>{
                this.loading=false;
                toastr.error("<h5>Server Error: "+error+"<h5>"); 
            }
        );
    }
    refreshShortlinkReport(){
        if(this.shortlinkName=="" || this.shortlinkName==null){
            return toastr.warning("<h5>Select Shorlink From DropDown<h5>");
        }
        else{
            this.shortlinkChanged();
        }
    }
    viewGraph(shorturl) {
        this.chart1Loader = true;
        this.showgraph = true;
        this.NineDayData = new Array(9);
        this.shorturl=shorturl;
        let startdate= moment().subtract(8, 'days').valueOf();
        let enddate=moment().valueOf();
        startdate=startdate.toString();
        enddate=enddate.toString();
        startdate=startdate.slice(0,-3);
        enddate=enddate.slice(0,-3);
        console.log("startdate",startdate);
        console.log("enddate",enddate);
        let first_date = new moment(moment().format("YYYY-MM-DD"));
        let first_date_ = first_date.format("YYYY-MM-DD");
        for (let i = 0; i < this.NineDayData.length; i++) {
            let d = {
                'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                'allClikcs': 0,
                'mobileClicks': 0
            };
            this.NineDayData[8 - i] = d;
        }
        console.log(this.NineDayData)
        this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/"+startdate+"/"+enddate).subscribe(
            data => {
                console.log(data)
                if(data.status){
                    if(data.data.length==0){
                        this.LoadScripts();
                    }
                    else{
                        for(let j=0;j<data.data.length;j++){
                            let date=moment.unix(data.data[j]['dateRrange']).format("YYYY-MM-DD");
                            for(let k=0;k<this.NineDayData.length;k++){
                                if(this.NineDayData[k]['date']==date){
                                    this.NineDayData[k]['allClikcs']=data.data[j]['allClikcs'];
                                    this.NineDayData[k]['mobileClicks']=data.data[j]['mobileClicks'];
                                    break;
                                }
                            }
                            if(j==data.data.length-1){
                                     this.LoadScripts()                       
                             }
                         }
                    }
                  
                }
               else{
                this.chart1Loader = false;
                this.show_loading = false;
               toastr.error("<h5>Error Retrieving Data<h5>");
               }
               
            },error=>{
                this.chart1Loader = false;
                this.show_loading = false;
               toastr.error("<h5>Server Error: "+error+"<h5>")
            }
        );

    }
    LoadScripts() {
        this._script.loadScripts('head', [
            'https://www.amcharts.com/lib/3/amcharts.js',
            'https://www.amcharts.com/lib/3/pie.js',
            'https://www.amcharts.com/lib/3/radar.js',
            'https://www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            'https://www.amcharts.com/lib/3/serial.js',
            'https://www.amcharts.com/lib/3/plugins/export/export.min.js',
            'https://www.amcharts.com/lib/3/themes/light.js'], true).then(() => {
                Helpers.loadStyles('head', 'https://www.amcharts.com/lib/3/plugins/export/export.css');
                this.chart1 = this.AmCharts.makeChart("m_amcharts_4", {
                    "theme": "light",
                    "type": "serial",
                    "hideCredits": true,
                    "dataProvider": this.NineDayData,
                    "valueAxes": [{
                        "stackType": "3d",
                        "unit": "%",
                        "position": "left",
                        "title": "Last 9 Day Data",
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "All Clicks: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2004",
                        "type": "column",
                        "valueField": "allClikcs"
                    }, {
                        "balloonText": "Mobile Clicks: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2005",
                        "type": "column",
                        "valueField": "mobileClicks"
                    }],
                    "plotAreaFillAlphas": 0.1,
                    "depth3D": 60,
                    "angle": 30,
                    "categoryField": "date",
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "export": {
                        "enabled": true
                    }
                });
                $('.chart-input').off().on('input change', function() {
                    var property = jQuery(this).data('property');
                    var target = this.chart1;
                    this.chart1.startDuration = 0;

                    if (property == 'topRadius') {
                        target = this.chart1.graphs[0];
                        if (this.value == 0) {
                            this.value = undefined;
                        }
                    }

                    target[property] = this.value;
                    this.chart1.validateNow();
                });               
            });
        this.chart1Loader = false;
        this.show_loading = false;
    }
    ninedayData(){
        this.chart1Loader = true;
        this.showgraph = true;
        this.NineDayData = new Array(9);
        let startdate= moment().subtract(8, 'days').valueOf();
        let enddate=moment().valueOf();
        startdate=startdate.toString();
        enddate=enddate.toString();
        startdate=startdate.slice(0,-3);
        enddate=enddate.slice(0,-3);
        console.log("startdate",startdate);
        console.log("enddate",enddate);
        let first_date = new moment(moment().format("YYYY-MM-DD"));
        let first_date_ = first_date.format("YYYY-MM-DD");
        for (let i = 0; i < this.NineDayData.length; i++) {
            let d = {
                'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                'allClikcs': 0,
                'mobileClicks': 0
            };
            this.NineDayData[8 - i] = d;
        }
        console.log(this.NineDayData)
        this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/"+startdate+"/"+enddate).subscribe(
            data => {
                console.log(data)
                if(data.status){
                    if(data.data.length==0){
                        this.LoadScripts();
                    }
                    else{
                        for(let j=0;j<data.data.length;j++){
                            let date=moment.unix(data.data[j]['dateRrange']).format("YYYY-MM-DD");
                            for(let k=0;k<this.NineDayData.length;k++){
                                if(this.NineDayData[k]['date']==date){
                                    this.NineDayData[k]['allClikcs']=data.data[j]['allClikcs'];
                                    this.NineDayData[k]['mobileClicks']=data.data[j]['mobileClicks'];
                                    break;
                                }
                            }
                            if(j==data.data.length-1){
                                     this.LoadScripts()                       
                             }
                         }
                    }
                }
               else{
                this.chart1Loader = false;
                this.show_loading = false;
               toastr.error("<h5>Error Retrieving Data<h5>");
               }
               
            },error=>{
                this.chart1Loader = false;
                this.show_loading = false;
               toastr.error("<h5>Server Error: "+error+"<h5>")
            }
        );
    }
    search_custom_date1() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range1 == "") return toastr.warning("<h5>Choose Dates<h5>");
        else if (this.calendar_range1 == "today") {
            this.from_date = moment();
            this.to_date = moment();

        }
        else if (this.calendar_range1 == "7days") {
            this.from_date = moment().subtract(6, 'days');
            this.to_date = moment();

        }
        else if (this.calendar_range1 == "15days") {

            this.from_date = moment().subtract(14, 'days');
            this.to_date = moment();

        }
        else if (this.calendar_range1 == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month');
            this.to_date = moment().subtract(1, 'month').endOf('month');
        }
        else if (this.calendar_range1 == "custom") {

            let s = $('#m_datepicker_11')[0].value;
            let e = $('#m_datepicker_21')[0].value;
            this.from_date=moment(s,"DD/MM/YYYY");
            this.to_date=moment(e,"DD/MM/YYYY");
            if (s == "" || e == "") return toastr.warning("<h5>Choose Dates<h5>");
        }
        if(this.shortlinkName==""|| this.shortlinkName==null){
            return toastr.warning("<h5>Select Shortlink From Dropdown<h5>");
        }
        else{
            let startdate=this.from_date.valueOf();
            let enddate=this.to_date.valueOf();
            startdate=startdate.toString();
            enddate=enddate.toString();
            startdate=startdate.slice(0,-3);
            enddate=enddate.slice(0,-3);
            this._appService.post_method(this._userService.shortLink+"report/"+this.shortlinkName+"/"+startdate+"/"+enddate,JSON.stringify({})).subscribe(
                data=>{
                    if(data.status){
                        toastr.success("<h6>"+data.message+"<h6>");
                        this.shortlinkChanged();
                        return;
                    }
                    else{
                       return toastr.error("<h6>"+data.message+"<h6>");                      
                    }
                },
                error=>{
                    toastr.error("<h5>Server Error: "+error+"<h5>");
                }
            );
        }
       
        
       

    }
    search_custom_date() {
        this.from_date1 = "";
        this.to_date1 = "";
        if (this.calendar_range == "") return toastr.warning("<h5>Choose Dates<h5>");
        else if (this.calendar_range == "today") {
            this.from_date1 = moment();
            this.to_date1 = moment();

        }
        else if (this.calendar_range == "7days") {
            this.from_date1 = moment().subtract(6, 'days');
            this.to_date1 = moment();

        }
        else if (this.calendar_range == "15days") {

            this.from_date1 = moment().subtract(14, 'days');
            this.to_date1 = moment();

        }
        else if (this.calendar_range == "month") {
            this.from_date1 = moment().subtract(1, 'month').startOf('month');
            this.to_date1 = moment().subtract(1, 'month').endOf('month');
        }
        else if (this.calendar_range == "custom") {

            let s = $('#m_datepicker_1')[0].value;
            let e = $('#m_datepicker_2')[0].value;
            this.from_date1=moment(s,"DD/MM/YYYY");
            this.to_date1=moment(e,"DD/MM/YYYY");
            if (s == "" || e == "") return toastr.warning("<h5>Choose Dates<h5>");
        } 
        let startdate=this.from_date1.valueOf();
        let enddate=this.to_date1.valueOf();
        startdate=startdate.toString();
        enddate=enddate.toString();
        startdate=startdate.slice(0,-3);
        enddate=enddate.slice(0,-3);
        this.timeIntervalData=true;
        this.intervalStartDate=startdate;
        this.intervalEndDate=enddate;
        this.visitactivity=[];
        this.show_loading=true;
        this._appService.get_method(this._userService.shortLink+this.shorturl.id+"/allClicks/0/10/"+startdate+"/"+enddate).subscribe(
                        data=>{
                            console.log(data);
                            if(data.status){                                
                                this.totalVisits=data.totalCount;
                                this.visitactivity=data.data;
                                for(let i=0;i<this.visitactivity.length;i++){
                                 this.visitactivity[i]['referer']=this.visitactivity[i]['referer'].toUpperCase();
                                 this.visitactivity[i]['country']=this.visitactivity[i]['country'].toUpperCase();
                                 this.visitactivity[i]['click_date']=moment.unix(this.visitactivity[i]['click_date']).format("DD/MM/YYYY hh:mm:ss A");
                                 let snd= this.visitactivity[i]['mobile_code'];

                                 let map = {
                                     "y":"/",
                                     "z":"-"
                                 }
                                 let tgt = snd.split("");
                                 tgt.forEach((g,idx) => {
                                     if(map.hasOwnProperty(g)){
                                         g = map[g];
                                     }
                                     let t=g.charCodeAt()
                                     if(t<127){
                                         tgt[idx]=t-33;
                                     }
                                     else{
                                         tgt[idx]=t-34-65;
                                     }
                                 })
                                 let phonenumber=""
                                 for(let k=tgt.length-1;k>=0;k--)
                                 {
                                     if(tgt[k]<10 && (k!=tgt.length-1))
                                         phonenumber +="0"+tgt[k];
                                     else
                                         phonenumber+=tgt[k];
                                     if(k==0){
                                         this.visitactivity[i]['mobile_code']=phonenumber
                                     }
                                 }
                             }
                            }
                            else{
                                toastr.error("<h5>"+data.message+"<h5>");
                            }
                            this.show_loading=false;
                         },
                         error=>{
                             this.show_loading=false;
                             toastr.error("<h5>Server Error: "+error+"<h5>");
                         }

        );
        console.log("from date to date",this.from_date1,this.to_date1);

    }
    validateEditShortlink(){
        if(this.shortlink_title_edit=="" || this.shortlink_title_edit==null){
            return false;
        }
        else if(this.shortlink_description_edit=="" || this.shortlink_description_edit==null){
            return false;
        }
        else{return true}
    }
    saveShortLink(){
        if(this.validateEditShortlink()){
            $('#editshortLink').modal('hide');
            let d={
                 "title" : this.shortlink_title_edit,
        "description" : this.shortlink_description_edit,
        "active": this.shortlink_track_edit,
        "mobile_tracking_enabled": this.shortlink_trackmble_edit
            }
            this._appService.post_method(this._userService.shortLink+this.editShorturl.id,JSON.stringify(d)).subscribe(
                data=>{
                    if(data.status){
                        this.getShortLinks();
                        toastr.success("<h5>"+data.message+"<h5>");
                    }
                    else{
                        toastr.error("<h5>"+data.message+"<h5>");
                    }
                },
                error=>{
                    toastr.error("<h5>Server Error: "+error+"<h5>");
                }
            );
        }
        else{
            return;
        }
      
    }
    ngOnDestroy() {
        try{
        this.AmCharts.destroyChart(this.chart1);
        }
        catch(e){}
    }
}
