import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { CookieService } from 'ngx-cookie-service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import { UserService } from '../../../../../auth/_services/user.service';
declare let toastr;
@Component({
    selector: 'app-sending-process',
    templateUrl: './sending-process.component.html',
    styles: []
})
export class SendingProcessComponent implements OnInit {
    statusreport = [{ 'status': 1, 'route': "NOT DONE", 'senderid': "NOT DONE", 'message': 'NOT DONE', 'totalsms': 'NOT DONE' }]
    p = 1;
    live_status_p = 1;
    live_status_report = [];
    user_id = "";
    show_loading = false;
    constructor(private _userService: UserService, private _script: ScriptLoaderService, private _cookieService: CookieService, private _appService: AppService) { }

    ngOnInit() {
        this._script.loadScripts('app-sending-process',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.user_id = this._cookieService.get('userrole');

        this.get_live_status_data();
    }
    view_dlr = false;
    back() {
        this.view_dlr = false;
    }
    get_live_status_data() {
        this.show_loading = true;
        this._appService.get_method(this._userService.sending_report).subscribe(
            data => {
                this.live_status_report = data.result;
                this.show_loading = false;

            },
            error => {
                this.show_loading = false;
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );
    }
    // show_dlr_livestatus(status:any)
    // {
    //     this.view_dlr=true;
    //     console.log(status);

    // }
    refreshData() {
        this.show_loading = true;
        this.get_live_status_data();
    }
    delete(status: any) {

        this._appService.delete_method(this._userService.sending_report + "/" + status.id).subscribe(
            data => {

                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
                this.get_live_status_data();
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }

}

