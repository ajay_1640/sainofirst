import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { FormsModule } from "@angular/forms";
import { DownloadComponent } from './download/download.component';
import { MisReportComponent } from './mis-report/mis-report.component';
import { ScheduleReportComponent } from './schedule-report/schedule-report.component';
import { SendingProcessComponent } from './sending-process/sending-process.component';
import { SentReportComponent } from './sent-report/sent-report.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { AnalyticsComponent } from './analytics/analytics.component';
// import {TimeserviceService} from './timeservice.service';
const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ReportsComponent
            },
            {
                "path": "mis-report",
                "component": MisReportComponent
            },
            {
                "path": "download-report",
                "component": DownloadComponent
            },
            {
                "path": "schedule-report",
                "component": ScheduleReportComponent
            },
            {
                "path": "sending-process-report",
                "component": SendingProcessComponent
            },
            {
                "path": "sent-report",
                "component": SentReportComponent
            },
            {
                "path": "analytics-report",
                "component": AnalyticsComponent
            },
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule, AmChartsModule
    ], exports: [
        RouterModule
    ],

    declarations: [
        ReportsComponent,
        DownloadComponent,
        MisReportComponent,
        ScheduleReportComponent,
        SendingProcessComponent,
        SentReportComponent,
        AnalyticsComponent,
    ]

})
export class ReportsModule {



}