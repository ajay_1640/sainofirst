import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import 'rxjs/Rx';
declare var $, jQuery: any;
declare let toastr;
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
declare var moment;
@Component({
    selector: 'app-analytics',
    templateUrl: './analytics.component.html',
    styles: []
})
export class AnalyticsComponent implements OnInit {
    private chart1: any;
    private chart2: any;
    private chart3: any;
    NineDayData = [];
    submittedAndSent = [];
    chart1Loader = false;
    chart2Loader = false;
    chart3Loader = false;
    bullet = "\u2022";
    totalSmsSent;
    total;
    calendar_range = "today";
    pending;
    DNDFailed: number = 0;
    Delivered: number = 0;
    Other: number = 0;
    Submitted: number = 0;
    numbers_count: number = 0;
    DNDFailed_percentage: number;
    Delivered_percentage: number;
    Other_percentage: number;
    Submitted_percentage: number;
    show_loading = false;
    show_calendar = false;
    from_date = "";
    to_date = "";
    chart1Loaded = false;
    chart2Loaded = false;
    chart3Loaded = false;
    searchdata = false;
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService, private AmCharts: AmChartsService) { }
    titles = "Sent SMS Total Analysis";
    ngOnInit() {
        this._script.loadScripts('app-analytics',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-analytics',
            ['assets/demo/demo3/base/toastr.js']);
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        this._script.loadScripts('app-analytics',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        this.chart1Loader = true;
        this.chart2Loader = true;
        this.chart3Loader = true;
        this.show_loading = true;
        // console.log(moment().format('DD/MM/YYYY'))
        this.getLastNineDaysData();
        this.getSubmittedDeliveredData();
        this.searchSmsAnalysis(moment().format('DD/MM/YYYY'), moment().format('DD/MM/YYYY'));
        // this.getAllData();
        let s = setInterval(() => {
            if (this.chart1Loaded && this.chart2Loaded && this.chart3Loaded) {
                this.LoadScripts();
                clearInterval(s);
            }

        }, 2000)
        // setTimeout(() => {

        // }, 4000);


    }

    getSubmittedDeliveredData() {
        this.submittedAndSent = [];
        this._appService.get_method(this._userService.analytics_lastmsg_report).subscribe(
            data => {
                // console.log(data);
                this.submittedAndSent = data.result;
                if (this.submittedAndSent.length == 0) return;
                this.submittedAndSent[0].numbers_count = parseInt(this.submittedAndSent[0].numbers_count, 10);

                //console.log(data);

                this.total = this.submittedAndSent[0].numbers_count;
                this.pending = (this.submittedAndSent[0].numbers_count) - (this.submittedAndSent[0]['sent']);
                // console.log(this.submittedAndSent[0]['numbers_count'],this.submittedAndSent[0]['sent'],(this.submittedAndSent[0].numbers_count)-(this.submittedAndSent[0]['sent']))
                this.totalSmsSent = this.submittedAndSent[0].sent;
                // throw new Error();

                //   catch(e) {
                //       this.pending=0;
                //       this.totalSmsSent=0;
                //       this.total=1;
                //    // console.log(e);
                //   }
                //  console.log(this.total,this.pending,this.totalSmsSent);
                this.chart2Loaded = true;
            },
            error => {
                // console.log(error);
            }
        );
    }
    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range == "") return;
        else if (this.calendar_range == "today") {
            this.from_date = moment().startOf('day').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
            // console.log(this.from_date,this.to_date);
            // this.from_date=moment(this.from_date,"DD/MM/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"DD/MM/YYYY").format('DD/MM/YYYY');
        }
        let endtime = moment(this.to_date, 'DD/MM/YYYY');
        let starttime = moment(this.from_date, 'DD/MM/YYYY');
        let duration = endtime.diff(starttime);
        // console.log(duration);
        if (duration > 2592000000) {
            return toastr.warning("<h5>Time Interval is Too Long Only One Month Interval Data Can be Fetched<h5>");
        }
        // this.show_status=false;
        //  this.downloadReport(this.from_date,this.to_date);
        //this.searchdata=true;
        this.searchSmsAnalysis(this.from_date, this.to_date);
        //console.log(this.from_date,this.to_date);

    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }
    getAllData() {
        this._appService.get_method(this._userService.analytics_total_report).subscribe(
            data => {
                this.Delivered = data.result[0].Delivered;
                this.DNDFailed = data.result[0].DNDFailed;
                this.Other = data.result[0].Other;
                this.numbers_count = data.result[0].numbers_count;
                this.Submitted = data.result[0].Submitted;
                this.Delivered_percentage = (this.Delivered / this.numbers_count) * 100;
                this.DNDFailed_percentage = (this.DNDFailed / this.numbers_count) * 100;
                this.Submitted_percentage = (this.Submitted / this.numbers_count) * 100;
                this.Other_percentage = (this.Other / this.numbers_count) * 100;
                // console.log(this.Delivered_percentage,this.DNDFailed_percentage,this.Other_percentage,this.Submitted_percentage);
                // console.log(data);
            },
            error => {
                // console.log(error);
            }
        )
    }
    getLastNineDaysData() {
        this.NineDayData = new Array(9);
        this._appService.get_method(this._userService.analytics_report).subscribe(
            data => {
                console.log("nine day data", data)
                let first_date = new moment(moment().format("YYYY-MM-DD"));
                let first_date_ = first_date.format("YYYY-MM-DD");
                if (data.result.length == 0) {
                    for (let i = 0; i < this.NineDayData.length; i++) {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'submitted': 0,
                            'delivered': 0

                        };
                        this.NineDayData[8 - i] = d;
                    }
                    return;
                }
                for (let i = 0; i < data.result.length; i++) {
                    let ar = data.result[i]['date'].split('T');
                    data.result[i]['date'] = ar[0];
                    let second_date = new moment(ar[0]);
                    let diff = first_date.diff(second_date, "days");
                    this.NineDayData[8 - diff] = data.result[i];

                }
                for (let i = 0; i < this.NineDayData.length; i++) {
                    if (typeof this.NineDayData[8 - i] == "undefined") {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'delivered': 0,
                            'submitted': 0
                        };
                        this.NineDayData[8 - i] = d;
                    }
                }
                this.chart1Loaded = true;
            },
            error => {
                // console.log(error);
            }
        );
    }
    LoadScripts() {
        this._script.loadScripts('head', [
            'https://www.amcharts.com/lib/3/amcharts.js',
            'https://www.amcharts.com/lib/3/pie.js',
            'https://www.amcharts.com/lib/3/radar.js',
            'https://www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            'https://www.amcharts.com/lib/3/serial.js',
            'https://www.amcharts.com/lib/3/plugins/export/export.min.js',
            'https://www.amcharts.com/lib/3/themes/light.js'], true).then(() => {
                Helpers.loadStyles('head', 'https://www.amcharts.com/lib/3/plugins/export/export.css');
                this.chart1 = this.AmCharts.makeChart("m_amcharts_4", {
                    "theme": "light",
                    "type": "serial",
                    "hideCredits": true,
                    "dataProvider": this.NineDayData,
                    "valueAxes": [{
                        "stackType": "3d",
                        "unit": "%",
                        "position": "left",
                        "title": "Last 9 Day Data",
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "Delivered: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2004",
                        "type": "column",
                        "valueField": "delivered"
                    }, {
                        "balloonText": "Submitted: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2005",
                        "type": "column",
                        "valueField": "submitted"
                    }],
                    "plotAreaFillAlphas": 0.1,
                    "depth3D": 60,
                    "angle": 30,
                    "categoryField": "date",
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "export": {
                        "enabled": true
                    }
                });
                jQuery('.chart-input').off().on('input change', function() {
                    var property = jQuery(this).data('property');
                    var target = this.chart1;
                    this.chart1.startDuration = 0;

                    if (property == 'topRadius') {
                        target = this.chart1.graphs[0];
                        if (this.value == 0) {
                            this.value = undefined;
                        }
                    }

                    target[property] = this.value;
                    this.chart1.validateNow();
                });
                this.chart2 = this.AmCharts.makeChart("m_amcharts_12", {
                    "type": "pie",
                    "theme": "light",

                    "hideCredits": true,
                    "dataProvider": [{
                        "title": "Sent SMS",
                        "value": this.totalSmsSent
                    }, {
                        "title": "Pending SMS",
                        "value": this.pending
                    }],
                    "titleField": "title",
                    "valueField": "value",
                    "labelRadius": 5,
                    "allLabels": [{
                        "y": "50%",
                        "align": "center",
                        "size": 15,
                        // "bold": 'bold',
                        "text": "Sent SMS:" + ((this.totalSmsSent / this.total) * 100).toFixed(2) + "%",
                        "color": "#555"
                    }, {
                        "y": "45%",
                        "align": "center",
                        "size": 15,
                        // "bold":'bold',
                        "text": "Pending SMS:" + ((this.pending / this.total) * 100).toFixed(2) + "%",
                        "color": "#555"
                    }],
                    "enabled": true,
                    "radius": "42%",
                    "innerRadius": "60%",
                    "labelText": "[[title]]",
                    "export": {
                        "enabled": true
                    }
                });
                var chartData = {
                    "1995": [
                        { "sector": "Delivered Messages", "size": this.Delivered_percentage },
                        { "sector": "Submitted Messages", "size": this.Submitted_percentage },
                        { "sector": "DND Failed Messages", "size": this.DNDFailed_percentage },
                        { "sector": "Others", "size": this.Other_percentage },

                    ],

                };
                var currentYear = 1995;
                this.chart3 = this.AmCharts.makeChart("m_amcharts_13", {
                    "type": "pie",
                    "theme": "light",

                    "hideCredits": true,
                    "dataProvider": [],
                    "valueField": "size",
                    "titleField": "sector",
                    "startDuration": 0,
                    "innerRadius": 80,
                    "pullOutRadius": 20,
                    "marginTop": 30,
                    "titles": [{
                        "text": this.titles
                    }],
                    "allLabels": [{
                        "y": "54%",
                        "align": "center",
                        "size": 25,
                        "bold": true,
                        "text": " ",
                        "color": "#555"
                    }, {
                        "y": "49%",
                        "align": "center",
                        "size": 15,
                        "text": " ",
                        "color": "#555"
                    }],
                    "listeners": [{
                        "event": "init",
                        "method": function(e) {
                            var chart = e.chart;

                            function getCurrentData() {
                                var data = chartData[currentYear];
                                // currentYear++;
                                // if (currentYear > 1999)
                                //     currentYear = 1995;
                                return data;
                            }

                            function loop() {
                                //  chart.allLabels[0].text = currentYear;
                                var data = getCurrentData();
                                chart.animateData(data, {
                                    duration: 1000,
                                    complete: function() {
                                        setTimeout(loop, 3000);
                                    }
                                });
                            }

                            loop();
                        }
                    }],
                    "export": {
                        "enabled": true
                    }
                });
            });
        this.chart1Loader = false;
        this.chart2Loader = false;
        this.chart3Loader = false;
        this.show_loading = false;
    }
    ngOnDestroy() {
        try{
        this.AmCharts.destroyChart(this.chart1);
        this.AmCharts.destroyChart(this.chart2);
        this.AmCharts.destroyChart(this.chart3);
        }
        catch(e){}
    }

    ninedayData() {
        this.chart1Loader = true;
        this.NineDayData = new Array(9);
        this._appService.get_method(this._userService.analytics_report).subscribe(
            data => {
                let first_date = new moment(moment().format("YYYY-MM-DD"));
                let first_date_ = first_date.format("YYYY-MM-DD");
                if (data.result.length == 0) {
                    for (let i = 0; i < this.NineDayData.length; i++) {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'submitted': 0,
                            'delivered': 0

                        };
                        this.NineDayData[8 - i] = d;
                    }
                    return;
                }
                for (let i = 0; i < data.result.length; i++) {
                    let ar = data.result[i]['date'].split('T');
                    data.result[i]['date'] = ar[0];
                    let second_date = new moment(ar[0]);
                    let diff = first_date.diff(second_date, "days");
                    this.NineDayData[8 - diff] = data.result[i];

                }
                for (let i = 0; i < this.NineDayData.length; i++) {
                    if (typeof this.NineDayData[8 - i] == "undefined") {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'delivered': 0,
                            'submitted': 0
                        };
                        this.NineDayData[8 - i] = d;
                    }
                }
                this.chart1 = this.AmCharts.makeChart("m_amcharts_4", {
                    "theme": "light",
                    "type": "serial",
                    "hideCredits": true,
                    "dataProvider": this.NineDayData,
                    "valueAxes": [{
                        "stackType": "3d",
                        "unit": "%",
                        "position": "left",
                        "title": "Last 9 Day Data",
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "Delivered: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2004",
                        "type": "column",
                        "valueField": "delivered"
                    }, {
                        "balloonText": "Submitted: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2005",
                        "type": "column",
                        "valueField": "submitted"
                    }],
                    "plotAreaFillAlphas": 0.1,
                    "depth3D": 60,
                    "angle": 30,
                    "categoryField": "date",
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "export": {
                        "enabled": true
                    }
                });
                jQuery('.chart-input').off().on('input change', function() {
                    var property = jQuery(this).data('property');
                    var target = this.chart1;
                    this.chart1.startDuration = 0;

                    if (property == 'topRadius') {
                        target = this.chart1.graphs[0];
                        if (this.value == 0) {
                            this.value = undefined;
                        }
                    }

                    target[property] = this.value;
                    this.chart1.validateNow();
                });
                this.chart1Loader = false;
                //console.log("refresh");
            },
            error => {
                this.chart1Loader = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                // console.log(error);
            }
        );

    }
    submittedvssent() {
        this.chart2Loader = true;
        this.submittedAndSent = [];
        this.total;
        this.pending;
        this.totalSmsSent;
        this._appService.get_method(this._userService.analytics_lastmsg_report).subscribe(
            data => {
                // console.log(data);
                this.submittedAndSent = data.result;
                if (this.submittedAndSent.length == 0) return;
                this.submittedAndSent[0].numbers_count = parseInt(this.submittedAndSent[0].numbers_count, 10);
                this.total = this.submittedAndSent[0]['numbers_count'];
                this.pending = this.submittedAndSent[0].numbers_count - this.submittedAndSent[0].sent;
                this.totalSmsSent = this.submittedAndSent[0].sent;
                //throw new Error();
                //   }
                //   catch(e) {
                //       this.pending=1;
                //       this.totalSmsSent=1;
                //    // console.log(e);
                //   }
                //             let label = this.chart2.createChild("m_amcharts_12.Label");
                // label.text = "Hello world!";
                // label.fontSize = 20;
                // label.align = "center";
                this.chart2 = this.AmCharts.makeChart("m_amcharts_12", {
                    "type": "pie",
                    "theme": "light",

                    "hideCredits": true,
                    "dataProvider": [{
                        "title": "Sent SMS",
                        "value": this.totalSmsSent
                    }, {
                        "title": "Pending SMS",
                        "value": this.pending
                    }],
                    "titleField": "title",
                    "valueField": "value",
                    "labelRadius": 5,
                    "allLabels": [{
                        "y": "50%",
                        "align": "center",
                        "size": 15,
                        // "bold": 'bold',
                        "text": "Sent SMS:" + ((this.totalSmsSent / this.total) * 100).toFixed(2) + "%",
                        "color": "#555"
                    }, {
                        "y": "45%",
                        "align": "center",
                        "size": 15,
                        // "bold":'',
                        "text": "Pending SMS:" + ((this.pending / this.total) * 100).toFixed(2) + "%",
                        "color": "#555"
                    }],
                    "enabled": true,
                    "radius": "42%",
                    "innerRadius": "60%",
                    "labelText": "[[title]]",
                    "export": {
                        "enabled": true
                    }
                });
                this.chart2Loader = false;

            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
                this.chart2Loader = false;
            }
        );
    }
    sentSmsAnalysis() {
        this.chart3Loader = true;
        let data = {
            'start_time': moment().format('DD/MM/YYYY'),
            'end_time': moment().format('DD/MM/YYYY')
        }
        this._appService.post_method(this._userService.analytics_total_report, JSON.stringify(data)).subscribe(
            data => {
                this.Delivered = data.result[0].Delivered;
                this.DNDFailed = data.result[0].DNDFailed;
                this.Other = data.result[0].Other;
                this.numbers_count = data.result[0].numbers_count;
                this.Submitted = data.result[0].Submitted;
                this.Delivered_percentage = (this.Delivered / this.numbers_count) * 100;
                this.DNDFailed_percentage = (this.DNDFailed / this.numbers_count) * 100;
                this.Submitted_percentage = (this.Submitted / this.numbers_count) * 100;
                this.Other_percentage = (this.Other / this.numbers_count) * 100;
                // console.log(this.Delivered_percentage,this.DNDFailed_percentage,this.Other_percentage,this.Submitted_percentage);
                // console.log(data);
                var chartData = {
                    "1995": [
                        { "sector": "Delivered Messages", "size": this.Delivered_percentage },
                        { "sector": "Submitted Messages", "size": this.Submitted_percentage },
                        { "sector": "DND Failed Messages", "size": this.DNDFailed_percentage },
                        { "sector": "Others", "size": this.Other_percentage },

                    ],
                };
                var currentYear = 1995;
                this.chart3 = this.AmCharts.makeChart("m_amcharts_13", {
                    "type": "pie",
                    "theme": "light",

                    "hideCredits": true,
                    "dataProvider": [],
                    "valueField": "size",
                    "titleField": "sector",
                    "startDuration": 0,
                    "innerRadius": 80,
                    "pullOutRadius": 20,
                    "marginTop": 30,
                    "titles": [{
                        "text": this.titles
                    }],
                    "allLabels": [{
                        "y": "54%",
                        "align": "center",
                        "size": 25,
                        "bold": true,
                        "text": " ",
                        "color": "#555"
                    }, {
                        "y": "49%",
                        "align": "center",
                        "size": 15,
                        "text": " ",
                        "color": "#555"
                    }],
                    "listeners": [{
                        "event": "init",
                        "method": function(e) {
                            var chart = e.chart;

                            function getCurrentData() {
                                var data = chartData[currentYear];
                                // currentYear++;
                                // if (currentYear > 1999)
                                //     currentYear = 1995;
                                return data;
                            }

                            function loop() {
                                //  chart.allLabels[0].text = currentYear;
                                var data = getCurrentData();
                                chart.animateData(data, {
                                    duration: 1000,
                                    complete: function() {
                                        setTimeout(loop, 3000);
                                    }
                                });
                            }

                            loop();
                        }
                    }],
                    "export": {
                        "enabled": true
                    }
                });
                this.chart3Loader = false;
            },
            error => {
                this.chart3Loader = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                // console.log(error);
            }
        )
    }
    searchSmsAnalysis(starttime, endtime) {
        this.chart3Loader = true;
        let data = {
            'start_time': starttime,
            'end_time': endtime
        }
        this._appService.post_method(this._userService.analytics_total_report, JSON.stringify(data)).subscribe(
            data => {
                // console.log(data);
                this.Delivered = data.result[0].Delivered;
                this.DNDFailed = data.result[0].DNDFailed;
                this.Other = data.result[0].Other;
                this.numbers_count = data.result[0].numbers_count;
                this.Submitted = data.result[0].Submitted;
                this.Delivered_percentage = (this.Delivered / this.numbers_count) * 100;
                this.DNDFailed_percentage = (this.DNDFailed / this.numbers_count) * 100;
                this.Submitted_percentage = (this.Submitted / this.numbers_count) * 100;
                this.Other_percentage = (this.Other / this.numbers_count) * 100;
                // console.log(this.Delivered_percentage,this.DNDFailed_percentage,this.Other_percentage,this.Submitted_percentage);
                // console.log(data);
                var chartData = {
                    "1995": [
                        { "sector": "Delivered Messages", "size": this.Delivered_percentage },
                        { "sector": "Submitted Messages", "size": this.Submitted_percentage },
                        { "sector": "DND Failed Messages", "size": this.DNDFailed_percentage },
                        { "sector": "Others", "size": this.Other_percentage },

                    ],
                };
                var currentYear = 1995;
                this.chart3 = this.AmCharts.makeChart("m_amcharts_13", {
                    "type": "pie",
                    "theme": "light",

                    "hideCredits": true,
                    "dataProvider": [],
                    "valueField": "size",
                    "titleField": "sector",
                    "startDuration": 0,
                    "innerRadius": 80,
                    "pullOutRadius": 20,
                    "marginTop": 30,
                    "titles": [{
                        "text": this.titles
                    }],
                    "allLabels": [{
                        "y": "54%",
                        "align": "center",
                        "size": 25,
                        "bold": true,
                        "text": " ",
                        "color": "#555"
                    }, {
                        "y": "49%",
                        "align": "center",
                        "size": 15,
                        "text": " ",
                        "color": "#555"
                    }],
                    "listeners": [{
                        "event": "init",
                        "method": function(e) {
                            var chart = e.chart;

                            function getCurrentData() {
                                var data = chartData[currentYear];
                                // currentYear++;
                                // if (currentYear > 1999)
                                //     currentYear = 1995;
                                return data;
                            }

                            function loop() {
                                //  chart.allLabels[0].text = currentYear;
                                var data = getCurrentData();
                                chart.animateData(data, {
                                    duration: 1000,
                                    complete: function() {
                                        setTimeout(loop, 3000);
                                    }
                                });
                            }

                            loop();
                        }
                    }],
                    "export": {
                        "enabled": true
                    }
                });
                this.chart3Loader = false;
                this.chart3Loaded = true;
            },
            error => {
                this.chart3Loader = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                // console.log(error);
            }
        )
    }
}
