import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { first } from 'rxjs/operators';
declare var $, jQuery: any;
declare let toastr;
declare var moment: any;
@Component({
    selector: 'app-mis-report',
    templateUrl: './mis-report.component.html',
    styleUrls: ['./mis-report.css']
})
export class MisReportComponent implements OnInit {

    mis_list = [{
        'crdate': 'No Data',
        'Delivered': 'No Data',
        'Submitted': 'No Data',
        'Other': 'No Data',
        'DNDFailed': 'No Data',
        'totcredit': 'No Data'
    }];

    ar: any;
    fromDate = "";
    toDate = "";
    from_date = "";
    to_date = "";
    calendar_range = "today";
    show_loading = false;
    show_calendar = false;
    tot = 0;
    Delivered = 0;
    Submitted = 0;
    Other = 0;
    DND = 0;
    Credit: any = 0;
    constructor(private _script: ScriptLoaderService, private _userService: UserService, private _appService: AppService) { }

    dateRange;
    p: number = 1;

    ngOnInit() {
        this._script.loadScripts('app-mis-report',
            ['assets/demo/demo3/base/bootstrap-select.js']);

        this._script.loadScripts('app-mis-report',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });


        this._script.loadScripts('app-mis-report',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        // $(function() {

        //     var start = moment();
        //     var end = moment();

        //     function cb(start, end) {
        //         $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        //     }

        //     $('#reportrange').daterangepicker({
        //         startDate: start,
        //         endDate: end,
        //         ranges: {
        //             'Today': [moment(), moment()],
        //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //             'This Month': [moment().startOf('month'), moment().endOf('month')],
        //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //         }
        //     }, cb);

        //     cb(start, end);

        // });

        // this.dateRange = $('#reportrange span').text();
        // this.ar = this.dateRange.split(' - ');
        // this.fromDate = this.ar[0];
        // this.toDate = this.ar[1];
        // $('#sandbox-container .input-daterange').datepicker({
        //     todayHighlight: true,
        //     autoclose: true,
        //     format: "DD/MM/YYYY",
        // });
        // (<any>$('#sandbox-container .input-daterange')).datepicker({
        //     todayHighlight: true,
        //     autoclose: true,
        //     format: "dd/mm/yyyy",
        // });
        // let s_date = moment().startOf('month').format("DD/MM/YYYY");
        // let e_date = moment().endOf('month').format("DD/MM/YYYY");

        this.getMisData_search(moment().startOf('day').format('DD/MM/YYYY'), moment().format('DD/MM/YYYY'));
    }
    refreshList() {
        if (this.from_date == "" && this.to_date == "")
            return toastr.warning("<h5>Choose Dates<h5>");
        else
            this.getMisData_search(this.from_date, this.to_date);
    }
    getMisData() {
        this.tot = 0;
        this.Delivered = 0;
        this.Submitted = 0;
        this.Other = 0;
        this.DND = 0;
        this.Credit = 0;
        this.show_loading = true;
        // console.log("in mis ", firstdate + " " + seconddate);
        let data_send = JSON.stringify({
            'start_time': "",
            'end_time': ""
        });
        this._appService.post_method(this._userService.mis_url, data_send).subscribe(
            data => {
                this.mis_list = data.result;
                for (let i = 0; i < this.mis_list.length; i++) {
                    this.tot += this.mis_list[i]['numbers_count'];
                    this.Delivered += parseInt(this.mis_list[i]['Delivered'], 10);
                    this.Submitted += parseInt(this.mis_list[i]['Submitted'], 10);
                    this.Other += parseInt(this.mis_list[i]['Other']);
                    this.DND += parseInt(this.mis_list[i]['DNDFailed']);
                    this.Credit += this.mis_list[i]['totcredit'];
                    // this.Credit=(this.Credit).toFixed(2);
                }
                this.Credit = parseFloat(this.Credit).toFixed(2)
                this.show_loading = false;
                //console.log('received ' + JSON.stringify(data));

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                //  console.log('post error');
            });
    }
    getMisData_search(startdate, enddate) {
        this.tot = 0;
        this.Delivered = 0;
        this.Submitted = 0;
        this.Other = 0;
        this.DND = 0;
        this.Credit = 0;
        this.show_loading = true;
        this.from_date = startdate;
        this.to_date = enddate;
        // console.log("in mis ", firstdate + " " + seconddate);
        let data_send = JSON.stringify({
            'start_time': startdate,
            'end_time': enddate
        });
        this._appService.post_method(this._userService.mis_url, data_send).subscribe(
            data => {
                this.mis_list = data.result;
                for (let i = 0; i < this.mis_list.length; i++) {
                    this.tot += this.mis_list[i]['numbers_count'];
                    this.Delivered += parseInt(this.mis_list[i]['Delivered'], 10);
                    this.Submitted += parseInt(this.mis_list[i]['Submitted'], 10);
                    this.Other += parseInt(this.mis_list[i]['Other']);
                    this.DND += parseInt(this.mis_list[i]['DNDFailed']);
                    this.Credit += this.mis_list[i]['totcredit'];
                    // this.Credit=(this.Credit).toFixed(2);
                }
                this.Credit = parseFloat(this.Credit).toFixed(2);
                this.show_loading = false;
                // console.log('received ' + JSON.stringify(data));

            },
            error => {
                this.show_loading = false;

                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }


    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

        // $('.input-sm').datepicker('setDate', null);
        //  $('#m_datepicker_1').datepicker('refresh');
        // $(".datepicker").datepicker();
    }
    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range == "") return;
        else if (this.calendar_range == "today") {
            this.from_date = moment().startOf('day').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
            // this.from_date=moment(this.from_date,"MM/DD/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"MM/DD/YYYY").format('DD/MM/YYYY');
        }

        this.getMisData_search(this.from_date, this.to_date);

    }

}
