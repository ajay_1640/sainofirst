import { Component, OnInit } from '@angular/core';
import { ComponentFactoryResolver, ViewChild, ViewContainerRef, ViewEncapsulation, } from '@angular/core';

@Component({
    selector: 'app-reports',
    templateUrl: "./reports.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ReportsComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
