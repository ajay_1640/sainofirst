import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../../app/app.service';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { timezone } from '../../../../pages/default/profile/my-profile/country codes';
// import {TimeserviceService} from '../timeservice.service';
// import moment from 'moment-timezone'; 

import * as JSZip from 'jszip';

declare let $;
declare let toastr;
declare let moment;
import * as XLSX from 'xlsx';
@Component({
    selector: 'app-sent-report',
    templateUrl: './sent-report.component.html',
    styleUrls: ['./sent-report.css']
})
export class SentReportComponent implements OnInit {
    constructor(private _appservice: AppService, private _cookieService: CookieService, private _userService: UserService, private _script: ScriptLoaderService) { }

    user_id: any;
    selected_report_id: number;
    p: number = 1;
    campaign_page_no: number = 1;
    dlr_report_page_no: number = 1;
    campaign_count: number = 0;
    dlr_report_count: number = 0;

    searchText = "";
    showDateAndTime = false;
    searchText1 = "";
    report: any;
    Dlrlimit = "";
    status_ = [];
    show_modal_loading = false;
    show_dlr_count = false;
    sent_count: number;
    reScheduleSentReport = "";
    DLR_type = "";
    DLR_from = "";
    DLR_to = "";
    dlr_type_ = [];
    r_route = "";
    r_senderid = "";
    r_message = "";
    r_characters = "";
    r_sms = "";
    r_schedule: false;
    r_credits = "";
    r_timezone = "";
    r_date = "";
    r_time = "";
    route_ = "";
    report_id = "";
    DNDFailed = "";
    Delivered = "";
    Other = "";
    Submitted = "";
    numbers_count = "";
    update_Dlr_date = "";

    show_loading = false;
    sent_report = [];
    dlr_details;
    sent_report_p = 1;
    routes_list = [];
    parentId = "0";
    timezoneobject = [];
    timezoneDropdown = {};
    ngOnInit() {
        // this.status_=["Delivered", "Submitted", "dndfailed", "Cancelled", "Others"];
        this.parentId = this._cookieService.get('parentid');
        this.user_id = this._cookieService.get('userrole');
        this._script.loadScripts('app-sent-report',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-sent-report',
            ['assets/demo/demo3/base/bootstrap-datetimepicker.js']);
        // this._script.loadScripts('app-sent-report',
        // ['assets/demo/demo3/base/moment-timezone-with-data-2012-2022.min.js']);
        this._script.loadScripts('app-sent-report',
            ['assets/build/js/moment-timezone.js']);
        this._script.loadScripts('app-sent-report',
            ['assets/build/js/moment-timezone-with-data.js']);
        this._script.loadScripts('app-sent-report',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        this.getSentCount(this._userService.url_sent_count);
        this.show_sent_report(this._userService.url_sent_report, this.campaign_page_no);
        this.getUserTimezone();
        this.get_admin_routes();
        this._script.loadScripts('app-sent-report',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


        jQuery(function() {
            var datepicker: any = $('input.datepicker');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');

                })
            }
        });
        let updateDlr_this = this;
        this.timezoneDropdown = timezone
        this.timezoneobject = Object.keys(this.timezoneDropdown);
        jQuery(function() {
            var datepicker: any = $('input#date2');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    updateDlr_this.update_Dlr_date = datepicker[0].value;
                    (<any>$(this)).datepicker('hide');

                })
            }
        });
        // $('input.timepicker').timepicker({
        //     change: function(time) {
        //         // the input field
        //         var element = $(this), text;
        //         // get access to this Timepicker instance
        //         var timepicker = element.timepicker();
        //         text = 'Selected time is: ' + timepicker.format(time);
        //         element.siblings('span.help-line').text(text);
        //     }
        // });
    }
    view_dlr = false;
    back() {
        this.dlr_report_page_no = 1;
        this.view_dlr = false;
    }
    rScheduleChanged() {

        $('input.datepicker')[0]['value'] = "";
        $('#m_timepicker_1')[0]['value'] = this.getCurrentTime(new Date());


    }
    getUserTimezone() {

        this._appservice.get_method(this._userService.profile_url).subscribe(
            data => {
                this.r_timezone = data.profile_details[0]['timezone'];
                $('.selectpicker').selectpicker('refresh')
                setTimeout(() => {
                    $('#selectpicker3').selectpicker('refresh');
                }, 200)
                // console.log(this.r_timezone);
            },
            error => {
            }
        );


    }
    getCurrentTime(date) {
        var hours = date.getHours(),
            minutes = date.getMinutes(),
            ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return hours + ':' + minutes + ' ' + ampm;
    }


    get_admin_routes() {
        this._appservice.get_method(this._userService.routes_url).subscribe(
            data => {
                this.routes_list = data.result;
                //  console.log(this.routes_list);

            },
            error => {

            }
        );
    }

    resend(report) {
        $('#m_modal_resend').modal('show');
        this.getUserTimezone();
        this.report_id = report.id;
        this.report = report;

        this.r_route = report.az_rname;
        this.r_senderid = report.senderid_name;
        this.r_message = report.message;
        this.r_characters = report.message.length;
        this.r_sms = report.numbers_count;
        this.r_credits = report.msg_credit;
        // this.r_timezone="IST";
        this.r_schedule = false;

    }

    calcTime(city, offset, type) {
        let d = new Date();
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        let nd;
        if (type == '+') {
            nd = new Date(utc + (3600000 * offset));
        }
        else {
            nd = new Date(utc - (3600000 * offset));
        }
        return nd.toLocaleString();

    }
    resendSave() {
        if (this.r_schedule && $('input#date1')[0]['value'] == "") {
            return toastr.warning("<h5>choose Date<h5>");
        }
        let d = {
            'is_scheduled': 0,
            'scheduled_time': ""
        }
        if (this.r_schedule) {
            let scheduled_time = new moment();
            scheduled_time = moment($('input#date1')[0]['value'] + " " + $('#m_timepicker_1')[0]['value'], "DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
            let timezone = this.r_timezone.substr(this.r_timezone.length - 7, 6);
            let targetCountryTime;
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            let targetCountryPresentTime;
            if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
            else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }
            targetCountryTime = moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            targetCountryTime = targetCountryTime.concat("GMT" + timezone);

            let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
            // console.log("final date",final_date);
            // console.log("targetcountry",targetCountryTime);
            // console.log("targetcountry present time",targetCountryPresentTime);
            if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
                return toastr.info(this.r_timezone + " Present time is Ahead of the Selected Time Please Choose Valid Time");
            }
            $('#m_modal_resend').modal('hide');
            this.show_modal_loading = true;

            // console.log(scheduled_time);
            d['is_scheduled'] = 1,
                d['scheduled_time'] = final_date;

            //  console.log(d);
            this._appservice.post_method(this._userService.report_resend + "" + this.report.id, JSON.stringify(d)).subscribe(
                data => {
                    if (data.status) {
                        toastr.success("<h5>" + data.message + "<h5>");
                        this.show_modal_loading = false;
                    }
                    else {
                        this.show_modal_loading = false;
                        toastr.error("<h5>" + data.message + "<h5>");
                    }
                },
                error => {
                    this.show_modal_loading = false;
                    toastr.error("<h5>Server Error " + error + "<h5>");
                }
            );



        }
        else {
            // let scheduled_time=new moment();
            // scheduled_time=moment().format("YYYY-MM-DD HH:mm:ss");
            // let final_date=moment().format("YYYY-MM-DD HH:mm:ss");
            //     let timezone=this.r_timezone.substr(this.r_timezone.length-7,6);
            //     let targetCountryTime;
            //     let timediff=timezone.substr(1,5).split(':'); 
            //     let hours=parseInt(timediff[0]);
            //     let  minutes=0;
            //     if(parseInt(timediff[1])!=0){ minutes=parseInt(timediff[1])/60; }
            //     let targetCountryPresentTime;
            //     if(timezone[0]=='+'){ targetCountryPresentTime=this.calcTime("country",hours+minutes,'+');  }
            //    else{ targetCountryPresentTime=this.calcTime("country",hours+minutes,'-'); }           
            //     targetCountryTime=moment(scheduled_time,['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            //     targetCountryTime=targetCountryTime.concat("GMT"+timezone);
            //     let final_date=moment(new Date(targetCountryTime).toLocaleString("en-US", {timeZone: "Asia/kolkata"}),'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
            //     console.log("final date",final_date);
            //     console.log("targetcountry",targetCountryTime);
            //     console.log("targetcountry present time",targetCountryPresentTime);
            //     if(moment(targetCountryPresentTime,'DD/MM/YYYY, HH:mm:ss').diff(final_date)>0){
            //         return toastr.info(this.r_timezone+" Present time is Ahead of Your System Time Please Choose Another Timezone");

            //     }
            // let scheduled_time=new moment();
            // scheduled_time=moment($('input#date1')[0]['value']+" "+$('#m_timepicker_1')[0]['value'],"DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
            //         let timezone=this.r_timezone.substr(this.r_timezone.length-7,6);
            //              let timediff=timezone.substr(1,5).split(':'); 
            //     let hours=parseInt(timediff[0]);
            //     let  minutes=0;
            //     if(parseInt(timediff[1])!=0){ minutes=parseInt(timediff[1])/60; }
            //     let targetCountryTime;
            //     if(timezone[0]=='+'){ targetCountryTime=this.calcTime("country",hours+minutes,'+');  }
            //    else{ targetCountryTime=this.calcTime("country",hours+minutes,'-'); }
            //    console.log("target country",targetCountryTime);
            //    targetCountryTime=moment(targetCountryTime,['DD/MM/YYYY, HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            //    targetCountryTime=targetCountryTime.concat("GMT"+timezone);
            //    console.log(targetCountryTime);
            //    console.log(typeof(targetCountryTime));
            //  //  targetCountryTime=moment(targetCountryTime,'DD/MM/YYYY, HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
            //  let final_date=moment(new Date(targetCountryTime).toLocaleString("en-US", {timeZone: "Asia/kolkata"}),'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');

            d['is_scheduled'] = 0,
                d['scheduled_time'] = "";
            $('#m_modal_resend').modal('hide');
            this.show_modal_loading = true;
            //  console.log(d);
            this._appservice.post_method(this._userService.report_resend + "" + this.report.id, JSON.stringify(d)).subscribe(
                data => {
                    if (data.status) {
                        this.show_modal_loading = false;
                        toastr.success("<h5>" + data.message + "<h5>");
                    }
                    else {
                        this.show_modal_loading = false;
                        toastr.error("<h5>" + data.message + "<h5>");
                    }
                },
                error => {
                    this.show_modal_loading = false;
                    toastr.error("<h5>Server Error " + error + "<h5>");
                }
            );

        }
    }
    removePopup(val) {
        this.showDateAndTime = false;
        let base = "schedule_";
        document.getElementById(base + val).style.display = 'none';

    }
    schedule(sc) {
        let base = "schedule_";
        let date = $('#schedule_' + sc.id + ' #date3')[0]['value'];
        if (date == "") {
            return toastr.warning("<h6>Choose Date<h6>");
        }
        date = moment(date + " " + $('#m_timepicker_' + sc.id)[0]['value'], "DD/MM/YYYY HH:mm A").format("YYYY-MM-DD HH:mm:ss");
        let timezone = this.reScheduleSentReport.substr(this.reScheduleSentReport.length - 7, 6);
        let targetCountryTime;
        let timediff = timezone.substr(1, 5).split(':');
        let hours = parseInt(timediff[0]);
        let minutes = 0;
        if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
        let targetCountryPresentTime;
        if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
        else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }
        targetCountryTime = moment(date, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
        targetCountryTime = targetCountryTime.concat("GMT" + timezone);
        let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
        //   console.log("final date",final_date);
        //   console.log("targetcountry",targetCountryTime);
        //   console.log("targetcountry present time",targetCountryPresentTime);
        if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(date, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
            return toastr.info("Present Time at " + this.reScheduleSentReport + " Ahead of the Selected Time Please Choose Valid Time");
        }
        let data = {
            'date': final_date
        }
        this._appservice.patch_method(this._userService.schedule_url + "/" + sc.id, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    document.getElementById(base + sc.id).style.display = 'none';
                    this.show_sent_report(this._userService.url_sent_report, this.campaign_page_no);
                }
                else { toastr.error("<h5>" + data.message + "<h5>"); }
            },
            error => { toastr.error("<h5>Server error " + error + "<h5>"); }

        )
    }
    deleteCampaign(sc) {
        this._appservice.delete_method(this._userService.schedule_url + "/" + sc.id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>" + error + "<h5>");
            }
        );
    }
    reSchedule(val) {

        console.log("reschedule", val);
        this.reScheduleSentReport = this.r_timezone;
        // this.getUserTimezone();
        if (!val.showResend && val.is_scheduled != 3) {
            $('.schedule_detail_pop').css('margin', '-153px 30px 0px -28.5%');
        }
        else {
            $('.schedule_detail_pop').css('margin', '-115px 30px 0px -28.5%');
        }
        this.showDateAndTime = false;
        let base = "schedule_";

        for (let i = 0; i < this.sent_report.length; i++) {
            if (this.sent_report[i]['id'] != val.id) {
                let s = base + this.sent_report[i]['id'];
                try {
                    // $('#m_timepicker_'+s)[0]['value']="";
                    document.getElementById(s).style.display = 'none';
                }
                catch (e) {

                }
            }
        }


        document.getElementById(base + val.id).style.display = 'inline-block';

        setTimeout(() => {

            var datepicker: any = $('input#date3');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    minDate: new Date(),
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');
                })
            }
            $('#date3').datepicker("refresh");
            $('#m_timepicker_' + val.id).timepicker('refresh');
            $('#m_timepicker_' + val.id)[0]['value'] = this.getCurrentTime(new Date());

        }, 200)
        $('.selectpicker').selectpicker('render')

        $('.selectpicker').selectpicker('refresh')



    }
    repush(report) {
        $('#m_modal_5').modal('show');
        this.status_ = ["Delivered", "Submitted", "dndfailed", "Cancelled", "Others"];
        this.dlr_type_ = ["kannel_dlr", "manual_dlr"];
        this.report_id = report.id;
        this.route_ = "";
        setTimeout(() => { $('.selectpicker').selectpicker('refresh'); }, 200);
    }
    repush_save() {
        //  console.log(this.route_);

        if (this.status_.length == 0) return toastr.warning("<h5>Select Status<h5>");
        if (this.dlr_type_.length == 0) return toastr.warning("<h5>Select Dlr Type<h5>");
        if (this.route_ == "") return toastr.warning("<h5>Select Route<h5>");
        $('#m_modal_5').modal('hide');

        this.show_modal_loading = true;

        let data = {
            'status': this.status_,
            'dlr_type': this.dlr_type_,
            'gateway': this.route_
        }

        this._appservice.post_method(this._userService.sent_repush + this.report_id, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    this.status_ = ["Delivered", "Submitted", "dndfailed", "Cancelled", "Others"];
                    this.dlr_type_ = ["kannel_dlr", "manual_dlr"];
                    this.route_ = "";
                    this.show_modal_loading = false;
                    toastr.success("<h5>" + data.message + "<h5>");
                }
                else {
                    this.show_modal_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h5> Server Error " + error + "<h5>");

            }
        );
    }
    updateDLR(report) {
        // console.log(report);
        $('#m_modal_update').modal('show');
        //    this.show_modal_loading=true;

        this.DLR_from = "";
        this.DLR_to = "";
        this.DLR_type = "all";
        this.Dlrlimit = "";
        this.report_id = report.id;
        this.DNDFailed = "";
        this.Delivered = "";
        this.Other = "";
        this.Submitted = "";
        this.numbers_count = "";
        // 24/10/18 04:07:07 PM
        this.dlrTypeChanged();
        if (report.is_scheduled == 1) {
            setTimeout(() => { $('.selectpicker').selectpicker('refresh'); }, 200);
            $('input#date2')[0]['value'] = moment(report.scheduled_time, "DD/MM/YYYY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[0];
            // console.log(report.scheduled_time,$('input#date2')[0]['value']);
            this.update_Dlr_date = moment(report.scheduled_time, "DD/MM/YYYY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[0];

            $('#m_timepicker_10_modal')[0]['value'] = moment(report.scheduled_time, "DD/MM/YYYY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[1];
            $('#m_timepicker_11_modal')[0]['value'] = moment(report.scheduled_time, "DD/MM/YYYY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[1];
        }
        else {
            setTimeout(() => { $('.selectpicker').selectpicker('refresh'); }, 200);
            $('input#date2')[0]['value'] = moment(report.created_at, "DD/MM/YYYY hh:mm:ss A").format("DD/MM/YYYY-hh:mm A").split('-')[0];
            // console.log(report.scheduled_time,$('input#date2')[0]['value']);
            this.update_Dlr_date = moment(report.created_at, "DD/MM/YYYY hh:mm:ss A").format("DD/MM/YYYY-hh:mm A").split('-')[0];

            $('#m_timepicker_10_modal')[0]['value'] = moment(report.created_at, "DD/MM/YYYY hh:mm:ss A").format("DD/MM/YYYY-hh:mm A").split('-')[1];
            $('#m_timepicker_11_modal')[0]['value'] = moment(report.created_at, "DD/MM/YYYY hh:mm:ss A").format("DD/MM/YYYY-hh:mm A").split('-')[1];
        }


    }
    updateDlrSend() {
        if (this.Dlrlimit == "" || !/^[0-9]+$/.test(this.Dlrlimit)) return toastr.warning("<h5>Invalid Limit<h5>");
        if (this.DLR_type == "") return toastr.warning("<h5>Select Dlr Type<h5>");
        if (this.DLR_from == "") return toastr.warning("<h5>Select From Status<h5>");
        if (this.DLR_to == "") return toastr.warning("<h5>Select To Status<h5>");
        if (this.update_Dlr_date == "") return toastr.warning("<h5>Select Date<h5>");

        var startTime = moment($('#m_timepicker_10_modal')[0]['value'], "HH:mm A");
        var endTime = moment($('#m_timepicker_11_modal')[0]['value'], "HH:mm A");
        var duration = moment.duration(endTime.diff(startTime));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes()) % 60;

        if (hours < 0 || minutes < 0) return toastr.warning("<h5>Invalid time<h5>");
        //    if(hours==0 && minutes==0)return toastr.warning("<h5>Same Time Choose Different Time<h5>");
        $('#m_modal_update').modal('hide');

        this.show_modal_loading = true;


        startTime = moment(startTime, ["HH:mm A"]).subtract(5.5, "hours").format("YYYY-MM-DD HH:mm:ss");
        endTime = moment(endTime, ["HH:mm A"]).subtract(5.5, "hours").format("YYYY-MM-DD HH:mm:ss");

        let d = {
            'limit': this.Dlrlimit,
            'from_status': this.DLR_from,
            'to_status': this.DLR_to,
            'dlr_type': this.DLR_type,
            'start_time': startTime,
            'end_time': endTime
        }

        this._appservice.patch_method(this._userService.sent_update + "" + this.report_id, JSON.stringify(d)).subscribe(
            data => {
                if (data.status) {
                    this.show_modal_loading = false;
                    this.DLR_from = "";
                    this.DLR_to = "";
                    this.DLR_type = "";
                    this.Dlrlimit = "";
                    this.DNDFailed = "";
                    this.Delivered = "";
                    this.Other = "";
                    this.Submitted = "";
                    this.numbers_count = "";
                    toastr.success("<h5>" + data.message + "<h5>");

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                    this.show_modal_loading = false;
                }

            },
            error => {
                toastr.error("<h5> Server Error " + error + "<h5>");
                this.show_modal_loading = false;

            }
        );


    }
    dlrTypeChanged() {
        this.show_dlr_count = true;
        let data;
        if (this.DLR_type == "all") data = { 'dlr_type': ['kannel_dlr', 'manual_dlr'] }
        else if (this.DLR_type == "original") data = { 'dlr_type': ['kannel_dlr'] }
        else data = { 'dlr_type': ['manual_dlr'] }
        this._appservice.post_method(this._userService.campaign_count + "" + this.report_id, JSON.stringify(data)).subscribe(
            data => {
                this.DNDFailed = data.result[0]['DNDFailed'];
                this.Delivered = data.result[0]['Delivered'];
                this.Other = data.result[0]['Other'];
                this.Submitted = data.result[0]['Submitted'];
                this.numbers_count = data.result[0]['numbers_count'];
                this.show_dlr_count = false;

            },
            error => {

            }

        );
    }
    ViewDlrSentReportId: any;
    show_dlr_sent_report(mid: number, dlr_report_count: number) {
        this.show_loading = true;
        this.dlr_report_count = dlr_report_count;
        this.ViewDlrSentReportId = mid;
        this.selected_report_id = mid;
        this._appservice.get_method(this._userService.url_dlr_sent_report + this.campaign_page_no + "/" + this.selected_report_id + "/" + this.dlr_report_page_no).subscribe(
            data => {

                if (data.status) {

                    this.dlr_details = data.data;
                    for (let i = 0; i < this.dlr_details.length; i++) {
                        this.dlr_details[i]['delivered_date'] = moment(this.dlr_details[i]['delivered_date']).format("DD/MM/YY hh:mm:ss A");

                        this.dlr_details[i]['created_at'] = moment(this.dlr_details[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                    }
                    this.show_loading = false;
                    this.view_dlr = true;
                }
                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>")

                }


            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            });
    }
    refreshList() {
        this.show_sent_report(this._userService.url_sent_report, 1);
    }
    dlrReportPageChanged(dlr_report_page_no: number) {
        this.dlr_report_page_no = dlr_report_page_no;
        this.show_dlr_sent_report(this.selected_report_id, this.dlr_report_count);
    }
    ViewDlrList() {
        this.view_dlr = true;
        this.show_loading = true;
        this._appservice.get_method(this._userService.url_dlr_sent_report + this.campaign_page_no + "/" + this.ViewDlrSentReportId + "/" + this.dlr_report_page_no).subscribe(
            data => {

                if (data.status) {

                    this.dlr_details = data.data;
                    for (let i = 0; i < this.dlr_details.length; i++) {
                        this.dlr_details[i]['delivered_date'] = moment(this.dlr_details[i]['delivered_date']).format("DD/MM/YY hh:mm:ss A");

                        this.dlr_details[i]['created_at'] = moment(this.dlr_details[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                    }
                    this.show_loading = false;
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                    this.show_loading = false;

                }


            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            });

    }
    show_sent_report(url: string, page_no: number) {
        this.show_loading = true;
        this.dlr_report_page_no = 1;
        this._appservice.get_method(url + "/" + page_no).subscribe(
            data => {
                console.log("sent report data++++++++++++++++++++++++", data.data);
                this.sent_report = data.data;
                for (let i = 0; i < this.sent_report.length; i++) {
                    var startTime = moment(this.sent_report[i]['scheduled_time']);
                    var endTime = moment();
                    var duration = endTime.diff(startTime) >= 0;
                    this.sent_report[i]['created_at'] = moment(this.sent_report[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                    // if(this.schedule_list[i]['scheduled_time'])
                    this.sent_report[i]['shownScheduledTime'] = moment(this.sent_report[i]['scheduled_time']).subtract(4.5, "hours").format("DD/MM/YY hh:mm A")
                    this.sent_report[i]['scheduled_time'] = moment(this.sent_report[i]['scheduled_time']).format("DD/MM/YY hh:mm A");
                    // this.sent_report[i]['shownScheduledTime']=this.sent_report[i]['scheduled_time'];
                    this.sent_report[i]['showResend'] = duration;
                    let s = this.sent_report[i]['message'].replace(/\n/g, ' ').match(/(.|[\r\n]){1,40}/g);
                    let s1 = "";
                    for (let j = 0; j < s.length; j++) {
                        s1 += s[j];
                        s1 += "\n";
                    }
                    this.sent_report[i]['message'] = s1;
                }

                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });

    }
    getSentCount(url: string) {
        this.show_loading = true;
        this._appservice.get_method(url).subscribe(
            data => {
                console.log("----------------", data);
                this.campaign_count = data.count;
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                console.log(error);
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    campaignPageChanged(page_no: number) {
        //alert("comming"+page_no);
        this.dlr_report_page_no = 1;
        this.campaign_page_no = page_no;
        this.show_sent_report(this._userService.url_sent_report, this.campaign_page_no);
    }
    download(val) {
        toastr.info("<h6>Preparing Data To Download<h6>");
        this._appservice.get_method(this._userService.url_dlr_sent_report + val.id).subscribe(
            data => {
                if (!data.status) {
                    return toastr.error("<h6>" + data.message + "<h6>");
                }
                let sentreport = [];
                for (let i = 0; i < data.data.length; i++) {
                    data.data[i]['delivered_date'] = moment(data.data[i]['delivered_date']).format("DD/MM/YY hh:mm:ss A");
                    data.data[i]['created_at'] = moment(data.data[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                    let d = {
                        'Created Date': data.data[i]['created_at'],
                        'Delivered Date': data.data[i]['delivered_date'],
                        'Mobile No': data.data[i]['mobile_number'],
                        'Msg Credit': data.data[i]['msgcredit'],
                        'Sender Id': data.data[i]['senderid'],
                        'Service Id': data.data[i]['service_id'],
                        'Status': data.data[i]['status'],
                        'Location': data.data[i]['location'],
                        // 'DLR response':data.data[i]['testing'],
                    }
                    sentreport.push(d);

                }
                let ws = XLSX.utils.json_to_sheet(sentreport);
                let wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, "SentReport");

                XLSX.writeFile(wb, "SentReport_" + val.user_name + "_" + val.sms_type + "_" + val.id + ".xlsx");
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );
    }


}