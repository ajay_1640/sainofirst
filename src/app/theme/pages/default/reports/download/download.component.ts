import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { saveAs } from 'file-saver/FileSaver';
import * as JSZip from 'jszip';
import * as XLSX from 'xlsx';
import { FileuploadService } from '../../../../../fileupload.service';
import * as SaveAs from 'file-saver';

declare var moment: any;
declare let toastr;
declare var $, jQuery: any;
@Component({
    selector: 'app-download',
    templateUrl: './download.component.html',
    styles: []
})
export class DownloadComponent implements OnInit {
    download_list = [{
        'fromdate': 'Not Done',
        'todate': 'Not Done',
        'requested': 'Not Done',
        'prepared': 'Not Done',
        'status': 'Not Done'

    }];
    p = 1;
    dateRange: any;
    ar: any;
    from_date = "";
    arr: any;
    show_loading = false;
    zip: any
    to_date = "";
    show_status = false;
    toDate: any;
    fromDate: any;
    calendar_range = "";
    show_calendar = false;
    requested_fromdate = "";
    requested_todate = "";
    requested = "";
    downloadReportsList = [];
    ready = "";
    readystatus = 0;
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _fileUploadService: FileuploadService, private _userService: UserService) { }

    ngOnInit() {
        this.getDownloadReports();
        this._script.loadScripts('app-download',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-download',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);

        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        this._script.loadScripts('app-download',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }
    handle;
    fullyPreparedReports = 0;
    partiallyPreparedReports = 0;
    getDownloadReports() {
        this.show_loading = true;
        this._appService.get_method(this._userService.downlaod_url).subscribe(
            data => {
                console.log(data)
                if (data.status) {
                    this.downloadReportsList = data.result;
                    this.ProcessdownloadReports();

                }
                else {
                    this.show_loading = false;
                    toastr.error("Error Retrieving Data");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error: " + error + "<h5>");
            }
        );
    }
    UpdateProgress(){
       
            if (this.partiallyPreparedReports > 0) {
                for (let i = 0; i < this.downloadReportsList.length; i++) {
                    if (this.downloadReportsList[i]['percent'] != 100) {
                        //    this.downloadReportsList[i]['percent']=this.getProgress(this.downloadReportsList[i]['id']);

                        this.getProgress(this.downloadReportsList[i]['id']).then((percentage) => {
                            this.downloadReportsList[i]['percent'] = percentage
                            console.log(".....", percentage, i)
                        }).catch((e) => {

                            // debugger
                        })
                        if (this.downloadReportsList[i]['percent'] == 100) {
                            this.partiallyPreparedReports--;
                        }
                    }
                }
            }
            else { 
                if(this.handle){
                    clearInterval(this.handle);
                 }
                }
    }
    ProcessdownloadReports() {
        this.fullyPreparedReports = 0;
        this.partiallyPreparedReports = 0;
        for (let i = 0; i < this.downloadReportsList.length; i++) {
            this.downloadReportsList[i]['from_date'] = moment(this.downloadReportsList[i]['from_date']).format("DD/MM/YY hh:mm:ss A");
            this.downloadReportsList[i]['to_date'] = moment(this.downloadReportsList[i]['to_date']).format("DD/MM/YY hh:mm:ss A");
            this.downloadReportsList[i]['requested_time'] = moment(this.downloadReportsList[i]['requested_time']).format("DD/MM/YY hh:mm:ss A");
            this.downloadReportsList[i]['prepared_time'] = moment(this.downloadReportsList[i]['prepared_time']).format("DD/MM/YY hh:mm:ss A");
            if (this.downloadReportsList[i]['status'] == 1) {
                this.downloadReportsList[i]['percent'] = 100;
                this.fullyPreparedReports++;
            }
            else {
                this.downloadReportsList[i]['percent'] = 0;
                this.partiallyPreparedReports++;
            }

        }
        for (let i = 0; i < this.downloadReportsList.length; i++) {
            if (this.downloadReportsList[i]['percent'] != 100) {
                //    this.downloadReportsList[i]['percent']=this.getProgress(this.downloadReportsList[i]['id']);

                this.getProgress(this.downloadReportsList[i]['id']).then((percentage) => {
                    this.downloadReportsList[i]['percent'] = percentage
                    console.log(".....", percentage, i)
                }).catch((e) => {

                    // debugger
                })
                if (this.downloadReportsList[i]['percent'] == 100) {
                    this.partiallyPreparedReports--;
                }
            }
        }
        this.UpdateProgress();
        this.handle=setInterval(()=>{
            this.UpdateProgress();
        },10000)
        this.show_loading = false;
    }
    getProgress(reportid) {
        return new Promise((resolve, reject) => {
            this._appService.get_method(this._userService.downlaod_url + "/" + reportid).subscribe(
                data => {
                    if (data.status) {
                        console.log(data);
                        resolve(data['percentage']);
                    }
                    else { resolve(0); }

                },
                error => {
                    clearInterval(this.handle);
                    toastr.error("<h5>Server Error : " + error + "<h5>");

                    resolve(0);
                }
            );
        })

    }
    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range == "") return toastr.warning("<h5>Choose Dates<h5>");
        else if (this.calendar_range == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
            // console.log(this.from_date,this.to_date);
            // this.from_date=moment(this.from_date,"DD/MM/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"DD/MM/YYYY").format('DD/MM/YYYY');
        }
        this.show_status = false;
        this.downloadReport(this.from_date, this.to_date);

        // console.log(this.from_date,this.to_date);

    }
    download() {
        this.zip.generateAsync({ type: "blob" })
            .then(function(content) {
                saveAs(content, "DownloadReport.zip");

            });
    }
    refreshList() {
        this.downloadReport(moment().format('DD/MM/YYYY'), moment().format('DD/MM/YYYY'));
    }
    downloadReport(starttime, endtime) {
        clearInterval(this.handle)
        this.requested_fromdate = starttime;
        this.requested_todate = endtime;
        this.requested = moment().format("DD/MM/YYYY hh:mm:ss A");
        this.ready = "";
        this.readystatus = 0;
        this.show_status = true;
        this.show_loading = true;
        this._appService.post_method(this._userService.downlaod_url, JSON.stringify({ 'start_time': starttime, 'end_time': endtime })).subscribe(
            data => {
                // if (!data.status) return toastr.error("<h5>" + data.message + "<h5>");
                // // this.show_status=false;
                // // this.show_status=true;
                // let result = data.result;
                // if (result.length == 0) {
                //     this.show_status = false;
                //     return toastr.info("<h6>No Data Found For the Given Interval<h6>");
                //     //this.show_status=false;
                // }
                // this.zip = new JSZip();
                // let limit = 1000000;
                // let noOfFiles = Math.ceil(result.length / limit);
                // this.arr = new Array(noOfFiles);
                // for (let j = 0; j < this.arr.length; j++) {
                //     this.arr[j] = [];
                // }

                // let file_name = 1;
                // for (let i = 1; i <= result.length; i++) {
                //     result[i - 1]['created_at'] = moment(result[i - 1]['created_at']).format("DD/MM/YYYY hh:mm A");
                //     result[i - 1]['delivered_date'] = moment(result[i - 1]['delivered_date']).format("DD/MM/YYYY hh:mm A");
                //     let d = {
                //         'Created Date': result[i - 1]['created_at'],
                //         'Delivered Date': result[i - 1]['delivered_date'],
                //         'Mobile Number': result[i - 1]['mobile_number'],
                //         'Message': result[i - 1]['msgdata'],
                //         'Message Credit': result[i - 1]['msgcredit'],
                //         'Sender Id': result[i - 1]['senderid'],
                //         'Service Id': result[i - 1]['service_id'],
                //         'Location': result[i - 1]['location'],
                //         'Status': result[i - 1]['status']

                //     }
                //     this.arr[file_name - 1].push(d);

                //     if (i % limit == 0) {
                //         file_name++
                //     }


                // }
                // for (let j = 0; j < this.arr.length; j++) {
                //     let ws = XLSX.utils.json_to_sheet(this.arr[j]);
                //     let wb = XLSX.utils.book_new();
                //     XLSX.utils.book_append_sheet(wb, ws, "DReport_" + starttime.replace(/\//g, ' ') + "_" + endtime.replace(/\//g, ' '));
                //     this.zip.file("Report_" + j + ".xlsx", XLSX.write(wb, { type: 'binary', compression: true }), { binary: true });
                // }
                // this.ready = moment().format("DD/MM/YYYY hh:mm:ss A");
                // this.readystatus = 1;
                if (data.status) {
                    this._appService.get_method(this._userService.downlaod_url).subscribe(
                        data => {
                            console.log(data.result)
                            this.downloadReportsList = data.result;
                            this.ProcessdownloadReports();
                        },
                        error => {
                            this.show_loading = false;
                            toastr.error("<h5>Server Error: " + error + "<h5>");
                        }
                    );
                }
                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    saveFile(url) {
        // let type="zip";
        // let name="asd";
        // if (data != null && navigator.msSaveBlob)
        //     return navigator.msSaveBlob(new Blob([data], { type: type }), name);
        var a = $("<a style='display: none;'/>");
        // var url = window.URL.createObjectURL(new Blob([data], { type: type }));
        // console.log("Url to download file",url)
        a.attr("href", url);
        a.attr("download", "Invalid File");
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }
   
    ngOnDestory() {
            if (this.handle) {
              clearInterval(this.handle);
            }
    }
    // getFile(downloadLink){
    //     this._fileUploadService.getImage(downloadLink).subscribe(
    //         (res) => {
    //             const a = document.createElement('a');
    //             a.href = URL.createObjectURL(res);
    //             a.download = "ihii_";
    //             document.body.appendChild(a);
    //             a.click();
    //         });
    // }
    // getFileDownload(){
    //     // SaveAs("http://159.69.120.60/saino-first/files/1/download_report/55_2018-11-14 23:00:00_2018-11-18 22:59:59_254.zip", 'helloworld.csv')
    //     let file = new Blob(['hello world'], { type: 'text/csv;charset=utf-8' });
    // SaveAs(file, 'helloworld.csv')
    // }
}
