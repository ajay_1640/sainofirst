import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import * as XLSX from 'xlsx';
import { CookieService } from 'ngx-cookie-service';
import { timezone } from '../../../../pages/default/profile/my-profile/country codes';

declare let moment;
declare let toastr;
declare var $, jQuery: any;
@Component({
    selector: 'app-schedule-report',
    templateUrl: './schedule-report.component.html',
    styleUrls: ['./schedule-report.css']
})
export class ScheduleReportComponent implements OnInit {
    campaign_page_no: number = 1;
    dlr_report_page_no: number = 1;
    campaign_count: number = 0;
    dlr_report_count: number = 0;
    p: number = 1;
    schedule_p = 1;
    showDateAndTime = false;
    searchText = "";
    searchText1 = "";
    show_loading = false;
    dlr_schedule = [];
    user_id = "";
    show_modal_loading = false;
    show_dlr_count = false;
    report_id = ""
    report;
    status_ = [];
    dlr_type_ = [];
    update_Dlr_date = "";
    route_ = "";
    DLR_from = "";
    DLR_to = "";
    DLR_type = "";
    Dlrlimit = "";
    DNDFailed = "";
    Delivered = "";
    viewDlrScheduleId: any;
    Other = "";
    Submitted = "";
    numbers_count = "";
    r_route = "";
    r_senderid = "";
    r_message = "";
    r_characters = "";
    r_sms = "";
    r_credits = "";
    r_timezone = "";
    r_schedule = false;
    routes_list = [];
    parentId = "0";
    schedule_list = [];
    timezoneDropdown = {};
    timezoneobject = [];
    reScheduleSentReport = "";


    constructor(private _script: ScriptLoaderService, private _cookieService: CookieService, private _userService: UserService, private _appService: AppService) { }

    ngOnInit() {
        this.parentId = this._cookieService.get('parentid');

        this.user_id = this._cookieService.get('userrole');
        //console.log(this.parentId,this.user_id);
        // this._script.loadScripts('app-schedule-report',
        // ['assets/demo/demo3/base/bootstrap-datetimepicker.js']);
        this._script.loadScripts('app-schedule-report',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-schedule-report',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        this._script.loadScripts('app-schedule-report',
            ['assets/demo/demo3/base/toastr.js']);

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.getScheduleList();
        this.getUserTimezone();
        this.get_admin_routes();
        this.getScheduleCount(this._userService.schedule_count_url);
        jQuery(function() {
            var datepicker: any = $('input.datepicker');

            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    // schedule_date = datepicker[0].value;
                    (<any>$(this)).datepicker('hide');

                })
            }
        });
        let updateDlr_this = this;
        jQuery(function() {
            var datepicker: any = $('input#date2');

            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    updateDlr_this.update_Dlr_date = datepicker[0].value;
                    (<any>$(this)).datepicker('hide');
                })
            }
        });

        this.timezoneDropdown = timezone
        this.timezoneobject = Object.keys(this.timezoneDropdown);
        jQuery(function() {
            var datepicker1: any = $('input#date3');

            if (datepicker1.length > 0) {
                datepicker1.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');
                })
            }
        });
    }
    refreshList() {
        this.getScheduleList();
    }
    getScheduleList() {
        this.show_loading = true;
        this._appService.get_method(this._userService.schedule_url + "/" + this.campaign_page_no).subscribe(
            data => {
                // this.schedule_list = data.data;
                // console.log(JSON.stringify(this.schedule_list) + 'schedule data');
                console.log(data.data)
                this.schedule_list = data.data;
                for (let i = 0; i < this.schedule_list.length; i++) {
                    var startTime = moment(this.schedule_list[i]['scheduled_time']);
                    var endTime = moment();
                    var duration = endTime.diff(startTime) >= 0;
                    this.schedule_list[i]['created_at'] = moment(this.schedule_list[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                    this.schedule_list[i]['shownScheduledTime'] = moment(this.schedule_list[i]['scheduled_time']).subtract(4.5, "hours").format("DD/MM/YY hh:mm A")

                    // if(this.schedule_list[i]['scheduled_time'])
                    this.schedule_list[i]['scheduled_time'] = moment(this.schedule_list[i]['scheduled_time']).format("DD/MM/YY hh:mm A");

                    this.schedule_list[i]['showResend'] = duration;
                    let s = this.schedule_list[i]['message'].replace(/\n/g, ' ').match(/(.|[\r\n]){1,40}/g);
                    let s1 = "";
                    for (let j = 0; j < s.length; j++) {
                        s1 += s[j];
                        s1 += "\n";
                    }
                    this.schedule_list[i]['message'] = s1;
                }
                // for (let i = 0; i < this.schedule_list.length; i++) {
                //     for (let j = i + 1; j < this.schedule_list.length; j++) {
                //         if (moment(this.schedule_list[i]['created_at']) < moment(this.schedule_list[j]['created_at'])) {
                //             let temp = this.schedule_list[i]['created_at'];
                //             this.schedule_list[i]['created_at'] = this.schedule_list[j]['created_at'];
                //             this.schedule_list[j]['created_at'] = temp;
                //         }
                //     }
                // }
                this.schedule_list = this.schedule_list.reverse();
                // this.schedule_list.sort((a,b) => (moment(a.created_at) > moment(b.created_at)) ? 1 : ((moment(b.created_at) > moment(a.created_at)) ? -1 : 0)); 
                // console.log("schedule list",this.schedule_list);
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    getUserTimezone() {

        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.r_timezone = data.profile_details[0]['timezone'];
                setTimeout(() => {
                    $('#selectpicker3').selectpicker('refresh');
                }, 50)
                // console.log(this.r_timezone);
            },
            error => {
            }
        );


    }
    view_dlr = false;
    back() {
        this.dlr_report_page_no = 1;
        this.view_dlr = false;
    }
    dlrReportPageChanged(dlr_report_page_no: number) {
        this.dlr_report_page_no = dlr_report_page_no;
        this.show_dlr_schedule_report(this.viewDlrScheduleId);
    }
    campaignPageChanged(page_no: number) {
        //alert("comming"+page_no);
        this.dlr_report_page_no = 1;
        this.campaign_page_no = page_no;
        this.getScheduleList();
    }
    removePopup(val) {
        this.showDateAndTime = false;
        let base = "schedule_";
        document.getElementById(base + val).style.display = 'none';
    }
    deleteCampaign(sc) {

        this._appService.delete_method(this._userService.schedule_url + "/" + sc.id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>" + error + "<h5>");
            }
        );
    }
    refreshDLRList() {

    }
    schedule(sc) {
        let base = "schedule_";
        let date = $('#schedule_' + sc.id + ' #date3')[0]['value'];
        if (date == "") {
            return toastr.warning("<h6>Choose Date<h6>");
        }
        // var startTime = moment(date + " " + $('#m_timepicker_' + sc.id)[0]['value'], "DD/MM/YYYY HH:mm A");     
        date = moment(date + " " + $('#m_timepicker_' + sc.id)[0]['value'], "DD/MM/YYYY HH:mm A").format("YYYY-MM-DD HH:mm:ss");
        let timezone = this.reScheduleSentReport.substr(this.reScheduleSentReport.length - 7, 6);
        let targetCountryTime;
        let timediff = timezone.substr(1, 5).split(':');
        let hours = parseInt(timediff[0]);
        let minutes = 0;
        if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
        let targetCountryPresentTime;
        if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
        else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }
        targetCountryTime = moment(date, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
        targetCountryTime = targetCountryTime.concat("GMT" + timezone);
        let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
        //   console.log("final date",final_date);
        //   console.log("targetcountry",targetCountryTime);
        //   console.log("targetcountry present time",targetCountryPresentTime);
        if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(date, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
            return toastr.info("Present Time at " + this.reScheduleSentReport + " Ahead of the Selected Time Please Choose Valid Time");
        }
        let data = {
            'date': final_date
        }
        this._appService.patch_method(this._userService.schedule_url + "/" + sc.id, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    document.getElementById(base + sc.id).style.display = 'none';
                    this.refreshList();

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server error " + error + "<h5>");
            }

        )
    }
    reSchedule(val) {
        if (!val.showResend && val.is_scheduled != 3) {
            $('.schedule_detail_pop').css('margin', '-153px 30px 0px -28.5%');
        }
        else {
            $('.schedule_detail_pop').css('margin', '-115px 30px 0px -28.5%');
        }

        // this.reScheduleSentReport=this.r_timezone;
        this.showDateAndTime = false;
        let base = "schedule_";
        this.reScheduleSentReport = this.r_timezone;
        for (let i = 0; i < this.schedule_list.length; i++) {
            if (this.schedule_list[i]['id'] != val.id) {
                let s = base + this.schedule_list[i]['id'];
                try {
                    // $('#m_timepicker_'+s)[0]['value']="";
                    document.getElementById(s).style.display = 'none';
                }
                catch (e) {

                }
            }
        }


        document.getElementById(base + val.id).style.display = 'inline-block';

        setTimeout(() => {

            var datepicker: any = $('input#date3');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    minDate: new Date(),
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');
                })
            }
            $('#date3').datepicker("refresh");
            $('#m_timepicker_' + val.id).timepicker('refresh');
            $('#m_timepicker_' + val.id)[0]['value'] = this.getCurrentTime(new Date());

        }, 200)
        $('.selectpicker').selectpicker('render');
        $('.selectpicker').selectpicker('refresh');

    }
    ViewDlrList() {
        this.show_loading = true;
        this._appService.get_method(this._userService.schedule_url + +this.campaign_page_no + "/" + this.viewDlrScheduleId + "/" + this.dlr_report_page_no).subscribe(
            data => {

                // this.view_dlr = true;
                this.dlr_schedule = data.data;
                // console.log(this.dlr_schedule);
                for (let i = 0; i < this.dlr_schedule.length; i++) {

                    this.dlr_schedule[i]['delivered_date'] = moment(this.dlr_schedule[i]['delivered_date']).format("DD/MM/YY hh:mm:ss A");
                    // this.dlr_schedule[i]['delivered_date']=moment(this.dlr_schedule[i]['delivered_date']).add(5.5,"hours").format("DD-MM-YYYY HH:mm:ss A");

                    this.dlr_schedule[i]['created_at'] = moment(this.dlr_schedule[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                }
                this.show_loading = false;

            },
            error => {
                toastr.error("<Server Error >" + error + "");
                // console.log('get error')
            });
    }
    show_dlr_schedule_report(schedule_id: any) {
        this.viewDlrScheduleId = schedule_id;
        this.show_loading = true;
        this.view_dlr = true;
        let selected_schedule_id = schedule_id;
        this._appService.get_method(this._userService.schedule_url + "/" + this.campaign_page_no + "/" + schedule_id + "/" + this.dlr_report_page_no).subscribe(
            data => {

                // this.view_dlr = true;
                this.dlr_schedule = data.data;
                // console.log(this.dlr_schedule);
                for (let i = 0; i < this.dlr_schedule.length; i++) {

                    this.dlr_schedule[i]['delivered_date'] = moment(this.dlr_schedule[i]['delivered_date']).format("DD/MM/YY hh:mm:ss A");
                    // this.dlr_schedule[i]['delivered_date']=moment(this.dlr_schedule[i]['delivered_date']).add(5.5,"hours").format("DD-MM-YYYY HH:mm:ss A");

                    this.dlr_schedule[i]['created_at'] = moment(this.dlr_schedule[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                }
                this.show_loading = false;

            },
            error => {
                toastr.error("<Server Error >" + error + "");
                // console.log('get error')
            });



    }
    rScheduleChanged() {
        $('input.datepicker')[0]['value'] = "";
        $('#m_timepicker_1')[0]['value'] = this.getCurrentTime(new Date());


    }
    getCurrentTime(date) {
        var hours = date.getHours(),
            minutes = date.getMinutes(),
            ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return hours + ':' + minutes + ' ' + ampm;
    }
    resend(report) {
        $('#m_modal_resend').modal('show');
        this.getUserTimezone();
        this.report_id = report.id;
        this.report = report;
        // console.log(report);
        this.r_route = report.az_rname;
        this.r_senderid = report.senderid_name;
        this.r_message = report.message;
        this.r_characters = report.message.length;
        this.r_sms = report.numbers_count;
        this.r_credits = report.msg_credit;
        this.r_timezone = "IST";
        this.r_schedule = false;

    }
    currenttime = "";
    getScheduleTime() {
        let scheduled_time = new moment();
        scheduled_time = moment($('input#date1')[0]['value'] + " " + $('#m_timepicker_1')[0]['value'], "DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
        let timezone = this.r_timezone.substr(this.r_timezone.length - 7, 6);
        let offset: any = new Date().getTimezoneOffset();
        offset = offset.toString();
        if (offset[0] == '-') {
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").add(parseInt(offset), "minutes").format("YYYY-MM-DD HH:mm:ss");
        }
        else {
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").subtract(parseInt(offset), "minutes").format("YYYY-MM-DD HH:mm:ss");
        }
        if (timezone[0] == '+') {
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").add(hours + minutes, "hours").format("YYYY-MM-DD HH:mm:ss");
        }
        else {
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").subtract(hours + minutes, "hours").format("YYYY-MM-DD HH:mm:ss");
        }
        return scheduled_time;
    }
    calcTime(city, offset, type) {

        // create Date object for current location
        let d = new Date();
        //   console.log("date ",d);
        // year, month, day, hours, minutes, seconds, milliseco

        // convert to msec
        // add local time zone offset 
        // get UTC time in msec
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        //   console.log("utc",utc);
        //   console.log("utc ",moment(utc).format('YYYY-MM-DD HH:mm'));
        //   console.log(d.getTimezoneOffset());

        // create new Date object for different city
        // using supplied offset
        let nd;
        if (type == '+') {
            nd = new Date(utc + (3600000 * offset));

        }
        else {
            nd = new Date(utc - (3600000 * offset));

        }

        // alert("The local time in " + city + " is " + nd.toLocaleString());
        // console.log("universal",d.toUTCString())
        // return time as a string
        return nd.toLocaleString();

    }
    resendSave() {


        if (this.r_schedule && $('input#date1')[0]['value'] == "") {
            return toastr.warning("<h5>choose Date<h5>");
        }

        let d = {
            'is_scheduled': 0,
            'scheduled_time': ""
        }
        // required_time = moment(required_time, ["YYYY-MM-DD HH:mm:ss A"]).subtract(5.5, "hours").format("YYYY-MM-DD HH:mm:ss");
        if (this.r_schedule) {
            let scheduled_time = new moment();
            scheduled_time = moment($('input#date1')[0]['value'] + " " + $('#m_timepicker_1')[0]['value'], "DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
            let timezone = this.r_timezone.substr(this.r_timezone.length - 7, 6);
            let targetCountryTime;
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            let targetCountryPresentTime;
            if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
            else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }


            targetCountryTime = moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            targetCountryTime = targetCountryTime.concat("GMT" + timezone);
            let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
            // console.log("final date",final_date);
            // console.log("targetcountry",targetCountryTime);
            // console.log("targetcountry present time",targetCountryPresentTime);
            if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
                return toastr.info(this.r_timezone + " Present time is Ahead of the Selected Time Please Choose Valid Time");
            }


            // console.log(scheduled_time);
            d['is_scheduled'] = 1,
                d['scheduled_time'] = final_date;

            //  console.log(d);
            $('#m_modal_resend').modal('hide');
            this.show_modal_loading = true;

            this._appService.post_method(this._userService.report_resend + "" + this.report.id, JSON.stringify(d)).subscribe(
                data => {
                    if (data.status) {
                        this.show_modal_loading = false;
                        toastr.success("<h5>" + data.message + "<h5>");
                    }
                    else {
                        this.show_modal_loading = false;
                        toastr.error("<h5>" + data.message + "<h5>");
                    }

                },
                error => {
                    this.show_modal_loading = false;
                    toastr.error("<h5>" + error + "<h5>");

                }
            );



        }
        else {
            //     let scheduled_time=new moment();
            //     scheduled_time=moment().format("YYYY-MM-DD HH:mm:ss");
            //     let timezone=this.r_timezone.substr(this.r_timezone.length-7,6);
            //     let targetCountryTime;
            //     let timediff=timezone.substr(1,5).split(':'); 
            //     let hours=parseInt(timediff[0]);
            //     let  minutes=0;
            //     if(parseInt(timediff[1])!=0){ minutes=parseInt(timediff[1])/60; }
            //     let targetCountryPresentTime;
            //     if(timezone[0]=='+'){ targetCountryPresentTime=this.calcTime("country",hours+minutes,'+');  }
            //    else{ targetCountryPresentTime=this.calcTime("country",hours+minutes,'-'); }


            //     targetCountryTime=moment(scheduled_time,['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            //     targetCountryTime=targetCountryTime.concat("GMT"+timezone);
            //     let final_date=moment(new Date(targetCountryTime).toLocaleString("en-US", {timeZone: "Asia/kolkata"}),'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
            //     console.log("final date",final_date);
            //     console.log("targetcountry",targetCountryTime);
            //     console.log("targetcountry present time",targetCountryPresentTime);
            //     if(moment(targetCountryPresentTime,'DD/MM/YYYY, HH:mm:ss').diff(moment(scheduled_time,['YYYY-MM-DD HH:mm:ss']))>0){
            //         return toastr.info(this.r_timezone+" Present time is Ahead of Your System Time Please Choose Another Timezone");
            //     }

            // scheduled_time=moment($('input#date1')[0]['value']+" "+$('#m_timepicker_1')[0]['value'],"DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
            //              let timezone=this.r_timezone.substr(this.r_timezone.length-7,6);
            //              let timediff=timezone.substr(1,5).split(':'); 
            //     let hours=parseInt(timediff[0]);
            //     let  minutes=0;
            //     if(parseInt(timediff[1])!=0){ minutes=parseInt(timediff[1])/60; }
            //     let targetCountryTime;
            //     if(timezone[0]=='+'){ targetCountryTime=this.calcTime("country",hours+minutes,'+');  }
            //    else{ targetCountryTime=this.calcTime("country",hours+minutes,'-'); }
            //    console.log("target country",targetCountryTime);
            //    targetCountryTime=moment(targetCountryTime,['DD/MM/YYYY, HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            //    targetCountryTime=targetCountryTime.concat("GMT"+timezone);
            //  //  targetCountryTime=moment(targetCountryTime,'DD/MM/YYYY, HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');
            //  let final_date=moment(new Date(targetCountryTime).toLocaleString("en-US", {timeZone: "Asia/kolkata"}),'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
            // let final_date=moment().format("YYYY-MM-DD HH:mm:ss");
            d['is_scheduled'] = 0,
                d['scheduled_time'] = "";
            $('#m_modal_resend').modal('hide');
            this.show_modal_loading = true;
            // console.log(d);
            this._appService.post_method(this._userService.report_resend + "" + this.report.id, JSON.stringify(d)).subscribe(
                data => {
                    if (data.status) {
                        this.show_modal_loading = false;
                        toastr.success("<h5>" + data.message + "<h5>");
                    }
                    else {
                        this.show_modal_loading = false;
                        toastr.error("<h5>" + data.message + "<h5>");
                    }

                },
                error => {
                    this.show_modal_loading = false;
                    toastr.error("<h5>" + error + "<h5>");

                }
            );

        }
    }
    get_admin_routes() {
        this._appService.get_method(this._userService.routes_url).subscribe(
            data => {
                this.routes_list = data.result;

                // console.log(data);
            },
            error => {
                // console.log(error);
            }
        );
    }
    repush(report) {
        $('#m_modal_5').modal('show');
        this.status_ = ["Delivered", "Submitted", "dndfailed", "Cancelled", "Others"];
        this.dlr_type_ = ["kannel_dlr", "manual_dlr"];
        this.report_id = report.id;
        this.route_ = "";
        setTimeout(() => { $('.selectpicker').selectpicker('refresh'); }, 200);
    }
    repush_save() {
        if (this.status_.length == 0) return toastr.warning("<h5>Select Status<h5>");
        if (this.dlr_type_.length == 0) return toastr.warning("<h5>Select Dlr Type<h5>");
        if (this.route_ == "") return toastr.warning("<h5>Select Route<h5>");
        $('#m_modal_3').modal('hide');
        this.show_modal_loading = true;
        // console.log(this.status_);
        // console.log(this.dlr_type_);
        // console.log(this.route_);
        let data = {
            'status': this.status_,
            'dlr_type': this.dlr_type_,
            'gateway': this.route_
        }
        // console.log(data);
        this._appService.post_method(this._userService.sent_repush + "" + this.report_id, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    this.status_ = ["Delivered", "Submitted", "dndfailed", "Cancelled", "Others"];
                    this.dlr_type_ = ["kannel_dlr", "manual_dlr"];
                    this.route_ = "";
                    this.show_modal_loading = false;
                    toastr.success("<h5>" + data.message + "<h5>");
                }
                else {
                    this.show_modal_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }
                // console.log(data);
            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                // alert("Error");
                // console.log(error.message);
            }
        );
    }
    updateDLR(report) {
        //  console.log(report);
        $('#m_modal_update').modal('show');
        this.DLR_from = "";
        this.DLR_to = "";
        this.DLR_type = "all";
        this.Dlrlimit = "";

        this.report_id = report.id;
        this.DNDFailed = "";
        this.Delivered = "";
        this.Other = "";
        this.Submitted = "";
        this.numbers_count = "";
        this.dlrTypeChanged();
        setTimeout(() => { $('.selectpicker').selectpicker('refresh'); }, 200);
        // console.log(moment(report.scheduled_time).format("DD/MM/YYYY-hh:mm A"));
        $('input#date2')[0]['value'] = moment(report.scheduled_time, "DD/MM/YY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[0];
        this.update_Dlr_date = moment(report.scheduled_time, "DD/MM/YY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[0];
        $('#m_timepicker_10_modal')[0]['value'] = moment(report.scheduled_time, "DD/MM/YY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[1];
        $('#m_timepicker_11_modal')[0]['value'] = moment(report.scheduled_time, "DD/MM/YY hh:mm A").format("DD/MM/YYYY-hh:mm A").split('-')[1];

    }
    getScheduleCount(url: string) {
        this.show_loading = true;
        this._appService.get_method(url).subscribe(
            data => {
                //console.log("----------------",data);
                this.campaign_count = data.count;
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                console.log(error);
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    updateDlrSend() {
        if (this.Dlrlimit == "" || !/^[0-9]+$/.test(this.Dlrlimit)) return toastr.warning("<h5>Invalid Limit<h5>");
        if (this.DLR_type == "") return toastr.warning("<h5>Select Dlr Type<h5>");
        if (this.DLR_from == "") return toastr.warning("<h5>Select From Status<h5>");
        if (this.DLR_to == "") return toastr.warning("<h5>Select To Status<h5>");
        if (this.update_Dlr_date == "") return toastr.warning("<h5>Select Date<h5>");

        var startTime = moment($('#m_timepicker_10_modal')[0]['value'], "HH:mm A");
        var endTime = moment($('#m_timepicker_11_modal')[0]['value'], "HH:mm A");
        var duration = moment.duration(endTime.diff(startTime));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes()) % 60;

        if (hours < 0 || minutes < 0) return toastr.warning("<h5>Invalid time<h5>");
        //    if(hours==0 && minutes==0)return toastr.warning("<h5>Same Time Choose Different Time<h5>");
        $('#m_modal_update').modal('hide');

        this.show_modal_loading = true;

        startTime = moment(startTime, ["HH:mm A"]).subtract(5.5, "hours").format("YYYY-MM-DD HH:mm:ss");
        endTime = moment(endTime, ["HH:mm A"]).subtract(5.5, "hours").format("YYYY-MM-DD HH:mm:ss");

        let d = {
            'limit': this.Dlrlimit,
            'from_status': this.DLR_from,
            'to_status': this.DLR_to,
            'dlr_type': this.DLR_type,
            'start_time': startTime,
            'end_time': endTime
        }
        //console.log(d);
        this._appService.patch_method(this._userService.sent_update + "" + this.report_id, JSON.stringify(d)).subscribe(
            data => {
                if (data.status) {
                    this.show_loading = false;
                    this.DLR_from = "";
                    this.DLR_to = "";
                    this.DLR_type = "";
                    this.Dlrlimit = "";
                    this.DNDFailed = "";
                    this.Delivered = "";
                    this.Other = "";
                    this.Submitted = "";
                    this.numbers_count = "";
                    toastr.success("<h5>successfully Updated<h5>");

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                    this.show_modal_loading = false;
                }

            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
                this.show_modal_loading = false;
            }
        );


    }
    dlrTypeChanged() {
        this.show_dlr_count = true;
        let data;
        if (this.DLR_type == "all") data = { 'dlr_type': ['kannel_dlr', 'manual_dlr'] }
        else if (this.DLR_type == "original") data = { 'dlr_type': ['kannel_dlr'] }
        else data = { 'dlr_type': ['manual_dlr'] }
        this._appService.post_method(this._userService.campaign_count + "" + this.report_id, JSON.stringify(data)).subscribe(
            data => {
                this.DNDFailed = data.result[0]['DNDFailed'];
                this.Delivered = data.result[0]['Delivered'];
                this.Other = data.result[0]['Other'];
                this.Submitted = data.result[0]['Submitted'];
                this.numbers_count = data.result[0]['numbers_count'];
                this.show_dlr_count = false;
                // console.log(data);
            },
            error => {
                this.show_dlr_count = false;

                toastr.error("<h5>Server Error " + error + "<h5>");

                // console.log(error);
            }

        );
    }
    download(val) {
        // console.log(val);
        toastr.info("<h6>Preparing Data To Download<h6>");
        this._appService.get_method(this._userService.schedule_url + "/" + val.id).subscribe(
            data => {
                let schedulereport = [];
                for (let i = 0; i < data.data.length; i++) {
                    data.data[i]['delivered_date'] = moment(data.data[i]['delivered_date']).format("DD/MM/YY hh:mm:ss A");
                    data.data[i]['created_at'] = moment(data.data[i]['created_at']).format("DD/MM/YY hh:mm:ss A");
                    let d = {
                        'Created Date': data.data[i]['created_at'],
                        'Delivered Date': data.data[i]['delivered_date'],
                        'Mobile No': data.data[i]['mobile_number'],
                        'Msg Credit': data.data[i]['msgcredit'],
                        'Sender Id': data.data[i]['senderid'],
                        'Service Id': data.data[i]['service_id'],
                        'Status': data.data[i]['status'],
                        'Location': data.data[i]['location'],
                        // 'DLR response':data.data[i]['testing'],
                    }
                    schedulereport.push(d);
                }
                let ws = XLSX.utils.json_to_sheet(schedulereport);
                let wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, "ScheduleReport");
                XLSX.writeFile(wb, "ScheduleReport_" + val.user_name + "_" + val.sms_type + "_" + val.id + ".xlsx");
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
                //  console.log(error);
            }
        );



        // let d=[];
        // d.push(val);
        // let ws = XLSX.utils.json_to_sheet(d);
        // let wb = XLSX.utils.book_new();
        // XLSX.utils.book_append_sheet(wb, ws, "schedule report");
        // XLSX.writeFile(wb, "Schedule Report_"+val.id+".xlsx");
        // console.log(val);

    }
}
