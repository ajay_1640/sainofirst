import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SmppService } from '../smpp.service';
import { ApiUrl } from '../api-url.maps';
declare let toastr;
declare let $;

@Component({
    selector: 'smpp-mis-report',
    templateUrl: 'smpp-mis-report.component.html'
})
export class SmppMisReportComponent implements OnInit {

    misReportPage = 1;
    loading = false;
    requestMisReportForm: FormGroup;
    fromDate: any;
    toDate: any;
    mis_status: any = [];
    appUsers: any = [];

    ngOnInit(): void {
        this.getFormPreData();
    }

    constructor(private _fb: FormBuilder, private _smppService: SmppService) {
        this.requestMisReportForm = this._fb.group({
            smppUserName: ["", Validators.required],
            fromDate: [0, Validators.required],
            toDate: [0, Validators.required]
        });
    }

    getStatus(smppUser, fromDate, toDate) {

        var date = this._smppService.getEpoch(this._smppService.getTodayDDMMYYY());
        this.loading = true;
        this._smppService.getMisStatus(smppUser, fromDate, toDate).subscribe(data => {
            //console.log(data);
            this.loading = false;
            this.mis_status = data;
            for (var i = 0; i < this.mis_status.length; i++) {
                const date = this._smppService.epochToLocatDate(this.mis_status[i].date);
                this.mis_status[i].date = date.substr(0, date.indexOf(' '));
                this.mis_status[i].totalDelivered = this.updateField(this.mis_status[i].totalSms, this.mis_status[i].totalDelivered);
                this.mis_status[i].totalSubmitted = this.updateField(this.mis_status[i].totalSms, this.mis_status[i].totalSubmitted);
                this.mis_status[i].totalFailed = this.updateField(this.mis_status[i].totalSms, this.mis_status[i].totalFailed);
                this.mis_status[i].totalDndFailed = this.updateField(this.mis_status[i].totalSms, this.mis_status[i].totalDndFailed);
            }
        },
            err => {
                this.loading = false;
                toastr.error("<h5>Server Error.. Failed to load Live Status.</h5>");
            });
    }

    updateField(total, customVal) {
        return customVal + " (" + this.calPercent(customVal, total).toFixed(1) + ")%";
    }

    calPercent(eX, total) {
        return (eX * 100) / total;
    }

    // todyasStatus(){
    //     this.statusType = 1; // 1 for today
    //     this.getStatus(this.statusType);
    // }

    refreshStatus() {
        this.misStatus();
    }

    misStatus() {
        this.fromDate = $('#misDatepickerFromDate')[0].value;
        this.toDate = $('#misDatepickerToDate')[0].value;
        if (this.fromDate && this.toDate) {
            this.fromDate = this._smppService.getEpoch(this.fromDate);
            this.toDate = this._smppService.getEpoch(this.toDate);
            if (this.fromDate > this.toDate) {
                toastr.error("<h5>'From Date' cannot be greater than 'To Date'.</h5>");
                return;
            }
            this.toDate = this.toDate + 24 * 60 * 60;
            this.getStatus(this.selectedSmppUserName, this.fromDate, this.toDate);
        }
        else {
            toastr.error("<h5>Please select valid dates.</h5>");
        }
    }

    updateDate() {
        this.fromDate = $('#misDatepickerFromDate')[0].value;
        this.toDate = $('#misDatepickerToDate')[0].value;
        this.selectedSmppUserName = this.requestMisReportForm.controls['smppUserName'].value;
        if (this.fromDate && this.toDate) {
            if (this.selectedSmppUserName) {
                this.fromDate = this._smppService.getEpoch(this.fromDate);
                this.toDate = this._smppService.getEpoch(this.toDate);
                if (this.fromDate > this.toDate) {
                    toastr.error("<h5>'From Date' cannot be greater than 'To Date'.</h5>");
                    return;
                }
                this.toDate = this.toDate + + 24 * 60 * 60;
                this.requestMisReportForm.controls['fromDate'].setValue(this.fromDate);
                this.requestMisReportForm.controls['toDate'].setValue(this.toDate);
            }
            else {
                toastr.error("<h5>Please select valid smpp account.</h5>");
            }
        }
        else {
            toastr.error("<h5>Please select valid date range for smpp account.</h5>");
        }
    }



    refreshRequestedReport() {
        // /this.getAllRequestedReport();
    }
    selectedSmppUserName;


    getFormPreData() {
        // console.log("GetFormPRedat");
        this._smppService.getAllSmppUsers().subscribe(
            data => {
                this.appUsers = data;
                setTimeout(() => {
                    $('#smppUserNameMis').selectpicker('refresh');
                }, 100)
            },
            error => {
                toastr.error("<h5>Server Error: Cannot Load Form Details");
            }
        );
    }

    handleSubmit() {
        //console.log("handle Submit called.");
        this.updateDate();

        if (this.fromDate && this.toDate && this.fromDate > 0 && this.selectedSmppUserName && this.fromDate < this.toDate) {
            //const formData = this.requestMisReportForm.value;
            //this.loading = true;
            this.getStatus(this.selectedSmppUserName, this.fromDate, this.toDate);
            // this._smppService.requestNewReport(formData).subscribe(
            //     data => {
            //         //toastr.success("<h5>Download report request created successfully.</h5>");
            //         this.loading = false;

            //         //this.getAllReportCoreLogic(formData['smppUserName']);
            //     },
            //     err => {
            //         this.loading = false;
            //         if (err.status > 500) {
            //             toastr.error("<h5>Couldn't complete mis report: Server Error</h5>");
            //         } else {
            //             toastr.error("<h5>Unable to fetch mis report: " + err['msg'] + "</h5>");
            //         }
            //         console.log(err);
            //     }
            // );
        }
    }

}

// getAllRequestedReport() {
    //     //const smppUserName = this.requestMisReportForm.controls['smppUserName'].value;
    //     //if(smppUserName && smppUserName != "")
    //         //this.getAllReportCoreLogic(smppUserName);
    // }

    // resetForm() {
    //     this.requestMisReportForm.reset();
    // }

// downloadFile(id, key){

    //     //var blob = new Blob([data], { type: "application/octet-stream" });
    //     //var url= window.URL.createObjectURL(blob);
    //     //window.open(url);
    //     id = id + "/" + key;
    //     var anchor = document.createElement("a");
    //     anchor.download = "Smpp-Report.zip";
    //     anchor.href =  this.getReportUrl(id);
    //     anchor.click();
    //     //window.URL.revokeObjectURL(url);
    //     anchor.remove();
    //   }

    // downloadReportFile(id){
    //     //this.downloadFile("", id);
    //     this._smppService.downloadFile(id).subscribe(
    //         data => { 
    //             this.downloadFile(data, id);
    //             console.log("download-success");
    //         },
    //         error => {
    //             console.log("Error downloading the file.", error)
    //         }
    //     );
    // }

    // getReportUrl(id) {
    //     return ApiUrl.smppReportUrl + id;
    // }
    // getDownloadFileName() {
    //     return "report.zip";
    // }