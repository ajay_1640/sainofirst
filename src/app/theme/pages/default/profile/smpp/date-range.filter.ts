import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'filter'
})
export class DateRange implements PipeTransform {
    transform(transactions: any[], dateRange: TowDates): any[] {
        if (!transactions) return [];
        if (!dateRange) return transactions;
        const date1 = dateRange.startDate;
        const date2 = dateRange.endDate;
        return transactions.filter(it => {
            return (it.transactionDate >= date1 && it.transactionDate <= date2);
        });
    }
}
export interface TowDates {
    startDate: Number;
    endDate: Number;
}