import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from './../../../../../app.service';
import { UserService } from '../../../../../auth/_services';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiUrl } from './api-url.maps';
import { SmppService } from './smpp.service';

declare var moment: any;
declare let toastr;
declare var $, jQuery: any;

@Component({
    selector: "app-smpp",
    templateUrl: "./smpp.component.html",
    styleUrls: ["smpp.component.css"]
})
export class SmppComponent implements OnInit {

    //baseUrl="/smpp/";
    baseUrl = "/saino-first/api/apis/smpp/";
    getAllAppUser = "appusers";
    getAllSmppUser = "users"
    addUserUrl = "users/details";
    addCreditsUrl = "users/balance";
    checkUserNameUrl = "users/checkName/";
    getWalletTransactionListUrl = "users/";
    smppReportUrl = "downloadReport/user/";
    weeklySmppMisDataUrl = "mis/all/last-7-days"


    constructor(private _script: ScriptLoaderService,
        private _appService: AppService, private _smppService: SmppService) { }

    page = 1;
    wallet_p = 1;
    p_report = 1;
    download_list = [{
        'fromdate': 'Not Done',
        'todate': 'Not Done',
        'requested': 'Not Done',
        'prepared': 'Not Done',
        'status': 'Not Done'

    }];
    p = 1;
    dateRange: any;
    ar: any;
    from_date = "";
    arr: any;
    zip: any
    to_date = "";
    show_status = false;
    toDate: any;
    fromDate: any;
    calendar_range = "";
    show_calendar = false;
    requested_fromdate = "";
    requested_todate = "";
    requested = "";
    ready = "";
    readystatus = 0;
    m_users_p = 1;
    show_loading = false;
    searchText = "";
    show_modal_loading = false;
    amount_ = "";
    note_ = "";

    selectedUser: any = {};

    selectedUserId = "";
    selectedUserBalance = "";
    selectedUserAppUserName = "";
    appUserList = [];
    appUserCredits = "";

    walletTransactionList: any = [];

    addAccountConnectivity = "";
    // addAccountPassword=""; 
    // addAccountRole="";
    // addAccountSessions="";
    // addAccountUserName="";
    // addAccountStatus="";


    calendar_range_transaction;
    interval;
    tran_p;
    tranrangeChanged;


    smppReportList = "";
    smppReportUsername = "";

    smppMisData = "";

    tran_history = "";

    isUserNameAvailable = false;
    ngOnInit() {


        this._script.loadScripts('app-smpp',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-smpp',
            ['assets/demo/demo3/base/toastr.js']);
        this._script.loadScripts('app-download',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);

        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        this.getAllAppUsersData();
        this.getWeeklySmppMisData();
    }

    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            $('#m_datepicker_3')[0].value = "";
            $('#m_datepicker_4')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }

    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        console.log("Hi dates Before: ", this.from_date, this.to_date);
        this.calendar_range = "custom";
        // if (this.calendar_range == "") return;
        // else
        if (this.calendar_range == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {

            // this.from_date = $('#m_datepicker_1')[0].value;
            // this.to_date = $('#m_datepicker_2')[0].value;
            this.from_date = $('#m_datepicker_3')[0].value;
            this.to_date = $('#m_datepicker_4')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
            // console.log(this.from_date,this.to_date);
            // this.from_date=moment(this.from_date,"DD/MM/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"DD/MM/YYYY").format('DD/MM/YYYY');
        }
        console.log("Hi dates: ", this.from_date, this.to_date);
        this.show_status = false;
        this.getSmppReport(this.from_date, this.to_date);

        // console.log(this.from_date,this.to_date);

    }

    getSmppReport(from: any, to: any) {

        console.log("hgdgsgfhjyt6r5esdfcgv345678");
        // this._appService.get_method(ApiUrl.smppReportRequestUrl+this.smppReportUsername).subscribe(
        //     data => {
        //         this.smppReportList=data;
        //         console.log("here"+JSON.stringify(data));
        //     },
        //     error => {
        //         toastr.error("<h5>Server Error " + error + "");
        //         this.show_loading = false;
        //     });
    }

    addAccount() {
        //this.show_modal_loading = true;
        $('#add_account').modal('show');

    }

    addUserProfile() {

    }

    refreshSmppStatusData() {

    }
    refreshSmppMisData() {

    }

    walletClicked(User: any) {
        $("#walletmodal").modal('show');
        this.selectedUser = User;

        this.selectedUserId = User.credits.username;
        // + " (App Username: " + User.appUserName + " )";
        this.selectedUserBalance = User.credits.balance;
        this.getWalletTransactionList();
    }

    transactionClicked(User: any) {

        //$("#transactions").modal('show');
        $("#wallettransactionmodal").modal('show');
        this.selectedUser = User;
    }

    //   {
    //     "context": [ "/smpp"],
    //       "target": "http://manu-smpp.herokuapp.com",
    //       "secure": false,
    //       "changeOrigin": true
    // },


    getAllAppUsersData() {
        this._appService.get_method(ApiUrl.smppUser).subscribe(
            data => {
                this.appUserList = data;
                //console.log("here"+JSON.stringify(data));
            },
            error => {
                toastr.error("<h5>Server Error " + error + "");
                this.show_loading = false;
            });
    }

    updateAppUserData() {
        this.getAllAppUsersData();
    }


    // addNewAccount(){
    //     let data_send = JSON.stringify({
    //         'connection': this.addAccountConnectivity,
    //         'password': this.addAccountPassword,
    //         'role':this.addAccountRole,
    //         'sessions': this.addAccountSessions,
    //         'smppUserName': this.selectedUserId,
    //         'status': this.addAccountStatus,
    //     });

    //     this._appService.put_method(ApiUrl.smppUser, data_send).subscribe(
    //         data => {
    //             console.log(JSON.stringify(data));
    //         },
    //         error => {
    //             toastr.error("<h5>Server Error " + error + "<h5>");
    //         }
    //     );
    // }

    onAddAccountSubmit({ value, valid }: { value: User, valid: boolean }) {
        console.log(value, valid);
        this._appService.post_method(this.baseUrl + this.addUserUrl, JSON.stringify(value)).subscribe(
            data => {
                console.log(JSON.stringify(data));
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }

    addCredits() {
        let data_send = JSON.stringify({
            'amount': this.amount_,
            'note': this.note_,
            'smppUserName': this.selectedUserId,
            'type': "string",
        });
        this._appService.post_method(this.baseUrl + this.addCreditsUrl, data_send).subscribe(
            data => {
                console.log(JSON.stringify(data));
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }

    getWalletTransactionList() {
        //transactions;
        this._smppService.getTransactionList(this.selectedUserId).subscribe(
            data => {
                this.walletTransactionList = data;
                for (var i = 0; i < this.walletTransactionList.length; i++) {
                    var transactionDate = new Date(this.walletTransactionList[i]['transactionDate'] * 1000) + "";
                    transactionDate = transactionDate.substring(0, transactionDate.indexOf('GMT'));
                    this.walletTransactionList[i]['transactionDate'] = transactionDate;
                }
                console.log("transactions: ", this.walletTransactionList);
            },
            error => {
                toastr.error("<h5>Server Error " + error + "");
                this.show_loading = false;
            });
    }



    transactionSearchData() {
        this.search_custom_date();
    }

    getWeeklySmppMisData() {
        // this._appService.get_method(this.baseUrl+this.weeklySmppMisDataUrl).subscribe(
        //     data => {
        //         this.smppMisData=data;
        //         //console.log("misdata"+JSON.stringify(data));
        //     },
        //     error => {
        //         toastr.error("<h5>Server Error " + error + "");
        //         this.show_loading = false;
        //     });
    }

}



interface User {
    smppUserName: String,
    appUserName: String,
    connection: String,
    sessions: String,
    password: String,
    status: String,
    role: String,
}