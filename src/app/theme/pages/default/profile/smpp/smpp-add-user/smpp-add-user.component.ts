import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SmppService } from '../smpp.service';
import { AppService } from '../../../../../../app.service';
import { ApiUrl } from '../api-url.maps';
import { UserService } from '../../../../../../auth/_services';
import { AppFormValidator } from '../app-form.validator';
declare let toastr;
declare let $;

@Component({
    selector: 'smpp-add-user',
    templateUrl: 'smpp-add-user.component.html',
    styleUrls: ['./../smpp.component.css']
})
export class SmppAddUserComponent implements OnInit {

    @Output() valueChange = new EventEmitter();

    ngOnInit(): void {
        this.getFormPreData();
    }
    loading = false;
    addNewSmppAccountForm: FormGroup;
    addNewSmppError;
    addNewSmppSuccess;
    formValidator = AppFormValidator;
    isUserNameAvailable;
    show_loading;

    constructor(private _fb: FormBuilder, private _smppService: SmppService, private _userService: UserService, private _appService: AppService) {

        //this.formValidator = new AppFormValidator(_smppService);

        this.addNewSmppAccountForm = this._fb.group({
            smppUserName: ["", Validators.required],
            appUserName: ["", Validators.required],
            connection: ["", Validators.required],
            sessions: ["", Validators.required],
            password: ["", [Validators.required, Validators.minLength(8)]],
            status: ["ACTIVE"],
            role: ["", Validators.required],
            gateway: ["", Validators.required]
        }, { validator: this.formValidator.AppUserNameValidator });
    }

    appUsers = [];
    connections = [];

    get addSmppCtrl() {
        return this.addNewSmppAccountForm.controls;
    }

    getFormPreData() {
        // console.log("GetFormPRedat");
        this._appService.get_method(this._userService.permission_manage_users_permissions).subscribe(
            data => {
                this.connections = [{ "id": "1", "name": "Promotional" }, { "id": "2", "name": "Transactional" }, { "id": "3", "name": "Promo-Trans" }];
                //console.log("Users data", data);
                this.appUsers = data.result;
                setTimeout(() => {
                    $('#appUserNameSelect').selectpicker('refresh');
                    $('#roleSelect').selectpicker('refresh');
                    $('#connectionNameSelect').selectpicker('refresh');
                }, 1)
            },
            error => {
                toastr.error("<h5>Server Error: Cannot Load Form Details");
            }
        );
    }

    checkUserName() {
        if (this.addSmppCtrl.smppUserName.value.length > 0) {
            this._appService.get_method(ApiUrl.smppUserName + this.addSmppCtrl.smppUserName.value).subscribe(
                data => {
                    if (data) this.isUserNameAvailable = true;
                    else this.isUserNameAvailable = false;
                },
                error => {
                    toastr.error("<h5>Server Error: Couldn't verify User Name");
                    this.show_loading = false;
                });
        }
        else {
            this.isUserNameAvailable = false;
        }
    }

    resetForm() {
        this.addNewSmppAccountForm.reset();
    }

    handleSubmit() {
        //console.log(this.addNewSmppAccountForm);
        console.log("submit called");
        const formData = this.addNewSmppAccountForm.value;
        console.log("formdata", formData);
        //this.resetForm();
        this.loading = true;
        this._smppService.addNewSmppUser(formData).subscribe(
            data => {
                this.addNewSmppSuccess = 'Successfully Created New SMPP User.';
                console.log(data);
                this.loading = false;
                this.valueChange.emit({});
                toastr.success("<h5>Added new Smpp User Successfully.</h5>");
                $('#add_account').modal('hide');
            },
            err => {
                if (err.status > 500) {
                    this.addNewSmppError = 'Can\'t create new user now, server error. Please try agian.';
                } else {
                    this.addNewSmppError = err.error.errorMsg;
                }
                this.loading = false;
                toastr.error("<h5>Couldn't add new Smpp User: " + err + "</h5>");
                // $('#add_account').modal('hide');
                console.log(err);
            }
        );
        this.resetForm();

    }

}
