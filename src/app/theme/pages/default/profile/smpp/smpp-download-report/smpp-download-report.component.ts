import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { SmppService } from '../smpp.service';
import { AppService } from '../../../../../../app.service';
import { UserService } from '../../../../../../auth/_services';
import { ApiUrl } from '../api-url.maps';
declare let toastr;
declare let $;

@Component({
    selector: 'smpp-download-report',
    templateUrl: 'smpp-download-report.component.html'
})
export class SmppDownloadReportComponent implements OnInit {

    p_report = 1;
    loading = false;
    requestDownloadReportForm: FormGroup;
    fromDate: any;
    toDate: any;
    smppReportList: any = [];
    appUsers: any = [];

    name = 52;

    //@Output() valueChange = new EventEmitter();

    ngOnInit(): void {
        this.getFormPreData();
    }



    //allReportsRequestedBySelectedSmppUser: any = [];

    constructor(private _fb: FormBuilder, private _smppService: SmppService) {

        //this.formValidator = new AppFormValidator(_smppService);

        this.requestDownloadReportForm = this._fb.group({
            smppUserName: ["", Validators.required],
            fromDate: [0, Validators.required],
            toDate: [0, Validators.required]
        });
    }


    updateDate() {

        this.fromDate = $('#m_datepicker_3')[0].value;
        this.toDate = $('#m_datepicker_4')[0].value;
        //console.log("Dates 0: ", this.fromDate, this.toDate);
        this.selectedSmppUserName = this.requestDownloadReportForm.controls['smppUserName'].value;
        //console.log("SMPP USER NAME: ", this.selectedSmppUserName)
        if (this.fromDate && this.toDate) {
            if (this.selectedSmppUserName) {
                this.fromDate = this._smppService.getEpoch(this.fromDate);
                this.toDate = this._smppService.getEpoch(this.toDate);
                if (this.fromDate > this.toDate) {
                    toastr.error("<h5>'From Date' cannot be greater than 'To Date'.</h5>");
                    return;
                }
                this.toDate = this.toDate + + 24 * 60 * 60;
                this.requestDownloadReportForm.controls['fromDate'].setValue(this.fromDate);
                this.requestDownloadReportForm.controls['toDate'].setValue(this.toDate);

                //console.log("Dates: ", this.fromDate, this.toDate);
            }
            else {
                toastr.error("<h5>Please select valid smpp account.</h5>");
            }
        }
        else {
            toastr.error("<h5>Please select valid date range for smpp account.</h5>");
        }
    }



    getAllRequestedReport() {
        const smppUserName = this.requestDownloadReportForm.controls['smppUserName'].value;
        if (smppUserName && smppUserName != "")
            this.getAllReportCoreLogic(smppUserName);
    }

    getAllReportCoreLogic(smppUserName) {
        this.loading = true;
        this._smppService.getAllRequestedReport(smppUserName).subscribe(
            data => {
                this.loading = false;
                this.smppReportList = data;
                //console.log("transaction list loaded.. length=", data["length"]);
                for (var i = 0; i < this.smppReportList.length; i++) {
                    this.smppReportList[i]['fromDate'] = this._smppService.epochToLocatDate(this.smppReportList[i]['fromDate']);
                    this.smppReportList[i]['toDate'] = this._smppService.epochToLocatDate(this.smppReportList[i]['toDate'] - 1);
                    this.smppReportList[i]['requestedDate'] = this._smppService.epochToLocatDate(this.smppReportList[i]['requestedDate']);
                    var preparedDateTemp = this.smppReportList[i]['preparedDate'];
                    if (preparedDateTemp)
                        this.smppReportList[i]['preparedDate'] = this._smppService.epochToLocatDate(preparedDateTemp);
                    else
                        this.smppReportList[i]['preparedDate'] = "";
                }

            },
            error => {
                this.loading = false;
                toastr.error("<h5>Server Error: Cannot Load Requested Reports");
            }
        );
    }

    refreshRequestedReport() {
        this.getAllRequestedReport();
    }
    selectedSmppUserName;

    downloadFile(id, key) {

        //var blob = new Blob([data], { type: "application/octet-stream" });
        //var url= window.URL.createObjectURL(blob);
        //window.open(url);
        id = id + "/" + key;
        var anchor = document.createElement("a");
        anchor.download = "Smpp-Report.zip";
        anchor.href = this.getReportUrl(id);
        anchor.click();
        //window.URL.revokeObjectURL(url);
        anchor.remove();
    }

    downloadReportFile(id) {
        //this.downloadFile("", id);
        this._smppService.downloadFile(id).subscribe(
            data => {
                this.downloadFile(data, id);
                console.log("download-success");
            },
            error => {
                console.log("Error downloading the file.", error)
            }
        );
    }

    getReportUrl(id) {
        return ApiUrl.smppReportUrl + id;
    }
    getDownloadFileName() {
        return "report.zip";
    }
    getFormPreData() {
        // console.log("GetFormPRedat");
        this._smppService.getAllSmppUsers().subscribe(
            data => {
                this.appUsers = data;
                setTimeout(() => {
                    $('#smppUserName').selectpicker('refresh');
                }, 100)
            },
            error => {
                toastr.error("<h5>Server Error: Cannot Load Form Details");
            }
        );
    }


    resetForm() {
        this.requestDownloadReportForm.reset();
    }

    handleSubmit() {
        //console.log("handle Submit called.");
        this.updateDate();
        if (this.fromDate && this.toDate && this.fromDate > 0 && this.selectedSmppUserName && this.fromDate < this.toDate) {
            //console.log(this.addNewSmppAccountForm);
            const formData = this.requestDownloadReportForm.value;
            //this.resetForm();
            //console.log(formData);
            this.loading = true;

            this._smppService.requestNewReport(formData).subscribe(
                data => {
                    toastr.success("<h5>Download report request created successfully.</h5>");
                    this.loading = false;
                    this.getAllReportCoreLogic(formData['smppUserName']);
                    //this.valueChange.emit({});
                    //$('#downloadReportModal').modal('hide');
                },
                err => {
                    //$('#downloadReportModal').modal('hide');
                    if (err.status > 500) {
                        toastr.error("<h5>Couldn't create new report request: Server Error</h5>");
                    } else {
                        toastr.error("<h5>Couldn't create new report request: " + err + "</h5>");
                    }
                    this.loading = false;
                    console.log(err);
                }
            );
        }
    }

}
