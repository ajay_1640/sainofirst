import { Component, Input, OnChanges } from '@angular/core';
import { SmppService } from '../smpp.service';
declare let toastr;
declare let $;

@Component({
    selector: 'smpp-balance-transaction',
    templateUrl: 'smpp-balance-transaction.component.html',
})
export class SmppBalanceTransactionComponent implements OnChanges {

    // @Output() valueChange = new EventEmitter();

    @Input('selectedUser') selectedUser: any = {
        'credits': {
            username: 'Loading..',
            balance: 0
        },
        'appUserName': 'Loading..'
    };
    //updateBalanceError : String; 
    loading = false;
    //updateSmppBalanceForm: FormGroup;
    //formValidator = AppFormValidator;
    //show_loading;
    walletTransactionList: any = [];
    wallet_p = 1;
    //selectedSmppUserBalance = this.selectedUser.credits.balance;
    //selectedSmppAppUserName = this.selectedUser.appUserName;
    //selectedSmppUserName = this.selectedUser == undefined ? "" : this.selectedUser.credits.username;

    constructor(private _smppService: SmppService) {
    }
    //console.log("creating manually..", this.selectedUser);
    //    if( this.selectedUser == undefined){
    //        console.log("creating manually.. inside if...");
    //        this.selectedUser = {
    //         'credits' : {
    //             username: 'Loading..'
    //         },
    //         'appUserName': 'Loading..'
    //         };
    //     }
    // this.updateSmppBalanceForm = this._fb.group({
    //     amount: ["", Validators.required],
    //     type: ["", Validators.required],
    //     smppUserName: [this.selectedSmppUserName],
    //     note: [""]
    // });



    ngOnChanges() {
        //console.log("creating manually.. on changes", this.selectedUser);
        // if(this.selectedUser.credits == undefined){
        //     this.selectedSmppUserName =  "Loading...";
        //     this.selectedSmppUserBalance = 0;
        //     this.selectedSmppAppUserName = "Loading...";
        // }
        // else{
        //     this.selectedSmppUserName =  this.selectedUser.credits.username;
        //     this.selectedSmppUserBalance = this.selectedUser.credits.balance;
        //     this.selectedSmppAppUserName = this.selectedUser.appUserName;
        // }
        // this.updateSmppBalanceForm.controls['smppUserName'].setValue(this.selectedSmppUserName);
        this.getWalletTransactionList();
    }

    // handleSubmit() {
    //     this.loading = true;
    //     const formData = this.updateSmppBalanceForm.value;
    //     console.log(formData);
    //     this.updateSmppBalanceForm.reset();
    //     this._smppService.updateSmppBalance(formData).subscribe(
    //         data => {
    //             console.log(data);
    //             this.selectedSmppUserBalance = data['balance'];
    //             this.loading = false;
    //             this.valueChange.emit({});
    //             toastr.success("<h5>Updated Balance Successfully.</h5>");
    //             $('#walletmodal').modal('hide');
    //         },
    //         err => {
    //             if (err.status > 500) {
    //                 this.updateBalanceError = 'Can\'t update balance now, server error. Please try agian.';
    //             } else {
    //                 this.updateBalanceError = err.error.errorMsg;
    //             }
    //             this.loading = false;
    //             toastr.error("<h5>" + this.updateBalanceError + "</h5>");
    //             $('#walletmodal').modal('hide');
    //             console.log(err);
    //         }
    //     );
    // }

    getWalletTransactionList() {
        //transactions;
        this.loading = true;
        if (this.selectedUser && this.selectedUser.credits && this.selectedUser.credits.username)
            this._smppService.getTransactionList(this.selectedUser.credits.username).subscribe(
                data => {
                    this.loading = false;
                    this.walletTransactionList = data;
                    for (var i = 0; i < this.walletTransactionList.length; i++) {
                        var transactionDate = new Date(this.walletTransactionList[i]['transactionDate'] * 1000) + "";
                        transactionDate = transactionDate.substring(0, transactionDate.indexOf('GMT'));
                        this.walletTransactionList[i]['transactionDate'] = transactionDate;
                    }
                    console.log("transactions: ", this.walletTransactionList);
                },
                error => {
                    toastr.error("<h5>Server Error " + error + "");
                    this.loading = false;
                });

    }

}
