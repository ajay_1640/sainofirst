import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ApiUrl } from './api-url.maps';
import { ResponseContentType } from '@angular/http';
declare var moment: any;

@Injectable()
export class SmppService {

    constructor(private _http: HttpClient) { }

    addNewSmppUser(smppUserData) {
        return this._http.post(ApiUrl.smppUser, smppUserData);
    }

    checkSmppUserName(smppUserName) {
        return this._http.get(ApiUrl.smppUserName + smppUserName);
    }

    updateSmppBalance(balanceUpdateData) {
        return this._http.put(ApiUrl.credits, balanceUpdateData);
    }

    getTransactionList(smppUserName) {
        return this._http.get(ApiUrl.getWalletTransactionListUrl + smppUserName + "/" + ApiUrl.transactionsPartUrl);
    }

    getTodayDDMMYYY() {
        var today: any = new Date();
        var dd: any = today.getDate();
        var mm: any = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = dd + '/' + mm + '/' + yyyy;
        return today;
    }

    getEpoch(dateString) {
        dateString = dateString + "";
        var dateArgs = dateString.match(/\d{2,4}/g),
            year = dateArgs[2],
            month = parseInt(dateArgs[1]) - 1,
            day = dateArgs[0]
        var milliseconds = (new Date(year, month, day).getTime()) / 1000;
        if (isNaN(milliseconds)) {
            return 0;
        }
        else {
            return milliseconds;
        }
    }

    getStatusToday(date) {
        return this._http.get(ApiUrl.smppLiveStatusByDayUrl + date);
    }

    getStatusBetween(startDate, endDate) {
        return this._http.get(ApiUrl.smppLiveStatusBetweenUrl + startDate + '/' + endDate);
    }

    downloadFile(id) {
        return this._http.get(ApiUrl.smppReportUrl + id, {
            responseType: "blob"
        });
    }

    epochToLocatDate(epoch) {
        return moment.unix(epoch).format('DD-MM-YYYY hh:mm:ss A')
    }

    requestNewReport(data) {
        return this._http.post(ApiUrl.smppReportUrl, data);
    }

    getAllRequestedReport(smppUserName) {
        return this._http.get(ApiUrl.smppReportRequestUrl + smppUserName);
    }

    getAllSmppUsers() {
        return this._http.get(ApiUrl.smppUser);
    }

    getMisStatus(smpUserName, fromDate, toDate) {
        return this._http.get(ApiUrl.misStatus + smpUserName + ApiUrl.misBetween + fromDate + "/" + toDate);
    }
}
