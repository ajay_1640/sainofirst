
import { Component, Input } from "@angular/core";
@Component({
    selector: "form-field-errors",
    template: `
  <span [hidden]="!formValidator?.showError(formName, fieldName)">
    <div *ngIf="formValidator?.isFieldInvalid(formName, fieldName)">
        <ul *ngFor="let error of formValidator.getCustomErrors(formName, fieldName)">
            <li style="color: red;"> {{ error.msg }} </li>
        </ul>
        <ul *ngIf="formValidator?.isRequired(formName, fieldName)">
            <li style="color: red;">This field is required.</li>
        </ul>
    </div>
  </span>`,
    styles: []
})
export class FormFieldErrorsComponent {
    @Input("formName") formName: any;
    @Input("fieldName") fieldName: any;
    @Input("handle") formValidator;
    constructor() {
        //console.log("Error Validator Called..!!");
    }
}
