import { Component } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { SmppService } from '../smpp.service';
import { AppService } from '../../../../../../app.service';
import { UserService } from '../../../../../../auth/_services';
import { ApiUrl } from '../api-url.maps';
import { PercentPipe } from '@angular/common';
declare let toastr;
declare let $;

@Component({
    selector: 'smpp-live-status',
    templateUrl: 'smpp-live-status.component.html'
})
export class SmppLiveStatusComponent {

    live_status_page = 1;
    loading = false;
    fromDate: any;
    toDate: any;
    statusType = 1;
    live_status: any = [];


    constructor(private _smppService: SmppService) {
        this.todyasStatus();
    }

    getStatus(todayOrBetween) {
        if (todayOrBetween == 1) {
            // today
            var date = this._smppService.getEpoch(this._smppService.getTodayDDMMYYY());
            this.loading = true;
            this._smppService.getStatusToday(date).subscribe(data => {
                //console.log(data);
                this.loading = false;
                this.live_status = data;
                for (var i = 0; i < this.live_status.length; i++) {
                    this.live_status[i].totalDelivered = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalDelivered);
                    this.live_status[i].totalSubmitted = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalSubmitted);
                    this.live_status[i].totalFailed = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalFailed);
                    this.live_status[i].totalDndFailed = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalDndFailed);
                }
            },
                err => {
                    this.loading = false;
                    toastr.error("<h5>Server Error.. Failed to load Live Status.</h5>");
                });
        }
        else {
            this.loading = true;
            this._smppService.getStatusBetween(this.fromDate, this.toDate).subscribe(data => {
                //console.log(data);
                this.loading = false;
                this.live_status = data;
                for (var i = 0; i < this.live_status.length; i++) {
                    this.live_status[i].totalDelivered = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalDelivered);
                    this.live_status[i].totalSubmitted = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalSubmitted);
                    this.live_status[i].totalFailed = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalFailed);
                    this.live_status[i].totalDndFailed = this.updateField(this.live_status[i].totalSms, this.live_status[i].totalDndFailed);
                }
            },
                err => {
                    this.loading = false;
                    toastr.error("<h5>Server Error.. Failed to load Live Status.</h5>");
                });
            // between
        }
    }

    updateField(total, customVal) {
        return customVal + " (" + this.calPercent(customVal, total).toFixed(1) + ")%";
    }

    calPercent(eX, total) {
        return (eX * 100) / total;
    }

    todyasStatus() {
        this.statusType = 1; // 1 for today
        this.getStatus(this.statusType);
    }

    refreshStatus() {
        if (this.statusType == 1) {
            this.todyasStatus();
        }
        else {
            this.customStatus();
        }
    }

    customStatus() {
        this.statusType = 2; // 2 for custom
        this.fromDate = $('#liveStatus_fromDatePicker')[0].value;
        this.toDate = $('#liveStatus_toDatePicker')[0].value;
        if (this.fromDate && this.toDate) {
            this.fromDate = this._smppService.getEpoch(this.fromDate);
            this.toDate = this._smppService.getEpoch(this.toDate);
            if (this.fromDate > this.toDate) {
                toastr.error("<h5>'From Date' cannot be greater than 'To Date'.</h5>");
                return;
            }
            this.toDate = this.toDate + 24 * 60 * 60;
            this.getStatus(this.statusType);
        }
        else {
            toastr.error("<h5>Please select valid dates.</h5>");
        }
    }
}
