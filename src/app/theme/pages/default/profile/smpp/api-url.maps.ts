export class ApiUrl {
    //private static readonly baseUrl = "http://159.69.120.60:5000";
    //private static readonly baseUrl = "http://localhost:5000";
    private static readonly baseUrl = "/saino-first/api/apis";
    private static readonly apiBase = ApiUrl.baseUrl + "/smpp/";

    static readonly appUser = ApiUrl.apiBase + "appusers";
    static readonly smppUser = ApiUrl.apiBase + "users";
    static readonly appUserDetails = ApiUrl.apiBase + "users/details";
    static readonly credits = ApiUrl.apiBase + "users/balance";
    static readonly smppUserName = ApiUrl.apiBase + "users/checkName/";
    static readonly getWalletTransactionListUrl = ApiUrl.apiBase + "users/";
    static readonly smppReportUrl = ApiUrl.apiBase + "downloadReport/"
    static readonly smppReportRequestUrl = ApiUrl.smppReportUrl + "user/";
    static readonly weeklySmppMisDataUrl = ApiUrl.apiBase + "mis/all/last-7-days";
    static readonly transactionsPartUrl = "transactions";
    private static readonly livestatus = ApiUrl.apiBase + "livestatus/";
    static readonly smppLiveStatusByDayUrl = ApiUrl.livestatus + "byDay/";
    static readonly smppLiveStatusBetweenUrl = ApiUrl.livestatus + "between/";

    static readonly misStatus = ApiUrl.apiBase + "mis/";
    static readonly misBetween = "/between/";



}
