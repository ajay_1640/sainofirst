import { NgModule } from '@angular/core';
import { SmppService } from './smpp.service';
import { SmppAddUserComponent } from './smpp-add-user/smpp-add-user.component';
import { FormFieldErrorsComponent } from './formFieldError.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DateRange } from './date-range.filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { SmppBalanceUpdateComponent } from './smpp-balance-update/smpp-balance-update.component';
import { SmppBalanceTransactionComponent } from './smpp-balance-update/smpp-balance-transaction.component';
import { SmppDownloadReportComponent } from './smpp-download-report/smpp-download-report.component';
import { SmppLiveStatusComponent } from './smpp-live-status/smpp-live-status.component';
import { SmppMisReportComponent } from './smpp-mis-report/smpp-mis-report.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
    declarations: [SmppAddUserComponent, FormFieldErrorsComponent, DateRange,
        SmppBalanceUpdateComponent, SmppBalanceTransactionComponent,
        SmppDownloadReportComponent, SmppLiveStatusComponent, SmppMisReportComponent],
    imports: [CommonModule, ReactiveFormsModule, NgxPaginationModule, Ng2SearchPipeModule],
    exports: [SmppAddUserComponent, DateRange, FormFieldErrorsComponent,
        SmppBalanceUpdateComponent, SmppBalanceTransactionComponent,
        SmppDownloadReportComponent, SmppLiveStatusComponent, SmppMisReportComponent],
    providers: [SmppService]
})
export class SmppModule { }
