import { AbstractControl } from "@angular/forms";
import { SmppService } from "./smpp.service";
import { InjectorInstance } from "../../../../../app.module";
import { HttpClientModule, HttpClient } from "@angular/common/http";

export class AppFormValidator {
    static smppService = new SmppService(InjectorInstance.get<HttpClient>(HttpClient));
    constructor(private _smppService: SmppService) {
        console.log("called..construct");
        AppFormValidator.smppService = _smppService;
    }
    static getFieldValidationClass(formName, formControl) {
        if (
            formName.controls[formControl].invalid &&
            (formName.controls[formControl].dirty ||
                formName.controls[formControl].touched)
        ) {
            return "has-danger";
        }
        if (formName.controls[formControl].valid) {
            return "has-success";
        }
    }
    static isFieldInvalid(formName, formControl) {
        return formName.controls[formControl].invalid;
    }
    static isRequired(formName, formControl) {
        return formName.controls[formControl].errors.required;
    }
    static showError(formName, formControl) {
        return (
            formName.controls[formControl].touched &&
            !formName.controls[formControl].valid
        );
    }
    static getCustomErrors(formName, formControl) {
        return formName.controls[formControl].errors.customErrors;
    }

    // static AppUserNameValidator(AC: AbstractControl) {
    //   console.log("Calling custom validator");
    //   console.log(AC);
    //   //const password = AC.get("appUserName").value; // to get value in input tag
    //   //console.log("Calling custom validator");
    //   if (1 == 1) {
    //     AC.get("appUserName").setErrors({ isFieldInvalid: true });
    //   } else {
    //     return null;
    //   }
    // }

    static AppUserNameValidator(AC: AbstractControl) {
        //const password = AC.get("appUserName").value; // to get value in input tag
        //console.log("Calling custom validator");
        //var isUserNameAvailable;
        var customErrors = [];
        const smppUserName = AC.get("smppUserName"); // to get value in input tag
        //console.log("SMPP USER CONTROL", smppUserName);
        if (smppUserName && smppUserName.value && smppUserName.value.length > 0) {
            AppFormValidator.smppService.checkSmppUserName(smppUserName.value).subscribe(
                data => {
                    if (data) return null;
                    else customErrors.push({ msg: "This Smpp Username is already taken." });
                    AC.get("smppUserName").setErrors({
                        customErrors: customErrors
                    });
                },
                error => {
                    console.log(error);
                });
        }
    }

    static MatchPassword(AC: AbstractControl) {
        const password = AC.get("password").value; // to get value in input tag
        const confirmPassword = AC.get("confirmPassword").value;
        if (password !== confirmPassword) {
            AC.get("confirmPassword").setErrors({ invalidPassword: true });
        } else {
            return null;
        }
    }

    static DateValidator(AC: AbstractControl) {
        const date = AC.get("dateOfBirth").value;
        const customErrors = [];
        if (
            date &&
            !date.match(
                /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/
            )
        ) {
            customErrors.push({ msg: "This date is invalid." });
        } else {
            return null;
        }
        AC.get("dateOfBirth").setErrors({
            customErrors: customErrors
        });
    }

    static NameValidator(AC: AbstractControl) {
        const name = AC.get("name").value;
        const customErrors = [];
        if (name && !name.match(/^[a-zA-Z ]*$/)) {
            customErrors.push({ msg: "Enter Valid Name." });
            if (name.match(/\d+/g)) {
                customErrors.push({ msg: "Name Cannot Contain Numbers." });
            }
            if (name.match(/[^A-Za-z0-9]/)) {
                customErrors.push({ msg: "Name Cannot Contain Special Caracters." });
            }
        } else {
            return null;
        }
        AC.get("name").setErrors({
            customErrors: customErrors
        });
    }
    static PanNumberValidator(AC: AbstractControl) {
        const pan = AC.get("pan").value;
        const customErrors = [];
        if (pan && pan.match("_")) {
            customErrors.push({ msg: "PAN Number is Invalid." });
        } else {
            return null;
        }
        AC.get("pan").setErrors({
            customErrors: customErrors
        });
    }
    static EmailValidator(AC: AbstractControl) {
        const email = AC.get("email").value;
        console.log("emailValidatorCalled");
        const customErrors = [];
        if (
            email &&
            !email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
        ) {
            customErrors.push({ msg: "Please Enter Valid Email Id." });
        } else {
            return null;
        }

        AC.get("email").setErrors({
            customErrors: customErrors
        });
    }

    static AadhaarNumberValidator(AC: AbstractControl) {
        const aadhar = AC.get("aadhar").value;
        const customErrors = [];
        if (aadhar && aadhar.match("_")) {
            customErrors.push({ msg: "Aadhar number is invalid." });
        } else {
            return null;
        }
        AC.get("aadhar").setErrors({
            customErrors: customErrors
        });
    }

    static MobileNumberValidator(AC: AbstractControl) {
        const input = AC.get("mobile");
        const mobile = input.value;
        const customErrors = [];
        if (mobile && mobile.match(/^[9,8,7]([0-9]{9})$/)) {
            console.log("No Err ", mobile);
            return null;
        } else if (
            mobile.match(/^([0-9]{10})$/) &&
            !mobile.match(/^[9,8,7]([0-9]{9})$/)
        ) {
            console.log("Err ", mobile);
            customErrors.push({
                msg: "Indian mobile number must start with 9, 8 or 7."
            });
        } else if (!mobile.match(/^([0-9]{10})$/)) {
            customErrors.push({ msg: "Mobile number must be of 10 Digits." });
        } else {
            customErrors.push({ msg: "Mobile number doesn't match with format." });
        }
        input.setErrors({
            customErrors: customErrors
        });
    }
}
