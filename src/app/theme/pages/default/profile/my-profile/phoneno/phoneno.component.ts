import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
export let p_no;
export let country_info: any;
declare var $, jQuery: any;
@Component({
    selector: 'app-phoneno',
    templateUrl: './phoneno.html',
    //styleUrls: ['./build/css/intlTelInput.css','./build/css/demo.css']
})

export class PhonenoComponent implements OnInit {

    constructor(private _script: ScriptLoaderService) { }

    ngOnInit() {

        this._script.loadScripts('app-my-profile',
            ['assets/build/js/intlTelInput.js']);
        country_info = $("#phone").intlTelInput("getSelectedCountryData");
        $("#phone").intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            // initialCountry: "auto",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            utilsScript: "assets/build/js/utils.js"
        });
    }

    isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
}
