var LOCALES = {
    'en': {
        'ax': 'AALAND ISLANDS',
        'af': 'AFGHANISTAN',
        'al': 'ALBANIA',
        'dz': 'ALGERIA',
        'as': 'AMERICAN SAMOA',
        'ad': 'ANDORRA',
        'ao': 'ANGOLA',
        'ai': 'ANGUILLA',
        'aq': 'ANTARCTICA',
        'ag': 'ANTIGUA AND BARBUDA',
        'ar': 'ARGENTINA',
        'am': 'ARMENIA',
        'aw': 'ARUBA',
        'au': 'AUSTRALIA',
        'at': 'AUSTRIA',
        'az': 'AZERBAIJAN',
        'bs': 'BAHAMAS',
        'bh': 'BAHRAIN',
        'bd': 'BANGLADESH',
        'bb': 'BARBADOS',
        'by': 'BELARUS',
        'be': 'BELGIUM',
        'bz': 'BELIZE',
        'bj': 'BENIN',
        'bm': 'BERMUDA',
        'bt': 'BHUTAN',
        'bo': 'BOLIVIA',
        'ba': 'BOSNIA AND HERZEGOWINA',
        'bw': 'BOTSWANA',
        'bv': 'BOUVET ISLAND',
        'br': 'BRAZIL',
        'io': 'BRITISH INDIAN OCEAN TERRITORY',
        'bn': 'BRUNEI DARUSSALAM',
        'bg': 'BULGARIA',
        'bf': 'BURKINA FASO',
        'bi': 'BURUNDI',
        'bq': 'CARIBBEAN NETHERLANDS',
        'kh': 'CAMBODIA',
        'cm': 'CAMEROON',
        'ca': 'CANADA',
        'cv': 'CAPE VERDE',
        'ky': 'CAYMAN ISLANDS',
        'cf': 'CENTRAL AFRICAN REPUBLIC',
        'td': 'CHAD',
        'cl': 'CHILE',
        'cn': 'CHINA',
        'cx': 'CHRISTMAS ISLAND',
        'cc': 'COCOS ISLANDS',
        'co': 'COLOMBIA',
        'km': 'COMOROS',
        'cd': 'CONGO',
        'cg': 'CONGO REPUBLIC',
        'ck': 'COOK ISLANDS',
        'cr': 'COSTA RICA',
        'ci': 'COTE DIVOIRE',
        'hr': 'CROATIA ',
        'cu': 'CUBA',
        'cw': 'CURACAO',
        'cy': 'CYPRUS',
        'cz': 'CZECH REPUBLIC',
        'dk': 'DENMARK',
        'dj': 'DJIBOUTI',
        'dm': 'DOMINICA',
        'do': 'DOMINICAN REPUBLIC',
        'ec': 'ECUADOR',
        'eg': 'EGYPT',
        'sv': 'EL SALVADOR',
        'gq': 'EQUATORIAL GUINEA',
        'er': 'ERITREA',
        'ee': 'ESTONIA',
        'et': 'ETHIOPIA',
        'fk': 'FALKLAND ISLANDS (MALVINAS)',
        'fo': 'FAROE ISLANDS',
        'fj': 'FIJI',
        'fi': 'FINLAND',
        'fr': 'FRANCE',
        'gf': 'FRENCH GUIANA',
        'pf': 'FRENCH POLYNESIA',
        'tf': 'FRENCH SOUTHERN TERRITORIES',
        'ga': 'GABON',
        'gm': 'GAMBIA',
        'ge': 'GEORGIA',
        'de': 'GERMANY',
        'gh': 'GHANA',
        'gi': 'GIBRALTAR',
        'gr': 'GREECE',
        'gl': 'GREENLAND',
        'gd': 'GRENADA',
        'gp': 'GUADELOUPE',
        'gu': 'GUAM',
        'gt': 'GUATEMALA',
        'gg': 'GUERNSEY',
        'gn': 'GUINEA',
        'gw': 'GUINEA-BISSAU',
        'gy': 'GUYANA',
        'ht': 'HAITI',
        'hm': 'HEARD AND MC DONALD ISLANDS',
        'hn': 'HONDURAS',
        'hk': 'HONG KONG',
        'hu': 'HUNGARY',
        'is': 'ICELAND',
        'in': 'INDIA',
        'id': 'INDONESIA',
        'ir': 'IRAN ',
        'iq': 'IRAQ',
        'ie': 'IRELAND',
        'im': 'ISLA DE MAN',
        'il': 'ISRAEL',
        'it': 'ITALY',
        'jm': 'JAMAICA',
        'jp': 'JAPAN',
        'je': 'JERSEY',
        'jo': 'JORDAN',
        'kz': 'KAZAKHSTAN',
        'ke': 'KENYA',
        'ki': 'KIRIBATI',
        'kp': 'KOREA NORTH ',
        'kr': 'KOREA SOUTH',
        'xk': 'KOSOVO',
        'kw': 'KUWAIT',
        'kg': 'KYRGYZSTAN',
        'la': 'LAO',
        'lv': 'LATVIA',
        'lb': 'LEBANON',
        'ls': 'LESOTHO',
        'lr': 'LIBERIA',
        'ly': 'LIBYAN ARAB JAMAHIRIYA',
        'li': 'LIECHTENSTEIN',
        'lt': 'LITHUANIA',
        'lu': 'LUXEMBOURG',
        'mo': 'MACAU',
        'mk': 'MACEDONIA',
        'mg': 'MADAGASCAR',
        'mw': 'MALAWI',
        'my': 'MALAYSIA',
        'mv': 'MALDIVES',
        'ml': 'MALI',
        'mt': 'MALTA',
        'mh': 'MARSHALL ISLANDS',
        'mq': 'MARTINIQUE',
        'mr': 'MAURITANIA',
        'mu': 'MAURITIUS',
        'yt': 'MAYOTTE',
        'mx': 'MEXICO',
        'fm': 'MICRONESIA',
        'md': 'MOLDOVA',
        'mc': 'MONACO',
        'mn': 'MONGOLIA',
        'me': 'MONTENEGRO',
        'ms': 'MONTSERRAT',
        'ma': 'MOROCCO',
        'mz': 'MOZAMBIQUE',
        'mm': 'MYANMAR',
        'na': 'NAMIBIA',
        'nr': 'NAURU',
        'np': 'NEPAL',
        'nl': 'NETHERLANDS',
        'an': 'NETHERLANDS ANTILLES',
        'nc': 'NEW CALEDONIA',
        'nz': 'NEW ZEALAND',
        'ni': 'NICARAGUA',
        'ne': 'NIGER',
        'ng': 'NIGERIA',
        'nu': 'NIUE',
        'nf': 'NORFOLK ISLAND',
        'mp': 'NORTHERN MARIANA ISLANDS',
        'no': 'NORWAY',
        'om': 'OMAN',
        'pk': 'PAKISTAN',
        'pw': 'PALAU',
        'ps': 'PALESTINA',
        'pa': 'PANAMA',
        'pg': 'PAPUA NEW GUINEA',
        'py': 'PARAGUAY',
        'pe': 'PERU',
        'ph': 'PHILIPPINES',
        'pn': 'PITCAIRN',
        'pl': 'POLAND',
        'pt': 'PORTUGAL',
        'pr': 'PUERTO RICO',
        'qa': 'QATAR',
        're': 'REUNION',
        'ro': 'ROMANIA',
        'ru': 'RUSSIAN FEDERATION',
        'rw': 'RWANDA',
        'sh': 'SAINT HELENA',
        'kn': 'SAINT KITTS AND NEVIS',
        'lc': 'SAINT LUCIA',
        'pm': 'SAINT PIERRE AND MIQUELON',
        'vc': 'SAINT VINCENT AND THE GRENADINES',
        'ws': 'SAMOA',
        'bl': 'SAN BARTOLOMÉ',
        'sm': 'SAN MARINO',
        'st': 'SAO TOME AND PRINCIPE',
        'sa': 'SAUDI ARABIA',
        'sn': 'SENEGAL',
        'cs': 'SERBIA AND MONTENEGRO',
        'rs': 'SERBIA',
        'sc': 'SEYCHELLES',
        'sl': 'SIERRA LEONE',
        'sg': 'SINGAPORE',
        'sx': 'SINT MAARTEN',
        'sk': 'SLOVAKIA',
        'si': 'SLOVENIA',
        'sb': 'SOLOMON ISLANDS',
        'so': 'SOMALIA',
        'za': 'SOUTH AFRICA',
        'gs': 'SOUTH GEORGIA ISLANDS',
        'es': 'SPAIN',
        'lk': 'SRI LANKA',
        'sd': 'SUDAN',
        'ss': 'SOUTH SUDAN',
        'sr': 'SURINAME',
        'sj': 'SVALBARD AND JAN MAYEN ISLANDS',
        'sz': 'SWAZILAND',
        'se': 'SWEDEN',
        'ch': 'SWITZERLAND',
        'sy': 'SYRIAN ARAB REPUBLIC',
        'tw': 'TAIWAN',
        'tj': 'TAJIKISTAN',
        'tz': 'TANZANIA',
        'th': 'THAILAND',
        'tl': 'TIMOR-LESTE',
        'tg': 'TOGO',
        'tk': 'TOKELAU',
        'to': 'TONGA',
        'tt': 'TRINIDAD AND TOBAGO',
        'tn': 'TUNISIA',
        'tr': 'TURKEY',
        'tm': 'TURKMENISTAN',
        'tc': 'TURKS AND CAICOS ISLANDS',
        'tv': 'TUVALU',
        'ug': 'UGANDA',
        'ua': 'UKRAINE',
        'ae': 'UNITED ARAB EMIRATES',
        'gb': 'UNITED KINGDOM',
        'us': 'UNITED STATES',
        'um': 'UNITED STATES MINOR OUTLYING ISLANDS',
        'uy': 'URUGUAY',
        'uz': 'UZBEKISTAN',
        'vu': 'VANUATU',
        'va': 'VATICAN CITY STATE',
        've': 'VENEZUELA',
        'vn': 'VIET NAM',
        'vg': 'VIRGIN ISLANDS (BRITISH)',
        'vi': 'VIRGIN ISLANDS (U.S.)',
        'wf': 'WALLIS AND FUTUNA ISLANDS',
        'eh': 'WESTERN SAHARA',
        'ye': 'YEMEN',
        'zm': 'ZAMBIA',
        'zw': 'ZIMBABWE '
    }
}
var countries = [
    {
        name: '',
        dialCode: '994',
        countryCode: 'az'
    },
    {
        name: '',
        dialCode: '1242',
        countryCode: 'bs'
    },
    {
        name: '',
        dialCode: '973',
        countryCode: 'bh'
    },
    {
        name: '',
        dialCode: '880',
        countryCode: 'bd'
    },
    {
        name: '',
        dialCode: '1246',
        countryCode: 'bb'
    },
    {
        name: '',
        dialCode: '375',
        countryCode: 'by'
    },
    {
        name: '',
        dialCode: '32',
        countryCode: 'be'
    },
    {
        name: '',
        dialCode: '501',
        countryCode: 'bz'
    },
    {
        name: '',
        dialCode: '229',
        countryCode: 'bj'
    },
    {
        name: '',
        dialCode: '1441',
        countryCode: 'bm'
    },
    {
        name: '',
        dialCode: '975',
        countryCode: 'bt'
    },
    {
        name: '',
        dialCode: '591',
        countryCode: 'bo'
    },
    {
        name: '',
        dialCode: '387',
        countryCode: 'ba'
    },
    {
        name: '',
        dialCode: '267',
        countryCode: 'bw'
    },
    {
        name: '',
        dialCode: '55',
        countryCode: 'br'
    },
    {
        name: '',
        dialCode: '246',
        countryCode: 'io'
    },
    {
        name: '',
        dialCode: '1284',
        countryCode: 'vg'
    },
    {
        name: '',
        dialCode: '673',
        countryCode: 'bn'
    },
    {
        name: '',
        dialCode: '359',
        countryCode: 'bg'
    },
    {
        name: '',
        dialCode: '226',
        countryCode: 'bf'
    },
    {
        name: '',
        dialCode: '257',
        countryCode: 'bi'
    },
    {
        name: '',
        dialCode: '855',
        countryCode: 'kh'
    },
    {
        name: '',
        dialCode: '237',
        countryCode: 'cm'
    },
    {
        name: '',
        dialCode: '1',
        countryCode: 'ca'
    },
    {
        name: '',
        dialCode: '238',
        countryCode: 'cv'
    },
    {
        name: '',
        dialCode: '599',
        countryCode: 'bq'
    },
    {
        name: '',
        dialCode: '1345',
        countryCode: 'ky'
    },
    {
        name: '',
        dialCode: '236',
        countryCode: 'cf'
    },
    {
        name: '',
        dialCode: '235',
        countryCode: 'td'
    },
    {
        name: '',
        dialCode: '56',
        countryCode: 'cl'
    },
    {
        name: '',
        dialCode: '86',
        countryCode: 'cn'
    },
    {
        name: '',
        dialCode: '61',
        countryCode: 'cx'
    },
    {
        name: '',
        dialCode: '57',
        countryCode: 'co'
    },
    {
        name: '',
        dialCode: '269',
        countryCode: 'km'
    },
    {
        name: '',
        dialCode: '243',
        countryCode: 'cd'
    },
    {
        name: '',
        dialCode: '242',
        countryCode: 'cg'
    },
    {
        name: '',
        dialCode: '682',
        countryCode: 'ck'
    },
    {
        name: '',
        dialCode: '506',
        countryCode: 'cr'
    },
    {
        name: '',
        dialCode: '225',
        countryCode: 'ci'
    },
    {
        name: '',
        dialCode: '385',
        countryCode: 'hr'
    },
    {
        name: '',
        dialCode: '53',
        countryCode: 'cu'
    },
    {
        name: '',
        dialCode: '599',
        countryCode: 'cw'
    },
    {
        name: '',
        dialCode: '357',
        countryCode: 'cy'
    },
    {
        name: '',
        dialCode: '420',
        countryCode: 'cz'
    },
    {
        name: '',
        dialCode: '45',
        countryCode: 'dk'
    },
    {
        name: '',
        dialCode: '253',
        countryCode: 'dj'
    },
    {
        name: '',
        dialCode: '1767',
        countryCode: 'dm'
    },
    {
        name: '',
        dialCode: '1',
        countryCode: 'do'
    },
    {
        name: '',
        dialCode: '593',
        countryCode: 'ec'
    },
    {
        name: '',
        dialCode: '20',
        countryCode: 'eg'
    },
    {
        name: '',
        dialCode: '503',
        countryCode: 'sv'
    },
    {
        name: '',
        dialCode: '240',
        countryCode: 'gq'
    },
    {
        name: '',
        dialCode: '291',
        countryCode: 'er'
    },
    {
        name: '',
        dialCode: '372',
        countryCode: 'ee'
    },
    {
        name: '',
        dialCode: '251',
        countryCode: 'et'
    },
    {
        name: '',
        dialCode: '500',
        countryCode: 'fk'
    },
    {
        name: '',
        dialCode: '298',
        countryCode: 'fo'
    },
    {
        name: '',
        dialCode: '679',
        countryCode: 'fj'
    },
    {
        name: '',
        dialCode: '358',
        countryCode: 'fi'
    },
    {
        name: '',
        dialCode: '33',
        countryCode: 'fr'
    },
    {
        name: '',
        dialCode: '594',
        countryCode: 'gf'
    },
    {
        name: '',
        dialCode: '689',
        countryCode: 'pf'
    },
    {
        name: '',
        dialCode: '241',
        countryCode: 'ga'
    },
    {
        name: '',
        dialCode: '220',
        countryCode: 'gm'
    },
    {
        name: '',
        dialCode: '995',
        countryCode: 'ge'
    },
    {
        name: '',
        dialCode: '49',
        countryCode: 'de'
    },
    {
        name: '',
        dialCode: '233',
        countryCode: 'gh'
    },
    {
        name: '',
        dialCode: '350',
        countryCode: 'gi'
    },
    {
        name: '',
        dialCode: '30',
        countryCode: 'gr'
    },
    {
        name: '',
        dialCode: '299',
        countryCode: 'gl'
    },
    {
        name: '',
        dialCode: '1473',
        countryCode: 'gd'
    },
    {
        name: '',
        dialCode: '590',
        countryCode: 'gp'
    },
    {
        name: '',
        dialCode: '1671',
        countryCode: 'gu'
    },
    {
        name: '',
        dialCode: '502',
        countryCode: 'gt'
    },
    {
        name: '',
        dialCode: '44',
        countryCode: 'gg'
    },
    {
        name: '',
        dialCode: '224',
        countryCode: 'gn'
    },
    {
        name: '',
        dialCode: '245',
        countryCode: 'gw'
    },
    {
        name: '',
        dialCode: '592',
        countryCode: 'gy'
    },
    {
        name: '',
        dialCode: '509',
        countryCode: 'ht'
    },
    {
        name: '',
        dialCode: '504',
        countryCode: 'hn'
    },
    {
        name: '',
        dialCode: '852',
        countryCode: 'hk'
    },
    {
        name: '',
        dialCode: '36',
        countryCode: 'hu'
    },
    {
        name: '',
        dialCode: '354',
        countryCode: 'is'
    },
    {
        name: '',
        dialCode: '91',
        countryCode: 'in'
    },
    {
        name: '',
        dialCode: '62',
        countryCode: 'id'
    },
    {
        name: '',
        dialCode: '98',
        countryCode: 'ir'
    },
    {
        name: '',
        dialCode: '964',
        countryCode: 'iq'
    },
    {
        name: '',
        dialCode: '353',
        countryCode: 'ie'
    },
    {
        name: '',
        dialCode: '44',
        countryCode: 'im'
    },
    {
        name: '',
        dialCode: '972',
        countryCode: 'il'
    },
    {
        name: '',
        dialCode: '39',
        countryCode: 'it'
    },
    {
        name: '',
        dialCode: '1876',
        countryCode: 'jm'
    },
    {
        name: '',
        dialCode: '81',
        countryCode: 'jp'
    },
    {
        name: '',
        dialCode: '44',
        countryCode: 'je'
    },
    {
        name: '',
        dialCode: '962',
        countryCode: 'jo'
    },
    {
        name: '',
        dialCode: '7',
        countryCode: 'kz'
    },
    {
        name: '',
        dialCode: '254',
        countryCode: 'ke'
    },
    {
        name: '',
        dialCode: '686',
        countryCode: 'ki'
    },
    {
        name: '',
        dialCode: '383',
        countryCode: 'xk'
    },
    {
        name: '',
        dialCode: '965',
        countryCode: 'kw'
    },
    {
        name: '',
        dialCode: '996',
        countryCode: 'kg'
    },
    {
        name: '',
        dialCode: '856',
        countryCode: 'la'
    },
    {
        name: '',
        dialCode: '371',
        countryCode: 'lv'
    },
    {
        name: '',
        dialCode: '961',
        countryCode: 'lb'
    },
    {
        name: '',
        dialCode: '266',
        countryCode: 'ls'
    },
    {
        name: '',
        dialCode: '231',
        countryCode: 'lr'
    },
    {
        name: '',
        dialCode: '218',
        countryCode: 'ly'
    },
    {
        name: '',
        dialCode: '423',
        countryCode: 'li'
    },
    {
        name: '',
        dialCode: '370',
        countryCode: 'lt'
    },
    {
        name: '',
        dialCode: '352',
        countryCode: 'lu'
    },
    {
        name: '',
        dialCode: '853',
        countryCode: 'mo'
    },
    {
        name: '',
        dialCode: '389',
        countryCode: 'mk'
    },
    {
        name: '',
        dialCode: '261',
        countryCode: 'mg'
    },
    {
        name: '',
        dialCode: '265',
        countryCode: 'mw'
    },
    {
        name: '',
        dialCode: '60',
        countryCode: 'my'
    },
    {
        name: '',
        dialCode: '960',
        countryCode: 'mv'
    },
    {
        name: '',
        dialCode: '223',
        countryCode: 'ml'
    },
    {
        name: '',
        dialCode: '356',
        countryCode: 'mt'
    },
    {
        name: '',
        dialCode: '692',
        countryCode: 'mh'
    },
    {
        name: '',
        dialCode: '596',
        countryCode: 'mq'
    },
    {
        name: '',
        dialCode: '222',
        countryCode: 'mr'
    },
    {
        name: '',
        dialCode: '230',
        countryCode: 'mu'
    },
    {
        name: '',
        dialCode: '262',
        countryCode: 'yt'
    },
    {
        name: '',
        dialCode: '52',
        countryCode: 'mx'
    },
    {
        name: '',
        dialCode: '691',
        countryCode: 'fm'
    },
    {
        name: '',
        dialCode: '373',
        countryCode: 'md'
    },
    {
        name: '',
        dialCode: '377',
        countryCode: 'mc'
    },
    {
        name: '',
        dialCode: '976',
        countryCode: 'mn'
    },
    {
        name: '',
        dialCode: '382',
        countryCode: 'me'
    },
    {
        name: '',
        dialCode: '1664',
        countryCode: 'ms'
    },
    {
        name: '',
        dialCode: '212',
        countryCode: 'ma'
    },
    {
        name: '',
        dialCode: '258',
        countryCode: 'mz'
    },
    {
        name: '',
        dialCode: '95',
        countryCode: 'mm'
    },
    {
        name: '',
        dialCode: '264',
        countryCode: 'na'
    },
    {
        name: '',
        dialCode: '674',
        countryCode: 'nr'
    },
    {
        name: '',
        dialCode: '977',
        countryCode: 'np'
    },
    {
        name: '',
        dialCode: '31',
        countryCode: 'nl'
    },
    {
        name: '',
        dialCode: '687',
        countryCode: 'nc'
    },
    {
        name: '',
        dialCode: '64',
        countryCode: 'nz'
    },
    {
        name: '',
        dialCode: '505',
        countryCode: 'ni'
    },
    {
        name: '',
        dialCode: '227',
        countryCode: 'ne'
    },
    {
        name: '',
        dialCode: '234',
        countryCode: 'ng'
    },
    {
        name: '',
        dialCode: '683',
        countryCode: 'nu'
    },
    {
        name: '',
        dialCode: '672',
        countryCode: 'nf'
    },
    {
        name: '',
        dialCode: '850',
        countryCode: 'kp'
    },
    {
        name: '',
        dialCode: '1670',
        countryCode: 'mp'
    },
    {
        name: '',
        dialCode: '47',
        countryCode: 'no'
    },
    {
        name: '',
        dialCode: '968',
        countryCode: 'om'
    },
    {
        name: '',
        dialCode: '92',
        countryCode: 'pk'
    },
    {
        name: '',
        dialCode: '680',
        countryCode: 'pw'
    },
    {
        name: '',
        dialCode: '970',
        countryCode: 'ps'
    },
    {
        name: '',
        dialCode: '507',
        countryCode: 'pa'
    },
    {
        name: '',
        dialCode: '675',
        countryCode: 'pg'
    },
    {
        name: '',
        dialCode: '595',
        countryCode: 'py'
    },
    {
        name: '',
        dialCode: '51',
        countryCode: 'pe'
    },
    {
        name: '',
        dialCode: '63',
        countryCode: 'ph'
    },
    {
        name: '',
        dialCode: '48',
        countryCode: 'pl'
    },
    {
        name: '',
        dialCode: '351',
        countryCode: 'pt'
    },
    {
        name: '',
        dialCode: '1',
        countryCode: 'pr'
    },
    {
        name: '',
        dialCode: '974',
        countryCode: 'qa'
    },
    {
        name: '',
        dialCode: '262',
        countryCode: 're'
    },
    {
        name: '',
        dialCode: '40',
        countryCode: 'ro'
    },
    {
        name: '',
        dialCode: '7',
        countryCode: 'ru'
    },
    {
        name: '',
        dialCode: '250',
        countryCode: 'rw'
    },
    {
        name: '',
        dialCode: '590',
        countryCode: 'bl'
    },
    {
        name: '',
        dialCode: '290',
        countryCode: 'sh'
    },
    {
        name: '',
        dialCode: '1869',
        countryCode: 'kn'
    },
    {
        name: '',
        dialCode: '1758',
        countryCode: 'lc'
    },
    {
        name: '',
        dialCode: '508',
        countryCode: 'pm'
    },
    {
        name: '',
        dialCode: '1784',
        countryCode: 'vc'
    },
    {
        name: '',
        dialCode: '685',
        countryCode: 'ws'
    },
    {
        name: '',
        dialCode: '378',
        countryCode: 'sm'
    },
    {
        name: '',
        dialCode: '239',
        countryCode: 'st'
    },
    {
        name: '',
        dialCode: '966',
        countryCode: 'sa'
    },
    {
        name: '',
        dialCode: '221',
        countryCode: 'sn'
    },
    {
        name: '',
        dialCode: '381',
        countryCode: 'rs'
    },
    {
        name: '',
        dialCode: '248',
        countryCode: 'sc'
    },
    {
        name: '',
        dialCode: '232',
        countryCode: 'sl'
    },
    {
        name: '',
        dialCode: '65',
        countryCode: 'sg'
    },
    {
        name: '',
        dialCode: '1721',
        countryCode: 'sx'
    },
    {
        name: '',
        dialCode: '421',
        countryCode: 'sk'
    },
    {
        name: '',
        dialCode: '386',
        countryCode: 'si'
    },
    {
        name: '',
        dialCode: '677',
        countryCode: 'sb'
    },
    {
        name: '',
        dialCode: '252',
        countryCode: 'so'
    },
    {
        name: '',
        dialCode: '27',
        countryCode: 'za'
    },
    {
        name: '',
        dialCode: '82',
        countryCode: 'kr'
    },
    {
        name: '',
        dialCode: '211',
        countryCode: 'ss'
    },
    {
        name: '',
        dialCode: '34',
        countryCode: 'es'
    },
    {
        name: '',
        dialCode: '94',
        countryCode: 'lk'
    },
    {
        name: '',
        dialCode: '249',
        countryCode: 'sd'
    },
    {
        name: '',
        dialCode: '597',
        countryCode: 'sr'
    },
    {
        name: '',
        dialCode: '47',
        countryCode: 'sj'
    },
    {
        name: '',
        dialCode: '268',
        countryCode: 'sz'
    },
    {
        name: '',
        dialCode: '46',
        countryCode: 'se'
    },
    {
        name: '',
        dialCode: '41',
        countryCode: 'ch'
    },
    {
        name: '',
        dialCode: '963',
        countryCode: 'sy'
    },
    {
        name: '',
        dialCode: '886',
        countryCode: 'tw'
    },
    {
        name: '',
        dialCode: '992',
        countryCode: 'tj'
    },
    {
        name: '',
        dialCode: '255',
        countryCode: 'tz'
    },
    {
        name: '',
        dialCode: '66',
        countryCode: 'th'
    },
    {
        name: '',
        dialCode: '670',
        countryCode: 'tl'
    },
    {
        name: '',
        dialCode: '228',
        countryCode: 'tg'
    },
    {
        name: '',
        dialCode: '690',
        countryCode: 'tk'
    },
    {
        name: '',
        dialCode: '676',
        countryCode: 'to'
    },
    {
        name: '',
        dialCode: '1868',
        countryCode: 'tt'
    },
    {
        name: '',
        dialCode: '216',
        countryCode: 'tn'
    },
    {
        name: '',
        dialCode: '90',
        countryCode: 'tr'
    },
    {
        name: '',
        dialCode: '993',
        countryCode: 'tm'
    },
    {
        name: '',
        dialCode: '1649',
        countryCode: 'tc'
    },
    {
        name: '',
        dialCode: '688',
        countryCode: 'tv'
    },
    {
        name: '',
        dialCode: '1340',
        countryCode: 'vi'
    },
    {
        name: '',
        dialCode: '256',
        countryCode: 'ug'
    },
    {
        name: '',
        dialCode: '380',
        countryCode: 'ua'
    },
    {
        name: '',
        dialCode: '971',
        countryCode: 'ae'
    },
    {
        name: '',
        dialCode: '44',
        countryCode: 'gb'
    },
    {
        name: '',
        dialCode: '1',
        countryCode: 'us'
    },
    {
        name: '',
        dialCode: '598',
        countryCode: 'uy'
    },
    {
        name: '',
        dialCode: '998',
        countryCode: 'uz'
    },
    {
        name: '',
        dialCode: '678',
        countryCode: 'vu'
    },
    {
        name: '',
        dialCode: '39',
        countryCode: 'va'
    },
    {
        name: '',
        dialCode: '58',
        countryCode: 've'
    },
    {
        name: '',
        dialCode: '84',
        countryCode: 'vn'
    },
    {
        name: '',
        dialCode: '681',
        countryCode: 'wf'
    },
    {
        name: '',
        dialCode: '212',
        countryCode: 'eh'
    },
    {
        name: '',
        dialCode: '967',
        countryCode: 'ye'
    },
    {
        name: '',
        dialCode: '260',
        countryCode: 'zm'
    },
    {
        name: '',
        dialCode: '263',
        countryCode: 'zw'
    },
    {
        name: '',
        dialCode: '358',
        countryCode: 'ax'
    }
];
const BFHTimezonesList = {
    AF: { "Asia/Kabul": "Kabul" },
    AL: { "Europe/Tirane": "Tirane" },
    DZ: { "Africa/Algiers": "Algiers" },
    AS: { "Pacific/Pago_Pago": "Pago Pago" },
    AD: { "Europe/Andorra": "Andorra" },
    AO: { "Africa/Luanda": "Luanda" },
    AI: { "America/Anguilla": "Anguilla" },
    AQ: { "Antarctica/Casey": "Casey", "Antarctica/Davis": "Davis", "Antarctica/DumontDUrville": "DumontDUrville", "Antarctica/Macquarie": "Macquarie", "Antarctica/Mawson": "Mawson", "Antarctica/McMurdo": "McMurdo", "Antarctica/Palmer": "Palmer", "Antarctica/Rothera": "Rothera", "Antarctica/South_Pole": "South Pole", "Antarctica/Syowa": "Syowa", "Antarctica/Vostok": "Vostok" },
    AG: { "America/Antigua": "Antigua" },
    AR: { "America/Argentina/Buenos_Aires": "Argentina / Buenos Aires", "America/Argentina/Catamarca": "Argentina / Catamarca", "America/Argentina/Cordoba": "Argentina / Cordoba", "America/Argentina/Jujuy": "Argentina / Jujuy", "America/Argentina/La_Rioja": "Argentina / La Rioja", "America/Argentina/Mendoza": "Argentina / Mendoza", "America/Argentina/Rio_Gallegos": "Argentina / Rio Gallegos", "America/Argentina/Salta": "Argentina / Salta", "America/Argentina/San_Juan": "Argentina / San Juan", "America/Argentina/San_Luis": "Argentina / San Luis", "America/Argentina/Tucuman": "Argentina / Tucuman", "America/Argentina/Ushuaia": "Argentina / Ushuaia" },
    AM: { "Asia/Yerevan": "Yerevan" },
    AW: { "America/Aruba": "Aruba" },
    AU: { "Australia/Adelaide": "Adelaide", "Australia/Brisbane": "Brisbane", "Australia/Broken_Hill": "Broken Hill", "Australia/Currie": "Currie", "Australia/Darwin": "Darwin", "Australia/Eucla": "Eucla", "Australia/Hobart": "Hobart", "Australia/Lindeman": "Lindeman", "Australia/Lord_Howe": "Lord Howe", "Australia/Melbourne": "Melbourne", "Australia/Perth": "Perth", "Australia/Sydney": "Sydney" },
    AT: { "Europe/Vienna": "Vienna" },
    AZ: { "Asia/Baku": "Baku" },
    BH: { "Asia/Bahrain": "Bahrain" },
    BD: { "Asia/Dhaka": "Dhaka" },
    BB: { "America/Barbados": "Barbados" },
    BY: { "Europe/Minsk": "Minsk" },
    BE: { "Europe/Brussels": "Brussels" },
    BZ: { "America/Belize": "Belize" },
    BJ: { "Africa/Porto-Novo": "Porto-Novo" },
    BM: { "Atlantic/Bermuda": "Bermuda" },
    BT: { "Asia/Thimphu": "Thimphu" },
    BO: { "America/La_Paz": "La Paz" },
    BA: { "Europe/Sarajevo": "Sarajevo" },
    BW: { "Africa/Gaborone": "Gaborone" },
    BR: { "America/Araguaina": "Araguaina", "America/Bahia": "Bahia", "America/Belem": "Belem", "America/Boa_Vista": "Boa Vista", "America/Campo_Grande": "Campo Grande", "America/Cuiaba": "Cuiaba", "America/Eirunepe": "Eirunepe", "America/Fortaleza": "Fortaleza", "America/Maceio": "Maceio", "America/Manaus": "Manaus", "America/Noronha": "Noronha", "America/Porto_Velho": "Porto Velho", "America/Recife": "Recife", "America/Rio_Branco": "Rio Branco", "America/Santarem": "Santarem", "America/Sao_Paulo": "Sao Paulo" },
    VG: { "America/Tortola": "Tortola" },
    BN: { "Asia/Brunei": "Brunei" },
    BG: { "Europe/Sofia": "Sofia" },
    BF: { "Africa/Ouagadougou": "Ouagadougou" },
    BI: { "Africa/Bujumbura": "Bujumbura" },
    CI: { "Africa/Abidjan": "Abidjan" },
    KH: { "Asia/Phnom_Penh": "Phnom Penh" },
    CM: { "Africa/Douala": "Douala" },
    CA: { "America/Atikokan": "Atikokan", "America/Blanc-Sablon": "Blanc-Sablon", "America/Cambridge_Bay": "Cambridge Bay", "America/Creston": "Creston", "America/Dawson": "Dawson", "America/Dawson_Creek": "Dawson Creek", "America/Edmonton": "Edmonton", "America/Glace_Bay": "Glace Bay", "America/Goose_Bay": "Goose Bay", "America/Halifax": "Halifax", "America/Inuvik": "Inuvik", "America/Iqaluit": "Iqaluit", "America/Moncton": "Moncton", "America/Montreal": "Montreal", "America/Nipigon": "Nipigon", "America/Pangnirtung": "Pangnirtung", "America/Rainy_River": "Rainy River", "America/Rankin_Inlet": "Rankin Inlet", "America/Regina": "Regina", "America/Resolute": "Resolute", "America/St_Johns": "St Johns", "America/Swift_Current": "Swift Current", "America/Thunder_Bay": "Thunder Bay", "America/Toronto": "Toronto", "America/Vancouver": "Vancouver", "America/Whitehorse": "Whitehorse", "America/Winnipeg": "Winnipeg", "America/Yellowknife": "Yellowknife" },
    CV: { "Atlantic/Cape_Verde": "Cape Verde" },
    KY: { "America/Cayman": "Cayman" },
    CF: { "Africa/Bangui": "Bangui" },
    TD: { "Africa/Ndjamena": "Ndjamena" },
    CL: { "America/Santiago": "Santiago", "Pacific/Easter": "Easter" },
    CN: { "Asia/Chongqing": "Chongqing", "Asia/Harbin": "Harbin", "Asia/Kashgar": "Kashgar", "Asia/Shanghai": "Shanghai", "Asia/Urumqi": "Urumqi" },
    CO: { "America/Bogota": "Bogota" },
    KM: { "Indian/Comoro": "Comoro" },
    CG: { "Africa/Brazzaville": "Brazzaville" },
    CR: { "America/Costa_Rica": "Costa Rica" },
    HR: { "Europe/Zagreb": "Zagreb" },
    CU: { "America/Havana": "Havana" },
    CY: { "Asia/Nicosia": "Nicosia" },
    CZ: { "Europe/Prague": "Prague" },
    CD: { "Africa/Kinshasa": "Kinshasa", "Africa/Lubumbashi": "Lubumbashi" },
    DK: { "Europe/Copenhagen": "Copenhagen" },
    DJ: { "Africa/Djibouti": "Djibouti" },
    DM: { "America/Dominica": "Dominica" },
    DO: { "America/Santo_Domingo": "Santo Domingo" },
    TP: {},
    EC: { "America/Guayaquil": "Guayaquil", "Pacific/Galapagos": "Galapagos" },
    EG: { "Africa/Cairo": "Cairo" },
    SV: { "America/El_Salvador": "El Salvador" },
    GQ: { "Africa/Malabo": "Malabo" },
    ER: { "Africa/Asmara": "Asmara" },
    EE: { "Europe/Tallinn": "Tallinn" },
    ET: { "Africa/Addis_Ababa": "Addis Ababa" },
    FO: { "Atlantic/Faroe": "Faroe" },
    FK: { "Atlantic/Stanley": "Stanley" },
    FJ: { "Pacific/Fiji": "Fiji" },
    FI: { "Europe/Helsinki": "Helsinki" },
    MK: { "Europe/Skopje": "Skopje" },
    FR: { "Europe/Paris": "Paris" },
    GA: { "Africa/Libreville": "Libreville" },
    GE: { "Asia/Tbilisi": "Tbilisi" },
    DE: { "Europe/Berlin": "Berlin" },
    GH: { "Africa/Accra": "Accra" },
    GR: { "Europe/Athens": "Athens" },
    GL: { "America/Danmarkshavn": "Danmarkshavn", "America/Godthab": "Godthab", "America/Scoresbysund": "Scoresbysund", "America/Thule": "Thule" },
    GD: { "America/Grenada": "Grenada" },
    GU: { "Pacific/Guam": "Guam" },
    GT: { "America/Guatemala": "Guatemala" },
    GN: { "Africa/Conakry": "Conakry" },
    GW: { "Africa/Bissau": "Bissau" },
    GY: { "America/Guyana": "Guyana" },
    HT: { "America/Port-au-Prince": "Port-au-Prince" },
    HN: { "America/Tegucigalpa": "Tegucigalpa" },
    HK: { "Asia/Hong_Kong": "Hong Kong" },
    HU: { "Europe/Budapest": "Budapest" },
    IS: { "Atlantic/Reykjavik": "Reykjavik" },
    IN: { "Asia/Kolkata": "Kolkata" },
    ID: { "Asia/Jakarta": "Jakarta", "Asia/Jayapura": "Jayapura", "Asia/Makassar": "Makassar", "Asia/Pontianak": "Pontianak" },
    IR: { "Asia/Tehran": "Tehran" },
    IQ: { "Asia/Baghdad": "Baghdad" },
    IE: { "Europe/Dublin": "Dublin" },
    IL: { "Asia/Jerusalem": "Jerusalem" },
    IT: { "Europe/Rome": "Rome" },
    JM: { "America/Jamaica": "Jamaica" },
    JP: { "Asia/Tokyo": "Tokyo" },
    JO: { "Asia/Amman": "Amman" },
    KZ: { "Asia/Almaty": "Almaty", "Asia/Aqtau": "Aqtau", "Asia/Aqtobe": "Aqtobe", "Asia/Oral": "Oral", "Asia/Qyzylorda": "Qyzylorda" },
    KE: { "Africa/Nairobi": "Nairobi" },
    KI: { "Pacific/Enderbury": "Enderbury", "Pacific/Kiritimati": "Kiritimati", "Pacific/Tarawa": "Tarawa" },
    KW: { "Asia/Kuwait": "Kuwait" },
    KG: { "Asia/Bishkek": "Bishkek" },
    LA: { "Asia/Vientiane": "Vientiane" },
    LV: { "Europe/Riga": "Riga" },
    LB: { "Asia/Beirut": "Beirut" },
    LS: { "Africa/Maseru": "Maseru" },
    LR: { "Africa/Monrovia": "Monrovia" },
    LY: { "Africa/Tripoli": "Tripoli" },
    LI: { "Europe/Vaduz": "Vaduz" },
    LT: { "Europe/Vilnius": "Vilnius" },
    LU: { "Europe/Luxembourg": "Luxembourg" },
    MO: { "Asia/Macau": "Macau" },
    MG: { "Indian/Antananarivo": "Antananarivo" },
    MW: { "Africa/Blantyre": "Blantyre" },
    MY: { "Asia/Kuala_Lumpur": "Kuala Lumpur", "Asia/Kuching": "Kuching" },
    MV: { "Indian/Maldives": "Maldives" },
    ML: { "Africa/Bamako": "Bamako" },
    MT: { "Europe/Malta": "Malta" },
    MH: { "Pacific/Kwajalein": "Kwajalein", "Pacific/Majuro": "Majuro" },
    MR: { "Africa/Nouakchott": "Nouakchott" },
    MU: { "Indian/Mauritius": "Mauritius" },
    MX: { "America/Bahia_Banderas": "Bahia Banderas", "America/Cancun": "Cancun", "America/Chihuahua": "Chihuahua", "America/Hermosillo": "Hermosillo", "America/Matamoros": "Matamoros", "America/Mazatlan": "Mazatlan", "America/Merida": "Merida", "America/Mexico_City": "Mexico City", "America/Monterrey": "Monterrey", "America/Ojinaga": "Ojinaga", "America/Santa_Isabel": "Santa Isabel", "America/Tijuana": "Tijuana" },
    FM: { "Pacific/Chuuk": "Chuuk", "Pacific/Kosrae": "Kosrae", "Pacific/Pohnpei": "Pohnpei" },
    MD: { "Europe/Chisinau": "Chisinau" },
    MC: { "Europe/Monaco": "Monaco" },
    MN: { "Asia/Choibalsan": "Choibalsan", "Asia/Hovd": "Hovd", "Asia/Ulaanbaatar": "Ulaanbaatar" },
    ME: { "Europe/Podgorica": "Podgorica" },
    MS: { "America/Montserrat": "Montserrat" },
    MA: { "Africa/Casablanca": "Casablanca" },
    MZ: { "Africa/Maputo": "Maputo" },
    MM: { "Asia/Rangoon": "Rangoon" },
    NA: { "Africa/Windhoek": "Windhoek" },
    NR: { "Pacific/Nauru": "Nauru" },
    NP: { "Asia/Kathmandu": "Kathmandu" },
    NL: { "Europe/Amsterdam": "Amsterdam" },
    AN: {},
    NZ: { "Pacific/Auckland": "Auckland", "Pacific/Chatham": "Chatham" },
    NI: { "America/Managua": "Managua" },
    NE: { "Africa/Niamey": "Niamey" },
    NG: { "Africa/Lagos": "Lagos" },
    NF: { "Pacific/Norfolk": "Norfolk" },
    KP: { "Asia/Pyongyang": "Pyongyang" },
    MP: { "Pacific/Saipan": "Saipan" },
    NO: { "Europe/Oslo": "Oslo" },
    OM: { "Asia/Muscat": "Muscat" },
    PK: { "Asia/Karachi": "Karachi" },
    PW: { "Pacific/Palau": "Palau" },
    PA: { "America/Panama": "Panama" },
    PG: { "Pacific/Port_Moresby": "Port Moresby" },
    PY: { "America/Asuncion": "Asuncion" },
    PE: { "America/Lima": "Lima" },
    PH: { "Asia/Manila": "Manila" },
    PN: { "Pacific/Pitcairn": "Pitcairn" },
    PL: { "Europe/Warsaw": "Warsaw" },
    PT: { "Atlantic/Azores": "Azores", "Atlantic/Madeira": "Madeira", "Europe/Lisbon": "Lisbon" },
    PR: { "America/Puerto_Rico": "Puerto Rico" },
    QA: { "Asia/Qatar": "Qatar" },
    RO: { "Europe/Bucharest": "Bucharest" },
    RU: { "Asia/Anadyr": "Anadyr", "Asia/Irkutsk": "Irkutsk", "Asia/Kamchatka": "Kamchatka", "Asia/Krasnoyarsk": "Krasnoyarsk", "Asia/Magadan": "Magadan", "Asia/Novokuznetsk": "Novokuznetsk", "Asia/Novosibirsk": "Novosibirsk", "Asia/Omsk": "Omsk", "Asia/Sakhalin": "Sakhalin", "Asia/Vladivostok": "Vladivostok", "Asia/Yakutsk": "Yakutsk", "Asia/Yekaterinburg": "Yekaterinburg", "Europe/Kaliningrad": "Kaliningrad", "Europe/Moscow": "Moscow", "Europe/Samara": "Samara", "Europe/Volgograd": "Volgograd" },
    RW: { "Africa/Kigali": "Kigali" },
    ST: { "Africa/Sao_Tome": "Sao Tome" },
    SH: { "Atlantic/St_Helena": "St Helena" },
    KN: { "America/St_Kitts": "St Kitts" },
    LC: { "America/St_Lucia": "St Lucia" },
    VC: { "America/St_Vincent": "St Vincent" },
    WS: { "Pacific/Apia": "Apia" },
    SM: { "Europe/San_Marino": "San Marino" },
    SA: { "Asia/Riyadh": "Riyadh" },
    SN: { "Africa/Dakar": "Dakar" },
    RS: { "Europe/Belgrade": "Belgrade" },
    SC: { "Indian/Mahe": "Mahe" },
    SL: { "Africa/Freetown": "Freetown" },
    SG: { "Asia/Singapore": "Singapore" },
    SK: { "Europe/Bratislava": "Bratislava" },
    SI: { "Europe/Ljubljana": "Ljubljana" },
    SB: { "Pacific/Guadalcanal": "Guadalcanal" },
    SO: { "Africa/Mogadishu": "Mogadishu" },
    ZA: { "Africa/Johannesburg": "Johannesburg" },
    GS: { "Atlantic/South_Georgia": "South Georgia" },
    KR: { "Asia/Seoul": "Seoul" },
    ES: { "Africa/Ceuta": "Ceuta", "Atlantic/Canary": "Canary", "Europe/Madrid": "Madrid" },
    LK: { "Asia/Colombo": "Colombo" },
    SD: { "Africa/Khartoum": "Khartoum" },
    SR: { "America/Paramaribo": "Paramaribo" },
    SZ: { "Africa/Mbabane": "Mbabane" },
    SE: { "Europe/Stockholm": "Stockholm" },
    CH: { "Europe/Zurich": "Zurich" },
    SY: { "Asia/Damascus": "Damascus" },
    TW: { "Asia/Taipei": "Taipei" },
    TJ: { "Asia/Dushanbe": "Dushanbe" },
    TZ: { "Africa/Dar_es_Salaam": "Dar es Salaam" },
    TH: { "Asia/Bangkok": "Bangkok" },
    BS: { "America/Nassau": "Nassau" },
    GM: { "Africa/Banjul": "Banjul" },
    TG: { "Africa/Lome": "Lome" },
    TO: { "Pacific/Tongatapu": "Tongatapu" },
    TT: { "America/Port_of_Spain": "Port of Spain" },
    TN: { "Africa/Tunis": "Tunis" },
    TR: { "Europe/Istanbul": "Istanbul" },
    TM: { "Asia/Ashgabat": "Ashgabat" },
    TC: { "America/Grand_Turk": "Grand Turk" },
    TV: { "Pacific/Funafuti": "Funafuti" },
    VI: { "America/St_Thomas": "St Thomas" },
    UG: { "Africa/Kampala": "Kampala" },
    UA: { "Europe/Kiev": "Kiev", "Europe/Simferopol": "Simferopol", "Europe/Uzhgorod": "Uzhgorod", "Europe/Zaporozhye": "Zaporozhye" },
    AE: { "Asia/Dubai": "Dubai" },
    GB: { "Europe/London": "London" },
    US: { "America/Adak": "Adak", "America/Anchorage": "Anchorage", "America/Boise": "Boise", "America/Chicago": "Chicago", "America/Denver": "Denver", "America/Detroit": "Detroit", "America/Indiana/Indianapolis": "Indiana / Indianapolis", "America/Indiana/Knox": "Indiana / Knox", "America/Indiana/Marengo": "Indiana / Marengo", "America/Indiana/Petersburg": "Indiana / Petersburg", "America/Indiana/Tell_City": "Indiana / Tell City", "America/Indiana/Vevay": "Indiana / Vevay", "America/Indiana/Vincennes": "Indiana / Vincennes", "America/Indiana/Winamac": "Indiana / Winamac", "America/Juneau": "Juneau", "America/Kentucky/Louisville": "Kentucky / Louisville", "America/Kentucky/Monticello": "Kentucky / Monticello", "America/Los_Angeles": "Los Angeles", "America/Menominee": "Menominee", "America/Metlakatla": "Metlakatla", "America/New_York": "New York", "America/Nome": "Nome", "America/North_Dakota/Beulah": "North Dakota / Beulah", "America/North_Dakota/Center": "North Dakota / Center", "America/North_Dakota/New_Salem": "North Dakota / New Salem", "America/Phoenix": "Phoenix", "America/Shiprock": "Shiprock", "America/Sitka": "Sitka", "America/Yakutat": "Yakutat", "Pacific/Honolulu": "Honolulu" },
    UY: { "America/Montevideo": "Montevideo" },
    UZ: { "Asia/Samarkand": "Samarkand", "Asia/Tashkent": "Tashkent" },
    VU: { "Pacific/Efate": "Efate" },
    VA: { "Europe/Vatican": "Vatican" },
    VE: { "America/Caracas": "Caracas" },
    VN: { "Asia/Ho_Chi_Minh": "Ho Chi Minh" },
    EH: { "Africa/El_Aaiun": "El Aaiun" },
    YE: { "Asia/Aden": "Aden" },
    ZM: { "Africa/Lusaka": "Lusaka" },
    ZW: { "Africa/Harare": "Harare" }
};

export const Currency = [
    { "country": "Indian Rupees - INR" },
    { "country": "Euro - EUR" }
];

// export const Currency=[

//     {"country":"Indian Rupees - INR"},
//     {"country":"Euro - EUR"},

//     {"country":"America (United States) Dollars - USD"},
// {"country":"Afghanistan Afghanis - AFN"},
// {"country":"Albania Leke - ALL"},
// {"country":"Algeria Dinars - DZD"},
// {"country":"Argentina Pesos - ARS"},
// {"country":"Australia Dollars - AUD"},
// {"country":"Austria Schillings - ATS"},
// {"country":"Bahamas Dollars - BSD"},
// {"country":"Bahrain Dinars - BHD"},
// {"country":"Bangladesh Taka - BDT"},
// {"country":"Barbados Dollars - BBD"},
// {"country":"Belgium Francs - BEF"},
// {"country":"Bermuda Dollars - BMD"},
// {"country":"Brazil Reais - BRL"},
// {"country":"Bulgaria Leva - BGN"},
// {"country":"Canada Dollars - CAD"},
// {"country":"CFA BCEAO Francs - XOF"},
// {"country":"CFA BEAC Francs - XAF"},
// {"country":"Chile Pesos - CLP"},
// {"country":"China Yuan Renminbi - CNY"},
// {"country":"RMB (China Yuan Renminbi) - CNY"},
// {"country":"Colombia Pesos - COP"},
// {"country":"CFP Francs - XPF"},
// {"country":"Costa Rica Colones - CRC"},
// {"country":"Croatia Kuna - HRK"},
// {"country":"Cyprus Pounds - CYP"},
// {"country":"Czech Republic Koruny - CZK"},
// {"country":"Denmark Kroner - DKK"},
// {"country":"Deutsche (Germany) Marks - DEM"},
// {"country":"Dominican Republic Pesos - DOP"},
// {"country":"Dutch (Netherlands) Guilders - NLG"},
// {"country":"Eastern Caribbean Dollars - XCD"},
// {"country":"Egypt Pounds - EGP"},
// {"country":"Estonia Krooni - EEK"},

// {"country":"Fiji Dollars - FJD"},
// {"country":"Finland Markkaa - FIM"},
// {"country":"France Francs - FRF*"},
// {"country":"Germany Deutsche Marks - DEM"},
// {"country":"Gold Ounces - XAU"},
// {"country":"Greece Drachmae - GRD"},
// {"country":"Guatemalan Quetzal - GTQ"},
// {"country":"Holland (Netherlands) Guilders - NLG"},
// {"country":"Hong Kong Dollars - HKD"}, 
// {"country":"Hungary Forint - HUF"},
// {"country":"Iceland Kronur - ISK"},
// {"country":"IMF Special Drawing Right - XDR"},

// {"country":"Indonesia Rupiahs - IDR"},
// {"country":"Iran Rials - IRR"}, 
// {"country":"Iraq Dinars - IQD"},
// {"country":"Ireland Pounds - IEP*"},
// {"country":"Israel New Shekels - ILS"},
// {"country":"Italy Lire - ITL*"},
// {"country":"Jamaica Dollars - JMD"},
// {"country":"Japan Yen - JPY"},
// {"country":"Jordan Dinars - JOD"},
// {"country":"Kenya Shillings - KES"},
// {"country":"Korea (South) Won - KRW"},
// {"country":"Kuwait Dinars - KWD"},
// {"country":"Lebanon Pounds - LBP"},
// {"country":"Luxembourg Francs - LUF"}, 
// {"country":"Malaysia Ringgits - MYR"},
// {"country":"Malta Liri - MTL"},
// {"country":"Mauritius Rupees - MUR"},
// {"country":"Mexico Pesos - MXN"},
// {"country":"Morocco Dirhams - MAD"},
// {"country":"Netherlands Guilders - NLG"}, 
// {"country":"New Zealand Dollars - NZD"},
// {"country":"Norway Kroner - NOK"},
// {"country":"Oman Rials - OMR"},
// {"country":"Pakistan Rupees - PKR"},
// {"country":"Palladium Ounces - XPD"},
// {"country":"Peru Nuevos Soles - PEN"},
// {"country":"Philippines Pesos - PHP"},
// {"country":"Platinum Ounces - XPT"},
// {"country":"Poland Zlotych - PLN"},
// {"country":"Portugal Escudos - PTE"},
// {"country":"Qatar Riyals - QAR"},
// {"country":"Romania New Lei - RON"}, 
// {"country":"Romania Lei - ROL"},
// {"country":"Russia Rubles - RUB"},
// {"country":"Saudi Arabia Riyals - SAR"},
// {"country":"Silver Ounces - XAG"},
// {"country":"Singapore Dollars - SGD"},
// {"country":"Slovakia Koruny - SKK"}, 
// {"country":"Slovenia Tolars - SIT"},
// {"country":"South Africa Rand - ZAR"},
// {"country":"South Korea Won - KRW"},
// {"country":"Spain Pesetas - ESP"},
// {"country":"Special Drawing Rights (IMF) - XDR"},
// {"country":"Sri Lanka Rupees - LKR"}, 
// {"country":"Sudan Dinars - SDD"},
// {"country":"Sweden Kronor - SEK"},
// {"country":"Switzerland Francs - CHF"},
// {"country":"Taiwan New Dollars - TWD"},
// {"country":"Thailand Baht - THB"},
// {"country":"Trinidad and Tobago Dollars - TTD"}, 
// {"country":"Tunisia Dinars - TND"},
// {"country":"Turkey New Lira - TRY"},
// {"country":"United Arab Emirates Dirhams - AED"},
// {"country":"United Kingdom Pounds - GBP"},
// {"country":"United States Dollars - USD"},
// {"country":"Venezuela Bolivares - VEB"}, 
// {"country":"Vietnam Dong - VND"},
// {"country":"Zambia Kwacha - ZMK"}

// ]

export const language_details = [
    { "lan": "Afrikanns" },
    { "lan": "Albanian" },
    { "lan": "Arabic" },
    { "lan": "Armenian" },
    { "lan": "Basque" },
    { "lan": "Bengali" },
    { "lan": "Bulgarian" },
    { "lan": "Catalan" },
    { "lan": "Cambodian" },
    { "lan": "Chinese (Mandarin)" },
    { "lan": "Croation" },
    { "lan": "Czech" },
    { "lan": "Danish" },
    { "lan": "Dutch" },
    { "lan": "English" },
    { "lan": "Estonian" },
    { "lan": "Fiji" },
    { "lan": "Finnish" },
    { "lan": "French" },
    { "lan": "Georgian" },
    { "lan": "German" },
    { "lan": "Greek" },
    { "lan": "Gujarati" },
    { "lan": "Hebrew" },
    { "lan": "Hindi" },
    { "lan": "Hungarian" },
    { "lan": "Icelandic" },
    { "lan": "Indonesian" },
    { "lan": "Irish" },
    { "lan": "Italian" },
    { "lan": "Japanese" },
    { "lan": "Javanese" },
    { "lan": "Korean" },
    { "lan": "Latin" },
    { "lan": "Latvian" },
    { "lan": "Lithuanian" },
    { "lan": "Macedonian" },
    { "lan": "Malay" },
    { "lan": "Malayalam" },
    { "lan": "Maltese" },
    { "lan": "Maori" },
    { "lan": "Marathi" },
    { "lan": "Mongolian" },
    { "lan": "Nepali" },
    { "lan": "Norwegian" },
    { "lan": "Persian" },
    { "lan": "Polish" },
    { "lan": "Portuguese" },
    { "lan": "Punjabi" },
    { "lan": "Quechua" },
    { "lan": "Romanian" },
    { "lan": "Russian" },
    { "lan": "Samoan" },
    { "lan": "Serbian" },
    { "lan": "Slovak" },
    { "lan": "Slovenian" },
    { "lan": "Spanish" },
    { "lan": "Swahili" },
    { "lan": "Swedish " },
    { "lan": "Tamil" },
    { "lan": "Tatar" },
    { "lan": "Telugu" },
    { "lan": "Thai" },
    { "lan": "Tibetan" },
    { "lan": "Tonga" },
    { "lan": "Turkish" },
    { "lan": "Ukranian" },
    { "lan": "Urdu" },
    { "lan": "Uzbek" },
    { "lan": "Vietnamese" },
    { "lan": "Welsh" },
    { "lan": "Xhosa" }

]


export const timezone = {
    "Andorra": ["Europe/Andorra(GMT +02:00)"],
    "United Arab Emirates": ["Asia/Dubai(GMT +04:00)"],
    "Afghanistan": ["Asia/Kabul(GMT +04:30)"],
    "Antigua And Barbuda": ["America/Antigua(GMT -04:00)"],
    "Anguilla": ["America/Anguilla(GMT -04:00)"],
    "Albania": ["Europe/Tirane(GMT +02:00)"],
    "Armenia": ["Asia/Yerevan(GMT +04:00)"],
    "Angola": ["Africa/Luanda(GMT +01:00)"],
    "Antarctica":
    ["Antarctica/McMurdo(GMT +13:00)",
        "Antarctica/Rothera(GMT -03:00)",
        "Antarctica/Palmer(GMT -03:00)",
        "Antarctica/Macquarie(GMT +11:00)",
        "Antarctica/Mawson(GMT +05:00)",
        "Antarctica/Davis(GMT +07:00)",
        "Antarctica/Casey(GMT +08:00)",
        "Antarctica/Vostok(GMT +06:00)",
        "Antarctica/DumontDUrville(GMT +10:00)",
        "Antarctica/Syowa(GMT -03:00)",
        "Antarctica/Troll(GMT +02:00)"],
    "Argentina":
    ["America/Argentina/Buenos_Aires(GMT -03:00)",
        "America/Argentina/Cordoba(GMT -03:00)",
        "America/Argentina/Salta(GMT -03:00)",
        "America/Argentina/Jujuy(GMT -03:00)",
        "America/Argentina/Tucuman(GMT -03:00)",
        "America/Argentina/Catamarca(GMT -03:00)",
        "America/Argentina/La_Rioja(GMT -03:00)",
        "America/Argentina/San_Juan(GMT -03:00)",
        "America/Argentina/Mendoza(GMT -03:00)",
        "America/Argentina/San_Luis(GMT -03:00)",
        "America/Argentina/Rio_Gallegos(GMT -03:00)",
        "America/Argentina/Ushuaia(GMT -03:00)"],
    "American Samoa": ["Pacific/Pago_Pago(GMT -11:00)"],
    "Austria": ["Europe/Vienna(GMT +02:00)"],
    "Australia":
    ["Australia/Lord_Howe(GMT +11:00)",
        "Australia/Hobart(GMT +11:00)",
        "Australia/Currie(GMT +11:00)",
        "Australia/Melbourne(GMT +11:00)",
        "Australia/Sydney(GMT +11:00)",
        "Australia/Broken_Hill(GMT +10:30)",
        "Australia/Brisbane(GMT +10:00)",
        "Australia/Lindeman(GMT +10:00)",
        "Australia/Adelaide(GMT +10:30)",
        "Australia/Darwin(GMT +09:30)",
        "Australia/Perth(GMT +08:00)",
        "Australia/Eucla(GMT +08:45)"],
    "Aruba": ["America/Aruba(GMT -04:00)"],
    "Aland Islands": ["Europe/Mariehamn(GMT +03:00)"],
    "Azerbaijan": ["Asia/Baku(GMT +04:00)"],
    "Bosnia And Herzegovina": ["Europe/Sarajevo(GMT +02:00)"],
    "Barbados": ["America/Barbados(GMT -04:00)"],
    "Bangladesh": ["Asia/Dhaka(GMT +06:00)"],
    "Belgium": ["Europe/Brussels(GMT +02:00)"],
    "Burkina Faso": ["Africa/Ouagadougou(GMT +00:00)"],
    "Bulgaria": ["Europe/Sofia(GMT +03:00)"],
    "Bahrain": ["Asia/Bahrain(GMT +03:00)"],
    "Burundi": ["Africa/Bujumbura(GMT +02:00)"],
    "Benin": ["Africa/Porto-Novo(GMT +01:00)"],
    "Saint Barthelemy": ["America/St_Barthelemy(GMT -04:00)"],
    "Bermuda": ["Atlantic/Bermuda(GMT -03:00)"],
    "Brunei Darussalam": ["Asia/Brunei(GMT +08:00)"],
    "Bolivia": ["America/La_Paz(GMT -04:00)"],
    "Bonaire": ["America/Kralendijk(GMT -04:00)"],
    "Brazil":
    ["America/Noronha(GMT -02:00)",
        "America/Belem(GMT -03:00)",
        "America/Fortaleza(GMT -03:00)",
        "America/Fort_Nelson(GMT -07:00)",
        "America/Recife(GMT -03:00)",
        "America/Araguaina(GMT -03:00)",
        "America/Maceio(GMT -03:00)",
        "America/Bahia(GMT -03:00)",
        "America/Sao_Paulo(GMT -03:00)",
        "America/Campo_Grande(GMT -04:00)",
        "America/Cuiaba(GMT -04:00)",
        "America/Santarem(GMT -03:00)",
        "America/Porto_Velho(GMT -04:00)",
        "America/Boa_Vista(GMT -04:00)",
        "America/Manaus(GMT -04:00)",
        "America/Eirunepe(GMT -05:00)",
        "America/Rio_Branco(GMT -05:00)"],
    "Bahamas": ["America/Nassau(GMT -04:00)"],
    "Bhutan": ["Asia/Thimphu(GMT +06:00)"],
    "Botswana": ["Africa/Gaborone(GMT +02:00)"],
    "Belarus": ["Europe/Minsk(GMT +03:00)"],
    "Belize": ["America/Belize(GMT -06:00)"],
    "Canada":
    ["America/St_Johns(GMT -02:30)",
        "America/Halifax(GMT -03:00)",
        "America/Glace_Bay(GMT -03:00)",
        "America/Moncton(GMT -03:00)",
        "America/Goose_Bay(GMT -03:00)",
        "America/Blanc-Sablon(GMT -04:00)",
        "America/Toronto(GMT -04:00)",
        "America/Nipigon(GMT -04:00)",
        "America/Thunder_Bay(GMT -04:00)",
        "America/Iqaluit(GMT -04:00)",
        "America/Pangnirtung(GMT -04:00)",
        "America/Resolute(GMT -05:00)",
        "America/Atikokan(GMT -05:00)",
        "America/Rankin_Inlet(GMT -05:00)",
        "America/Winnipeg(GMT -05:00)",
        "America/Rainy_River(GMT -05:00)",
        "America/Regina(GMT -06:00)",
        "America/Swift_Current(GMT -06:00)",
        "America/Edmonton(GMT -06:00)",
        "America/Cambridge_Bay(GMT -06:00)",
        "America/Yellowknife(GMT -06:00)",
        "America/Inuvik(GMT -06:00)",
        "America/Creston(GMT -07:00)",
        "America/Dawson_Creek(GMT -07:00)",
        "America/Vancouver(GMT -07:00)",
        "America/Whitehorse(GMT -07:00)",
        "America/Dawson(GMT -07:00)",
        "America/Montreal(GMT -04:00)"],
    "Cocos (Keeling) Islands": ["Indian/Cocos(GMT +06:30)"],
    "Congo (Democratic Republic)": ["Africa/Kinshasa(GMT +01:00)", "Africa/Lubumbashi(GMT +02:00)"],
    "Central African Republic": ["Africa/Bangui(GMT +01:00)"],
    "Congo": ["Africa/Brazzaville(GMT +01:00)"],
    "Switzerland": ["Europe/Zurich(GMT +02:00)"],
    "Cote D\"Ivoire": ["Africa/Abidjan(GMT +00:00)"],
    "Cook Islands": ["Pacific/Rarotonga(GMT -10:00)"],
    "Chile": ["America/Santiago(GMT -03:00)", "America/Punta Arenas(GMT -03:00)", "Pacific/Easter(GMT -05:00)"],
    "Cameroon": ["Africa/Douala(GMT +01:00)"],
    "China":
    ["Asia/Shanghai(GMT +08:00)",
        "Asia/Harbin(GMT +08:00)",
        "Asia/Chongqing(GMT +08:00)",
        "Asia/Urumqi(GMT +06:00)",],
    "Colombia": ["America/Bogota(GMT -05:00)"],
    "Costa Rica": ["America/Costa_Rica(GMT -06:00)"],
    "Cuba": ["America/Havana(GMT -04:00)"],
    "Cape Verde": ["Atlantic/Cape_Verde(GMT -01:00)"],
    "Curacao": ["America/Curacao(GMT -04:00)"],
    "Christmas Island": ["Indian/Christmas(GMT +07:00)"],
    "Cyprus": ["Asia/Nicosia(GMT +03:00)", "Asia/Famagusta(GMT +03:00)"],
    "Czech Republic": ["Europe/Prague(GMT +02:00)"],
    "Germany": ["Europe/Berlin(GMT +02:00)", "Europe/Busingen(GMT +02:00)"],
    "Djibouti": ["Africa/Djibouti(GMT +03:00)"],
    "Denmark": ["Europe/Copenhagen(GMT +02:00)"],
    "Dominica": ["America/Dominica(GMT -04:00)"],
    "Dominican Republic": ["America/Santo_Domingo(GMT -04:00)"],
    "Algeria": ["Africa/Algiers(GMT +01:00)"],
    "Ecuador": ["America/Guayaquil(GMT -05:00)", "Pacific/Galapagos(GMT -06:00)"],
    "Estonia": ["Europe/Tallinn(GMT +03:00)"],
    "Egypt": ["Africa/Cairo(GMT +02:00)"],
    "Western Sahara": ["Africa/El_Aaiun(GMT +01:00)"],
    "Eritrea": ["Africa/Asmara(GMT +03:00)"],
    "Spain": ["Europe/Madrid(GMT +02:00)", "Africa/Ceuta(GMT +02:00)", "Atlantic/Canary(GMT +01:00)"],
    "Ethiopia": ["Africa/Addis_Ababa(GMT +03:00)"],
    "Finland": ["Europe/Helsinki(GMT +03:00)"],
    "Fiji": ["Pacific/Fiji(GMT +12:00)"],
    "Falkland Islands (Malvinas)": ["Atlantic/Stanley(GMT -03:00)"],
    "Micronesia (Federated States Of)": ["Pacific/Chuuk(GMT +10:00)", "Pacific/Pohnpei(GMT +11:00)", "Pacific/Kosrae(GMT +11:00)"],
    "Faroe Islands": ["Atlantic/Faroe(GMT +01:00)"],
    "France": ["Europe/Paris(GMT +02:00)"],
    "Gabon": ["Africa/Libreville(GMT +01:00)"],
    "United Kingdom": ["Europe/London(GMT +01:00)"],
    "Grenada": ["America/Grenada(GMT -04:00)"],
    "Georgia": ["Asia/Tbilisi(GMT +04:00)"],
    "French Guiana": ["America/Cayenne(GMT -03:00)"],
    "Guernsey": ["Europe/Guernsey(GMT +01:00)"],
    "Ghana": ["Africa/Accra( GMT +00:00)"],
    "Gibraltar": ["Europe/Gibraltar(GMT +02:00)"],
    "Greenland":
    ["America/Godthab(GMT -02:00)",
        "America/Danmarkshavn(GMT +00:00)",
        "America/Scoresbysund(GMT +00:00)",
        "America/Thule(GMT -03:00)"],
    "Gambia": ["Africa/Banjul(GMT +00:00)"],
    "Guinea": ["Africa/Conakry(GMT +00:00)"],
    "Guadeloupe": ["America/Guadeloupe(GMT -04:00)"],
    "Equatorial Guinea": ["Africa/Malabo(GMT +01:00)"],
    "Greece": ["Europe/Athens(GMT +03:00)"],
    "South Georgia And Sandwich Isl.": ["Atlantic/South_Georgia(GMT -02:00)"],
    "Guatemala": ["America/Guatemala(GMT -06:00)"],
    "Guam": ["Pacific/Guam(GMT +10:00)"],
    "Guinea-Bissau": ["Africa/Bissau(GMT +00:00)"],
    "Guyana": ["America/Guyana(GMT -04:00)"],
    "Hong Kong": ["Asia/Hong_Kong(GMT +08:00)"],
    "Honduras": ["America/Tegucigalpa(GMT -06:00)"],
    "Croatia": ["Europe/Zagreb(GMT +02:00)"],
    "Haiti": ["America/Port-au-Prince(GMT -04:00)"],
    "Hungary": ["Europe/Budapest(GMT +02:00)"],
    "Indonesia":
    ["Asia/Jakarta(GMT +07:00)",
        "Asia/Pontianak(GMT +07:00)",
        "Asia/Makassar(GMT +08:00)",
        "Asia/Jayapura(GMT +09:00)"],
    "Ireland": ["Europe/Dublin(GMT +01:00)"],
    "Israel": ["Asia/Jerusalem(GMT +03:00)"],
    "Isle Of Man": ["Europe/Isle_of_Man(GMT +01:00)"],
    "India": ["Asia/Kolkata(GMT +05:30)"],
    "British Indian Ocean Territory": ["Indian/Chagos(GMT +06:00)"],
    "Iraq": ["Asia/Baghdad(GMT +03:00)"],
    "Iran (Islamic Republic Of)": ["Asia/Tehran(GMT +03:30)"],
    "Iceland": ["Atlantic/Reykjavik(GMT +00:00)"],
    "Italy": ["Europe/Rome(GMT +02:00)"],
    "Jersey": ["Europe/Jersey(GMT +01:00)"],
    "Jamaica": ["America/Jamaica(GMT -05:00)"],
    "Jordan": ["Asia/Amman(GMT +03:00)"],
    "Japan": ["Asia/Tokyo(GMT +09:00)"],
    "Kenya": ["Africa/Nairobi(GMT +03:00)"],
    "Kyrgyzstan": ["Asia/Bishkek(GMT +06:00)"],
    "Cambodia": ["Asia/Phnom_Penh(GMT +07:00)"],
    "Kiribati":
    ["Pacific/Tarawa(GMT +12:00)", "Pacific/Enderbury(GMT +13:00)", "Pacific/Kiritimati(GMT +14:00)"],
    "Comoros": ["Indian/Comoro(GMT +03:00)"],
    "Saint Kitts And Nevis": ["America/St_Kitts(GMT -04:00)"],
    "North Korea": ["Asia/Pyongyang(GMT +09:00)"],
    "Korea": ["Asia/Seoul(GMT +09:00)"],
    "Kuwait": ["Asia/Kuwait(GMT +03:00)"],
    "Cayman Islands": ["America/Cayman(GMT -05:00)"],
    "Kazakhstan":
    ["Asia/Almaty(GMT +06:00)",
        "Asia/Qyzylorda(GMT +06:00)",
        "Asia/Atyrau(GMT +05:00)",
        "Asia/Aqtobe(GMT +05:00)",
        "Asia/Aqtau(GMT +05:00)",
        "Asia/Oral(GMT +05:00)"],
    "Lao People\"s Democratic Republic": ["Asia/Vientiane(GMT +07:00)"],
    "Lebanon": ["Asia/Beirut(GMT +03:00)"],
    "Saint Lucia": ["America/St_Lucia(GMT -04:00)"],
    "Liechtenstein": ["Europe/Vaduz(GMT +02:00)"],
    "Sri Lanka": ["Asia/Colombo(GMT +05:30)"],
    "Liberia": ["Africa/Monrovia(GMT +00:00)"],
    "Lesotho": ["Africa/Maseru(GMT +02:00)"],
    "Lithuania": ["Europe/Vilnius(GMT +03:00)"],
    "Luxembourg": ["Europe/Luxembourg(GMT +02:00)"],
    "Latvia": ["Europe/Riga(GMT +03:00)"],
    "Libyan Arab Jamahiriya": ["Africa/Tripoli(GMT +02:00)"],
    "Morocco": ["Africa/Casablanca(GMT +01:00)"],
    "Monaco": ["Europe/Monaco(GMT +02:00)"],
    "Moldova": ["Europe/Chisinau(GMT +03:00)"],
    "Montenegro": ["Europe/Podgorica(GMT +02:00)"],
    "Saint Martin": ["America/Marigot(GMT -04:00)"],
    "Madagascar": ["Indian/Antananarivo(GMT +03:00)"],
    "Marshall Islands": ["Pacific/Majuro(GMT +12:00)", "Pacific/Kwajalein(GMT +12:00)"],
    "Macedonia": ["Europe/Skopje(GMT +02:00)"],
    "Mali": ["Africa/Bamako(GMT +00:00)"],
    "Myanmar": ["Asia/Rangoon(GMT +06:30)"],
    "Mongolia": ["Asia/Ulaanbaatar(GMT +08:00)", "Asia/Hovd(GMT +07:00)", "Asia/Choibalsan(GMT +08:00)"],
    "Macao": ["Asia/Macau(GMT +08:00)"],
    "Northern Mariana Islands": ["Pacific/Saipan(GMT +10:00)"],
    "Martinique": ["America/Martinique(GMT -04:00)"],
    "Mauritania": ["Africa/Nouakchott(GMT +00:00)"],
    "Montserrat": ["America/Montserrat(GMT -04:00)"],
    "Malta": ["Europe/Malta(GMT +02:00)"],
    "Mauritius": ["Indian/Mauritius(GMT +04:00)"],
    "Maldives": ["Indian/Maldives(GMT +05:00)"],
    "Malawi": ["Africa/Blantyre(GMT +02:00)"],
    "Mexico":
    ["America/Mexico_City(GMT -05:00)",
        "America/Cancun(GMT -05:00)",
        "America/Merida(GMT -05:00)",
        "America/Monterrey(GMT -05:00)",
        "America/Matamoros(GMT -05:00)",
        "America/Mazatlan(GMT -06:00)",
        "America/Chihuahua(GMT -06:00)",
        "America/Ojinaga(GMT -06:00)",
        "America/Hermosillo(GMT -07:00)",
        "America/Tijuana(GMT -07:00)",
        "America/Santa_Isabel(GMT -07:00)",
        "America/Bahia_Banderas(GMT -05:00)"],
    "Malaysia": ["Asia/Kuala_Lumpur(GMT +08:00)", "Asia/Kuching(GMT +08:00)"],
    "Mozambique": ["Africa/Maputo(GMT +02:00)"],
    "Namibia": ["Africa/Windhoek(GMT +02:00)"],
    "New Caledonia": ["Pacific/Noumea(GMT +11:00)"],
    "Niger": ["Africa/Niamey(GMT +01:00)"],
    "Norfolk Island": ["Pacific/Norfolk(GMT +11:00)"],
    "Nigeria": ["Africa/Lagos(GMT +01:00)"],
    "Nicaragua": ["America/Managua(GMT -06:00)"],
    "Netherlands": ["Europe/Asterdam(GMT +02:00)"],
    "Norway": ["Europe/Oslo(GMT +02:00)"],
    "Nepal": ["Asia/Kathmandu(GMT +05:45)"],
    "Nauru": ["Pacific/Nauru(GMT +12:00)"],
    "Niue": ["Pacific/Niue(GMT -11:00)"],
    "New Zealand": ["Pacific/Auckland(GMT +13:00)", "Pacific/Chatham(GMT +13:45)"],
    "Oman": ["Asia/Muscat(GMT +04:00)"],
    "Panama": ["America/Panama(GMT -05:00)"],
    "Peru": ["America/Lima(GMT -05:00)"],
    "French Polynesia": ["Pacific/Tahiti(GMT -10:00)", "Pacific/Marquesas(GMT -09:30)", "Pacific/Gambier(GMT -09:00)"],
    "Papua New Guinea": ["Pacific/Port_Moresby(GMT +10:00)", "Pacific/Bougainville(GMT +11:00)"],
    "Philippines": ["Asia/Manila(GMT +08:00)"],
    "Pakistan": ["Asia/Karachi(GMT +05:00)"],
    "Poland": ["Europe/Warsaw(GMT +02:00)"],
    "Saint Pierre And Miquelon": ["America/Miquelon(GMT -02:00)"],
    "Pitcairn": ["Pacific/Pitcairn(GMT -08:00)"],
    "Puerto Rico": ["America/Puerto_Rico(GMT -04:00)"],
    "Palestinian Territory (Occupied)": ["Asia/Gaza(GMT +03:00)", "Asia/Hebron(GMT +03:00)"],
    "Portugal": ["Europe/Lisbon(GMT +01:00)", "Atlantic/Madeira(GMT +01:00)", "Atlantic/Azores(GMT +00:00)"],
    "Palau": ["Pacific/Palau(GMT +09:00)"],
    "Paraguay": ["America/Asuncion(GMT -03:00)"],
    "Qatar": ["Asia/Qatar(GMT +03:00)"],
    "Reunion": ["Indian/Reunion(GMT +04:00)"],
    "Romania": ["Europe/Bucharest(GMT +03:00)"],
    "Serbia": ["Europe/Belgrade(GMT +02:00)"],
    "Russian Federation":
    ["Europe/Kaliningrad(GMT +02:00)",
        "Asia/Srednekolymsk(GMT +11:00)",
        "Europe/Saratov(GMT +04:00)",
        "Asia/Tomsk(GMT +07:00)",
        "Europe/Astrakhan(GMT +04:00)",
        "Europe/Ulyanovsk(GMT +04:00)",
        "Europe/Kirov(GMT +03:00)",
        "Europe/Moscow(GMT +03:00)",
        "Europe/Volgograd(GMT +03:00)",
        "Europe/Samara(GMT +04:00)",
        "Europe/Simferopol(GMT +03:00)",
        "Asia/Yekaterinburg(GMT +05:00)",
        "Asia/Omsk(GMT +06:00)",
        "Asia/Barnaul(GMT +07:00)",
        "Asia/Novosibirsk(GMT +07:00)",
        "Asia/Novokuznetsk(GMT +07:00)",
        "Asia/Krasnoyarsk(GMT +07:00)",
        "Asia/Irkutsk(GMT +08:00)",
        "Asia/Chita(GMT +09:00)",
        "Asia/Yakutsk(GMT +09:00)",
        "Asia/Khandyga(GMT +09:00)",
        "Asia/Vladivostok(GMT +10:00)",
        "Asia/Sakhalin(GMT +11:00)",
        "Asia/Ust-Nera(GMT +10:00)",
        "Asia/Magadan(GMT +11:00)",
        "Asia/Kamchatka(GMT +12:00)",
        "Asia/Anadyr(GMT +12:00)"],
    "Rwanda": ["Africa/Kigali(GMT +02:00)"],
    "Saudi Arabia": ["Asia/Riyadh(GMT +03:00)"],
    "Solomon Islands": ["Pacific/Guadalcanal(GMT +11:00)"],
    "Seychelles": ["Indian/Mahe(GMT +04:00)"],
    "Sudan": ["Africa/Khartoum(GMT +02:00)"],
    "Sweden": ["Europe/Stockholm(GMT +02:00)"],
    "Singapore": ["Asia/Singapore(GMT +08:00)"],
    "Saint Helena": ["Atlantic/St_Helena(GMT +00:00)"],
    "Slovenia": ["Europe/Ljubljana(GMT +02:00)"],
    "Svalbard And Jan Mayen": ["Arctic/Longyearbyen(GMT +02:00)"],
    "Slovakia": ["Europe/Bratislava(GMT +02:00)"],
    "Sierra Leone": ["Africa/Freetown(GMT +00:00)"],
    "San Marino": ["Europe/San_Marino(GMT +02:00)"],
    "Senegal": ["Africa/Dakar(GMT +00:00)"],
    "Somalia": ["Africa/Mogadishu(GMT +03:00)"],
    "Suriname": ["America/Paramaribo(GMT -03:00)"],
    "South Sudan": ["Africa/Juba(GMT +03:00)"],
    "Sao Tome And Principe": ["Africa/Sao_Tome(GMT +01:00)"],
    "El Salvador": ["America/El_Salvador(GMT -06:00)"],
    "Sint Maarten": ["America/Lower_Princes(GMT -04:00)"],
    "Syrian Arab Republic": ["Asia/Damascus(GMT +03:00)"],
    "Swaziland": ["Africa/Mbabane(GMT +02:00)"],
    "Turks And Caicos Islands": ["America/Grand_Turk(GMT -04:00)"],
    "Chad": ["Africa/Ndjamena(GMT +01:00)"],
    "French Southern Territories": ["Indian/Kerguelen(GMT +05:00)"],
    "Togo": ["Africa/Lome(GMT +00:00)"],
    "Thailand": ["Asia/Bangkok(GMT +07:00)"],
    "Tajikistan": ["Asia/Dushanbe(GMT +05:00)"],
    "Tokelau": ["Pacific/Fakaofo(GMT +13:00)"],
    "Timor-Leste": ["Asia/Dili(GMT +09:00)"],
    "Turkmenistan": ["Asia/Ashgabat(GMT +05:00)"],
    "Tunisia": ["Africa/Tunis(GMT +01:00)"],
    "Tonga": ["Pacific/Tongatapu(GMT +13:00)"],
    "Turkey": ["Europe/Istanbul(GMT +03:00)"],
    "Trinidad And Tobago": ["America/Port_of_Spain(GMT -04:00)"],
    "Tuvalu": ["Pacific/Funafuti(GMT +12:00)"],
    "Taiwan": ["Asia/Taipei(GMT +08:00)"],
    "Tanzania": ["Africa/Dar_es_Salaam(GMT +03:00)"],
    "Ukraine": ["Europe/Kiev(GMT +03:00)", "Europe/Uzhgorod(GMT +03:00)", "Europe/Zaporozhye(GMT +03:00)"],
    "Uganda": ["Africa/Kampala(GMT +03:00)"],
    "United States Outlying Islands": ["Pacific/Johnston(GMT -05:00)", "Pacific/Midway(GMT -11:00)", "Pacific/Wake(GMT +12:00)"],
    "United States":
    ["America/New_York(GMT -04:00)",
        "America/Detroit(GMT -04:00)",
        "America/Kentucky/Louisville(GMT -04:00)",
        "America/Kentucky/Monticello(GMT -04:00)",
        "America/Indiana/Indianapolis(GMT -04:00)",
        "America/Indiana/Vincennes(GMT -04:00)",
        "America/Indiana/Winamac(GMT -04:00)",
        "America/Indiana/Marengo(GMT -04:00)",
        "America/Indiana/Petersburg(GMT -04:00)",
        "America/Indiana/Vevay(GMT -04:00)",
        "America/Chicago(GMT -05:00)",
        "America/Indiana/Tell_City(GMT -05:00)",
        "America/Indiana/Knox(GMT -05:00)",
        "America/Menominee(GMT -05:00)",
        "America/North_Dakota/Center(GMT -05:00)",
        "America/North_Dakota/New_Salem(GMT -05:00)",
        "America/North_Dakota/Beulah(GMT -05:00)",
        "America/Denver(GMT -06:00)",
        "America/Boise(GMT -06:00)",
        "America/Phoenix(GMT -07:00)",
        "America/Los_Angeles(GMT -07:00)",
        "America/Anchorage(GMT -08:00)",
        "America/Juneau(GMT -08:00)",
        "America/Sitka(GMT -08:00)",
        "America/Yakutat(GMT -08:00)",
        "America/Nome(GMT -08:00)",
        "America/Adak(GMT -09:00)",
        "America/Metlakatla(GMT -08:00)",
        "Pacific/Honolulu(GMT -10:00)"],
    "Uruguay": ["America/Montevideo(GMT -03:00)"],
    "Uzbekistan": ["Asia/Samarkand(GMT +05:00)", "Asia/Tashkent(GMT +05:00)"],
    "Holy See (Vatican City State)": ["Europe/Vatican(GMT +02:00)"],
    "Saint Vincent And Grenadines": ["America/St_Vincent(GMT -04:00)"],
    "Venezuela": ["America/Caracas(GMT -04:00)"],
    "Virgin Islands (British)": ["America/Tortola(GMT -04:00)"],
    "Virgin Islands (U.S.)": ["America/St_Thomas(GMT -04:00)"],
    "Viet Nam": ["Asia/Ho_Chi_Minh(GMT +07:00)"],
    "Vanuatu": ["Pacific/Efate(GMT +11:00)"],
    "Wallis And Futuna": ["Pacific/Wallis(GMT +12:00)"],
    "Samoa": ["Pacific/Apia(GMT +14:00)"],
    "Yemen": ["Asia/Aden(GMT +03:00)"],
    "Mayotte": ["Indian/Mayotte(GMT +03:00)"],
    "South Africa": ["Africa/Johannesburg(GMT +02:00)"],
    "Zambia": ["Africa/Lusaka(GMT +02:00)"],
    "Zimbabwe": ["Africa/Harare(GMT +02:00)"]
}