import { Helpers } from '../../../../../helpers';
import { FormsModule } from '@angular/forms';
import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation, } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { FileuploadService } from '../../../../../fileupload.service';
import { Currency } from './country codes';
import { language_details } from './country codes';
import { timezone } from './country codes';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { AuthenticationService } from '../../../../../auth/_services/authentication.service';
import { parse, format, AsYouType, isValidNumber } from 'libphonenumber-js';
//import {PhonenoComponent,p_no,country_info} from './phoneno/phoneno.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import swal from 'sweetalert2';
import { Subject } from 'rxjs/Subject';
import { Location, TranslationWidth } from '@angular/common';
declare let moment;
declare var $, jQuery: any;
declare let toastr: any;

@Component({
    selector: 'app-my-profile',
    templateUrl: "./my-profile.component.html",
    styleUrls: ['./my-profile.component.css']

})
export class MyProfileComponent implements OnInit {
    disable = false;

    val: any = { status: false, message: "" };
    /*User_details_new:any={
        address:"",
        city:"",
        client_name:"",
        company_name:"",
        language:"",
        pincode:"",
        statename:"",
        tax_no:"",
        timezone:"",
        user_type:"",
        website:"",
                       }
                       */

    Otp_entered = "";
    validphone: boolean;
    User_details: any = {
        address: "",
        balance: "",
        base_line: "",
        city: null,
        client_name: "",
        company_name: "",
        currency: "",
        email_id: "",
        email_verify: "",
        faxno: null,
        is_cutting: "",
        language: null,
        mobile_no: "",
        permissions: "",
        pincode: null,
        pre_cutting: "",
        start_num_from: "",
        statename: "",
        stop_sending_msg: "",
        tax_no: null,
        timezone: null,
        user_name: "",
        user_role: "",
        user_status: "",
        user_type: "",
        userid: "",
        country: "",
        website: null,
        designation: ""


    };
    NDNC = [];
    uploadShow1 = false;
    uploadShow2 = false;
    uploadShow3 = false;
    individual_file = "";
    company_file = "";
    verifiedEmail = "";
    p_no = "";
    validphone_no: boolean;
    old_pwd = '';
    p_no_otp = "";
    new_pwd = '';
    conf_new_pwd = '';
    login_history;
    sec_value;
    Username = '';
    kycDetails = [];
    kyc_status = 0;
    u_id = null;
    show_loading = false;
    asYouType: any;
    format: any;
    parse: any;
    showKyc = true;
    validphonenumber: any;
    individual = [];
    company = [];
    individualtype = [];
    companytype = [];
    currencyDropdown = [];
    languageDropdown = [];
    timezoneDropdown = {};
    timezoneobject = [];
    individualfilestatus = 0;
    companyfilestatus = 0;
    ndncfilestatus = 0;
    constructor(private http: HttpClient, private _fileupload: FileuploadService, private _appService: AppService, private _userservice: UserService, private _script: ScriptLoaderService, private location: Location) {

    }
    ngOnInit() {

        // console.log(this.currencyDropdown);
        // this._script.loadScripts('app-my-profile',
        //     ['assets/demo/demo3/base/countrydropdown.js']);

        // this._script.loadScripts('app-my-profile',
        //     ['assets/demo/demo3/base/bootstrap-formhelpers-selectbox.js']);

        //     console.log("loaded");
        //    console.log( this._script.loadScripts('app-my-profile',
        //     ['./assets/flagstrap-master/dist/js/jquery.flagstrap.js'])  );  
        //     console.log("loaded");
        this._script.loadScripts('app-my-profile',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/build/js/intlTelInput.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/build/js/bootstrap-formhelpers.min.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/build/js/bootstrap-formhelpers-currencies.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/build/js/bootstrap-formhelpers-countries.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/build/js/bootstrap-formhelpers-languages.js']);


        this.currencyDropdown = Currency;
        this.languageDropdown = language_details;
        this.timezoneDropdown = timezone;

        this._script.loadScripts('app-my-profile',
            ['assets/demo/demo3/base/input-mask.js']);
        this._script.loadScripts('app-my-profile',
            ['assets/demo/demo3/base/toastr.js']);

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // this._appService.get_method('http://35.238.188.102/saino-first/api/user/profileprofile_details').subscribe(data=>{console.log(data);})
        this.show_loading = true;
        this.get_user_details();
        this.showKyc = true;
        this.getKycDetails();
        // $(document).ready(function() {
        //     $("#countries").msDropdown();
        //     })
        // let s=this;
        // $('#uploadindividual').on('click',function(e){
        //     console.log(s.individualfilestatus);
        //     return false;
        //     // e.preventDefault();
        //     // if(s.individualfilestatus==1){
        //     //     toastr.warning("<h5>Your Individual File is Already Approved<h5>");
        //     // }
        //     // else{
        //         // $(this).trigger('click');
        //         // console.log("hii",e.files);

        //     // }
        // })
        //$('#currency').selectpicker({size:4});  
        //$('.selectpicker').selectpicker({size:4});
        // $('#currency').selectpicker('refresh');
        // $("#selectpicker1").selectpicker({ size:4});
        $("#phone").intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            initialCountry: "in",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            utilsScript: "assets/build/js/utils.js"
        });
        this.individual = [
            { proof: 'Aadhaar Card', val: 'audhar' },
            { proof: 'PAN Card', val: 'pan' },
            { proof: 'Passport', val: 'passport' },
            { proof: 'Voter Identity Card', val: 'voter' },
            { proof: 'Driving License', val: 'driving' },
        ]
        this.company = [
            { proof: 'GST Certificate', val: 'gst' },
            { proof: 'Registration certificate', val: 'registration' },
            { proof: 'Income Tax Returns', val: 'income' },
            { proof: 'Any Utility Bill', val: 'utility' }
        ]
        this.individualtype = [
            'audhar',
            'pan',
            'passport',
            'voter',
            'driving',
        ]
        this.companytype = [
            'gst',
            'registration',
            'income',
            'utility'
        ]
        this.NDNC = ['NDNC'];
        this.timezoneobject = Object.keys(this.timezoneDropdown);



    }

    getKycDetails() {
        // this.kycdocs=false;
        this._appService.get_method(this._userservice.kyc_status).subscribe(
            data => {
                this.kyc_status = data.status;
                this._appService.get_method(this._userservice.profile_kyc).subscribe(
                    data => {
                        this.kycDetails = data.data;
                        //  console.log(data);

                        try {
                            for (let i = 0; i < this.kycDetails.length; i++) {
                                let ar = this.kycDetails[i]['kyc_path'].split(',');
                                for (let j = 0; j < ar.length; j++) {
                                    let arr = ar[j].split('/');
                                    let arr1 = arr[arr.length - 1].split('-');

                                    if (this.individualtype.includes(arr1[0])) {
                                        this.kycDetails[i]['individual'] = arr1[0];
                                        if (this.kyc_status == 0) {
                                            this.individualfilestatus = 2;
                                        }
                                        else if (this.kyc_status == 1) { this.individualfilestatus = 1; }
                                        else { this.individualfilestatus = 2 }
                                        //  this.kycDetails[i]['individual_path']=this.files+ar[j];

                                    }
                                    else {
                                        this.kycDetails[i]['individual'] = "No file";
                                        //  this.kycDetails[i]['individual_path']="";
                                    }
                                    if (this.companytype.includes(arr1[0])) {
                                        this.kycDetails[i]['company'] = arr1[0];
                                        if (this.kyc_status == 0) {
                                            this.companyfilestatus = 2;
                                        }
                                        else if (this.kyc_status == 1) { this.companyfilestatus = 1; }
                                        else { this.companyfilestatus = 2 }
                                        // this.kycDetails[i]['company_path']=this.files+ar[j];

                                    }
                                    else {
                                        this.kycDetails[i]['company'] = "No file";
                                        this.companyfilestatus = 0;
                                        // this.kycDetails[i]['company_path']="";
                                    }
                                    if (this.NDNC.includes(arr1[0])) {
                                        this.kycDetails[i]['ndnc'] = arr1[0];
                                        if (this.kyc_status == 0) {
                                            this.ndncfilestatus = 2;
                                        }
                                        else if (this.kyc_status == 1) { this.ndncfilestatus = 1; }
                                        else { this.ndncfilestatus = 2 }
                                        // this.kycDetails[i]['ndnc_path']=this.files+ar[j];
                                    }
                                    else {
                                        this.kycDetails[i]['ndnc'] = "No file";

                                        this.ndncfilestatus = 0;

                                        // this.kycDetails[i]['ndnc_path']="";
                                    }

                                }
                            }
                        }
                        catch (e) { }
                        //console.log("kyc data",this.kycDetails);

                        this.showKyc = false;

                    },
                    error => {
                        this.showKyc = false;


                    }
                );
            },
            error => {
                this.showKyc = false;

            }
        );



    }
    get_user_details() {
        this.u_id = null;
        this._appService.get_method(this._userservice.profile_url).subscribe(
            data => {
                // console.log(data);
                this.User_details = data.profile_details[0];
                this.verifiedEmail = data.profile_details[0]['email_id'];
                if (this.User_details.language == "" || this.User_details.language == null) { this.User_details['language'] = "English"; }
                if (this.User_details.currency == "" || this.User_details.currency == null) { this.User_details['currency'] = "India Rupees - INR"; }
                this.User_details.mobile_no = parseInt(this.User_details.mobile_no, 10);
                this.p_no = this.User_details.mobile_no;
                this.u_id = this.User_details.userid;
                // this.User_details_new=data.profile_details[0];
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');
                }, 200)

                this.show_loading = false;

                // console.log(this.p_no);
                // console.log(this.User_details);
                // console.log("verify",this.p_no==this.User_details.mobile_no);
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
                this.show_loading = false;

            }
        );
        // this.get_kyc_details(u_id);

    }
    // get_kyc_details(u_id:any)
    // {
    //     this._appService.get_method(this._userservice.profile_kyc).subscribe(
    //         data=>{
    //            // console.log(data);
    //         }
    //         );
    // }
    kycStatus() {
        this._appService.get_method(this._userservice.kyc_status).subscribe(
            data => {
                this.kyc_status = data.status;
            },
            error => {

            }
        );
    }

    get_login_history_details() {
        this.show_loading = true;
        this._appService.get_method(this._userservice.login_history_url).subscribe(
            data => {
                if (data.status) {
                    // console.log(data);
                    this.login_history = data.login_history;
                    for (let i = 0; i < this.login_history.length; i++) {
                        this.login_history[i]['login_date'] = moment(this.login_history[i]['login_date']).format("DD/MM/YY hh:mm A ");
                    }
                    this.show_loading = false;
                }

                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }
                //  console.log(data);
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                // console.log('post error')
            });
    }
    status_2fa;
    get_2fa() {
        this._appService.get_method(this._userservice.twofa_url).subscribe(
            data => {
                // console.log('in get' + JSON.stringify(data));
                this.status_2fa = data.two_factor_status;
            },
            error => {
                // console.log('post error')
            });
    }

    update_2fa() {
        let status;
        //  console.log(this.sec_value + 'here');
        if (this.sec_value) status = 1;
        else status = 0;
        let data_ = {
            'status': status
        };
        this._appService.patch_method(this._userservice.twofa_url, JSON.stringify(data_)).subscribe(
            data => {
                if (data.status || data.status) { toastr.success("<h5>" + data.message + "<h5>"); }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => { toastr.error("<h5>Server Error " + error + "<h5>"); });
    }
    update_pwd() {
        this.old_pwd = this.old_pwd.trim();
        this.new_pwd = this.new_pwd.trim();
        this.conf_new_pwd = this.conf_new_pwd.trim();
        let password_regex = /(?=.*\d{1,})(?=.*\W{1,})(?=.*\w{1,}).*$/;
        if (this.new_pwd == "" || this.old_pwd == "" || this.conf_new_pwd == "") {
            return toastr.warning('<h5>Fields Are Empty<h5>');
        }
        else if (this.new_pwd.length <= 4) {
            return toastr.warning('<h5>Password is too short<h5>');
        }
        else if (!password_regex.test(this.new_pwd)) {
            return toastr.warning('<h6>Must Contain atleast One digit One alphabet One Symbol<h6>');
        }
        else if (this.new_pwd != this.conf_new_pwd) {
            return toastr.warning('<h5>Passwords Doesn\'t match<h5>');
        }
        else {
            this.show_loading = true;
            let update_data: string = JSON.stringify({ 'old_password': this.old_pwd, 'new_password': this.new_pwd });
            this._appService.patch_method(this._userservice.change_pwd_url, update_data).subscribe(
                data => {
                    if (data.status) toastr.success("<h5>" + data.message + "<h5>");
                    else toastr.error("<h5>" + data.message + "<h5>");

                },
                error => {
                    toastr.error("<h5>Server Error " + error + "<h5>");
                });
            this.show_loading = false;
        }
    }
    updateProfile() {

        if (this.validateProfile()) {
            toastr.info("<h5>Updating Details<h5>");
            let data = JSON.stringify(this.User_details);
            this._appService.patch_method(this._userservice.profile_url, data).subscribe(
                data => {
                    //  console.log(data);
                    if (data.status || data.success) {
                        toastr.success("<h5>" + data.message + "<h5>");
                        this.show_loading = true;
                        this.get_user_details();
                    }
                    else {
                        toastr.error("<h5>" + data.message + "<h5>");
                    }
                },
                error => { toastr.error("<h5>Server Error " + error + "<h5>"); }
            )
        }

    }


    // onfullname(event: any) {
    //     this.User_details.client_name = event.target.value.trim();

    // }
    // ontaxno(event: any) {
    //     this.User_details.tax_no = event.target.value.trim();

    // }
    // ondesignation(event: any) {
    //     this.User_details.user_type = event.target.value.trim();

    // }
    // onlanguage(event: any) {
    //     this.User_details.language = event.target.value.trim();

    // }
    // ontimezone(event: any) {
    //     this.User_details.timezone = event.target.value.trim();

    // }
    // oncompanyname(event: any) {
    //     this.User_details.company_name = event.target.value.trim();

    // }
    // onaddress(event: any) {
    //     this.User_details.address = event.target.value.trim();

    // }
    // oncity(event: any) {
    //     this.User_details.city = event.target.value.trim();

    // }
    // onstate(event: any) {
    //     this.User_details.statename = event.target.value.trim();

    // }
    // onpincode(event: any) {
    //     this.User_details.pincode = event.target.value.trim();

    // }
    // onwebsite(event: any) {
    //     this.User_details.website = event.target.value.trim();

    // }
    validatefullname(): boolean {
        if (this.User_details.client_name == null) {
            toastr.warning("<h5>Full name Field is required<h5>");
            return false;
        }
        this.User_details.client_name = this.User_details.client_name.trim();
        if (this.User_details.client_name == "") {
            toastr.warning("<h5>Full name Field is required<h5>");
            return false;
        }
        else {
            var fullname = /^[a-zA-Z]+$/;
            if (this.User_details.client_name.length < 4 || this.User_details.client_name.length > 30) {
                toastr.warning("<h6>Full name Field Length Should be between 4 and 30<h6>");
                return false;
            }
            else {
                if (!fullname.test(this.User_details.client_name.replace(/\s/g, ""))) {
                    toastr.warning("<h5>Full name Field is Invalid<h5>");
                    return false;
                }
                else {
                    return true;
                }

            }

        }

    }
    validateDesignation(): boolean {
        if (this.User_details.designation == null) {
            // toastr.warning("<h6>Designation Field is Required<h6>");
            return true;
        }
        this.User_details.designation = this.User_details.designation.trim();
        if (this.User_details.designation == "") {
            // toastr.warning("<h6>Designation Field is Required<h6>");
            return true;
        }
        else {
            let type = /^[a-zA-Z]+$/;
            if (!type.test(this.User_details.designation.replace(/\s/g, ''))) {
                toastr.warning("<h5>Designation Field is Invalid<h5>");
                return false;
            }
            else {
                return true;
            }
        }

    }
    validatelanguage(): boolean {
        // this.User_details.language=this.User_details.language.trim();
        if (this.User_details.language == "" || this.User_details.language == null) {
            toastr.warning("<h5>Please Choose Language <h5>");
            return false;
        }
        else {
            return true;
        }
        // else{
        //     var language = /^[a-zA-Z]+$/;
        //     if(this.User_details.language.length<4 || this.User_details.language.length>20)
        //     {
        //         toastr.warning("<h6>Language Field Length Should be between 4 and 20<h6>");
        //         return false;
        //     }
        //     else {
        //         if (!language.test(this.User_details.language)){
        //             toastr.warning("<h5>Language Field is Invalid<h5>");
        //             return false;
        //         }
        //         else{
        //             return true;
        //         }

        //     }

        // }


    }
    // validatetimezone(): boolean {
    //     if (this.User_details.timezone != null && this.User_details.timezone != "") return true;
    //     return false;
    // }
    validatecompanyname(): boolean {
        if (this.User_details.company_name == null) {
            // toastr.warning("<h5>Company Field is Invalid<h5>");
            return true;

        }
        this.User_details.company_name = this.User_details.company_name.trim();
        if (this.User_details.company_name != "") {

            // let s=this.User_details.company_name.replace(new RegExp(/[@#$%^*~`]/,'g'),'');

            // s=this.User_details.company_name.replace(new RegExp(/\d/,'g'),'');
            // console.log(s)
            // if(s==""){
            //     toastr.warning("<h5>Company Field is Invalid<h5>");
            //     return false;
            // }

            // var companyname = /^[0-9]$/;
            if (!/[0-9a-zA-Z]{1,}/.test(this.User_details.company_name)) {
                toastr.warning("<h5>Company Name Should Contain Only Numbers and Alphabets<h5>");
                return false;
            }
            if (this.User_details.company_name.length < 6 || this.User_details.company_name.length > 30) {
                toastr.warning("<h5>Company Name Should be Between 6 to 30 Characters<h5>");
                return false;
            }

            else {
                return true;
            }
        }
        else {
            // toastr.warning("<h5>Company Name Field is Required<h5>");
            return true;
        }


    }
    validateCurrency(): boolean {
        if (this.User_details.currency == null || this.User_details.currency == "") {
            toastr.warning("<h5>Please Choose Currency");
            return false;
        }
        else {
            return true;
        }
    }
    validateaddress(): boolean {
        if (this.User_details.address == null) {

            // toastr.warning("<h5>Address Field is Required<h5>");
            return true;

        }
        this.User_details.address = this.User_details.address.trim();
        if (this.User_details.address == "") {
            // toastr.warning("<h5>Address Field is Required<h5>");
            return true;
        }
        else {
            if (this.User_details.address.length < 5 || this.User_details.address.length > 150) {
                toastr.warning("<h5>Address Field Length Should be between 5 to 150 Characters<h5>");
                return false;
            }
            else if (/^[0-9]$/.test(this.User_details.address.replace(/\s/g, ''))) {
                toastr.warning("<h5>Address Field is Invalid<h5>");
                return false;
            }
            else {
                return true;
            }
        }

    }
    validatecity(): boolean {
        if (this.User_details.city == null) {
            // toastr.warning("<h5>City Field is Required<h5>");
            return true;
        }
        this.User_details.city = this.User_details.city.trim();
        if (this.User_details.city == "") {
            // toastr.warning("<h5>City Field is Required<h5>");
            return true;
        }
        else {
            if (this.User_details.city.length < 3 || this.User_details.city.length > 20) {
                toastr.warning("<h5>City Field Length Should be between 3 to 20 Characters<h5>");
                return false;
            }
            else {
                var city = /^[a-zA-Z]+$/;
                if (!city.test(this.User_details.city.replace(/\s/g, ""))) {
                    toastr.warning("<h5>City Field is Invalid<h5>");
                    return false;
                }
                else {
                    return true;
                }
            }

        }

    }
    validatestate(): boolean {
        if (this.User_details.statename == null) {
            // toastr.warning("<h5>State Field is Required<h5>");
            return true;

        }
        this.User_details.statename = this.User_details.statename.trim();
        if (this.User_details.statename == "") {
            // toastr.warning("<h5>State Field is Required<h5>");
            return true;
        }
        else {
            if (this.User_details.statename.length < 3 || this.User_details.statename.length > 20) {
                toastr.warning("<h5>State Field Length Should be between 3 to 20 Characters<h5>");
                return false;
            }
            else {
                let state = /^[a-zA-Z]+$/;
                if (!state.test(this.User_details.statename.replace(/\s/g, ""))) {
                    toastr.warning("<h5>State Field is Invalid<h5>");
                    return false;
                }
                else {
                    return true;
                }
            }

        }

    }
    validatecountry(): boolean {
        if (this.User_details.country == null) {
            // toastr.warning("<h5>Country Field is Required<h5>");
            return true;

        }
        this.User_details.country = this.User_details.country.trim();
        if (this.User_details.country == "") {
            // toastr.warning("<h5>Country Field is Required<h5>");
            return true;
        }
        else {
            if (this.User_details.country.length < 3 || this.User_details.country.length > 20) {
                toastr.warning("<h5>Country Field Length Should be between 3 to 20 Characters<h5>");
                return false;
            }
            else {
                let country = /^[a-zA-Z]+$/;
                if (!country.test(this.User_details.country.replace(/\s/g, ""))) {
                    toastr.warning("<h5>Country Field is Invalid<h5>");
                    return false;
                }
                else {
                    return true;
                }
            }

        }


    }
    validatepincode(): boolean {
        if (this.User_details.pincode == null) {
            // toastr.warning("<h5>Pincode Field is Required<h5>");
            return true;
        }
        this.User_details.pincode = this.User_details.pincode.trim();
        if (this.User_details.pincode == "") {
            // toastr.warning("<h5>Pincode Field is Required<h5>");
            return true;
        }
        else {
            if (this.User_details.pincode.length != 6) {
                toastr.warning("<h5>Pincode Field Length Should be 6<h5>");
                return false;
            }
            else {
                let pincode = /^\d{6,}$/;
                if (!pincode.test(this.User_details.pincode)) {
                    toastr.warning("<h5>Pincode Field is Invalid<h5>");
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }
    validatewebsite(): boolean {
        if (this.User_details.website == null) {
            // toastr.warning("<h5>Website Field is Required<h5>");
            return true;
        }
        this.User_details.website = this.User_details.website.trim();
        if (this.User_details.website == "") {
            // toastr.warning("<h5>Website Field is Required<h5>");
            return true;
        }
        else {
            var re = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm;
            if (!re.test(this.User_details.website.toLowerCase())) {
                toastr.warning("<h5>Website Field Is Invalid<h5>");
                return false;
            }
            else {
                return true;
            }
        }

    }
    validatetaxno(): boolean {
        return true;
        //     if(this.User_details.tax_no==null){
        //         // toastr.warning("<h5>Tax No Field is Required<h5>");
        //         return true;
        //     }
        //     this.User_details.tax_no=this.User_details.tax_no.trim();
        //   //  var tax_number = /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}Z\d{1}?$/;
        //     if (this.User_details.tax_no != "") {
        //         return true;

        //     }
        //     else{
        //         // toastr.warning("<h5>Tax No Field is Required<h5>");
        //         return true;

        //     }
    }
    validateTimezone(): boolean {
        //this.User_details.timezone=this.User_details.timezone.trim();
        if (this.User_details.timezone == "" || this.User_details.timezone == null) {
            toastr.warning("<h5>Timezone Field is Required<h5>");
            return false;
        }
        else {
            return true;
        }
        // else{
        //     if (/^[a-zA-Z]+$/.test(this.User_details.timezone)){
        //         return true;
        //     } 
        //     else{
        //         toastr.warning("<h5>TimeZone Field Is invalid<h5>");
        //         return false;
        //     }

        // }

    }
    validatePhoneno(): boolean {
        let country_info = $("#phone").intlTelInput("getSelectedCountryData");

        //this.asYouType = new AsYouType('IN').input('63016399979');
        //this.format = format('6401639997', 'IN', 'International');
        this.parse = (parse(this.p_no.toString(), country_info.iso2.toUpperCase()));
        // console.log(this.parse.length);
        if (!Object.keys(this.parse).length) {
            toastr.warning("<h5>Invalid Phone no<h5>");
            return false;
        }
        else {
            if (isValidNumber(this.parse)) {
                // console.log("valid phone no"); 
                return true;
            }
            else {
                toastr.warning("<h5>Invalid Phone no<h5>");
                return false;
            }

        }

    }
    changed(val) {
        this.validphone_no = val;
        // console.log(this.User_details.mobile_no);
    }
    validateProfile() {
        this.val.status = false;
        if (!this.validatefullname()) return false;
        //console.log("fullname");
        if (!this.validateDesignation()) return false;
        //console.log("designation");
        if (this.User_details.email_id == "") {
            toastr.warning("<h6>Email Field is Required<h6>");
            return false;
        }

        if (this.verifiedEmail != this.User_details.email_id) {
            toastr.warning("<h6>Validate Email Id Through OTP to Update<h6>");
            return false;
        }
        //  console.log("email");
        if (this.p_no == "" || this.p_no == null) {
            toastr.warning("<h6>Mobile NumberField Is Required<h6>");
            return false;
        }
        if (this.p_no != this.User_details.mobile_no) {
            toastr.warning("<h6>Validate Mobile Number Through OTP to Update<h6>");
            return false;
        }

        // console.log("phone");
        if (!this.validatePhoneno()) return false;
        //  console.log("phone")
        //   if(!this.validateCurrency())return false;
        //  console.log("currency");
        if (!this.validatelanguage()) return false;
        //  console.log("language")
        if (!this.validatecompanyname()) return false;
        //  console.log("companyname");
        if (!this.validateaddress()) return false;
        //   console.log("address");
        if (!this.validatecity()) return false;
        //   console.log("city");
        if (!this.validatestate()) return false;
        //   console.log("state");

        // if (!this.validatecountry()) return false;
        if (!this.validatepincode()) return false;
        //   console.log("pincode");
        if (!this.validatewebsite()) return false;
        //   console.log("website");
        if (!this.validatetaxno()) return false;
        //  console.log("taxno");
        //   if (!this.validateTimezone()) return false;
        //  console.log("timezone")

        this.val.status = true;
        return this.val.status;
    }
    select1_changed(val) {
        // console.log("changed1",val);
        this.individual_file = val;

    }
    select2_changed(val) {
        //console.log("changed2",val);
        this.company_file = val;

    }
    validateFile(name: String) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        ext = ext.toLowerCase();
        if (ext.toLowerCase() == 'png' || ext.toLowerCase() == 'jpg' || ext.toLowerCase() == 'jpeg') {
            return true;
        }
        else {
            return false;
        }
    }

    upload_1(files: FileList, target) {
        if (!this.validateFile(files[0].name)) {
            target.value = "";
            return toastr.warning("<h5>Only png,jpg,jpeg files are allowed<h5>");
        }
        if (this.individual_file == "") {
            target.value = "";
            return toastr.warning("<h5>choose Individual Doc file type and Upload again<h5>");
        }
        let file = files[0];
        if (file == null) {
            target.value = "";
            return toastr.warning("<h5>Please choose file<h5>");
        }
        this.uploadShow1 = true;
        let filename = file.name.split(',').join('_');

        const formData = new FormData();
        formData.append(this.individual_file, file, filename);

        this._fileupload.file_patch_method(this._userservice.profile_kyc, formData).subscribe(
            data => {
                if (data.status) {

                    toastr.success("<h5>Individual Doc File Uploaded Successfully<h5>");
                }
                else {
                    toastr.error("<h5>Error Uploading Individual Doc File<h5>");
                }
                this.uploadShow1 = false;



            },
            error => {
                toastr.error("<h5>Server Error  " + error + "<h5>");
                this.uploadShow1 = false;

            }
        );

    }
    upload_2(files: FileList, target) {
        if (!this.validateFile(files[0].name)) {
            target.value = "";
            return toastr.warning("<h5>Only png,jpg,jpeg files are allowed<h5>");
        }

        if (this.company_file == "") {
            target.value = "";
            return toastr.warning("<h5>Choose Company Doc file type and Upload again<h5>");
        }
        let file = files[0];
        if (file == null) {
            target.value = "";
            return toastr.warning("<h5>Please choose file<h5>");
        }
        this.uploadShow2 = true;
        let filename = file.name.split(',').join('_');

        const formData = new FormData();
        formData.append(this.company_file, file, filename);
        this._fileupload.file_patch_method(this._userservice.profile_kyc, formData).subscribe(
            data => {
                if (data.status) {


                    toastr.success("<h5>Company Doc File Uploaded Successfully<h5>");
                }
                else {
                    toastr.error("<h5>Error Uploading Company Doc File<h5>");
                }
                this.uploadShow2 = false;

            },
            error => {
                this.uploadShow2 = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );

    }
    upload_3(files: FileList, target) {
        if (!this.validateFile(files[0].name)) {
            target.value = "";
            return toastr.warning("<h5>Only png,jpg,jpeg files are allowed<h5>");
        }
        let file = files[0];
        if (file == null) {
            target.value = "";
            return toastr.warning("<h5>Please choose file<h5>");
        }
        this.uploadShow3 = true;
        let filename = file.name.split(',').join('_');

        const formData = new FormData();
        formData.append("NDNC", file, filename);
        this._fileupload.file_patch_method(this._userservice.profile_kyc, formData).subscribe(
            data => {
                if (data.status) {

                    toastr.success("<h5>NDNC File Uploaded Successfully<h5>");
                }
                else {
                    toastr.error("<h5>Error Uploading NDNC File<h5>");

                }
                this.uploadShow3 = false;
            },
            error => {
                toastr.error("<h5>Server Error" + error + "<h5>");
                this.uploadShow3 = false;

                // console.log(error);
            }
        );
    }
    p_no_changed() {
        //  console.log(this.p_no,this.User_details.mobile_no);
    }
    otp = "0987";
    email_otp = "";
    changedEmail = "";
    email_otp_send = false;
    sendOtpMail() {
        if (this.verifiedEmail == this.User_details.email_id) {
            return toastr.info("<h5>Email is Already Verified<h5>");

        }
        this.User_details.email_id = this.User_details.email_id.trim();
        if (this.User_details.email_id == "") {
            return toastr.warning("Email Id Field is Required");
        }
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.User_details.email_id)) {
            return toastr.warning("<h5>Invalid Email<h5>");
        }
        this.email_otp_send = true;
        this.changedEmail = this.User_details.email_id;
        this._appService.post_method(this._userservice.email_otp, JSON.stringify({ 'email_id': this.changedEmail })).subscribe(
            data => {
                //  console.log(data);
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    $('#verifyEmail').modal('show');
                    this.email_otp_send = false;
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                    this.email_otp_send = false;

                }
            },
            error => {
                toastr.error("<h5>Server error " + error + "<h5>");
            }
        );



    }
    verifyOtpMail() {
        toastr.info("Validating Otp");
        if (this.email_otp == "") { return toastr.warning("<h5>OTP Field is Required<h5>"); }
        if (!/^[0-9]{4}$/.test(this.email_otp)) { return toastr.warning("<h5>OTP Value is Invalid<h5>"); }
        this._appService.post_method(this._userservice.verify_email_otp, JSON.stringify({ 'email_id': this.changedEmail, 'otp': this.email_otp })).subscribe(
            data => {
                if (data.status) {
                    $('#verify').modal('hide');
                    this.verifiedEmail = this.User_details.email_id = this.changedEmail;
                    toastr.success("<h6>" + data.message + "<h6>");
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }
            },
            error => {
                toastr.error("<h6>" + error + "<h6>");
            }
        );


    }

    VerifyOtp() {
        if (this.p_no == this.User_details.mobile_no) {
            return toastr.info("<h5>Mobile Numbers is Already Verified<h5>");
        }
        this.disable = true;
        this.Otp_entered = "";
        this.p_no_otp = this.p_no;
        if (this.p_no == "" || !this.validatePhoneno()) {
            this.disable = false;
            return toastr.warning("<h5>Invalid Phone no<h5>");
        }
        toastr.info("<h5>Sending Otp...<h5>");
        this._appService.post_method(this._userservice.register_otp, JSON.stringify({ 'number': this.p_no })).subscribe(
            data => {

                if (data.status) {
                    this.disable = false;
                    toastr.info("<h6>OTP has Been sent to the Phone no " + this.p_no + "<h6>");
                    $('#verify').modal('show');
                    return;
                }
                else {
                    this.disable = false;

                    toastr.error("<h5>Error Sending OTP<h5>");
                }
            },
            error => {
                this.disable = false;

                toastr.error("<h5>Server Error" + error + "<h5>");

            }
        )
    }
    verifyOtp() {
        if (this.Otp_entered == "" || !/^[0-9]{4}$/.test(this.Otp_entered))
            return toastr.warning("<h5>Invalid Otp<h5>");
        toastr.info("<h5>Verifying...<h5>");
        this._appService.post_method(this._userservice.verify_register_otp, JSON.stringify({ 'number': this.p_no_otp, 'otp': this.Otp_entered })).subscribe(
            data => {

                if (data.status) {
                    this.p_no = this.p_no_otp;
                    this.User_details.mobile_no = this.p_no;

                    return toastr.success("<h5>Otp Verification Successful<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error" + error + "<h5>");
            }
        )
    }
    passwordClicked() {
        this.old_pwd = "";
        this.new_pwd = "";
        this.conf_new_pwd = "";
    }
    resetData() {
        //  console.log(this.User_details);
        this.User_details.address = "",
            this.User_details.balance = "",
            this.User_details.base_line = "",
            this.User_details.city = null,
            this.User_details.client_name = "",
            this.User_details.company_name = "",
            this.User_details.currency = "",
            this.User_details.email_id = "",
            this.User_details.email_verify = "",
            this.User_details.faxno = null,
            this.User_details.is_cutting = "",
            this.User_details.language = '',
            //this.User_details.mobile_no="",
            this.User_details.permissions = "",
            this.User_details.pincode = null,
            this.User_details.pre_cutting = "",
            this.User_details.start_num_from = "",
            this.User_details.statename = "",
            //  this.User_details.stop_sending_msg-"",
            this.User_details.tax_no = null,
            this.User_details.timezone = '',
            //this.User_details.user_name= "",
            this.User_details.user_role = "",
            this.User_details.user_status = "",
            this.User_details.user_type = "",
            this.User_details.userid = "",
            // this.User_details.country="",
            this.p_no = '';
        this.User_details.website = null


    }
}
