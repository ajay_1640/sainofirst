import { Component, OnInit } from '@angular/core';
import { FileuploadService } from '../../../../../fileupload.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { CookieService } from 'ngx-cookie-service';


declare let toastr;
declare let $;
@Component({
    selector: 'app-kyc',
    templateUrl: "./kyc.component.html",
    styleUrls: ['./kyc.component.css']
})
export class KycComponent implements OnInit {
    searchText = "";
    p: number = 1;
    some_list = [];
    individual = [];
    documentType = "";
    company = [];
    NDNC = []
    files = "";
    show_loading = false;
    constructor(private _fileUploadService: FileuploadService, private _cookieService: CookieService, private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this.files = this._userService.files;

        this._script.loadScripts('app-kyc',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "maxOpened":1,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // this._appService.get_method(this._userService.profile_kyc).subscribe(
        //     data=>{

        //     },
        //     error=>{
        //         toastr.error("<h5>Server Error "+error+"<h5>");
        //     }

        // );
        this.getKycDetails();
        this.individual = [
            'audhar',
            'pan',
            'passport',
            'voter',
            'driving',
        ]
        this.company = [
            'gst',
            'registration',
            'income',
            'utility'
        ]
        this.NDNC = ['NDNC'];
    }
    delete_sender() {

    }
    view_image(path, status, type) {
        // console.log("url",path);
        if (status == "No file") {
            return toastr.warning("<h5>No File to View<h5>");
        }
        $("#myModal_img_ROI").modal('show');
        this.documentType = type;
        $('#myModal_img_ROI img').attr('src', "");
        $('#myModal_img_ROI img').attr('src', path);
    }

    getKycDetails() {
        this.show_loading = true;
        this._appService.get_method(this._userService.manage_user_kyc).subscribe(
            data => {
                this.some_list = data.result;
                console.log("kyc data", data);
                for (let i = 0; i < this.some_list.length; i++) {
                    let ar = [];
                    try { ar = this.some_list[i]['kyc_path'].split(','); }
                    catch (e) { }
                    if (this.some_list[i]['kyc_path'] == null || this.some_list[i]['kyc_path'] == '') {
                        this.some_list[i]['individual'] = "No file";
                        this.some_list[i]['individual_path'] = "";
                        this.some_list[i]['company'] = "No file";
                        this.some_list[i]['company_path'] = "";
                        this.some_list[i]['ndnc'] = "No file";
                        this.some_list[i]['ndnc_path'] = "";

                    }
                    else {
                        for (let j = 0; j < ar.length; j++) {
                            let arr = ar[j].split('/');
                            let arr1 = arr[arr.length - 1].split('-');
                            if (this.individual.includes(arr1[0])) {
                                this.some_list[i]['individual'] = arr1[0];
                                this.some_list[i]['individual_path'] = this.files + ar[j];

                            }
                            else {
                                this.some_list[i]['individual'] = "No file";
                                this.some_list[i]['individual_path'] = "";
                            }
                            if (this.company.includes(arr1[0])) {
                                this.some_list[i]['company'] = arr1[0];
                                this.some_list[i]['company_path'] = this.files + ar[j];

                            }
                            else {
                                this.some_list[i]['company'] = "No file";
                                this.some_list[i]['company_path'] = "";
                            }
                            if (this.NDNC.includes(arr1[0])) {
                                this.some_list[i]['ndnc'] = arr1[0];
                                this.some_list[i]['ndnc_path'] = this.files + ar[j];
                            }
                            else {
                                this.some_list[i]['ndnc'] = "No file";
                                this.some_list[i]['ndnc_path'] = "";
                            }

                        }
                    }


                }
                this.show_loading = false;

            },
            error => {
                this.show_loading = false;

                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    kycApprove(user) {
        let d = {
            'user_id': user.userid,
            'status': 1
        }
        // console.log(user);
        this._appService.patch_method(this._userService.manage_user_kyc, JSON.stringify(d)).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.getKycDetails();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    kycDisapprove(user) {
        let d = {
            'user_id': user.userid,
            'status': 2
        }
        this._appService.patch_method(this._userService.manage_user_kyc, JSON.stringify(d)).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.getKycDetails();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    saveFile(name, type, data) {
        if (data != null && navigator.msSaveBlob)
            return navigator.msSaveBlob(new Blob([data], { type: type }), name);
        var a = $("<a style='display: none;'/>");
        var url = window.URL.createObjectURL(new Blob([data], { type: type }));
        a.attr("href", url);
        a.attr("download", name);
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }

    downloadFile(downloadLink, username, type) {

        if (type == "No file") { return toastr.warning("<h5>No File To downlaod<h5>"); }
        this._fileUploadService.getImage(downloadLink).subscribe(
            (res) => {
                const a = document.createElement('a');
                a.href = URL.createObjectURL(res);
                a.download = username + "_" + type;
                document.body.appendChild(a);
                a.click();
            });
    }





}
