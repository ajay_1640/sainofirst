import { Pipe, PipeTransform } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'serverlog'
})
export class ServerlogPipe implements PipeTransform {
    //constructor(){}
    Url = "http://35.238.188.102/status.html?password=saniofrst@3579";
    constructor(private sanitizer: DomSanitizer, private _appService: AppService) {
        this.sanitizer = sanitizer;
        // this.transform(this.Url);
    }

    transform() {
        // console.log(Url);
        // console.log("transform called");
        // console.log("pipe",this.sanitizer.bypassSecurityTrustResourceUrl("http://35.238.188.102/status.html?password=saniofrst@3579"));
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.Url);

    }

}

// export class CoursesComponent implements OnInit  {
//   constructor(public router: Router,private sanitizer: DomSanitizer,private _coursesService: CoursesService) { }

//   courses=this._coursesService.getCourses();
//   ngOnInit() {
//   }
//     trustSrcUrl = function(data){
//     return this.sanitizer.bypassSecurityTrustResourceUrl(data);
//   }
//   VideoURL=this.trustSrcUrl(this.courses[0].url);
//   changeVideo=function(url){
//       this.VideoURL=this.trustSrcUrl(url);
//   }
//   import { Component, Pipe } from '@angular/core';
// import { DomSanitizationService } from '@angular/platform-browser';
// @Pipe({name: 'secureUrl'})
// export class Url {




// }