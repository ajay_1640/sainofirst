import { Component, OnInit, TRANSLATIONS_FORMAT } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { keyframes } from '@angular/core/src/animation/dsl';
import { ElementInstructionMap } from '@angular/animations/browser/src/dsl/element_instruction_map';
import { UserService } from './../../../../../auth/_services/user.service';
import * as XLSX from 'xlsx';
import swal from 'sweetalert2';
import { CookieService } from 'ngx-cookie-service';

import { ServerlogPipe } from './serverlog.pipe';
import { timeout } from 'rxjs/operator/timeout';

declare let toastr;

declare var $, jQuery: any;
declare var moment: any;
@Component({
    selector: 'app-mgmtk',
    templateUrl: "mgmtk.component.html",
    styles: [],
    // pipes: [ServerlogPipe]
})


export class MgmtkComponent implements OnInit {
    readFileStatus = "";
    gatewayAttributes = [
        'gateways_id',
        'id',
        'company',
        'ip',
        'username',
        'password',
        'sending_port',
        'receive_port',
        'mode',
        'system_type',
        'gateway_name',
        'no_of_sessions',
        'is_senderid',
        'for_all_senderids',
        'create_date',
        'dnd_check',
        'gateway_id'
    ]
    savingBrand = false;
    show_modal_loading = false;
    files_ = "";
    color = 'color';
    create_gateway_id = "";
    show_edit_gateway = false;
    route_name_add = "";
    route_edit_id = "";
    wallet_status_p = 1;
    wallet_status_total = null;
    route_edit = "";
    route_edit_gateway = "";
    wallet_status_credit = null;
    wallet_status_debit = null;
    wallet_stat = [];
    show_loading_modal = false;
    user_id = "";
    credit = /^credit$/i;
    debit = /^debit$/i;
    p_block_content = 1;
    exclude_number_p = 1;
    exclude_p = 1;
    branding_p = 1;
    block_content_edit = "";
    block_content_object: any;
    multiple_content = "";
    show_loading = false;
    newErrorStatusAdded = false;
    file_ext = "";
    new_status = "";
    new_error_code = "";
    gatewayNameRoutes = "";
    routeNameRoutes = "";
    brand_list = [];
    searchText_gateway: null;
    selected_error_status = "";
    wallet_calendar_range = "";
    show_wallet_calendar = false;
    ar: any;
    serverlog_url = "";
    fromDate = '';
    toDate = '';
    dateRange;
    block_list = "";
    p: number = 1;
    error_p = 1;
    admin_routes = [];
    admin_routes1 = [];
    block_numbers = [];
    admin_routes_selected = [];
    gateway_p: number = 1;
    routes_p: number = 1;
    searchText = "";
    multiple_nos = "";
    block_num_edit = "";
    Gateway_name = "";
    send_flash = "";
    route_routename = "";
    route_ipaddress = "";
    route_username = "";
    route_status = "";
    route_companyname = "";
    route_port = "";
    route_rport = "";
    newErrorCodes = [];
    route_system_type = "";
    route_mode = "";
    route_no_of_sessions = 0;
    from_date = "";
    to_date = "";
    route_password = "";
    route_gatewaycode = "";
    route_dndcheck: any = false;
    route_senderId: any = false;
    gateway_list: any = [];
    routes_list: any = [];
    routes_list1: any = [];
    block_content = [];
    error_codes = [];
    error_codes1 = [];
    block_content_routes = [];
    block_numbers_routes = [];
    block_content_routes_exclude_users = [];
    block_content_routes_exclude_users_numbers = [];
    users_list = [];
    excluded_user = "";
    searchTextWalletStats = "";
    current_blocknumber: any;
    added_excluded_users = [];
    removed_excluded_users = [];
    selected_routes = [];
    removed_routes = [];
    selected_routes_checked = [];
    excluded_user_number = "";
    added_excluded_users_numbers = [];
    removed_excluded_users_numbers = [];
    selected_routes_numbers = [];
    removed_routes_numbers = [];
    selected_routes_checked_numbers = [];
    route_valid = false;
    brand_colour = "";
    brand_sitename = "";
    brand_favicon = "";
    brand_logo = "";
    brand_title = "";
    brand_user_name = "";
    brand_userid = ""
    brand_website = ""
    brand_wid;
    interval = [];
    constructor(private _cookieService: CookieService, private _script: ScriptLoaderService, private _serverlog: ServerlogPipe, private _userService: UserService, private _appService: AppService) {


    }

    show_mgmtk = false;
    ngOnInit() {
        this.user_id = this._cookieService.get('userid');
        this.files_ = this._userService.files;
        this._script.loadScripts('app-mgmtk',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        this._script.loadScripts('app-mgmtk',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-mgmtk',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        this.interval = [
            { 'val': "today", "interval": 'Today' },
            { 'val': "7days", "interval": 'Last 7 days' },
            { 'val': "15days", "interval": 'Last 15 days' },
            { 'val': "month", "interval": 'Last month' },
            { 'val': "custom", "interval": 'Custom' },
        ]

        $('#selectpicker1').selectpicker({ size: 4 });
        $('#selectpicker2').selectpicker({ size: 4 });
        $('#activation').selectpicker({ size: 4 });
        $('#activation1').selectpicker({ size: 4 });
        $('#error_code').selectpicker({ size: 4 });
        $('#create_route').on('shown.bs.modal', function(e) { $('#route_mode1').selectpicker('refresh'); });
        $('#block_content_modal').on('shown.bs.modal', function(e) { $('#activation').selectpicker('refresh'); });
        $('#block_nos_modal').on('shown.bs.modal', function(e) { $('#activation1').selectpicker('refresh'); });
        $('#sandbox-container .input-daterange').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        this.serverlog_url = this._userService.server_log;
        $(function() {

            var start = moment();
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            }

            $('#reportrange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);


        });

        // $('#create_route').on('shown.bs.modal', function (e) {
        //     $('.m_selectpicker').selectpicker('refresh');

        //       })
        // $('#selectpicker1').selectpicker('refresh');

        this.dateRange = $('#reportrange span').text();
        this.ar = this.dateRange.split(' - ');
        this.fromDate = this.ar[0];
        this.toDate = this.ar[1];
        this.get_users();

        this.getGatewayData();
        this.getRouteData();
        // this.getRoutesData();
        // $('#selectpicker1').selectpicker('refresh');
        // this.get_blocked_content();
        this.get_brands();
        this.get_admin_routes();
        this.walletStatusClicked();


        /*  $('#sandbox-container .input-daterange').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });*/
    }
    route_clicked() {
        this.getGatewayData()
        $('#selectpicker1').selectpicker('refresh');
        $('#selectpicker2').selectpicker('refresh');



    }
    get_brands() {
        this.show_loading = true;
        this.brand_list = [];
        this._appService.get_method(this._userService.branding).subscribe(
            data => {
                // console.log(data.result);
                this.brand_list = data.result;
                this.show_loading = false;
                // console.log(data.result);
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

                // console.log("brand list error");
            }
        );
    }
    get_admin_routes() {
        this.admin_routes = [];
        this.admin_routes1 = [];
        this._appService.get_method(this._userService.routes_url).subscribe(
            data => {
                for (let i = 0; i < data.result.length; i++) {
                    let d = {
                        'az_routeid': data.result[i]['az_routeid'],
                        'az_rname': data.result[i]['az_rname'],
                        'status': false
                    }
                    this.admin_routes.push(d);
                    this.admin_routes1.push(d);
                }

                // console.log("admin routes",this.admin_routes);
            },
            error => {
                // console.log(error);
            }
        );
    }
    get_blocked_content() {
        this.show_loading = true;
        this.block_content = [];
        this._appService.get_method(this._userService.admin_block_content).subscribe(
            data => {

                this.block_content = data.result.reverse();
                //console.log(this.block_content);
                for (let i = 0; i < this.block_content.length; i++) {
                    this.block_content[i]['added_date'] = moment(this.block_content[i]['added_date']).format("DD-MM-YYYY hh:mm:ss A");
                }
                this.show_loading = false;
            },
            error => {

                // console.log(error);
            }
        );
    }
    get_users() {
        this._appService.get_method(this._userService.admin_users_list).subscribe(
            data => {
                this.users_list = data.result;
                //  console.log("users list",this.users_list);
            },
            error => {
                // console.log(error);
            }
        );


    }
    get_blocked_excluded_members() {
        this.admin_routes1 = this.admin_routes;
        this.block_content_routes_exclude_users = [];
        this.selected_routes_checked = [];


        this._appService.get_method(this._userService.admin_block_content_routes).subscribe(
            data => {
                this.block_content_routes = data.result;
                for (let i = 0; i < this.admin_routes1.length; i++) {
                    this.selected_routes_checked.push({ 'status': false });

                    for (let j = 0; j < this.block_content_routes.length; j++) {
                        if (this.admin_routes1[i]['az_routeid'] == this.block_content_routes[j]['az_routeid']) {

                            this.admin_routes1[i]['status'] = true;
                            this.selected_routes_checked[i]['status'] = true;
                            // this.selected_routes.push(this.admin_routes[j]['az_routeid'].toString());

                            break;
                        }
                    }
                }
                //  console.log("routes",data.result);
                //  console.log("selected routes content",this.selected_routes_checked);
            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h6>Server Error: " + error + "<h6>")
                // console.log(error);
            }
        );

        this._appService.get_method(this._userService.admin_block_content_exclude_users).subscribe(
            data => {
                for (let i = 0; i < data.result.length; i++) {
                    for (let j = 0; j < this.users_list.length; j++) {
                        if (this.users_list[j]['userid'] == data.result[i]['userid']) {
                            this.block_content_routes_exclude_users.push(this.users_list[j]);
                        }
                    }
                }
                this.show_modal_loading = false;

                //  console.log("excluded users",this.block_content_routes_exclude_users);
            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h6>Server Error: " + error + "<h6>")
                // console.log(error);
            }
        );



    }
    get_blocked_excluded_members_numbers() {
        this.admin_routes1 = this.admin_routes;
        this.selected_routes_checked_numbers = [];
        this.block_content_routes_exclude_users_numbers = [];

        this._appService.get_method(this._userService.admin_block_numbers_routes).subscribe(
            data => {
                // console.log("blocked numbeers routes",data.result);
                this.block_numbers_routes = data.result;
                for (let i = 0; i < this.admin_routes1.length; i++) {

                    this.selected_routes_checked_numbers.push({ 'status': false });
                    for (let j = 0; j < this.block_numbers_routes.length; j++) {
                        if (this.admin_routes1[i]['az_routeid'] == this.block_numbers_routes[j]['az_routeid']) {

                            this.admin_routes1[i]['status'] = true;
                            this.selected_routes_checked_numbers[i]['status'] = true;
                            // this.selected_routes.push(this.admin_routes[j]['az_routeid'].toString());

                            break;
                        }
                    }
                }
                // console.log("checked numbers",this.selected_routes_checked_numbers);
                //  console.log("routes",data.result);
                //  console.log("selected routes content",this.selected_routes_checked_numbers);

            },
            error => {
                //  this.show_modal_loading=false;
                toastr.error("<h6>Server Error: " + error + "<h6>");
                // console.log(error);
            }
        );

        this._appService.get_method(this._userService.admin_block_numbers_exclude_users).subscribe(
            data => {
                for (let i = 0; i < data.result.length; i++) {
                    for (let j = 0; j < this.users_list.length; j++) {
                        if (this.users_list[j]['userid'] == data.result[i]['userid']) {
                            this.block_content_routes_exclude_users_numbers.push(this.users_list[j]);
                        }
                    }
                }
                this.show_modal_loading = false;

                // console.log("excluded numbers users",this.block_content_routes_exclude_users_numbers);
            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h6>Server Error: " + error + "<h6>");
                // console.log(error);
            }
        );



    }
    getGatewayData() {
        this.show_loading = true;
        this.gateway_list = [];
        this._appService.get_method(this._userService.admin_gateway_url).subscribe(
            data => {

                this.gateway_list = data.result.concat().reverse();
                // console.log("gateway data is here");
                // console.log(this.gateway_list);
                for (let i = 0; i < this.gateway_list.length; i++) {
                    this.gateway_list[i]['create_date'] = moment(this.gateway_list[i]['create_date']).format('DD/MM/YYYY');
                }
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;

            }
        );



    }
    getRoutesData() {

        this.routes_list = [];
        this._appService.get_method(this._userService.admin_routes_url).subscribe(
            data => {
                this.routes_list = data.result.concat();
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['az_create_date'] = moment(this.routes_list[i]['az_create_date']).format('DD-MM-YYYY');
                    //console.log(this.routes_list[i]['az_create_date']);
                }
                // console.log("routes data is here");
                // console.log(this.routes_list);
                $('#selectpicker1').selectpicker('refresh');
                $('#selectpicker2').selectpicker('refresh');





            },
            error => {
                // console.log(error);
            }
        );
    }
    getRouteData() {
        this.show_loading = true;
        this.routes_list1 = [];
        this._appService.get_method(this._userService.admin_routes_url).subscribe(
            data => {
                this.routes_list1 = data.result.concat();
                for (let i = 0; i < this.routes_list1.length; i++) {
                    this.routes_list1[i]['az_create_date'] = moment(this.routes_list1[i]['az_create_date']).format('DD-MM-YYYY');
                    //console.log(this.routes_list[i]['az_create_date']);
                }
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h6>Server error " + error + "<h6>");
            }
        );
    }

    block_content_clicked() {

        this.readFileStatus = "content";
        this.get_blocked_content();

    }





    create_new_gateway() {
        $('#create_route').modal('show');
        this.show_loading_modal = true;
        this.show_edit_gateway = false;
        this.newErrorStatusAdded = false;
        this.route_companyname = "";
        this.route_ipaddress = "";
        this.route_username = "";
        this.route_password = "";
        this.route_gatewaycode = "";
        this.route_port = "";
        this.route_rport = "";
        this.route_system_type = "";
        this.route_no_of_sessions = 0;
        this.route_dndcheck = false;
        this.route_senderId = false;
        this.route_mode = "";
        let codes = {};
        this.error_codes = [];
        this.error_codes1 = [];
        this.newErrorCodes = [];
        this._appService.get_method(this._userService.admin_err_codes).subscribe(
            data => {
                let dat = data.result;
                // console.log(data)
                let val = Object.keys(dat[0]);
                for (let j = 0; j < val.length; j++) {
                    codes = {};
                    if (val[j] != "id" && val[j] != "gateway_id") {
                        codes['status'] = val[j];
                        codes['code'] = dat[0][val[j]];
                        this.error_codes.push(codes);
                        this.error_codes1.push(codes);
                        // codes.push(dat[val[j]]);
                    }
                }
                setTimeout(() => { $('.selectpicker').selectpicker("refresh") }, 200);
                // $('#create_route').on('shown.bs.modal', function (e) {
                this.show_loading_modal = false;


                //})
                //  console.log(this.error_codes1);



            },
            error => {
                // console.log(error);
            }

        );
    }
    update_block_content() {
        if (this.block_content_edit != null || this.block_content_edit != "") {
            $('#m_modal_block_content').modal('hide');
            let data = this.block_content_object;
            // data.content=this.block_content_edit;
            //data=JSON.stringify(data);
            let content_ = JSON.stringify({ "content": this.block_content_edit });

            this._appService.patch_method(this._userService.admin_block_content + "/" + data.id, content_).subscribe(
                data => {
                    if (data.status) {
                        this.get_blocked_content();

                        // console.log(data);

                        toastr.success("<h6>" + data.message + "<h6>")
                    }

                    else {
                        toastr.error("<h6>" + data.message + "<h6>");
                    }

                },
                error => {
                    toastr.error("<h6>Server Error " + error + "<h6>");
                }
            );
        }
        else {
            return toastr.warning("Invalid Block Content");
        }

    }
    current_group() { }
    current_blockcontent(contentindex: any) {
        $('#m_modal_block_content').modal('show');
        this.block_content_object = contentindex;
        this.block_content_edit = contentindex['content'];
    }
    delete_brand(brand) {
        this._appService.delete_method(this._userService.branding + "" + brand.wid).subscribe(
            data => {
                if (data.status) {
                    this.show_loading = true;
                    this.get_brands();
                    toastr.success("<h6>" + data.message + "<h6>");
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );
    }
    delete_content(content: any) {
        let id = content.id;
        this._appService.delete_method(this._userService.admin_block_content + "/" + id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h6>" + data.message + "<h6>");
                    this.get_blocked_content();
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }


            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );

    }
    add_multiple_blockcontent() {
        this.multiple_content = this.multiple_content.trim();
        if (this.multiple_content == "") {
            return toastr.warning("<h5>No Block Content Found<h5>");
        }
        $('#m_modal_content').modal('hide');
        let data = "";
        let str = this.multiple_content.split("\n");
        for (let i = 0; i < str.length; i++) {
            data = data.concat(str[i].toString());
            data = data.concat(",");
        }
        data = data.substring(0, data.lastIndexOf(','));

        this._appService.post_method(this._userService.admin_block_content, JSON.stringify({ "content": data })).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h6>" + data.message + "<h6>");
                    this.multiple_content = "";
                    this.get_blocked_content();
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }

            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );

    }
    EditRoute(route) {
        $('#gateway_modal').modal('show');
        this.route_edit = "";
        this.route_edit_gateway = "";
        this.route_edit = route.az_rname;
        this.route_edit_id = route.az_routeid;
        $('#selectpicker_edit_route').selectpicker('refresh');

    }
    edit_route() {
        if (this.route_edit_gateway == "") {
            return toastr.warning("<h5>Select Gateway<h5>");
        }
        $('#gateway_modal').modal('hide');
        let data = {
            'route_name': this.route_edit,
            'gateway': this.route_edit_gateway,
            'status': 1
        }
        this._appService.patch_method(this._userService.routes_url + "/" + this.route_edit_id, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Successfully Updated<h5>");

                    this.route_edit_gateway = "";
                    this.getRouteData();

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );




    }
    SaveRoute() {

        let route_id;
        if (this.gatewayNameRoutes == "") {
            toastr.warning("<h5>Invalid Gateway name<h5>");

            return;
        }
        if (this.routeNameRoutes == "") {
            toastr.warning("<h5>Invalid Route Name<h5>");
            return;
        }
        this.show_loading = true;
        let data = {
            'gateway': this.gatewayNameRoutes,
            'route_name': this.routeNameRoutes,
            'status': 1
        }
        for (let i = 0; i < this.routes_list1.length; i++) {
            if (this.routes_list1[i]['az_rname'] == this.routeNameRoutes) {
                route_id = this.routes_list1[i]['az_routeid'];
                break;
            }
        }

        this._appService.patch_method(this._userService.routes_url + "/" + route_id, JSON.stringify(data)).subscribe(
            data => {
                // console.log(data);

                if (data.status) {
                    this.gatewayNameRoutes = "";
                    this.routeNameRoutes = "";
                    toastr.success("<h5>Updated Successfully<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>")
                }

                this.getRouteData();
                this.show_loading = false;
                // setTimeout(()=>{this.getRoutesData();},200);
            },
            error => {
                this.show_loading = false;
                toastr.error("<h6>Server Error" + error + "<h6>");
            }
        );
    }
    walletStatusClicked() {
        this.show_loading = true;
        this.searchTextWalletStats = "";
        this.wallet_calendar_range = "";
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 200)
        if (this.users_list.length == 0) {
            this._appService.get_method(this._userService.admin_users_list).subscribe(
                data => {
                    this.users_list = data.result;
                    this._appService.get_method(this._userService.wallet_stats).subscribe(
                        data => {
                            // console.log(data);
                            this.wallet_stat = [];
                            this.wallet_stat = data.result;
                            for (let i = 0; i < this.wallet_stat.length; i++) {
                                this.wallet_stat[i]['date'] = moment(this.wallet_stat[i]['date']).format("DD/MM/YYYY hh:mm A");
                                if (this.wallet_stat[i]['amount_type'] == "credit") {
                                    this.wallet_status_credit += this.wallet_stat[i]['amount'];
                                }
                                else {
                                    this.wallet_status_debit += this.wallet_stat[i]['amount'];
                                }
                                for (let j = 0; j < this.users_list.length; j++) {
                                    if (this.wallet_stat[i]['userid'] == this.users_list[j]['userid']) {
                                        this.wallet_stat[i]['username'] = this.users_list[j]['user_name'];

                                    }
                                    if (this.wallet_stat[i]['from_userid'] == this.users_list[j]['userid']) {
                                        this.wallet_stat[i]['from_username'] = this.users_list[j]['user_name'];
                                    }
                                }

                            }
                            try {
                                this.wallet_status_credit = this.wallet_status_credit.toFixed(2);
                                this.wallet_status_debit = this.wallet_status_debit.toFixed(2);
                            }
                            catch (e) { }
                            this.wallet_stat = this.wallet_stat.reverse();
                            this.show_loading = false;

                        },
                        error => {
                            this.show_loading = false;
                            toastr.error("<h5>Server Error " + error + "<h5>");

                        }
                    );
                    //  console.log("users list",this.users_list);
                },
                error => {
                    // console.log(error);
                }
            );
        }
        else {
            this._appService.get_method(this._userService.wallet_stats).subscribe(
                data => {
                    // console.log(data);
                    this.wallet_stat = [];
                    this.wallet_stat = data.result;
                    for (let i = 0; i < this.wallet_stat.length; i++) {
                        this.wallet_stat[i]['date'] = moment(this.wallet_stat[i]['date']).format("DD/MM/YYYY hh:mm A");
                        if (this.wallet_stat[i]['amount_type'] == "credit") {
                            this.wallet_status_credit += this.wallet_stat[i]['amount'];
                        }
                        else {
                            this.wallet_status_debit += this.wallet_stat[i]['amount'];
                        }
                        for (let j = 0; j < this.users_list.length; j++) {
                            if (this.wallet_stat[i]['userid'] == this.users_list[j]['userid']) {
                                this.wallet_stat[i]['username'] = this.users_list[j]['user_name'];

                            }
                            if (this.wallet_stat[i]['from_userid'] == this.users_list[j]['userid']) {
                                this.wallet_stat[i]['from_username'] = this.users_list[j]['user_name'];
                            }
                        }

                    }
                    try {
                    this.wallet_status_credit = this.wallet_status_credit.toFixed(2);
                        this.wallet_status_debit = this.wallet_status_debit.toFixed(2);
                    }
                    catch (e) { }
                    this.wallet_stat = this.wallet_stat.reverse();
                    this.show_loading = false;

                },
                error => {
                    this.show_loading = false;
                    toastr.error("<h5>Server Error " + error + "<h5>");

                }
            );
        }


    }
    walletStatusSearch(starttime, endtime) {
        this.show_loading = true;
        let d = {
            'start_time': starttime,
            'end_time': endtime
        }

        this._appService.post_method(this._userService.wallet_stats, JSON.stringify(d)).subscribe(
            data => {
                this.wallet_stat = [];
                this.wallet_status_credit = 0;
                this.wallet_status_debit = 0;

                this.wallet_stat = data.result.reverse();
                for (let i = 0; i < this.wallet_stat.length; i++) {
                    this.wallet_stat[i]['date'] = moment(this.wallet_stat[i]['date']).format("DD/MM/YYYY hh:mm A");
                    if (this.wallet_stat[i]['amount_type'] == "credit") {
                        this.wallet_status_credit += this.wallet_stat[i]['amount'];
                    }
                    else {
                        this.wallet_status_debit += this.wallet_stat[i]['amount'];
                    }
                    for (let j = 0; j < this.users_list.length; j++) {
                        if (this.wallet_stat[i]['userid'] == this.users_list[j]['userid']) {
                            this.wallet_stat[i]['username'] = this.users_list[j]['user_name'];

                        }
                        if (this.wallet_stat[i]['from_userid'] == this.users_list[j]['userid']) {
                            this.wallet_stat[i]['from_username'] = this.users_list[j]['user_name'];
                        }
                    }

                }
                try {
                    this.wallet_status_credit = this.wallet_status_credit.toFixed(2);
                    this.wallet_status_debit = this.wallet_status_debit.toFixed(2);
                }
                catch (e) { }
                this.show_loading = false;

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );
    }
    add_multiple_blocknumbers() {
        this.multiple_nos = this.multiple_nos.trim();
        if (this.multiple_nos == "") { return toastr.warning("<h5>No Number Found<h5>"); }
        $('#m_modal_6').modal('hide');
        let data = "";
        let str = this.multiple_nos.split("\n");
        for (let i = 0; i < str.length; i++) {
            data = data.concat(str[i].toString());
            data = data.concat(",");
        }
        //console.log(data);
        data = data.substring(0, data.lastIndexOf(','));
        //console.log(data);

        this._appService.post_method(this._userService.admin_block_numbers, JSON.stringify({ "number": data })).subscribe(
            data => {
                //  console.log(data);
                if (data.status) {
                    toastr.success("<h6>" + data.message + "<h6>");
                    this.multiple_nos = "";
                    this.blockNumbersClicked();
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }

            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");

            }
        );

    }
    EditGateway(gateway) {
        // console.log(gateway);
        $('#create_route').modal('show')
        //    this._appService.get_method(this._userService.admin_gateway_url+"/"+gateway['gateways_id']).subscribe(
        //        data=>{
        //         console.log(data);
        //        },
        //        error=>{
        //            toastr.error("Server Error: "+error);
        //        }
        //    );
        this.show_edit_gateway = true;
        this.show_loading_modal = true;
        this.create_gateway_id = gateway.id;

        this.route_companyname = gateway.company;
        this.route_ipaddress = gateway.ip;
        this.route_username = gateway.username;
        this.route_password = gateway.password;
        this.route_gatewaycode = gateway.gateway_name;
        this.route_port = gateway.sending_port;
        this.route_rport = gateway.receive_port;;

        this.route_system_type = gateway.system_type;
        this.route_no_of_sessions = gateway.no_of_sessions;
        this.route_dndcheck = gateway.dnd_check;
        this.route_senderId = gateway.is_senderid;
        this.route_mode = gateway.mode;
        //$('#route_mode1').selectpicker('refresh')

        let s = Object.keys(gateway);

        this.error_codes = [];
        this.error_codes1 = [];
        for (let i = 0; i < s.length; i++) {
            if (!this.gatewayAttributes.includes(s[i])) {
                let codes = {
                    'status': s[i],
                    'code': gateway[s[i]]
                }
                this.error_codes.push(codes);
                this.error_codes1.push(codes);
                // console.log(codes);
            }
        } this.show_loading_modal = false;
        this.selected_error_status = "";
        setTimeout(() => { $('.selectpicker').selectpicker('refresh'); }, 1000);
        // codes = {};
        // if (val[j] != "id" && val[j] != "gateway_id") {
        //     codes['status'] = val[j];
        //     codes['code'] = dat[0][val[j]];
        //     this.error_codes.push(codes);
        //     this.error_codes1.push(codes);
        //     // codes.push(dat[val[j]]);
        // }
    }
    deleteGateway(gateway) {

        this._appService.delete_method(this._userService.admin_gateway_url + "/" + gateway.id).subscribe(
            data => {

                this.getGatewayData();
                if (data.status) {
                    this.getGatewayData();
                    toastr.success("<h5>successfully Deleted<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    edit_group() {

    }

    delete_sender() {

    }
    download() {

    }

    create_route() {

        if (this.validatenewroute() == true) {
            $('#create_route').modal('hide');
            let report_type = [];
            let ecode = [];
            let new_report_type = [];
            let new_ecode = [];
            if (this.route_dndcheck) { this.route_dndcheck = 1 } else { this.route_dndcheck = 0 }
            if (this.route_senderId) { this.route_senderId = 1 } else { this.route_senderId = 0 }
            let route_data = {
                'company_name': this.route_companyname,
                'ip_address': this.route_ipaddress,
                'user_name': this.route_username,
                'password': this.route_password,
                'gateway': this.route_gatewaycode,
                'port': this.route_port,
                'receive_port': this.route_rport,
                'mode': this.route_mode,
                'system_type': this.route_system_type,
                'no_of_sessions': this.route_no_of_sessions,
                'dnd_check': this.route_dndcheck,
                'is_open': this.route_senderId,

            };
            for (let i = 0; i < this.error_codes1.length; i++) {
                report_type.push(this.error_codes1[i]['status']);
                ecode.push(this.error_codes1[i]['code'].toString());
            }
            for (let i = 0; i < this.newErrorCodes.length; i++) {
                new_report_type.push(this.newErrorCodes[i]);
                for (let j = 0; j < this.error_codes1.length; j++) {
                    if (this.error_codes1[j]['status'] == this.newErrorCodes[i]) {
                        new_ecode.push(this.error_codes1[j]['code']);
                    }
                }

            }
            if (this.show_edit_gateway) {
                route_data['report_type'] = report_type;
                route_data['e_codes'] = ecode;
                route_data['add_status'] = false;

                if (this.newErrorStatusAdded) {
                    route_data['add_status'] = true;
                    route_data['new_report_type'] = new_report_type;
                    route_data['new_report_type_codes'] = new_ecode;

                }
                this._appService.patch_method(this._userService.admin_gateway_url + "/" + this.create_gateway_id, JSON.stringify(route_data)).subscribe(
                    data => {
                        if (data.status) {
                            this.getGatewayData();
                            toastr.success("<h5>Gateway Updated<h5>");
                        }
                        else {
                            toastr.error('<h5>' + data.message + '<h5>');
                        }
                    },

                    error => {
                        toastr.error("<h5>Server Error " + error + "<h5>");
                    }
                );
            }
            else {
                route_data['report_type'] = report_type;
                route_data['e_codes'] = ecode;
                route_data['add_status'] = false;

                if (this.newErrorStatusAdded) {
                    route_data['add_status'] = true;
                    route_data['new_report_type'] = new_report_type;
                    route_data['new_report_type_codes'] = new_ecode;

                }
                this._appService.post_method(this._userService.admin_gateway_url, JSON.stringify(route_data)).subscribe(
                    data => {

                        if (data.status) {
                            this.getGatewayData();

                            toastr.success("<h5>Gateway Added Successfully<h5>");
                        }
                        else {
                            toastr.error("<h5>" + data.message + "<h5>");
                        }

                    },
                    error => {
                        toastr.error("<h5>Server Error " + error + "<h5>");
                    }
                );

            }


        }

    }
    validatenewroute() {

        this.route_valid = false;
        // if(!this.validateroutename()) 
        if (!this.validategatewaycode()) return false;//if(!this.validateroutename())
        if (!this.validateroutecompanyname()) return false;
        if (!this.validaterouteip()) return false;
        if (!this.validateSystemTye()) return false;
        if (!this.validaterouteport()) return false;
        if (!this.validaterouteReceivingPort()) return false;
        if (!this.validaterouteusername()) return false;
        if (!this.validateroutepassword()) return false;
        // if (!this.validateroutestatus()) return alert("Status is Not Selected");
        if (!this.validaterouteMode()) return toastr.warning("<h5>Select Mode<h5>");
        if (!this.validateroutenoofsessions()) return toastr.warning("<h6>Invalid Session Enter between 1 to 20<h6>");
        let v = this.validateErrorCodes();
        if (v != "__valid__") return toastr.warning("<h5>Need Error Code For " + v + "<h5>");
        this.route_valid = true;
        return this.route_valid;
    }
    validateroutenoofsessions() {
        let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        if (!arr.includes(this.route_no_of_sessions)) return false;
        return true;
    }
    validateErrorCodes() {
        for (let j = 0; j < this.error_codes1.length; j++) {
            if (this.error_codes1[j]['code'] == "") {
                return this.error_codes1[j]['status'];
            }
        }
        return "__valid__";
    }
    validaterouteMode() {
        if (this.route_mode == "") {
            return false;
        }
        return true;
    }
    validateSystemTye() {
        this.route_system_type = this.route_system_type.trim();
        if (this.route_system_type == "") {
            toastr.warning("<h5>System Type Field is Required<h5>");
            return false;
        }
        else {
            return true;

        }
    }
    validaterouteReceivingPort() {
        this.route_rport = this.route_rport.trim();
        if (this.route_rport != "") {
            if (/^[0-9]{1,}$/.test(this.route_rport)) {
                return true;
            }
            else {
                toastr.warning("<h5>Receiving port Field Should Contain Only Numbers<h5>");
                return false;
            }
        }
        else {
            toastr.warning("<h5>Receiving Field Port is Required<h5>");
            return false;
        }
    }
    validateroutename() {
        if (this.route_routename != "" && this.route_routename.length >= 3) {
            let s = /^[a-zA-Z]{1,}$/;
            if (s.test(this.route_routename)) return true;
            return false;

        }
        return false;

    }
    validaterouteip() {
        this.route_ipaddress = this.route_ipaddress.trim();
        if (this.route_ipaddress == "" || this.route_ipaddress == null) {
            toastr.warning("<h5>Ip Field Is required<h5>");
            return false;
        }
        else {
            return true;
            // let v = 0;
            // let str = this.route_ipaddress.trim().split('.');
            // if (str.length == 4) {
            //    for (let i = 0; i < str.length; i++) {
            //             //console.log(parseInt(str[i]),10)
            //             if(!/^[0-9]{1,3}$/.test(str[i])){
            //                 toastr.warning("<h5>Invalid Ip Address block<h5>"+[str[i]]);
            //                 return false;
            //             }
            //             if (parseInt(str[i]) < 0 || parseInt(str[i]) > 255) 
            //             {
            //                 toastr.warning("<h5>Invalid Ip block<h5>"+(i+1));
            //                 return false;
            //             }
            //             if (parseInt(str[i]) == 0) v++;
            //         }
            //         if (v == 4){
            //             toastr.warning("<h5>Invalid Ip address<h5>");
            //             return false;
            //         }
            //         else{
            //             return true;
            //         }
            // }
            // else{
            //     toastr.warning("<h5>Ip address format is Invalid<h5>");
            //     return false;
            // }

        }


    }
    validaterouteusername() {
        this.route_username = this.route_username.trim();
        if (this.route_username == "") {
            toastr.warning("<h5>User name Field is Required<h5>");
            return false;
        }
        else {
            return true;
            // if(this.route_username.length <3 || this.route_username.length>20) {
            //     toastr.warning("<h5>User name Field Length Should be between 3 and 20 Characters<h5>");
            //     return false;
            // }
            // else if(!/^[a-zA-Z]{1,}\w{1,}$/.test(this.route_username)){
            //     toastr.warning("<h5>User name Field Format is Invalid<h5>");
            //     return false;
            // }
            // else{
            //     return true;
            // }
        }
    }
    validateroutestatus() {
        if (this.route_status != "") return true;
        return false;

    }
    validateroutecompanyname() {
        this.route_companyname = this.route_companyname.trim();
        if (this.route_companyname == "" || this.route_companyname == null) {
            toastr.warning("<h5>Company Name Field is Required<h5>");
            return false;
        }
        else {
            if (this.route_companyname.length < 4 || this.route_companyname.length > 20) {
                toastr.warning("<h5>Company Name Field Length Should be between 4 and 20 <h5>");
                return false;

            }
            else {
                if (!/^[a-zA-Z]+$/.test(this.route_companyname.replace(/\s/g, ''))) {
                    toastr.warning("<h5>Company Name Field is Invalid<h5>");
                    return false;
                }
                else {
                    return true;
                }
            }

        }



    }
    validaterouteport() {
        this.route_port = this.route_port.trim();
        if (this.route_port != "") {
            if (/^[0-9]{1,}$/.test(this.route_port)) {
                return true;
            }
            else {
                toastr.warning("<h5>Port Number Field Should Contain Only Numbers<h5>");
                return false;
            }
        }
        else {
            toastr.warning("<h5>Port Field is Required<h5>");
            return false;

        }
    }
    validateroutepassword() {
        this.route_password = this.route_password.trim();
        if (this.route_password == "" || this.route_password == null) {
            toastr.warning("<h5>Password Field is Required<h5>");
            return false;
        }
        else {
            return true;
            // let passwordregex = /(?=.*\d{1,})(?=.*\W{1,})(?=.*\w{1,}).*$/;
            // if (!passwordregex.test(this.route_password)) {
            //     toastr.warning("<h5>Password Format is Incorrect it Should Contain Alphabet,Number and Special Character<h5>");
            //     return false;
            // }
            // else{
            //     return true;
            // }


        }


    }
    validategatewaycode() {
        this.route_gatewaycode = this.route_gatewaycode.trim();
        if (this.route_gatewaycode == "" || this.route_gatewaycode == null) {
            toastr.warning("<h5>Gateway Code Field is Required<h5>");
            return false;
        }
        else {
            let gatewayregex = /^[a-zA-Z]{1,}\w{1,}$/;
            if (!gatewayregex.test(this.route_gatewaycode)) {
                toastr.warning("<h5>Gateway Code Field Should start with ALphabet and Can contain Numbers,Alphabet or Underscore<h5>");
                return false;
            }
            else {
                return true;
            }
        }



    }
    validateFile(name: String) {
        let ext = name.substring(name.lastIndexOf('.') + 1);
        this.file_ext = ext.toLowerCase();
        if (ext.toLowerCase() == 'csv' || ext.toLowerCase() == 'txt' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'xls' || ext.toLowerCase() == 'ods') {
            return true;
        }
        return false;

    }
    readFile(files: FileList, target) {
        let arrayBuffer: any;
        let file: File = files.item(0);
        target.value = ""

        if (!this.validateFile(file.name)) {
            return toastr.warning('Selected file format is not supported.\n Only txt,csv,ods,xlxs,xls are supported currently');
        }

        // console.log(this.file_ext);
        let myReader: FileReader = new FileReader();
        myReader.onload = (e) => {
            // console.log("file reader working");
            if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods') {

                arrayBuffer = myReader.result;
                let data = new Uint8Array(arrayBuffer);
                let arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                let bstr = arr.join("");
                let workbook = XLSX.read(bstr, { type: "binary" });

                for (let j = 0; j < workbook.SheetNames.length; j++) {
                    var sheet_name = workbook.SheetNames[j];
                    /* Get worksheet */
                    var worksheet = workbook.Sheets[sheet_name];
                    //console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
                    var result = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                    let head = Object.keys(result[0]);
                    if (this.readFileStatus == "content") {
                        for (let k = 0; k < result.length; k++) {
                            this.multiple_content = this.multiple_content.concat(result[k][head[0]]);
                            this.multiple_content = this.multiple_content.concat("\n");
                        }
                    }
                    else if (this.readFileStatus == "numbers") {
                        for (let k = 0; k < result.length; k++) {
                            this.multiple_nos = this.multiple_nos.concat(result[k][head[0]]);
                            this.multiple_nos = this.multiple_nos.concat("\n");
                        }
                    }




                }

                toastr.info("<h6>File Reading Successful<h6>");
                if (this.readFileStatus == "numbers") {
                    $('#nav-home-tab').removeClass('active');
                    $('#nav-contact-tab').removeClass('active');
                    $('#nav-home-tab').addClass('active');
                    $('#nav-home').removeClass('show active');
                    $('#nav-contact').removeClass('show active');
                    $('#nav-home').addClass('show active');
                }
                else {
                    $('#nav-home1-tab').removeClass('active');
                    $('#nav-contact1-tab').removeClass('active');
                    $('#nav-home1-tab').addClass('active');
                    $('#nav-home-content').removeClass('show active');
                    $('#nav-keyword').removeClass('show active');
                    $('#nav-home-content').addClass('show active');


                }
                // console.log(this.multiple_content);
                this.make_unique();
                return true;

            }
            else if (this.file_ext == 'txt' || this.file_ext == 'csv') {
                let file_data = myReader.result;
                if (this.readFileStatus == "content") {
                    this.multiple_content = this.multiple_content.concat(file_data.toString());
                    this.multiple_content = this.multiple_content.concat('\n');
                }
                else if (this.readFileStatus == "numbers") {
                    this.multiple_nos = this.multiple_nos.concat(file_data.toString());
                    this.multiple_nos = this.multiple_nos.concat('\n');
                }
                toastr.info("<h6>File Reading Successful<h6>");
                if (this.readFileStatus == "numbers") {
                    $('#nav-home-tab').removeClass('active');
                    $('#nav-contact-tab').removeClass('active');
                    $('#nav-home-tab').addClass('active');
                    $('#nav-home').removeClass('show active');
                    $('#nav-contact').removeClass('show active');
                    $('#nav-home').addClass('show active');
                }
                else {
                    $('#nav-home1-tab').removeClass('active');
                    $('#nav-contact1-tab').removeClass('active');
                    $('#nav-home1-tab').addClass('active');
                    $('#nav-home-content').removeClass('show active');
                    $('#nav-keyword').removeClass('show active');
                    $('#nav-home-content').addClass('show active');


                }
                this.make_unique();
            }
            else {
                return toastr.warning('Selected file format is not supported.\n Only txt,csv,xlxs,xls are supported currently');
            }
            // this.update_count();

            // this.reading_data = false;
        }

        if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods')
            myReader.readAsArrayBuffer(file);
        else
            myReader.readAsText(file);

        //  console.log("multiple-content  ", this.multiple_content);

    }

    make_unique() {
        if (this.readFileStatus == "content") {
            //this.multiple_content = this.numbers.replace(/\,/g, '\n');
            let xs = this.multiple_content.split("\n");
            xs = xs.toString().split("\,");
            //console.log(xs+'after split');
            let unique_array = xs.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
            });
            // console.log(unique_array+'new msg');
            let new_msg = unique_array.toString();
            new_msg = new_msg.replace(/\,/g, '\n');
            this.multiple_content = new_msg;
        }
        else if (this.readFileStatus == "numbers") {
            let xs = this.multiple_nos.split("\n");
            xs = xs.toString().split("\,");
            //console.log(xs+'after split');
            let unique_array = xs.filter(function(elem, index, self) {
                return index == self.indexOf(elem);
            });
            // console.log(unique_array+'new msg');
            let new_msg = unique_array.toString();
            new_msg = new_msg.replace(/\,/g, '\n');
            this.multiple_nos = new_msg;
        }


    }
    refresh_clicked() {

    }
    addRoute() {
        if (this.route_name_add == "") {
            return toastr.warning("<h5>Route Name Field is Required<h5>");

        }
        // if(!/^[a-zA-Z]+$/.test(this.route_name_add))
        // {
        //     return toastr.warning("<h5>Route Name Field Should Contain Only Alphabet<h5>");
        // } 
        this.show_loading = true;

        let data = {
            'route_name': this.route_name_add
        }
        this._appService.post_method(this._userService.routes_url, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    this.route_name_add = "";
                    this.getRouteData();
                    toastr.success("<h5>Route Added Successfully<h5>");
                    this.route_name_add = "";
                    setTimeout(() => {
                        $('#selectpicker1').selectpicker('refresh');
                        $('#selectpicker2').selectpicker('refresh');
                    }, 3000);
                }
                else {
                    this.show_loading = false;

                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        )
    }
    updatekannel() {
        toastr.info("Updating Kannel Please Wait...");
        this._appService.get_method(this._userService.admin_kannel).subscribe(
            data => {
                if (data.status) {
                    toastr.success("Successfully Updated");
                }
                else { toastr.error("Error Updating kannel"); }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        )

    }
    deleteRoute(route) {
        this.show_loading = true;
        this._appService.delete_method(this._userService.routes_url + "/" + route.az_routeid).subscribe(
            data => {
                // console.log(data);
                this.getRouteData();
                //  this.getRoutesData();
                // for(let i=0;i<this.routes_list.length;i++){
                //     if(this.routes_list[i]['az_routeid']==route.az_routeid){
                //         this.routes_list.splice(i,1);
                //         break;
                //     }
                // }
                // console.log(this.routes_list);
                $('.selectpicker').selectpicker('refresh');

                if (data.status) {
                    toastr.success("<h5>Deleted Successfully<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );
    }
    kannel_start_clicked() {
        this.show_loading = true;

        this._appService.get_method(this._userService.sms_traffic_kannel_start).subscribe(
            data => {
                this.show_loading = false;
                //  console.log($('#iframe_1').src = this._serverlog.transform());

                if (data.status) {
                    toastr.success("<h5>Kannel Started<h5>");

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );
    }
    kannel_stop_clicked() {
        this.show_loading = true;

        this._appService.get_method(this._userService.sms_traffic_kannel_stop).subscribe(
            data => {
                this.show_loading = false;

                // console.log($('#iframe_1').src = this._serverlog.transform());
                // console.log(this._serverlog.Url);

                if (data.status) {
                    toastr.success("<h5>Kannel Stopped<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );

    }
    clr_sent_clicked() {
        this.show_loading = true;
        this._appService.get_method(this._userService.sms_traffic_clear_sent).subscribe(
            data => {
                this.show_loading = false;
                if (data.status) {
                    toastr.success("<h5>Sent Cleared<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );

    }
    clr_dlr_clicked() {
        this.show_loading = true;
        this._appService.get_method(this._userService.sms_traffic_clear_dlr).subscribe(
            data => {
                this.show_loading = false;
                if (data.status) {
                    toastr.success("<h5>DLR Cleared<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );

    }
    str_clicked() {
        this.show_loading = true;
        this._appService.get_method(this._userService.sms_traffic_clear_store).subscribe(
            data => {
                this.show_loading = false;
                if (data.status) {
                    toastr.success("<h5>STR Cleared<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );


    }
    server_log_clicked() {
        this.show_loading = true;
        $('#iframe_1').attr('src', "");
        setTimeout(() => {
            $('#iframe_1').attr('src', this._userService.server_log);
            this.show_loading = false;
        }, 1000);

        // console.log(this._serverlog.Url);
    }

    add_new_status() {
        if (this.new_status == "") return toastr.warning("<h5>Invalid Status<h5>");
        this.newErrorStatusAdded = true;
        for (let i = 0; i < this.error_codes1.length; i++) {
            if (this.error_codes1[i]['status'] == this.new_status) {
                return toastr.warning("<h5>Status is already present<h5>");

            }
        }
        this.error_codes1.push({ 'status': this.new_status, 'code': "" });
        this.newErrorCodes.push(this.new_status);
        setTimeout(() => { $('#error_select').selectpicker('refresh') })
        this.new_status = "";
    }

    update_error_code() {
        // this.newErrorStatusAdded=true;
        // console.log(this.selected_error_status);
        // console.log(this.new_error_code);
        if (this.selected_error_status != "") {
            // if(!/^[0-9]+$/.test(this.new_error_code)){

            //   return  toastr.warning("<h5>Invalid Error Code<h5>");
            // }
            for (let i = 0; i < this.error_codes1.length; i++) {
                // console.log("error code",this.error_codes1[i]);
                if (this.error_codes1[i]['status'] == this.selected_error_status) {
                    this.error_codes1[i]['code'] = this.new_error_code;

                }
            }
            for (let j = 0; j < this.error_codes.length; j++) {
                if (this.error_codes[j]['status'] == this.selected_error_status) this.error_codes[j]['code'] = this.new_error_code;

            }
            toastr.success("<h5>Error Code Updated<h5>");
            // console.log("new error codes",this.newErrorCodes);
            this.new_error_code = "";
            this.selected_error_status = "";

        }
        else {
            toastr.warning("<h6>Select Error Status<h6>")
        }
        setTimeout(() => { $('.selectpicker').selectpicker('refresh') }, 500)
    }
    wallet_rangeChanged() {
        if (this.wallet_calendar_range == "custom") {

            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_wallet_calendar = true;
        }
        else {
            this.show_wallet_calendar = false;
        }
    }
    eventoccured(index) {

        // console.log(this.selected_routes);
        // console.log(this.removed_routes);
        // console.log("selected checked",this.selected_routes_checked);

        if (!this.selected_routes_checked[index]['status']) {
            let index_ = this.removed_routes.indexOf(this.admin_routes[index]['az_routeid'].toString());
            if (index_ > -1) {
                this.removed_routes.splice(index_, 1);
                this.selected_routes_checked[index]['status'] = !this.selected_routes_checked[index]['status'];
                // console.log(this.selected_routes);
                // console.log(this.removed_routes);
                return
            }

            this.selected_routes.push(this.admin_routes[index]['az_routeid'].toString());
            this.selected_routes_checked[index]['status'] = !this.selected_routes_checked[index]['status'];
        }
        else {
            // console.log(this.admin_routes[index]['az_routeid'].toString());
            let index_ = this.selected_routes.indexOf(this.admin_routes[index]['az_routeid'].toString());
            if (index_ > -1) {
                this.selected_routes.splice(index_, 1);
                this.selected_routes_checked[index]['status'] = !this.selected_routes_checked[index]['status'];
                // console.log(this.selected_routes);
                // console.log(this.removed_routes);
                return
            }
            this.removed_routes.push(this.admin_routes[index]['az_routeid'].toString());
            //  console.log("pushing");
            this.selected_routes_checked[index]['status'] = !this.selected_routes_checked[index]['status'];


        }
        //    console.log(this.selected_routes);
        //     console.log(this.removed_routes);


    }
    eventoccured_numbers(index) {
        // console.log(this.selected_routes_numbers);
        // console.log(this.removed_routes_numbers);
        if (!this.selected_routes_checked_numbers[index]['status']) {
            let index_ = this.removed_routes_numbers.indexOf(this.admin_routes[index]['az_routeid'].toString());
            if (index_ > -1) {
                this.removed_routes_numbers.splice(index_, 1);
                this.selected_routes_checked_numbers[index]['status'] = !this.selected_routes_checked_numbers[index]['status'];
                // console.log(this.selected_routes_numbers);
                // console.log(this.removed_routes_numbers);
                return
            }

            this.selected_routes_numbers.push(this.admin_routes[index]['az_routeid'].toString());
            this.selected_routes_checked_numbers[index]['status'] = !this.selected_routes_checked_numbers[index]['status'];
        }
        else {
            // console.log(this.admin_routes[index]['az_routeid'].toString());
            let index_ = this.selected_routes_numbers.indexOf(this.admin_routes[index]['az_routeid'].toString());
            if (index_ > -1) {
                this.selected_routes_numbers.splice(index_, 1);
                this.selected_routes_checked_numbers[index]['status'] = !this.selected_routes_checked_numbers[index]['status'];
                // console.log(this.selected_routes_numbers);
                // console.log(this.removed_routes_numbers);
                return
            }
            this.removed_routes_numbers.push(this.admin_routes[index]['az_routeid'].toString());

            this.selected_routes_checked_numbers[index]['status'] = !this.selected_routes_checked_numbers[index]['status'];

        }
        //    console.log(this.selected_routes_numbers);
        //    console.log(this.removed_routes_numbers);

    }
    activation_clicked() {
        $("#block_content_modal").modal('show');
        this.excluded_user = "";
        this.added_excluded_users = [];
        this.removed_excluded_users = [];
        this.selected_routes = [];
        this.removed_routes = [];
        this.selected_routes_checked = [];
        this.show_modal_loading = true;
        this.get_blocked_excluded_members();

        // setTimeout(()=>{
        //     debugger
        //     $('#activation').selectpicker('refresh');

        // },100)

    }
    activation_numbers_clicked() {
        this.excluded_user_number = "";
        this.added_excluded_users_numbers = [];
        this.removed_excluded_users_numbers = [];
        this.selected_routes_numbers = [];
        this.removed_routes_numbers = [];
        this.selected_routes_checked_numbers = [];
        $("#block_nos_modal").modal('show');
        this.show_modal_loading = true;

        this.get_blocked_excluded_members_numbers();
        //  this.get_blocked_excluded_members_numbers();
        // $('#activation1').selectpicker('refresh');




    }
    delete_excluded_user(userid) {
        // console.log(userid);
        let index;
        for (let i = 0; i < this.block_content_routes_exclude_users.length; i++) {
            if (this.block_content_routes_exclude_users[i]['userid'] == userid) {
                index = i;
            }
        }
        let index_ = this.added_excluded_users.indexOf(this.block_content_routes_exclude_users[index]['userid'].toString());
        // console.log(index_);
        if (index_ > -1) {
            this.added_excluded_users.splice(index_, 1);
        }
        else {
            this.removed_excluded_users.push(this.block_content_routes_exclude_users[index]['userid'].toString());
        }
        this.block_content_routes_exclude_users.splice(index, 1);
        // console.log(this.removed_excluded_users);
        // console.log(this.added_excluded_users);
    }
    add_excluded_user() {
        if (this.excluded_user == "") return toastr.warning("Select User");
        for (let i = 0; i < this.block_content_routes_exclude_users.length; i++) {
            if (this.block_content_routes_exclude_users[i]['userid'] == this.users_list[this.excluded_user]['userid']) {
                return toastr.warning("<h5>User is Already Present in the list<h5>");
            }
        }
        this.added_excluded_users.push(this.users_list[this.excluded_user]['userid'].toString());
        this.block_content_routes_exclude_users.push(this.users_list[this.excluded_user]);
        // console.log(this.removed_excluded_users);
        // console.log(this.added_excluded_users);
    }
    delete_excluded_user_number(userid) {
        //console.log(userid);
        let index;
        for (let i = 0; i < this.block_content_routes_exclude_users_numbers.length; i++) {
            if (this.block_content_routes_exclude_users_numbers[i]['userid'] == userid) {
                index = i;
            }
        }
        let index_ = this.added_excluded_users_numbers.indexOf(this.block_content_routes_exclude_users_numbers[index]['userid'].toString());
        // console.log(index_);
        if (index_ > -1) {
            this.added_excluded_users_numbers.splice(index_, 1);
        }
        else {
            this.removed_excluded_users_numbers.push(this.block_content_routes_exclude_users_numbers[index]['userid'].toString());
        }
        this.block_content_routes_exclude_users_numbers.splice(index, 1);
        // console.log("numbers",this.removed_excluded_users_numbers);
        // console.log("numbers",this.added_excluded_users_numbers);
    }
    add_excluded_user_number() {
        // console.log(this.excluded_user_number);
        // console.log(this.users_list[this.excluded_user_number]['userid']);
        if (this.excluded_user_number == "") return toastr.warning("Select User");
        for (let i = 0; i < this.block_content_routes_exclude_users_numbers.length; i++) {
            if (this.block_content_routes_exclude_users_numbers[i]['userid'] == this.users_list[this.excluded_user_number]['userid']) {
                return toastr.warning("<h5>User is Already Present in the list<h5>");
            }
        }
        let index = this.removed_excluded_users_numbers.indexOf(this.users_list[this.excluded_user_number]['userid'].toString());
        if (index > -1) {
            this.removed_excluded_users_numbers.splice(index, 1);
        }
        else {
            this.added_excluded_users_numbers.push(this.users_list[this.excluded_user_number]['userid'].toString());

        }
        this.block_content_routes_exclude_users_numbers.push(this.users_list[this.excluded_user_number]);
        // console.log("numbers",this.removed_excluded_users_numbers);
        // console.log("numbers",this.added_excluded_users_numbers);

    }
    Update_excluded_users() {
        //    console.log("added routes",this.selected_routes);
        //     console.log("removed routes",this.removed_routes);
        //     console.log("added users",this.added_excluded_users);
        //     console.log("removed users",this.removed_excluded_users);
        $('#block_content_modal').modal('hide');
        let routes = {
            'added_routes': this.selected_routes,
            'remove_routes': this.removed_routes
        }
        let users = {
            'added_users': this.added_excluded_users,
            'remove_users': this.removed_excluded_users
        }
        if (routes['added_routes'].length != 0 || routes['remove_routes'].length != 0) {
            this._appService.patch_method(this._userService.admin_block_content_routes, JSON.stringify(routes)).subscribe(
                data => {
                    if (data.status) {
                        toastr.success("<h6>Routes " + data.message + "<h6>");
                    }
                    else {
                        toastr.error("<h6>Routes " + data.message + "<h6>")
                    }
                    // console.log(data);
                },
                error => {
                    toastr.error("<h6> Server Error:Routes Updation " + error + "<h6>");
                }
            );
        }
        else {
            toastr.success("<h6>Routes Updated Successfully<h6>");
        }

        if (users['added_users'].length != 0 || users['remove_users'].length != 0) {
            this._appService.patch_method(this._userService.admin_block_content_exclude_users, JSON.stringify(users)).subscribe(
                data => {
                    if (data.status) {
                        toastr.success("<h6>Users " + data.message + "<h6>");
                    }
                    else {
                        toastr.error("<h6>Users " + data.message + "<h6>")
                    }
                    // console.log(data);
                },
                error => {
                    toastr.error("<h6> Server Error:Users " + error + "<h6>");
                }
            );
        }
        else {
            toastr.success("<h6>Users Updated Successfully<h6>");
        }



        this.activation_clicked();

    }
    Update_excluded_users_numbers() {
        //    console.log("added routes",this.selected_routes_numbers);
        //     console.log("removed routes",this.removed_routes_numbers);
        //     console.log("added users",this.added_excluded_users_numbers);
        //     console.log("removed users",this.removed_excluded_users_numbers);
        $('#block_nos_modal').modal('hide');
        let routes = {
            'added_routes': this.selected_routes_numbers,
            'remove_routes': this.removed_routes_numbers
        }
        let users = {
            'added_users': this.added_excluded_users_numbers,
            'remove_users': this.removed_excluded_users_numbers
        }
        if (routes['added_routes'].length != 0 || routes['remove_routes'].length != 0) {
            this._appService.patch_method(this._userService.admin_block_numbers_routes, JSON.stringify(routes)).subscribe(
                data => {
                    if (data.status) {
                        toastr.success("<h6>Routes " + data.message + "<h6>");
                    }
                    else {
                        toastr.error("<h6>routes " + data.message + "<h6>")
                    }
                    // console.log(data);
                },
                error => {
                    this.show_modal_loading = false;
                    toastr.error("<h6> Server Error:Routes " + error + "<h6>");
                }
            );
        }
        else {
            toastr.success("<h5>Routes Updated Successfully<h5>");
        }

        if (users['added_users'].length != 0 || users['remove_users'].length != 0) {
            this._appService.patch_method(this._userService.admin_block_numbers_exclude_users, JSON.stringify(users)).subscribe(
                data => {
                    if (data.status) {
                        this.show_modal_loading = false;
                        toastr.success("<h6>Users " + data.message + "<h6>");
                    }
                    else {
                        this.show_modal_loading = false;
                        toastr.error("<h6>Users " + data.message + "<h6>")
                    }
                    // console.log(data);
                },
                error => {
                    this.show_modal_loading = false;
                    toastr.error("<h6> Server Error:Users " + error + "<h6>");
                }
            );

        }

        else {
            toastr.success("<h5>Users Updated Successfully<h5>");
            this.show_modal_loading = false;
        }
        this.activation_numbers_clicked();

    }
    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        if (this.wallet_calendar_range == "") return;
        else if (this.wallet_calendar_range == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.wallet_calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.wallet_calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.wallet_calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.wallet_calendar_range == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
            // this.from_date=moment(this.from_date,"MM/DD/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"MM/DD/YYYY").format('DD/MM/YYYY');
        }
        this.walletStatusSearch(this.from_date, this.to_date);

    }
    currentBlockNumber(number) {
        $('#m_modal_3').modal('show');
        this.current_blocknumber = number;
        this.block_num_edit = number.numbers;

    }
    update_blocknum() {
        if (this.block_num_edit == "") return toastr.warning("<h5>Block Number is Empty<h5>");
        if (!/^[0-9]{10}$/.test(this.block_num_edit)) return toastr.warning("<h5>Block Number is Invalid<h5>");
        $('#m_modal_3').modal('hide');
        this._appService.patch_method(this._userService.admin_block_numbers + "/" + this.current_blocknumber.id, JSON.stringify({ 'number': this.block_num_edit })).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h6>" + data.message + "<h6>");
                    this.blockNumbersClicked();
                    // console.log(this.block_numbers);
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }
                // console.log(data);
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        )
    }

    deleteCurrentBlockNumber(number) {
        //console.log(number);
        let data = {
            'number': number.numbers
        }
        this._appService.delete_method(this._userService.admin_block_numbers + "/" + number.id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h6>" + data.message + "<h6>");
                    this.blockNumbersClicked();
                }
                else {
                    toastr.error("<h6>" + data.message + "<h6>");
                }
                // console.log(data);
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        )

    }
    blockNumbersClicked() {
        this.readFileStatus = "numbers";
        this.show_loading = true;
        this._appService.get_method(this._userService.admin_block_numbers).subscribe(
            data => {
                // console.log(data);
                this.block_numbers = data.result;
                for (let i = 0; i < this.block_numbers.length; i++) {
                    for (let j = i + 1; j < this.block_numbers.length; j++) {
                        if (moment(this.block_numbers[i]['added_date']) > moment(this.block_numbers[j]['added_date'])) {
                            let temp = this.block_numbers[i];
                            this.block_numbers[i] = this.block_numbers[j];
                            this.block_numbers[j] = temp;

                        }
                    }
                }
                this.block_numbers = this.block_numbers.reverse();
                for (let i = 0; i < this.block_numbers.length; i++) {
                    this.block_numbers[i]['added_date'] = moment(this.block_numbers[i]['added_date']).format("DD-MM-YYYY hh:mm:ss A");
                }

                this.show_loading = false;
            },
            error => {
                // console.log(error);
            }
        );
    }
    brandingClicked(brand) {
        $('#branding_modal').modal('show');
        this.brand_website = brand.domain_name;
        this.color = brand.colour;
        // this.brand_colour=brand.colour;
        this.brand_sitename = brand.website;
        this.brand_favicon = "Choose a Favicon";
        this.brand_logo = "Choose a Logo";
        this.brand_title = brand.title;
        this.brand_user_name = brand.user_name;
        this.brand_wid = brand.wid;
        if (brand.logo != "" && brand.logo != null) {
            this.brand_logo = "logo is Present";
        }
        if (brand.favicon != "" && brand.favicon != null) {
            this.brand_favicon = "Favicon is Present";
        }

    }
    logo_file(files) {
        if (files[0] == null) {
            this.brand_logo = "";
            return toastr.warning("<h5>Invalid File<h5>");
        }
        let ar = files[0].type.split('/');
        if (ar[0] == 'image') this.brand_logo = files[0];
        else return toastr.warning("<h5>Only Image Files are allowed<h5>");
        // console.log("logo file",this.brand_logo);

    }
    favicon_file(files) {
        if (files[0] == null) {
            this.brand_favicon = "";
            return toastr.warning("<h5>Invalid File<h5>");
        }
        let ar = files[0].type.split('/');
        if (ar[0] == 'image') this.brand_favicon = files[0];
        else return toastr.warning("<h5>Only Image Files allowed<h5>");
        //console.log("favicon file",this.brand_favicon);
    }
    brandingSaveChanges() {
        if (this.validateBranding() == true) {
            $('#branding_modal').modal('hide');
            const data = new FormData()
            data.append('domain_name', this.brand_website);
            data.append('title', this.brand_title);
            data.append('website', this.brand_sitename);
            data.append('logo', this.brand_logo);
            data.append('favicon', this.brand_favicon);
            data.append('colour', $('#colorpicker').css('color'));

            this.savingBrand = true;
            //console.log("in patch branding",this.brand_logo,this.brand_favicon);
            this._appService.patch_file_method(this._userService.branding + this.brand_wid, data).subscribe(
                data => {
                    // console.log(data);
                    if (data.status) {

                        this.get_brands();
                        toastr.success("<h5>Branding Successfully Updated<h5>");
                    }
                    else {

                        toastr.error("<h5>" + data.message + "<h5>");
                    }
                    this.savingBrand = false;
                },
                error => {

                    this.savingBrand = false;
                    toastr.error("<h5>Server Error<h5>");
                }
            );
        }

    }
    validateBranding() {
        let valid = false;
        if (!this.validateBrandingWebsite()) return toastr.warning("<h5>Invalid Domain Name<h3>");
        if (!this.validateBrandingSiteTitle()) return toastr.warning("<h5>Invalid Site Title<h5>");
        if (!this.validateBrandingSiteName()) return toastr.warning("<h5>Invalid Site Name<h5>");
        //    if(!this.validateBrandColor())return toastr.warning("<h5>please Choose color<h5>");
        if (!this.validateBrandIcon()) return toastr.warning("<h5>Please Upload Icon<h5>");
        if (!this.validateBrandFavicon()) return toastr.warning("<h5>Please Upload Favicon<h5>");

        valid = true;
        return valid;
    }
    validateBrandingWebsite() {
        this.brand_website = this.brand_website.trim();
        if (this.brand_website == "" || !/^(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm.test(this.brand_website)) {
            return false;
        }
        // if(/^[a-zA-Z]{4,}$/.test(web))return true;
        return true;

    }
    validateBrandingSiteTitle() {
        this.brand_title = this.brand_title.trim();
        if (this.brand_title == "" || !/^[a-zA-Z]+$/.test(this.brand_title.replace(/\s/g, ''))) {
            return false;
        }
        let web = this.brand_title.split(' ').join("");
        if (/^[a-zA-Z]{4,}$/.test(web)) return true;
        return false;
    }
    validateBrandColor() {
        this.brand_colour = this.brand_colour.trim();
        if (this.brand_colour == "") return false;
        return true;
    }
    validateBrandingSiteName() {
        this.brand_sitename = this.brand_sitename.trim();
        if (this.brand_sitename == "" || !/^[a-zA-Z]+$/.test(this.brand_sitename.replace(/\s/g, ''))) return false;

        // if(/^[a-zA-Z]{4,}$/.test(web))return true;
        return true;

    }
    validateBrandIcon() {
        if (this.brand_logo == "Logo is Present") return true;
        if (typeof this.brand_logo === "undefined" || this.brand_logo == "") return false;
        return true;
    }
    validateBrandFavicon() {
        if (this.brand_favicon == "Favicon is Present") return true;
        if (typeof this.brand_favicon === "undefined" || this.brand_favicon == "") return false;
        return true;
    }
    openBlockNumberModal() {
        $('#m_modal_6').modal('show');
        this.multiple_nos = "";
    }
    openBlockContentModal() {
        this.multiple_content = "";
        $('#m_modal_content').modal('show');
    }
}
