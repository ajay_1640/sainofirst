import { Component, OnInit } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { AppService } from '../../../../../app.service';
import { CookieService } from 'ngx-cookie-service';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
declare var $;
declare let toastr;
declare var moment: any;
@Component({
    selector: 'app-live-status',
    templateUrl: "./live-status.component.html",
    styleUrls: ['./live-status.css']
})
export class LiveStatusComponent implements OnInit {
    p: number = 1;
    block_list = "";
    live_status = [];
    // gateway_list: any[]= [
    //     {
    //       "id": 41,
    //       "gateway_name": "sfTRANS"
    //     },
    //     {
    //       "id": 40,
    //       "gateway_name": "sfpromotrans"
    //     },
    //     {
    //       "id": 39,
    //       "gateway_name": "newgateway"
    //     },
    //     {
    //       "id": 38,
    //       "gateway_name": "A12345asf"
    //     },
    //     {
    //       "id": 37,
    //       "gateway_name": "asdfghj"
    //     },
    //     {
    //       "id": 35,
    //       "gateway_name": "Test500"
    //     }
    //   ];
    che_list: any = [
        {
            "id": 41,
            "gateway_name": "sfTRANS"
        },

    ];
    gatewayNameRoutes = "";
    show_loading = false;
    total = 0;
    Delivered = 0;
    msgCredit = 0.0;
    Submitted = 0;
    Other = 0;
    DND = 0;
    calendar_range = "";
    show_calendar = false;
    dlr = 0;
    MsgCredit: any = 0;
    DeliveredPercent = 0;
    SubmittedPercent = 0;
    OtherPercent = 0;
    DNDPercent = 0;
    dlrPercent = 0;
    userid = null;
    constructor(private _script: ScriptLoaderService, private _cookieService: CookieService, private _appService: AppService, private _userService: UserService) {
        this.che_list = []
    }

    ngOnInit() {
        this.userid = this._cookieService.get('userid')

        this._script.loadScripts('app-live-status',
            ['assets/demo/demo3/base/toastr.js']);

        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.getGatewayData();
        this.show_loading = true;
        this.get_live_status();

    }
    ngAfterViewInit() {
        this.getGatewayData();
    }
    from_date = "";
    to_date = "";
    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range == "") return toastr.warning("<h5>Choose Dates<h5>");
        else if (this.calendar_range == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {
            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
        }
        // this.show_status = false;
        // this.downloadReport(this.from_date, this.to_date);

        // console.log(this.from_date,this.to_date);
        this.show_loading = true;
        this.live_status = [];
        let data = {
            'start_time': this.from_date,
            'end_time': this.to_date
        }
        if (this.userid == '1') {
            // if(this.gatewayNameRoutes=="" || this.gatewayNameRoutes==null){
            //     // return toastr.warning("<h5>Choose Gateway<h5>");
            // }
            data['gateway'] = this.gatewayNameRoutes;
        }
        this._appService.post_method(this._userService.profile_live_status, JSON.stringify(data)).subscribe(
            data => {
                this.live_status = data.data;
                this.show_loading = false;
                this.DeliveredPercent = 0;
                this.SubmittedPercent = 0;
                this.OtherPercent = 0;
                this.DNDPercent = 0;
                this.dlrPercent = 0;
                this.MsgCredit = 0;
                this.total = 0;
                this.Delivered = 0;
                this.Submitted = 0;
                this.Other = 0;
                this.DND = 0;
                this.dlr = 0;
                // console.log(data.data);
                for (let i = 0; i < this.live_status.length; i++) {
                    this.total += this.live_status[i]['tot'];
                    this.Delivered += this.live_status[i]['Delivered'];
                    this.Submitted += this.live_status[i]['Submitted'];
                    this.Other += this.live_status[i]['Other'];
                    this.DND += this.live_status[i]['DND'];
                    this.dlr += this.live_status[i]['Actual DLR'];
                    this.MsgCredit += this.live_status[i]['msgcredit'];
                    this.live_status[i]['actualDlr'] = this.live_status[i]['Actual DLR'];
                    this.live_status[i]['Delivered_percent'] = ((this.live_status[i]['Delivered'] / this.live_status[i]['tot']) * 100).toFixed(2);
                    this.live_status[i]['Submitted_percent'] = ((this.live_status[i]['Submitted'] / this.live_status[i]['tot']) * 100).toFixed(2);
                    this.live_status[i]['Other_percent'] = ((this.live_status[i]['Other'] / this.live_status[i]['tot']) * 100).toFixed(2);
                    this.live_status[i]['DND_percent'] = ((this.live_status[i]['DND'] / this.live_status[i]['tot']) * 100).toFixed(2);
                }
                this.MsgCredit = parseInt(this.MsgCredit).toFixed(2);
                if (this.live_status.length != 0) {
                    this.DeliveredPercent = parseInt(((this.Delivered / this.total) * 100).toFixed(2));
                    this.SubmittedPercent = parseInt(((this.Submitted / this.total) * 100).toFixed(2));
                    this.OtherPercent = parseInt(((this.Other / this.total) * 100).toFixed(2));
                    this.DNDPercent = parseInt(((this.DND / this.total) * 100).toFixed(2));
                    this.dlrPercent = parseInt(((this.dlr / this.total) * 100).toFixed(2));
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );

    }
    get_live_status() {
        this.show_loading = true;
        this.live_status = [];
        this._appService.get_method(this._userService.profile_live_status).subscribe(
            data => {
                this.live_status = data.data;
                this.show_loading = false;
                this.DeliveredPercent = 0;
                this.SubmittedPercent = 0;
                this.OtherPercent = 0;
                this.DNDPercent = 0;
                this.dlrPercent = 0;
                this.MsgCredit = 0;
                // console.log(data.data);
                for (let i = 0; i < this.live_status.length; i++) {
                    this.total += this.live_status[i]['tot'];
                    this.Delivered += this.live_status[i]['Delivered'];
                    this.Submitted += this.live_status[i]['Submitted'];
                    this.Other += this.live_status[i]['Other'];
                    this.DND += this.live_status[i]['DND'];
                    this.dlr += this.live_status[i]['Actual DLR'];
                    this.MsgCredit += this.live_status[i]['msgcredit'];
                    this.live_status[i]['actualDlr'] = this.live_status[i]['Actual DLR'];
                    this.live_status[i]['Delivered_percent'] = ((this.live_status[i]['Delivered'] / this.live_status[i]['tot']) * 100).toFixed(2);
                    this.live_status[i]['Submitted_percent'] = ((this.live_status[i]['Submitted'] / this.live_status[i]['tot']) * 100).toFixed(2);
                    this.live_status[i]['Other_percent'] = ((this.live_status[i]['Other'] / this.live_status[i]['tot']) * 100).toFixed(2);
                    this.live_status[i]['DND_percent'] = ((this.live_status[i]['DND'] / this.live_status[i]['tot']) * 100).toFixed(2);
                }
                this.MsgCredit = parseInt(this.MsgCredit).toFixed(2);
                if (this.live_status.length != 0) {
                    this.DeliveredPercent = parseInt(((this.Delivered / this.total) * 100).toFixed(2));
                    this.SubmittedPercent = parseInt(((this.Submitted / this.total) * 100).toFixed(2));
                    this.OtherPercent = parseInt(((this.Other / this.total) * 100).toFixed(2));
                    this.DNDPercent = parseInt(((this.DND / this.total) * 100).toFixed(2));
                    this.dlrPercent = parseInt(((this.dlr / this.total) * 100).toFixed(2));
                }
            },
            error => {
                this.show_loading = false;
                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        );
    }
    refreshData() {
        this.get_live_status();
    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }
    getGatewayData() {
        this.show_loading = true;
        // this.gateway_list = [];
        let that = this;
        this._appService.get_method(this._userService.admin_gateway_names_url).subscribe(
            data => {
                this.che_list = []
                this.che_list = data.result.slice();

                this._script.loadScripts('app-live-status',
                    ['assets/demo/demo3/base/bootstrap-select.js']);
                // console.log("gateway data is here");
                // console.log("----------",this.gateway_list);
                //debugger
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;

            }
        );
    }
}
