import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../../app.service';
import { UserService } from '../../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { Router } from '@angular/router';
import { PreviousrouteService } from '../previousroute.service';
import { ProfileComponent } from '../../profile.component';
declare let toastr;
declare let moment;
declare let $;
@Component({
    selector: 'app-api',
    templateUrl: './api.component.html',
    styles: []
})
export class ApiComponent implements OnInit {
    show_loading = false;
    p = 1;
    apikey = "";
    apis = [];
    showBackButton = true;
    previousUrl = "/app/profile/api";
    selectedapps = [];
    selectedappsedit = [];
    editApiKey = false;
    apps = [

        // {'app':"Digital Marketing",'val':"digital_marketing"},
        // {'app':"SEO",'val':"seo"},
        // {'app':"Chat Bots",'val':"chat_bots"},
        // {'app':"Inter SMS",'val':"inter_sms"}

    ]
    apps1 = [
        'bulk_sms', 'bulk_voice', 'ivr_toll', 'missed_call', 'short_code', 'whatsapp'
    ]
    apps1values = {
        'bulk_sms': "Bulk SMS",
        'bulk_voice': "Bulk Voice",
        'ivr_toll': "IVR Toll",
        'missed_call': "Missed Call",
        'short_code': "Short Code",
        'whatsapp': 'Whatsapp'
        //  'digital_marketing': "Digital Marketing",
        //  'seo': "SEO",
        //  'chat_bots': "Chat Bots",
        //  'inter_sms': "Inter SMS"
    }
    apppermissions = [];
    constructor(private _userService: UserService, private _profileComponent: ProfileComponent, private _appService: AppService, private _script: ScriptLoaderService, private _previousroute: PreviousrouteService, private router: Router) {

    }

    ngOnInit() {
        this._script.loadScripts('app-api',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // this._previousroute.getPreviousUrl();
        this.previousUrl = this._previousroute.previousUrl;
        if (this.previousUrl == '/app/profile/api' || this.previousUrl=='/') {
            this.showBackButton = false;
        }
        this.getApiKeys();
        this.appDetails();
    }
    goBack() {
        this.router.navigate([this.previousUrl])
        this._profileComponent.pathSelect();
    }
    appDetails() {
        this._appService.get_method(this._userService.apps).subscribe(
            data => {
                //console.log("app data",data);
                this.apppermissions = [];
                this.apppermissions = data.apps.split(",");
                // console.log("app permissions",this.apppermissions)
                if (this.apppermissions.includes('bulk_sms')) {
                    this.apps.push({ 'app': "Bulk SMS", 'val': "bulk_sms" });
                }
                if (this.apppermissions.includes('bulk_voice')) {
                    this.apps.push({ 'app': "Bulk Voice", 'val': "bulk_voice" });
                }
                if (this.apppermissions.includes('ivr_toll')) {
                    this.apps.push({ 'app': "IVR Toll", 'val': "ivr_toll" });
                }
                if (this.apppermissions.includes('missed_call')) {
                    this.apps.push({ 'app': "Missed Call", 'val': "missed_call" });
                }
                if (this.apppermissions.includes('short_code')) {
                    this.apps.push({ 'app': "Short Code", 'val': "short_code" });
                }
                if (this.apppermissions.includes('whatsapp')) {
                    this.apps.push({ 'app': "Whatsapp", 'val': "whatsapp" });
                }
            },
            error => {
                // console.log(error);
            }
        )
    }
    getApiKeys() {
        this.show_loading = true;
        this._appService.get_method(this._userService.apiKeys).subscribe(
            data => {
                //console.log(data);
                if (data.status) {
                    this.apis = data.result.reverse();
                    // console.log(this.apis);       
                    for (let i = 0; i < this.apis.length; i++) {
                        this.apis[i]['created_time'] = moment(this.apis[i]['created_time']).format('DD/MM/YYYY hh:mm A');
                        let stringarr = [];
                        let string = this.apis[i]['api_key'];
                        for (let j = 0; j < string.length; j += 30) {
                            stringarr.push(string.substring(j, Math.min((j + 30), string.length)));
                        }
                        this.apis[i]['api_key'] = stringarr.join('\n');

                    }
                    for (let i = 0; i < this.apis.length; i++) {
                        let apps = this.apis[i]['apps'].split(',');
                        let appstr = "";
                        for (let j = 1; j <= apps.length; j++) {
                            if (this.apps1.includes(apps[j - 1])) {
                                appstr += this.apps1values[apps[j - 1]] + ",";
                            }
                            if (j % 3 == 0 && apps.length % 3 != 0) { appstr += '\n'; }
                        }
                        this.apis[i]['apps1'] = appstr.substring(0, appstr.length - 1);
                    }
                    this.show_loading = false;
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                toastr.error("<h5>Server Error: " + error + "<h5>");
                // console.log(error);
            }
        );
    }

    createApi() {
        this.selectedapps = [];
        //console.log("hii");
        $('#createapi').modal('show');
        setTimeout(() => {
            $('#apppicker').selectpicker('refresh');
            this.selectedapps = []

        }, 200)


    }
    createApiKey() {
        if (this.selectedapps.length == 0) {
            toastr.warning("<h5>Select Apps<h5>");
            return;
        }
        // console.log(this.selectedapps);
        $('#createapi').modal('hide');
        this._appService.post_method(this._userService.apiKeys, JSON.stringify({ 'apps': this.selectedapps.join(',') })).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.getApiKeys();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h6>" + error + "<h6>");
            }
        );

    }
    currentApi(api) {
        this.selectedappsedit = api.apps.split(',');
        $('#editapi').modal('show');
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 200)
    }
    deleteapi(api) {
        this._appService.delete_method(this._userService.apiKeys + "/" + api.id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.getApiKeys();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h6>" + error + "<h6>");
            });
    }
    updateapi() {
        if (this.selectedappsedit.length == 0) {
            toastr.warning("<h5>Select Apps<h5>");
            return;
        }
        // this._appService.patch_method(this._userService.apiKeys+"/")
        $('#editapi').modal('hide');
    }
}
