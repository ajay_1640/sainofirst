import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ManageCustomersComponent } from './manage-customers/manage-customers.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { LiveStatusComponent } from './live-status/live-status.component';
import { SenderIdApprovalComponent } from './sender-id-approval/sender-id-approval.component';
import { MgmtkComponent } from './mgmtk/mgmtk.component';
import { TranHistoryComponent } from './tran-history/tran-history.component';
import { WalletComponent } from './wallet/wallet.component';
import { KycComponent } from './kyc/kyc.component';
import { AlertsComponent } from './alerts/alerts.component';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { PhonenoComponent } from './my-profile/phoneno/phoneno.component';
import { ServerlogPipe } from './mgmtk/serverlog.pipe';
import { ColorPickerModule } from 'ngx-color-picker';
import { Permission } from './permission';
import { ApiComponent } from './api/api/api.component';
import { SmppComponent } from './smpp/smpp.component';
import { SmppModule } from './smpp/smpp.module';


const routes: Routes = [
    {
        "path": "",
        "component": ProfileComponent,
        "children": [
            {
                "path": "",
                "component": MyProfileComponent,

            },
            {
                "path": "transaction-history",
                "component": TranHistoryComponent,
            },
            {
                "path": "kyc",
                "component": KycComponent,
                'canActivate': [Permission]
            },
            {
                "path": "alerts",
                "component": AlertsComponent,
            },
            {
                "path": "api",
                "component": ApiComponent,
            },
            {
                "path": "wallet",
                "component": WalletComponent,
            },
            {
                "path": "manage-customers",
                "component": ManageCustomersComponent,
                'canActivate': [Permission]
            },

            {
                "path": "live-status",
                "component": LiveStatusComponent,
                'canActivate': [Permission]
            },
            {
                "path": "sender-id-approval",
                "component": SenderIdApprovalComponent,
                'canActivate': [Permission]
            },
            {
                "path": "mgmtk",
                "component": MgmtkComponent,
                'canActivate': [Permission]
            },
            {
                "path": "smpp",
                "component": SmppComponent,
                'canActivate': [Permission]
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,SmppModule ,FormsModule,ReactiveFormsModule, NgxPaginationModule,
        AngularMultiSelectModule,
        Ng2SearchPipeModule,
        ColorPickerModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        })
        
    ], exports: [
        RouterModule
    ], declarations: [
        ProfileComponent,
        ManageCustomersComponent,
        MyProfileComponent,
        LiveStatusComponent,
        SenderIdApprovalComponent,
        MgmtkComponent,
        TranHistoryComponent,
        WalletComponent,
        KycComponent,
        AlertsComponent,
        PhonenoComponent,
        ServerlogPipe,
        ApiComponent,
        SmppComponent,

    ],
    providers: [ServerlogPipe, Permission]
})
export class ProfileModule {



}
