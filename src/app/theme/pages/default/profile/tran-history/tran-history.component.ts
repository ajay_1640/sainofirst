import { Component, OnInit, ModuleWithComponentFactories } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { AppService } from '../../../../../app.service';
import { ThrowStmt } from '@angular/compiler';
import { toBase64String } from '@angular/compiler/src/output/source_map';
declare var $, jQuery: any;
declare var moment: any;
declare let toastr;
@Component({
    selector: 'app-tran-history',
    templateUrl: './tran-history.component.html',
    styles: []
})
export class TranHistoryComponent implements OnInit {

    // mis_url = "http://35.238.188.102/saino-first/api/reports/mis";
    mis_list: any;
    ar: any;
    from_date = '';
    to_date = '';
    transaction_history = [];
    calendar_range = "";
    show_calendar = false;
    show_loading = false;
    credit = /^credit$/i;
    debit = /^debit$/i;
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }

    dateRange;
    p: number = 1;
    ngOnInit() {
        this.calendar_range = "";
        this._script.loadScripts('app-tran-history',
            ['assets/demo/demo3/base/bootstrap-select.js']);

        this._script.loadScripts('app-tran-history',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        this._script.loadScripts('app-tran-history',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        $('#sandbox-container .input-daterange').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });

        // $('.input-daterange input').each(function() {
        //     $(this).datepicker('clearDates');
        // });

        // $(function() {

        //     var start = moment().subtract(29, 'days');
        //     var end = moment();


        //     function cb(start, end) {

        //         $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));

        //     }

        //     $('#reportrange').daterangepicker({
        //         startDate: start,
        //         endDate: end,
        //         ranges: {
        //             'Today': [moment(), moment()],
        //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //             'This Month': [moment().startOf('month'), moment().endOf('month')],
        //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //         }
        //     }, cb);


        //     cb(start, end);

        // });

        // this.dateRange = $('#reportrange span').text();
        // this.ar = this.dateRange.split(' - ');
        // this.fromDate = this.ar[0];
        // this.toDate = this.ar[1];
        // this.getTransactionData();
        this.show_loading = true;
        this.getTran_data();

    }
    getTran_data() {
        this._appService.get_method(this._userService.profile_transaction_history).subscribe(
            data => {
                this.transaction_history = data.data;
                for (let i = 0; i < this.transaction_history.length; i++) {
                    this.transaction_history[i]['date'] = moment(this.transaction_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");
                }
                this.transaction_history = this.transaction_history.reverse();
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    getTransactionData(startdate, enddate) {
        this.show_loading = true;
        let data_send = JSON.stringify({
            'start_time': startdate,
            'end_time': enddate
        });
        this.transaction_history = [];
        this._appService.post_method(this._userService.profile_transaction_history, data_send).subscribe(
            data => {
                try {
                    this.transaction_history = data.data;
                    for (let i = 0; i < this.transaction_history.length; i++) {
                        this.transaction_history[i]['date'] = moment(this.transaction_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");

                    }
                    this.transaction_history = this.transaction_history.reverse();
                }
                catch (e) { }
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    search_custom_date() {

        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range == "") return;
        else if (this.calendar_range == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("Choose Dates");
            // this.from_date=moment(this.from_date,"MM/DD/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"MM/DD/YYYY").format('DD/MM/YYYY');
        }
        this.getTransactionData(this.from_date, this.to_date);

    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }
    }
}
