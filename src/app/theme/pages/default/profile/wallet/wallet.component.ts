import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

declare let $;
declare let toastr;
@Component({
    selector: 'app-wallet',
    templateUrl: "./wallet.component.html",
    styles: [],
})
export class WalletComponent implements OnInit {
    wallet_balance: any = "";
    amount: number = 0;
    tax: number = 0;
    total_amount: number = 0;
    show_loading = false;
    constructor(private _appService: AppService, private _userService: UserService, private _script: ScriptLoaderService) { }

    ngOnInit() {
        this._script.loadScripts('app-live-status',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        this._appService.get_method(this._userService.wallet_url).subscribe(data => {
            this.wallet_balance = data.data.balance;
            try { this.wallet_balance = this.wallet_balance.toFixed(2) }
            catch (e) { }
        }
        );
        // console.log(this.wallet_balance);
    }

    calc() {
        this.tax = (.18 * this.amount);
        this.total_amount = (+this.amount) + (+this.tax);
    }

    btn_clk(val) {
        this.amount = val;
        this.calc();
    }

    addAmount() {
        if (!/^(?:0|[1-9]\d*)(?:\.(?!.*000|[1-9]{3})\d+)?$/.test(this.amount.toString())) {
            toastr.warning("<h5>Enter Correct Amount<h5>");
            return;
        }
        if (this.amount < 10) {
            toastr.warning("<h5>Minimum Amount is Rs. 10/-<h5>");
            return;
        }
        this.show_loading = true;
        let data = {
            'amount': this.total_amount
        }
        this._appService.post_method(this._userService.paymentUrl, JSON.stringify(data)).subscribe(
            data => {
                if (data.status) {
                    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
                    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

                    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                    var left = ((width / 2) - (800 / 2)) + dualScreenLeft;
                    var top = ((height / 2) - (800 / 2)) + dualScreenTop;
                    var newWindow = window.open(data.url, "_blank", 'rel=noopener,toolbar=yes,scrollbars=yes, width=' + 800 + ', height=' + 800 + ', top=' + top + ', left=' + left);
                    // if (window.focus) {
                    //     newWindow.focus();
                    // }
                    if (!newWindow || newWindow.closed || typeof newWindow.closed == 'undefined') {
                        toastr.warning("Please Allow Pop-Ups for Sainofirst To Continue");
                        this.show_loading = false;
                    }
                    else {
                        this.amount = 0;
                        this.total_amount = 0;
                        this.show_loading = false;
                    }

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                    this.show_loading = false;
                }
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
                this.show_loading = false;
            }
        );




    }
}
