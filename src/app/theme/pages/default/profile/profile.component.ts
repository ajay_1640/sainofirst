import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { AppService } from '../../../../app.service';
import { CookieService } from 'ngx-cookie-service';
import { Router, NavigationEnd } from '@angular/router';
import { UserService } from '../../../../auth/_services/user.service';
import { WalletService } from '../../../layouts/header-nav/wallet.service';

@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {
    user_role = "";
    user_role4 = "4";
    showWallet = true;
    user_id = "";
    profile_name = "";
    profile_email_id = "";
    show_loading1 = false;
    settingsShow: boolean = false;
    constructor(private _appService: AppService, private _walletService: WalletService, private _router: Router, private _cookieService: CookieService, private _userService: UserService) {
        this.settingsShow = false;
        this._walletService.onSettingsChanged.subscribe((m: any) => {
            this.settingsShow = m;
            // console.log("settings show",this.settingsShow);

        })
        let s: any = this._router.url.toString();
        let s1 = s.lastIndexOf('/');
        let path = s.substring(s1 + 1, s.length);
        // console.log("path",path);
        this._walletService.showsettings = false;
        if (path == 'manage-customers' || path == 'sender-id-approval' || path == 'mgmtk' || path == 'live-status' || path == 'smpp' || path == 'kyc') {
            this._walletService.showsettings = true;
        }
        // if(window.location.hostname!='app.sainofirst.com'){
        //     this.showWallet=false;
        // }
        this.settingsShow = this._walletService.showsettings;

        this._walletService.onProfileDropdown.subscribe((m: any) => {

            // let path=this._router.url.substring(s+1,this._router.url.length);
            //  console.log("in profile page menu item",m);

            $("#profile").removeClass('selected')
            $("#wallet").removeClass('selected')
            $("#transaction-history").removeClass('selected')
            $("#alerts").removeClass('selected')
            $("#api").removeClass('selected')
            $("#manage-customers").removeClass('selected');
            $("#sender-id-approval").removeClass('selected');
            $("#mgmtk").removeClass('selected');
            $("#smpp").removeClass('selected');
            $("#live-status").removeClass('selected');
            $("#kyc").removeClass('selected');
            $("#" + m).addClass('selected');


        })
    }
    ngAfterViewInit() {
        this._walletService.onSettingsChanged.subscribe((m: any) => {

            this.settingsShow = m;
            //  console.log("ng after settings show",this.settingsShow);

        })
    }
    AfterViewInit() {
        this._walletService.onSettingsChanged.subscribe((m: any) => {

            this.settingsShow = m;
            // console.log("after view init settings show",this.settingsShow);

        })
    }
    ngOnInit() {
        if (window.location.hostname != 'app.sainofirst.com') {
            this.showWallet = false;
        }

        this.user_role = this._cookieService.get('userrole');
        this.user_id = this._cookieService.get('userid');
        this._walletService.onSettingsChanged.subscribe((m: any) => {

            this.settingsShow = m;
            //  console.log("ng on init settings show",this.settingsShow);

        })
        // console.log("user role",this.user_role);
        // console.log("user id",this.user_id);
        this.show_loading1 = true;
        var $li = $('#profileItems li').click(function() {
            $li.removeClass('selected');
            // console.log($li)
            // $li.css('color','black');
            $(this).addClass('selected');
            // $(this).css('color','white');

        });

        let s = this._router.url.lastIndexOf('/');
        let path = this._router.url.substring(s + 1, this._router.url.length);
        //  console.log(path)
        $('#profile').removeClass('selected')
        $('#wallet').removeClass('selected')
        $('#transaction-history').removeClass('selected')
        $('#alerts').removeClass('selected')
        $('#api').removeClass('selected')
        $('#manage-customers').removeClass('selected')
        $('#sender-id-approval').removeClass('selected')
        $('#mgmtk').removeClass('selected')
        $('#smpp').removeClass('selected')
        $('#live-status').removeClass('selected')
        $('#kyc').removeClass('selected')
        $("#" + path).addClass('selected');
        // this.pathSelect();

        this.profileDetails();
        this.appPermissions();

    }
    pathSelect() {
        let s = "";
        let path = "";
        this._router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                s = event.url;
                // console.log("path select called")
                let ind = s.lastIndexOf('/');
                path = s.substring(ind + 1, this._router.url.length);
                //   console.log(path)
                $('#profile').removeClass('selected')
                $('#wallet').removeClass('selected')
                $('#transaction-history').removeClass('selected')
                $('#alerts').removeClass('selected')
                $('#api').removeClass('selected')
                $('#manage-customers').removeClass('selected')
                $('#sender-id-approval').removeClass('selected')
                $('#mgmtk').removeClass('selected')
                $('#smpp').removeClass('selected')
                $('#live-status').removeClass('selected')
                $('#kyc').removeClass('selected')
                $("#" + path).addClass('selected');
            };
        });

    }
    profileDetails() {
        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.profile_name = data.profile_details[0].client_name;
                this.profile_email_id = data.profile_details[0].email_id;

            },
            error => { }
        )
    }

    live_status = false;
    open_senderid = false;
    KYC = false;
    appPermissions() {
        this._appService.get_method(this._userService.permission_manage_users_permissions + "" + this.user_id + "/permissions").subscribe(
            data => {
                let p_data = data.data[0];
                // console.log("permissions",p_data);
                let permission = p_data.permissions;
                if (permission != null && permission != "") {
                    let arr = permission.split(",");
                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i] == "LiveStatus") this.live_status = true;
                        else if (arr[i] == "openSenderId") this.open_senderid = true;
                        else if (arr[i] == "kyc") this.KYC = true;

                    }
                }
                this.show_loading1 = false;
            },
            error => {
                // console.log(error);
            }
        );

    }

}