import { Component, OnInit } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import swal from 'sweetalert2';
import { CookieService } from 'ngx-cookie-service';
import { ViewChild, ElementRef } from '@angular/core';

declare let moment;
declare let $;
declare let toastr;

@Component({
    selector: 'app-sender-id-approval',
    templateUrl: "./sender-id-approval.component.html",
    styles: []
})
export class SenderIdApprovalComponent implements OnInit {
    // @ViewChild('closeBtn') closeBtn: ElementRef;
    @ViewChild('myModal') myModal;
    constructor(private _script: ScriptLoaderService, private _cookieService: CookieService, private _appService: AppService, private _userService: UserService) { }
    searchText = "";
    sender_id_p = 1;
    show_loading = false;
    hidemodal = false;
    show_loading_modal = false;
    isavailable: boolean = true;
    p: number = 1;
    SelectedgatewayIndex = -1;
    showAandD = false;
    openSenderid = false;
    check = "";
    transact = "";
    senderid_list = [];
    users_list = [];
    user_sid = "";
    user_ids = [];
    dropdownList = [];
    routes_of_gateways = [];
    selectedItems = [];
    username = "";
    userrole = "";
    sender_id_name = "";
    edit_senderid: boolean = false;
    gateway_list = [];
    closeMe = '';
    ngOnInit() {
        this._script.loadScripts('app-sender-id-approval',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this.getUserList();
        this.showAandD = false;
        this.userrole = this._cookieService.get('userrole');
        this.getPermission();
        if (this.userrole == "1") { this.showAandD = true; }
        this.getSenderIdlist();
        this.getRoutes();
        $('#m_modal_6').on('shown.bs.modal', function(e) {
            setTimeout(() => {
                $('#user_list_select').selectpicker('refresh');
            }, 200);
        })
        $('#myModal').on('hidden.bs.modal', function(e) {
            $('#m_modal_6').modal('show')
        })
        this._script.loadScripts('app-sender-id-approval',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        this.getGatewayData();
        $("input:checkbox").on('click', function() {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

    }



    openModel() {
        this.myModal.nativeElement.className = 'modal fade show';
    }
    closeModel() {
        // debugger
        this.myModal.nativeElement.className = 'modal hide';
        this.myModal.nativeElement.style.display = 'none'
    }
    getGatewayData() {

        this.gateway_list = [];
        this._appService.get_method(this._userService.admin_gateway_url).subscribe(
            data => {
                this.gateway_list = data.result.concat();
                // console.log("gateway data is here");
                for (let i = 0; i < this.gateway_list.length; i++) {
                    this.gateway_list[i]['status'] = false;
                    this.gateway_list[i]['create_date'] = moment(this.gateway_list[i]['create_date']).format('DD/MM/YYYY');
                }
                // console.log(this.gateway_list);

            },
            //error => { console.log(error); }
        );


    }
    getPermission() {
        this.openSenderid = false;
        this._appService.get_method(this._userService.permission_manage_users_permissions + this._cookieService.get('userid') + "/permissions").subscribe(
            data => {
                //console.log(data.data[0]['permissions']);
                let ar = data.data[0]['permissions'].split(',');
                if (ar.includes('openSenderId')) {

                    this.openSenderid = true;
                }
                else {
                    this.openSenderid = false;
                }
            },
            error => {
                //console.log(error);
            }
        );
    }
    getSenderIdlist() {
        this.show_loading = true;
        this._appService.get_method(this._userService.manage_users_senderid).subscribe(
            data => {
                // if(!data.status)return toastr.error("<h5>"+data.message+"<h5>");
                this.routes_of_gateways = [];
                // console.log(data);
                try {
                    this.senderid_list = data.sender_information.reverse();
                }
                catch (e) { }
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['crdate'] = moment(this.senderid_list[i]['crdate']).format('DD-MM-YYYY');
                    let routes = " ";
                    let showAandD = true;
                    for (let j = 1; j <= this.senderid_list[i]['routes'].length; j++) {
                        routes = routes.concat(this.senderid_list[i]['routes'][j - 1]['route_name'].toString());
                        if (this.userrole != "1") {
                            if (this.senderid_list[i]['routes'][j - 1]['is_senderid'] == 1) showAandD = false;
                        }

                        routes = routes.concat(',');
                        if (j % 3 == 0) {
                            routes = routes.concat("\n");
                        }

                    }
                    this.senderid_list[i]['showAandD'] = showAandD;
                    routes = routes.substr(0, routes.length - 1);
                    this.routes_of_gateways.push(routes);

                }
                //console.log("senderid list",this.senderid_list);
                this.show_loading = false;

                // console.log(this.routes_of_gateways);
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error: " + error + "<h5>");
                // console.log(error);

            }
        );

    }

    getRoutes() {
        this._appService.get_method(this._userService.user_routes_url).subscribe(
            data => {
                // console.log(data);
                // console.log("routes",data.data);
                this.dropdownList = data.data.concat();
                for (let i = 0; i < data.data.length; i++) {
                    this.selectedItems.push({ 'routeid': data.data[i]['az_routeid'], 'selected': false });
                    // this.add_sender_selectedItems.push({ 'routeid': data.result[i]['az_routeid'], 'selected': false });
                }
                //  console.log(this.dropdownList);

            },
            error => {
                //  console.log(error); 
            }
        );
    }

    getUserList() {
        this._appService.get_method(this._userService.manage_user_add_account).subscribe(
            data => {
                // console.log(data);
                this.users_list = data.result;
                $('#user_list_select').selectpicker('refresh');
            },
            error => { }
        );
    }
    routeselected(checkbxindex: any) {
        if (this.selectedItems[checkbxindex]['selected']) { this.selectedItems[checkbxindex]['selected'] = false; }
        else { this.selectedItems[checkbxindex]['selected'] = true; }

    }
    add_sender_id() {

        this.show_loading_modal = true;
        this.edit_senderid = false;
        this.username = "";
        this.sender_id_name = "";
        for (let i = 0; i < this.selectedItems.length; i++) { this.selectedItems[i]['selected'] = false; }
        for (let i = 0; i < this.gateway_list.length; i++) {
            this.gateway_list[i]['status'] = false;
        }
        //  this.getUserList();
        this.show_loading_modal = false;
        $('#m_modal_6').modal('show');


    }
    edit_sender_id(sender_id: any) {
        // console.log(sender_id);
        $('#m_modal_6').modal('show');
        this.show_loading_modal = true;
        this.SelectedgatewayIndex = -1;
        this.edit_senderid = true;
        this.username = sender_id.user_name;
        this.user_sid = sender_id.sid;

        //for (let i = 0; i < this.selectedItems.length; i++) { this.selectedItems[i]['selected'] = false; this.gateway_list[i]['status']=false; }
        let routes = [];
        this.sender_id_name = sender_id.senderid;
        for (let i = 0; i < sender_id.routes.length; i++) {
            routes.push(sender_id.routes[i].route_id);
        }

        for (let j = 0; j < this.selectedItems.length; j++) {
            this.selectedItems[j]['selected'] = false;

            for (let i = 0; i < routes.length; i++) {

                if (this.selectedItems[j]['routeid'] == routes[i]) {
                    this.selectedItems[j]['selected'] = true;
                }
            }
        }

        for (let k = 0; k < this.gateway_list.length; k++) {
            this.gateway_list[k]['status'] = false;
        }
        this.SelectedgatewayIndex = -1;
        for (let i = 0; i < this.gateway_list.length; i++) {
            if (this.gateway_list[i]['gateway_name'] == sender_id.gatewayid) {
                this.gatewayselected(i);
                this.show_loading_modal = false;
                return;
            }

        }
        //  console.log( gateway_list
        this.show_loading_modal = false;

    }
    delete_sender(sender_id: any) {

        this._appService.delete_method(this._userService.manage_users_senderid + sender_id.sid).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Sender id Deleted Successfully<h5>");
                    this.getSenderIdlist();

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");

            }

        )

    }
    get_add_userid() {
        for (let i = 0; i < this.users_list.length; i++) {
            if (this.users_list[i]['user_name'] == this.username) {
                return this.users_list[i]['userid'];


            }

        }

    }
    get_selected_routes() {
        let routes = [];
        // console.log("in routes",this.selectedItems);
        for (let i = 0; i < this.selectedItems.length; i++) {
            if (this.selectedItems[i]['selected']) {
                // console.log(this.selectedItems[i]['routeid']);
                routes.push(this.selectedItems[i]['routeid']);
                // console.log(routes);

            }

        }
        return routes;

    }
    gatewayselected(index) {
        this.gateway_list[index]['status'] = !this.gateway_list[index]['status'];
        for (let i = 0; i < this.gateway_list.length; i++) {
            if (i != index) {
                this.gateway_list[i]['status'] = false;
            }
        }
        this.SelectedgatewayIndex = index;
        //  console.log(this.gateway_list[index])
    }
    create_senderid() {

        // console.log(this.selectedItems);
        let user_id = this.get_add_userid();
        let senderid = this.sender_id_name;
        let sid = this.user_sid;
        let routes = this.get_selected_routes();
        if (this.validateform() == true) {
            //this.closeMe = 'modal';
            $('#m_modal_6').modal('hide');
            let gateway = null;
            if (this.SelectedgatewayIndex == -1) {
                gateway = null;
            }
            else if (!this.gateway_list[this.SelectedgatewayIndex]['status']) {
                gateway = null;
            }
            else {
                gateway = this.gateway_list[this.SelectedgatewayIndex]['gateway_name'];
            }
            if (this.edit_senderid) {

                let data = JSON.stringify({
                    'uid': user_id,
                    'sid': sid,
                    'sender_id': senderid,
                    'routes': routes,
                    'gateway': gateway
                });

                this._appService.patch_method(this._userService.manage_users_senderid + this.user_sid, data).subscribe(
                    data => {
                        // console.log(data);
                        if (data.status) {
                            this.getSenderIdlist();
                            this.username = "";
                            this.sender_id_name = "";
                            for (let i = 0; i < this.selectedItems.length; i++) { this.selectedItems[i]['selected'] = false; }
                            for (let j = 0; j < this.gateway_list.length; j++) {
                                this.gateway_list[j]['status'] = false;
                            }
                            //$('#m_modal_6').modal('hide');
                            //console.log(this.selectedItems);
                            toastr.success("<h5>Successfully Updated Sender Id<h5>");
                        }
                        else {
                            toastr.error("<h5>" + data.message + "<h5>");
                        }


                    },
                    error => {
                        toastr.error("<h5>Server Error " + error + "<h5>");

                    }
                );
            }
            else {
                let data1 = JSON.stringify({
                    'user_id': user_id,
                    'sender_id': senderid,
                    'routes': routes,
                    'gateway': gateway

                });
                this._appService.post_method(this._userService.manage_users_senderid, data1).subscribe(
                    data => {
                        // console.log(data);
                        if (data.status) {
                            this.getSenderIdlist();
                            this.username = "";
                            this.sender_id_name = "";
                            for (let i = 0; i < this.selectedItems.length; i++) { this.selectedItems[i]['selected'] = false; }
                            //console.log(this.selectedItems);
                            for (let j = 0; j < this.gateway_list.length; j++) {
                                this.gateway_list[j]['status'] = false;
                            }
                            toastr.success("<h5>Successfully added new Sender Id<h5>");
                        }
                        else {
                            toastr.error("<h5>" + data.message + "<h5>");
                        }


                    },
                    error => {
                        toastr.error("<h5>Server Error " + error + "<h5>");

                    }
                );

            }

        }



    }
    approve(sender: any) {
        // console.log(sender);
        toastr.info("Approving");
        let data = JSON.stringify({
            'status': 1,
            'user_id': sender.userid
        });
        this._appService.patch_method(this._userService.manage_users_senderid + sender.sid + "/approve", data).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    toastr.success("<h5>Sender Id Approved<h5>");
                    this.getSenderIdlist();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");

                }

            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );
    }
    disapprove(sender: any) {
        toastr.info("Rejecting");
        // console.log(sender);
        let data = JSON.stringify({
            'status': 2,
            'user_id': sender.userid
        });
        this._appService.patch_method(this._userService.manage_users_senderid + sender.sid + "/approve", data).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    toastr.success("<h5>Sender Id Disapproved<h5>");
                    this.getSenderIdlist();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");

                }

            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );


    }
    validateform() {
        //console.log(this.edit_senderid);
        if (this.username == "") return toastr.warning("Invalid UserId");
        if (this.sender_id_name == "") return toastr.warning("Sender id is Empty");
        if (this.sender_id_name.length < 6 || this.sender_id_name.length > 12) {
            return toastr.warning("<h5>SenderId name Length Should be between 6 to 12 characters<h5>");
        }
        if (!/^[a-zA-Z]+$/.test(this.sender_id_name)) return toastr.warning("SenderId name is Invalid Only Alphabet are acceptable");
        let routes = this.get_selected_routes()
        if (routes.length == 0) return toastr.warning("Choose Routes");
        // if((this.SelectedgatewayIndex==-1 || !this.gateway_list[this.SelectedgatewayIndex]['status']) )return toastr.warning("Select Gateway");


        return true;

    }
}
