import { Component, OnInit } from '@angular/core';
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from './../../../../../app.service';
import swal from 'sweetalert2';
//import {UserService} from './auth/_services/user.service';
import { UserService } from '../../../../../auth/_services';
import { country_code } from './countrycodes';
import { Currency } from '../my-profile/country codes';
import { language_details } from '../my-profile/country codes';
import { timezone } from '../my-profile/country codes';

import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import * as XLSX from 'xlsx';
import { WalletService } from '../../../../../theme/layouts/header-nav/wallet.service';
import { CookieService } from 'ngx-cookie-service';

import { toBase64String } from '@angular/compiler/src/output/source_map';
declare var $, jQuery: any;
declare let toastr;
declare let moment;

@Component({
    selector: 'app-manage-customers',
    templateUrl: "./manage-customers.component.html",
    styleUrls: ['./manage-customers.css']
})
export class ManageCustomersComponent implements OnInit {
    edit_account = false;
    show_pricing = false;
    color = 'color';
    credit = /^credit$/i;
    debit = /^debit$/i;
    showIndianPricing = false;
    pricingg_p = 1;
    brand_patch = false;
    pricing_permissions_india = [];
    pricing_permissions_global = [];
    export_show = false;
    import_show = false;
    currencyDropdown = [];
    languageDropdown = [];
    timezoneDropdown = {};
    m_users_p = 1;
    savingBrand = false;
    parent_permissions = [];
    parentRoutes = [];
    permis_uid = "";
    status1_ = "";
    status2_ = "";
    options_1 = [];
    options_1_1 = [];
    options_2_2 = [];
    options_2 = [];
    show_loading = false;
    option1_value = "";
    option2_value = "";
    wallet_history = [];
    pricing_permissions = [];
    adminroutes = [];
    w_userid = "";
    userrole = "";
    w_user_name = "User name";
    w_email_id = "email id";
    w_balance = "";
    wallet_p = 1;
    pricingi_p = 1;
    tran_p = 1;
    edit_price_country = "";
    edit_price_gateway = "";
    edit_price_current_price = "";
    edit_price_new_price;
    p_id = "";
    p_u_id = "";
    add_pricing_gateway = "";
    add_pricing_new_price: number = -1;
    dlr_status_1: number;
    dlr_status_2: number;
    brand_website_ = "";
    brand_sitename_ = "";
    brand_favicon_ = "Choose file";
    brand_logo_ = "Choose file";
    brand_sitetitle_ = "";
    brand_color_ = "";
    from_date = "";
    to_date = "";
    logo;
    favicon;

    // user_status = -1;
    // user_role = -1;
    user_role_arr = [];
    user_status_arr = [
        {
            'status': 'Inactive',
            'val': '0'

        },
        {
            'status': 'Active',
            'val': '1'
        }
    ];
    // searchTextUser="";
    // searchText34="";
    bulksms_access = false;
    whatsapp_access = false;
    missedcall_access = false;
    shortcode_access = false;
    ivr_tollfree_access = false;
    voicecall_access = false;
    cutting_service = false;
    dnd_refund = false;
    live_status = false;
    open_senderid = false;
    block_number = false;
    KYC = false;
    startfrom_ = "";
    cutting_ = null;
    calendar_range_transaction = "";
    show_calendar_transaction = false;

    branding_ = [];
    w_id = "";
    brand_wid = "";
    transaction_uid = "";

    sender_id_name = "";
    p: number = 1;
    new_pwd = "";
    old_pwd = "";
    check = "";
    transact = "";
    send_flash = "";
    user_list;
    //user_list2;
    user_old_pwd = "";
    userid = "";
    dropdownList = [];
    selectedItems = [];
    parentApps = [];
    // add_sender_selectedItems=[];
    dropdownSettings = {};

    User_details: any = {
        _client_name: "",
        _company_name: "",
        _currency: "",
        _email_id: "",
        _language: "",
        _mobile_no: "",
        _timezone: "",
        _user_role: "",
        _user_status: "",
        _user_type: "",
        _user_name: "",
        _user_psw: ""
    };
    add_user: any = {

        _client_name: "",
        _company_name: "",
        _currency: "",
        _email_id: "",
        _language: "",
        _mobile_no: "",
        _timezone: "",
        _user_role: null,
        _user_status: null,
        _user_type: "",
        _user_name: "",
        _user_psw: ""
    };



    validated = {
        status: null,
        message: ""
    }
    amount_ = "";
    note_ = "";
    tran_history = [];
    timezoneObjects = [];
    apps = [];
    appsPresent = [];
    interval = [];
    user_id = null;

    constructor(private _walletService: WalletService, private _script: ScriptLoaderService, private _router: Router, private _cookieService: CookieService, private _userService: UserService, private _appService: AppService) { }

    ngOnInit() {
        this.userrole = this._cookieService.get('userrole');
        this.user_id = this._cookieService.get('userid');
        // console.log(this.user_id);
        this.currencyDropdown = Currency;
        this.languageDropdown = language_details;
        this.timezoneDropdown = timezone;
        this.timezoneObjects = Object.keys(timezone);
        this.assignRoles();
        this.getRoutes();
        //  this.add_new_pricing_modal();
        this._script.loadScripts('app-manage-customers',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-manage-customers',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-manage-customers',
            ['assets/demo/demo3/base/input-mask.js']);
        this._script.loadScripts('app-manage-customers',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        this._script.loadScripts('app-manage-customers',
            ['assets/build/js/countrySelect.min.js']);

        $('#sandbox-container .input-daterange').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        this.getUserDetails();
        $("#country").countrySelect();
        $('#selectpicker3').selectpicker()
        $('#add_account').on('shown.bs.modal', function(e) {
            $('#selectpicker1').selectpicker('refresh');
            $('#selectpicker2').selectpicker('refresh');
            $('.selectpicker').selectpicker('refresh');
            // console.log("hii");
            // do something...
        });
        let s = this;
        $('#transactions').on('shown.bs.modal', function(e) {
            s.calendar_range_transaction = "";
            setTimeout(() => {
                $('.selectpicker').selectpicker('refresh');

            }, 200)
        })
        let ss = this;
        $('#walletmodal').on('hidden.bs.modal', (e) => {
            ss.getUserDetails();
        })
        $('#ind_pricing_modal_add').on('shown.bs.modal', function(e) {
            $('.m_selectpicker').selectpicker('refresh');

        });
        this.interval = [
            { 'val': "today", "interval": 'Today' },
            { 'val': "7days", "interval": 'Last 7 days' },
            { 'val': "15days", "interval": 'Last 15 days' },
            { 'val': "month", "interval": 'Last month' },
            { 'val': "custom", "interval": 'Custom' },
        ]
        this.apps = [
            'bulk_sms', 'bulk_voice', 'ivr_toll', 'missed_call', 'short_code', 'digital_marketing', 'seo', 'chat_bots', 'inter_sms', 'whatsapp'
        ]
        this.appsPresent = [
            // 'digital_marketing','seo','chat_bots','inter_sms'
        ]
        this.options_1 = [
            { 'status': "Delivered", 'value': 'Delivered' },
            { 'status': "DND failed", 'value': 'DNDfailed' },
            { 'status': "Others", 'value': 'others' }
        ];
        this.options_2 = [
            { 'status': "Delivered", 'value': 'Delivered' },
            { 'status': "DND failed", 'value': 'DNDfailed' },
            { 'status': "Others", 'value': 'others' }
        ];
        $('#sandbox-container .input-daterange').datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        this.options_1_1 = this.options_1;
        this.options_2_2 = this.options_2;
        $("#country").countrySelect("setCountry", "India");


    }
    getUserDetails() {
        this.show_loading = true;
        this._appService.get_method(this._userService.manage_user_add_account).subscribe(
            data => {
                if (data.status) {
                    this.user_list = data.result.reverse();
                    for (let i = 0; i < this.user_list.length; i++) {
                        if(this.user_list[i]['balance']!=null && this.user_list[i]['balance']!='undefined' && this.user_list[i]['balance']!=""){
                            try{
                                this.user_list[i]['balance']=parseFloat(this.user_list[i]['balance']);
                                this.user_list[i]['balance']=this.user_list[i]['balance'].toFixed(2);
                            }
                            catch(e){
                                this.user_list[i]['balance']=0.00;
                            }
                        }
                        else{
                            this.user_list[i]['balance']=0.00;
                        }
                      
                        let ar = this.user_list[i]['relationship'].split('->');
                        this.user_list[i]['relationship'] = ar.join('->\n');
                        let s = this.user_list[i]['relationship'].match(/(.|[\r\n]){1,30}/g);
                        let s1 = "";
                        for (let j = 0; j < s.length; j++) {
                            s1 += s[j];
                            // s1+="\n";
                        }
                        this.user_list[i]['relationship'] = s1;

                    }

                    // console.log(this.user_list);
                }
                this.show_loading = false;
            },
            error => {
                toastr.error("<h5>Server Error " + error + "");
                this.show_loading = false;
            });

    }
    assignRoles() {
        if (this._cookieService.get('userrole') == "1") {
            this.user_role_arr = [
                {
                    'role': 'Distributer',
                    'val': '2'
                },
                {
                    'role': 'Reseller',
                    'val': '3'
                },
                {
                    'role': 'User',
                    'val': '4'
                },
                {
                    'role': 'Support',
                    'val': '5'
                }
            ]

                ;


        }
        else if (this._cookieService.get('userrole') == "2") {

            this.user_role_arr = [
                {
                    'role': 'Distributer',
                    'val': '2'
                },

                {
                    'role': 'User',
                    'val': '4'
                },

            ]

                ;
        }
        else if (this._cookieService.get('userrole') == "3") {

            this.user_role_arr = [
                {
                    'role': 'Reseller',
                    'val': '3'
                },

                {
                    'role': 'User',
                    'val': '4'
                },

            ]

                ;
        }
    }
    getRoutes() {
        this._appService.get_method(this._userService.user_routes_url).subscribe(
            data => {
                // console.log(data);
                if (!data.status) {
                    toastr.error("<h6>Server Error " + data.message + "<h6>");
                }
                this.dropdownList = data.data.concat();
                for (let i = 0; i < data.data.length; i++) {
                    this.selectedItems.push({ 'routeid': data.data[i]['az_routeid'], 'selected': false });
                    // this.add_sender_selectedItems.push({ 'routeid': data.data[i]['az_routeid'], 'selected': false });
                }
                // console.log(this.dropdownList);

            },
        );
    }
    show_pricing_menu(User: any) {
        this.showIndianPricing = false;
        // console.log(User);
        if (User.country == "India") {
            this.showIndianPricing = true;
        }
        this.show_pricing = true;
        this.show_loading = true;
        this.p_u_id = User.userid;
        this._appService.get_method(this._userService.permission_manage_users_permissions + User.userid + "/" + "pricing").subscribe(
            data => {
                this.pricing_permissions = data.data;
                for (let i = 0; i < this.pricing_permissions.length; i++) {
                    if (this.pricing_permissions[i]['iso2'] == "IN") {
                        this.pricing_permissions_india.push(this.pricing_permissions[i]);

                    }
                    else {
                        this.pricing_permissions_global.push(this.pricing_permissions[i]);
                    }
                }
                this.show_loading = false;

            },
            error => {
                toastr.error("<h5>Server error " + error + "<h5>");
                this.show_loading = false;

            }
        );

    }
    edit_pricing_india(pricing: any) {

        $('#ind_pricing_modal_edit').modal('show');
        this.edit_price_new_price = "";
        this.edit_price_country = pricing.name;
        this.edit_price_gateway = pricing.az_rname;
        this.edit_price_current_price = pricing.price;
        this.p_id = pricing.id;
    }
    // add_new_pricing_modal() {
    //     this._appService.get_method(this._userService.routes_url).subscribe(
    //         data => {

    //             this.adminroutes = data.result;
    //         }
    //     );
    // }
    save_pricing_details() {
        if (this.edit_price_new_price == "" || this.edit_price_new_price == null || this.edit_price_new_price <= 0) return toastr.warning("Check the price");
        $('#ind_pricing_modal_edit').modal('hide');
        this.show_loading = true;
        this._appService.patch_method(this._userService.permission_manage_users_permissions + this.p_u_id + "/pricing/" + this.p_id, JSON.stringify({ 'price': this.edit_price_new_price.toString() })).subscribe(
            data => {
                //  console.log(data);
                if (!data.status) {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this._appService.get_method(this._userService.permission_manage_users_permissions + this.p_u_id + "/" + "pricing").subscribe(
                        data => {
                            this.pricing_permissions_global = [];
                            this.pricing_permissions_india = [];
                            this.pricing_permissions = data.data;
                            for (let i = 0; i < this.pricing_permissions.length; i++) {
                                if (this.pricing_permissions[i]['iso2'] == "IN") {
                                    this.pricing_permissions_india.push(this.pricing_permissions[i]);

                                }
                                else {
                                    this.pricing_permissions_global.push(this.pricing_permissions[i]);
                                }
                            }
                            this.show_loading = false;
                        },
                        error => {
                            toastr.error("<h6>Server Error " + error + "<h6>");
                            this.show_loading = false;

                        }
                    );
                }
            },
            error => {
                this.show_loading = false;

                toastr.error("<h6>Server Error " + error + "<h6>");
            }
        )


    }
    transaction_history(User: any) {
        this.transaction_uid = User.userid;

        this.show_modal_loading = true;
        this._appService.get_method(this._userService.permission_manage_users_permissions + User.userid + "/" + "wallet-history").subscribe(
            data => {
                if (!data.status) {
                    return toastr.error("<h5>" + data.message + "<h5>");
                }
                this.tran_history = data.data;
                for (let i = 0; i < this.tran_history.length; i++) {
                    this.tran_history[i]['date'] = moment(this.tran_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");
                }
                this.tran_history = this.tran_history.reverse();
                this.show_modal_loading = false;
            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );

    }
    transaction_history_search(startdate, enddate) {
        let data_send = JSON.stringify({
            'start_time': startdate,
            'end_time': enddate
        });
        this.show_modal_loading = true;
        this._appService.post_method(this._userService.permission_manage_users_permissions + this.transaction_uid + "/" + "wallet-history", data_send).subscribe(
            data => {
                if (!data.status) return toastr.error("<h5>" + data.message + "<h5>");
                this.tran_history = [];
                try {
                    this.tran_history = data.data;
                    for (let i = 0; i < this.tran_history.length; i++) {
                        this.tran_history[i]['date'] = moment(this.tran_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");
                    }
                    this.tran_history = this.tran_history.reverse();

                }

                catch (e) {

                }

                this.show_modal_loading = false;
            },
            error => {
                this.show_modal_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );

    }
    remove_pricing() {
        this.show_pricing = false;
    }
    // timezone="";
    addAccount() {
        this.edit_account = false;
        this.show_modal_loading = true;
        this.user_old_pwd = "";
        //this.User_details._cli= "";
        // this.User_details=this.add_user;
        this.User_details._client_name = "",
            this.User_details._company_name = "",
            this.User_details._currency = "",
            this.User_details._email_id = "",
            this.User_details._language = "English",
            this.User_details._mobile_no = "",
            this.User_details._timezone = "",
            this.User_details._user_role = "4",
            this.User_details._user_status = "1",
            this.User_details._user_type = "",
            this.User_details._user_name = "",
            // this.User_details._user_name= "",

            this.User_details._user_psw = ""
        $('#add_account').modal('show');
        this._appService.get_method(this._userService.manage_user_currency).subscribe(
            data => {
                this._appService.get_method(this._userService.profile_url).subscribe(
                    data => {
                        this.User_details._timezone = data.profile_details[0]['timezone'];
                        setTimeout(() => {
                            $('.selectpicker').selectpicker('refresh');

                        }, 200)
                        this.show_modal_loading = false;
                    },
                    error => {
                        this.show_modal_loading = false;
                    }
                );
                // console.log(data);
                if (data.result == 'Indian Rupees - INR') {
                    this.User_details._currency = "Indian Rupees - INR";
                }
                else { this.User_details._currency = "Euro - EUR" }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200)

            },
            error => {
                //this.show_modal_loading=false;
                toastr.error("<h5>Server Error: " + error + "<h5>");
            }
        );
    }
    addUserProfile() {

        if (this.validateProfile() == true) {
            //  console.log("hii")
            // console.log(this.validateProfile());
            $('#add_account').modal('hide');
            let User = {
                client_name: this.User_details._client_name,
                company_name: this.User_details._company_name,
                currency: this.User_details._currency,
                email_id: this.User_details._email_id,
                language: this.User_details._language,
                mobile_no: this.User_details._mobile_no.toString(),
                timezone: this.User_details._timezone,
                user_role: this.User_details._user_role,
                user_status: this.User_details._user_status,
                // user_type: this.User_details._user_type,
                user_name: this.User_details._user_name,
                user_psw: this.User_details._user_psw,
                designation: this.User_details._user_type

            };

            if (!this.edit_account) {
                this._appService.post_method(this._userService.check_username, JSON.stringify({ 'user_name': this.User_details._user_name })).subscribe(
                    data => {
                        if (data.status) {
                            this._appService.post_method(this._userService.manage_user_add_account, JSON.stringify(User)).subscribe(
                                data => {
                                    if (!data.status) return toastr.error("<h5>" + data.message + "<h5>");
                                    toastr.success("<h5>" + data.message + "<h5>");
                                    this.User_details = {};
                                    this.User_details = this.add_user;
                                    this.user_list = "";
                                    this.show_loading = true;
                                    this.user_list = [];
                                    this._appService.get_method(this._userService.manage_user_add_account).subscribe(
                                        data => {
                                            this.user_list = data.result.reverse();
                                            for (let i = 0; i < this.user_list.length; i++) {
                                                this.user_list[i]['balance'] = this.user_list[i]['balance'].toFixed(2);
                                                let ar = this.user_list[i]['relationship'].split('->');
                                                this.user_list[i]['relationship'] = ar.join('->\n');
                                                let s = this.user_list[i]['relationship'].match(/(.|[\r\n]){1,30}/g);
                                                let s1 = "";
                                                for (let j = 0; j < s.length; j++) {
                                                    s1 += s[j];
                                                    // s1+="\n";
                                                }
                                                this.user_list[i]['relationship'] = s1;
                                            }
                                            //this.user_list2 = this.user_list;
                                            //  console.log(data.result);
                                            this.show_loading = false;

                                        },
                                        error => {
                                            toastr.error("<h5>Server Error " + error + "<h5>");

                                        }

                                    )
                                },
                                error => { toastr.error("<h5>Server Error " + error + "<h5>"); }
                            );
                        }
                        else {
                            toastr.warning("<h5>User with given user id already exist<h5>");
                        }
                    }
                );



            }
            else {
                this._appService.patch_method(this._userService.manage_user_add_account + "/" + this.userid, JSON.stringify(User)).subscribe(

                    data => {
                        // console.log(data);
                        if (!data.status) return toastr.error("<h5>" + data.message + "<h5>");
                        toastr.success("<h5>" + data.message + "<h5>");

                        // $('#add_account').toggle();
                        // secondly remove 'modal-open' class from body and '.modal-backdrop' at the end of the page.
                        // $('body').removeClass('modal-open');
                        //  $('.modal-backdrop').remove();
                        this.user_list = "";
                        this.show_loading = true;
                        this.user_list = [];
                        this._appService.get_method(this._userService.manage_user_add_account).subscribe(
                            data => {
                                this.user_list = data.result.reverse();
                                for (let i = 0; i < this.user_list.length; i++) {
                                    this.user_list[i]['balance'] = this.user_list[i]['balance'].toFixed(2);
                                    let ar = this.user_list[i]['relationship'].split('->');
                                    this.user_list[i]['relationship'] = ar.join('->\n');
                                    let s = this.user_list[i]['relationship'].match(/(.|[\r\n]){1,30}/g);
                                    let s1 = "";
                                    for (let j = 0; j < s.length; j++) {
                                        s1 += s[j];
                                        // s1+="\n";
                                    }
                                    this.user_list[i]['relationship'] = s1;
                                }
                                // this.user_list2 = this.user_list;
                                //  console.log(data.result);
                                this.show_loading = false;
                                //  $("#add_account").modal("toggle");
                            },
                            error => {
                                toastr.error("<h5>Server Error " + error + "<h5>");

                            }
                        );
                    },
                    error => { toastr.error("<h5>Server Error " + error + "<h5>"); }
                );

            }

        }


        else {
            toastr.warning("<h6>" + this.validated.message + "<h6>");

        }



    }
    // console.log(this.validated.status);
    // console.log(this.validated.message);
    // console.log(this.User_details);
    // if (this.User_details._user_role == "Support") this.User_details._user_role = 3;
    // else if (this.User_details._user_role == "User") this.User_details._user_role = 2;
    // else if (this.User_details._user_role == "Reseller") this.User_details._user_role = 1;
    // else { this.User_details._user_role = 0; }
    // if (this.User_details.user_status == "Active") this.User_details.user_status = 1;
    // else { this.User_details.user_status = 0; }
    edit_profile(User: any) {



        //  console.log("editprofile",User);
        // this.user_role = null;
        // this.user_status = null;
        //console.log(localStorage.getItem('currentUser'));

        this.edit_account = true;
        this.User_details._client_name = User.client_name;
        this.User_details._company_name = User.company_name;
        this.User_details._currency = User.currency;
        this.User_details._email_id = User.email_id;
        this.User_details._language = User.language;
        this.User_details._mobile_no = User.mobile_no;
        this.User_details._timezone = User.timezone;
        this.User_details._user_status = User.otp_status;
        this.User_details._user_role = User.user_role;
        this.User_details._user_type = User.designation;
        this.User_details._user_name = User.user_name;
        //  const usr=User.user_role;
        //  this.User_details=User;
        this.User_details._user_psw = User.user_psw;
        this.user_old_pwd = User.user_psw;
        this.userid = User.userid;
        $('#add_account').modal('show');
        // console.log("user details",this.User_details);
        // console.log("user password",this.User_details._user_psw);
        // console.log("usr",usr);
        //  console.log("usd",this.User_details);



    }
    wallet_clicked(User: any) {
        $("#walletmodal").modal('show');
        this.show_loading = true;
        this.w_userid = User.userid;
        this.note_ = "";
        this.amount_ = "";
        this._appService.get_method(this._userService.permission_manage_users_permissions + User.userid + "/" + "wallet").subscribe(
            data => {
                //  console.log(data);
                this.w_user_name = data.data[0].user_name;
                this.w_email_id = data.data[0].email_id;
                this.w_balance = data.data[0].balance;
                this.w_balance = parseInt(this.w_balance).toFixed(2);

            },
            error => {
            }
        );
        this._appService.get_method(this._userService.permission_manage_users_permissions + User.userid + "/" + "wallet-history").subscribe(
            data => {
                // console.log(data);
                if (!data.status) {
                    return toastr.error("<h5>" + data.message + "<h5>");
                }
                this.wallet_history = data.data;
                for (let i = 0; i < this.wallet_history.length; i++) {
                    this.wallet_history[i]['date'] = moment(this.wallet_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");
                }
                this.wallet_history = this.wallet_history.reverse();
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;

                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );



    }
    show_modal_loading = false;
    permissions_profile(User: any) {
        $('#permissions').modal('show');
        this.permis_uid = User.userid;
        this.show_modal_loading = true;
        // this.option1_value="others";
        this.bulksms_access = false;
        this.whatsapp_access = false;
        this.missedcall_access = false;
        this.shortcode_access = false;
        this.ivr_tollfree_access = false;
        this.voicecall_access = false;
        this.cutting_service = false;
        this.dnd_refund = false;
        this.live_status = false;
        this.open_senderid = false;
        this.block_number = false;
        this.KYC = false;
        this.startfrom_ = "";
        this.cutting_ = null;
        this.option1_value = "";
        this.option2_value = "";
        this.dlr_status_1 = null;
        this.dlr_status_2 = null;
        $('#bulksms').css('pointer-events', 'none')
        $('#voicecall').css('pointer-events', 'none')
        $('#missedcall').css('pointer-events', 'none')
        $('#shortcode').css('pointer-events', 'none')
        $('#ivrtollfree').css('pointer-events', 'none')
        $('#whatsapp').css('pointer-events', 'none')
        // console.log(User);
        for (let j = 0; j < this.dropdownList.length; j++) {
            this.selectedItems[j]['selected'] = false;
        }
        let routes = [];
        this.parent_permissions = [];
        this._appService.get_method(this._userService.permission_manage_users_permissions + User.userid + "/apps").subscribe(
            data => {
                if (data.status) {
                    let s = data.apps.split(',');
                    for (let i = 0; i < s.length; i++) {

                        if (s[i] == 'bulk_sms') { this.bulksms_access = true; $('#bulksms').css('pointer-events', 'all') }
                        else if (s[i] == 'bulk_voice') { this.voicecall_access = true; $('#voicecall').css('pointer-events', 'all') }
                        else if (s[i] == 'missed_call') { this.missedcall_access = true; $('#missedcall').css('pointer-events', 'all') }
                        else if (s[i] == 'short_code') { this.shortcode_access = true; $('#shortcode').css('pointer-events', 'all') }
                        else if (s[i] == 'ivr_toll') { this.ivr_tollfree_access = true; $('#ivrtollfree').css('pointer-events', 'all') }
                        else if (s[i] == 'whatsapp') { this.whatsapp_access = true; $('#whatsapp').css('pointer-events', 'all') }

                    }
                    // console.log("user app permissions",data)
                }
                else {
                    toastr.error("Server Error:Error Getting App Permissions");
                }
                // console.log(data.apps);

            },
            error => {
                toastr.error("Server Error:Error Getting App Permissions");
            }
        );
        this.parentApps = [];
        this._appService.get_method(this._userService.permission_manage_users_permissions + User.parent_id + "/apps").subscribe(
            data => {
                if (data.status) {
                    let s = data.apps.split(',');
                    for (let i = 0; i < s.length; i++) {
                        if (s[i] == 'bulk_sms') { this.parentApps.push('bulk_sms') }
                        else if (s[i] == 'bulk_voice') { this.parentApps.push('bulk_voice') }
                        else if (s[i] == 'missed_call') { this.parentApps.push('missed_call') }
                        else if (s[i] == 'short_code') { this.parentApps.push('short_code') }
                        else if (s[i] == 'ivr_toll') { this.parentApps.push('ivr_toll') }
                        else if (s[i] == 'whatsapp') { this.parentApps.push('whatsapp') }
                    }
                    // console.log("parent apps",data,this.parentApps)
                }
                else {
                    toastr.error("Server Error:Error Getting App Permissions");
                }
            },
            error => {
                toastr.error("Server Error:Error Getting App Permissions");
            }
        )
        this._appService.get_method(this._userService.permission_manage_users_permissions + "" + User.parent_id + "/permissions").subscribe(
            data => {
                try {
                    // console.log("parent permissions",data.data[0]['permissions']);
                    if (data.data[0]['permissions'] != "" && data.data[0]['permissions'] != null) {
                        this.parent_permissions = data.data[0]['permissions'].split(',');

                    }

                }
                catch (e) { }

            },
            error => {
                toastr.error("Server Error:Error Getting Parent Permissions");
                this.show_modal_loading = false;
            }
        )
        this._appService.get_method(this._userService.permission_manage_users_routes + "" + User.userid + "/routes").subscribe(
            data => {
                routes = data.routes;
                for (let i = 0; i < routes.length; i++) {
                    for (let j = 0; j < this.dropdownList.length; j++) {
                        if (this.dropdownList[j]['az_routeid'] == routes[i]) {
                            this.selectedItems[j]['selected'] = true;
                        }
                    }
                }
                // console.log("routes data",data);
            },
            error => {
                toastr.error("Server Error:Error Getting Route Permissions");
                this.show_modal_loading = false;
            }
        );
        this.parentRoutes = [];
        this._appService.get_method(this._userService.permission_manage_users_routes + "" + User.parent_id + "/routes").subscribe(
            data => {
                let routes = data.routes;
                //    console.log("parent routes",routes);
                for (let i = 0; i < routes.length; i++) {
                    for (let j = 0; j < this.dropdownList.length; j++) {
                        if (this.dropdownList[j]['az_routeid'] == routes[i]) {
                            // this.selectedItems[j]['selected'] = true;
                            this.parentRoutes.push(routes[i]);
                        }
                    }
                }
            },
            error => {
                toastr.error("Server Error:Error Getting Route Permissions");
                // this.show_modal_loading=false;
            }
        );
        this._appService.get_method(this._userService.permission_manage_users_permissions + "" + User.userid + "/permissions").subscribe(
            data => {
                let p_data = data.data[0];
                //  console.log("permissions",data.data[0]);
                let permission = p_data.permissions;
                // console.log("permissions1",p_data.permissions);
                if (permission != null && permission != "") {
                    let arr = permission.split(",");
                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i] == "DND_OR_NOT") this.dnd_refund = true;
                        else if (arr[i] == "LiveStatus") this.live_status = true;
                        else if (arr[i] == "openSenderId") this.open_senderid = true;
                        else if (arr[i] == "cuttingService") this.cutting_service = true;
                        else if (arr[i] == "kyc") this.KYC = true;

                    }

                }
                else { }
                if (this.cutting_service && p_data.is_cutting) {
                    this.cutting_service = true;
                    this.cutting_ = p_data.pre_cutting
                    this.startfrom_ = p_data.start_num_from;
                }
                // console.log(p_data.cutting_percent);
                if (p_data.cutting_percent != null && p_data.cutting_percent != "") {
                    // console.log(p_data.cutting_percent);
                    let arr = p_data.cutting_percent.split(",");
                    let arr1 = arr[0].split(":");
                    let arr2 = arr[1].split(":");
                    this.dlr_status_1 = parseInt(arr1[1], 10);
                    this.option1_value = arr1[0];
                    this.dlr_status_2 = parseInt(arr2[1], 10);
                    this.option2_value = arr2[0];
                    $('.selectpicker').selectpicker('refresh');
                    // console.log("options",this.option1_value,this.option2_value);
                }
                this.show_modal_loading = false;
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');
                }, 500);
            },
            error => {
                toastr.error("Server Error:Error Getting Permissions");
                this.show_modal_loading = false;
            }
        );
        //  console.log((this.userrole=='2'||this.userrole=='3' || this.userrole=='4' || this.userrole=='5') && this.parentRoutes.includes(21))
    }
    update_permissions() {
        if (this.validatePermissions() == true) {
            $('#permissions').modal('hide');

            let data = {
                "permissions": null,
                "is_cutting": null,
                "start_num_from": null,
                "pre_cutting": null,
                "cutting_percent": ""
            }
            let data1 = {
                "permissions": null,
                "is_cutting": null,
                "start_num_from": "",
                "pre_cutting": "",
                "cutting_percent": ""
            }
            let permissions = "";
            let routesUpdated = [];
            // console.log(this.selectedItems);
            for (let i = 0; i < this.selectedItems.length; i++) {
                if (this.selectedItems[i]['selected']) {
                    routesUpdated.push(this.selectedItems[i]['routeid']);
                }
                // console.log(routesUpdated);
            }

            if (this.live_status) permissions = permissions + "LiveStatus,";
            if (this.dnd_refund) permissions = permissions + "DND_OR_NOT,";
            if (this.open_senderid) permissions = permissions + "openSenderId,";
            if (this.KYC) permissions = permissions + "kyc";
            if (this.cutting_service) {
                permissions = permissions + ",cuttingService";

                data.permissions = permissions;
                data.is_cutting = 1;
                data.start_num_from = this.startfrom_;
                data.pre_cutting = this.cutting_;
                data.cutting_percent = data.cutting_percent + this.option1_value + ":" + this.dlr_status_1 + ",";
                data.cutting_percent = data.cutting_percent + this.option2_value + ":" + this.dlr_status_2;

                // console.log(data);

                this._appService.patch_method(this._userService.permission_manage_users_permissions + "" + this.permis_uid + "/permissions", JSON.stringify(data)).subscribe(
                    data => {
                        if (data.status) {
                            // toastr.success("<h6>Route Permissions Updated Successfully<h6>");
                            this._appService.patch_method(this._userService.permission_manage_users_routes + "" + this.permis_uid + "/routes", JSON.stringify({ 'routes': routesUpdated })).subscribe(
                                data => {
                                    if (data.status) {
                                        // toastr.success("<h6>Permissions Updated Successfully<h6>");
                                        this.updateAppPermissions();
                                    }
                                    else { toastr.error("<h5>" + data.message + "<h5>"); }

                                },
                                error => { return toastr.error("<h5> Server Error " + error + "<h5>"); }
                            );
                        }
                        else { toastr.error("<h5>" + data.message + "<h5>"); }
                    },
                    error => { toastr.error("<h5>Server Error " + error + "<h5>"); }

                );

            }
            else {
                data1.permissions = permissions;
                data1.is_cutting = 0;
                // console.log(data1);
                this._appService.patch_method(this._userService.permission_manage_users_permissions + "" + this.permis_uid + "/permissions", JSON.stringify(data1)).subscribe(
                    data => {
                        if (data.status) {

                            this._appService.patch_method(this._userService.permission_manage_users_routes + "" + this.permis_uid + "/routes", JSON.stringify({ 'routes': routesUpdated })).subscribe(
                                data => {
                                    if (data.status) {
                                        this.updateAppPermissions()
                                    }
                                    else {
                                        toastr.error("<h5>" + data.message + "<h5>");

                                    }
                                },
                                error => { toastr.error("<h5>Server Error " + error + "<h5>"); }
                            );
                        }
                        else { toastr.error("<h5>" + data.message + "<h5>"); }
                    },
                    error => { toastr.error("<h5>Server Error " + error + "<h5>"); }
                );

            }

            // let app=this.appsPresent.join();

        }




    }
    updateAppPermissions() {
        let app: string = '';
        if (this.bulksms_access) { app = app.concat('bulk_sms,') }
        if (this.voicecall_access) { app = app.concat('bulk_voice,') }
        if (this.missedcall_access) { app = app.concat('missed_call,') }
        if (this.shortcode_access) { app = app.concat('short_code,') }
        if (this.ivr_tollfree_access) { app = app.concat('ivr_toll,') }
        if (this.whatsapp_access) { app = app.concat('whatsapp,') }
        try {
            app = app.substring(0, app.length - 1)
        }
        catch (e) { }
        // console.log(app);
        this._appService.post_method(this._userService.permission_manage_users_permissions + this.permis_uid + "/apps", JSON.stringify({ 'apps': app })).subscribe(
            data => {
                if (data.status) { toastr.success("<h6>Permissions Updated Successfully<h6>"); this._walletService.appPermissionsChanged(); }
                else { toastr.error("<h5>" + data.message + "<h5>"); }
            },
            error => { toastr.error("<h5>Server Error " + error + "<h5>"); }
        );
    }

    validatePermissions() {
        let valid = false;
        //   console.log(this.option1_value);
        //   console.log(this.option2_value);
        //   console.log("startfrom",this.startfrom_);
        //   console.log("cutting%",this.cutting_);
        if (this.cutting_service) {
            if (this.startfrom_ == null || this.startfrom_ == "") return toastr.warning("<h5>Invalid Details for StartFrom<h5>");
            if (parseInt(this.startfrom_, 10) < 0) return toastr.warning("<h5>Invalid Details For startFrom<h5>");
            if (this.cutting_ == "" || this.cutting_ == null) return toastr.warning("<h5>Invalid Details for Cutting%<h5>");
            if (this.dlr_status_1 == null || this.dlr_status_1 < 0) return toastr.warning("<h5>Invalid Details for DlrStatus% 1<h5>");

            //  if()return  toastr.warning("Invalid Details for DlrStatus% 1");
            // console.log((/^[^.-]$/.test(this.dlr_status_1.toString())))
            if (this.dlr_status_2 == null || this.dlr_status_2 < 0) return toastr.warning("<h5>Invalid Details for DlrStatus% 2<h5>");
            if (this.option1_value == "") return toastr.warning("<h5>Select status for DlrStatus 1<h5>");
            if (this.option2_value == "") return toastr.warning("<h5>Select status for DlrStatus 2<h5>");
        }

        valid = true;
        return valid;

    }
    handleCuttingPercent(event) {
        if (event.target.value < 0) {
            this.cutting_ = null;
        }
        else if (event.target.value > 80) {
            this.cutting_ = null;
        }
    }

    handleChange(event) {
        if (event.target.value < 0 || event.target.value > 100) this.dlr_status_1 = null;
        else if (this.dlr_status_1 + this.dlr_status_2 > 100) {
            this.dlr_status_1 = 100 - this.dlr_status_2;
        }
        // let num=this.dlr_status_1.toString();
        // num = num.replace(/^0+/, "");
        // console.log(num);
        // this.dlr_status_1=parseInt(num,10);
        // if(this.dlr_status_1==NaN){this.dlr_status_1=null;}
        // console.log(this.dlr_status_1);
    }
    handleChange1(event) {
        if (event.target.value < 0 || event.target.value > 100) this.dlr_status_2 = null;
        else if (this.dlr_status_1 + this.dlr_status_2 > 100) this.dlr_status_2 = 100 - this.dlr_status_1;

    }
    eventoccured(checkbxindex: any) {
        // console.log("checked event occured");
        if (this.selectedItems[checkbxindex]['selected']) { this.selectedItems[checkbxindex]['selected'] = false; }
        else { this.selectedItems[checkbxindex]['selected'] = true; }
        // console.log(this.selectedItems);

    }
    // addsenderid_route(checkbxindex:any){
    //     console.log("checked event occured");
    //     if (this.add_sender_selectedItems[checkbxindex]['selected']) { this.add_sender_selectedItems[checkbxindex]['selected'] = false; }
    //     else { this.add_sender_selectedItems[checkbxindex]['selected'] = true; }
    //     console.log(this.add_sender_selectedItems);

    // }
    validateEmail(): boolean {
        this.User_details._email_id = this.User_details._email_id.trim();
        if (this.User_details._email_id == "" || this.User_details._email_id == null) {
            this.validated.message = "Email Field is Required";
            return false;
        }
        else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.User_details._email_id)) {
            this.validated.message = "Invalid Email Field";
            return false;
        }
        else {
            return true;
        }

    }
    validatefullname(): boolean {
        this.User_details._client_name = this.User_details._client_name.trim();
        if (this.User_details._client_name != "" && this.User_details._client_name != null) {
            let f_name = /^[a-zA-Z]+$/;
            let fullname = this.User_details._client_name.replace(/\s/g, "");
            if (this.User_details._client_name.length < 4) {
                this.validated.message = "Full Name Field is too small Minimum Characters Should be 4";
                return false;
            }
            if (this.User_details._client_name.length > 30) {
                this.validated.message = "Full Name Field is too Long Maximum Characters Should be 30";
                return false;
            }
            if (!f_name.test(fullname)) {
                this.validated.message = "Invalid Characters In Full Name Field";
                return false;
            }
            else {
                return true;
            }

        }
        else {
            this.validated.message = "Full Name Field is Required";
            return false;
        }
    }
    validateDesignation(): boolean {
        this.User_details._user_type = this.User_details._user_type.trim();
        if (this.User_details._user_type == "" || this.User_details._user_type == null) {
            this.validated.message = "Designation Field Is Required";
            return false;
        }
        else {
            let regexp = /^[a-zA-Z]+$/;
            let Designation = this.User_details._user_type.replace(/\s/g, "");
            Designation = Designation.replace(regexp, "");
            if (Designation != "") {
                this.validated.message = "Designation Field is Invalid";
                return false;
            }
            else if (this.User_details._user_type.length <= 20) {
                return true;
            }
            else {
                this.validated.message = "Designation Field is too long Maximum Characters Should be 20";
                return false;
            }

        }

    }
    validatelanguage(): boolean {
        // this.User_details._language=this.User_details._language.trim();
        if (this.User_details._language == "" || this.User_details._language == null) {
            this.validated.message = "Please Choose Language";
            return false;
        }
        else {
            return true;
        }
        // else if(this.User_details._language.length<4){
        //     this.validated.message="Language Field is Too Small Mimimum Characters Should be 4";
        //     return false;
        // }
        // else if(this.User_details._language.length>20){
        //     this.validated.message="Language Field is Too big Maximum Characters Should be 20";
        //     return false;
        // }
        // else {
        //     let regexp = /^[a-zA-Z]+$/;
        //     let language = this.User_details._language;
        //     language = language.replace(regexp, "");

        //     if (language != "") {
        //         this.validated.message="Language Field Is Invalid";
        //         return false;
        //     }
        //     else{
        //         return true;
        //     }
        // }

    }
    validatetimezone(): boolean {
        // this.User_details._timezone=this.User_details._timezone.trim();
        if (this.User_details._timezone == "" || this.User_details._timezone == null) {
            this.validated.message = "Please Choose TimeZone";
            return false;
        }
        else {
            return true;
        }
        // else if(this.User_details._timezone.length<4){
        //     this.validated.message="TimeZone Field is Too Small Mimimum Characters Should be 4";
        //     return false;
        // }
        // else if(this.User_details._timezone.length>20){
        //     this.validated.message="Timezone Field is Too big Maximum Characters Should be 20";
        //     return false;
        // }
        //   else if(!/^[a-zA-Z]+$/.test(this.User_details._timezone)){
        //     this.validated.message="TimeZone Field is Invalid";
        //     return false;
        //   }
        //   else{
        //       return true;
        //   }

    }
    validatecompanyname(): boolean {
        this.User_details._company_name = this.User_details._company_name.trim();
        if (this.User_details._company_name == null || this.User_details._company_name == "") {
            this.validated.message = "Company Name Field is Required";
            return false;
        }
        else if (this.User_details._company_name.length < 6 || this.User_details._company_name > 20) {
            this.validated.message = "Company Name Field Length should be between 6 and 20 characters";
            return false;

        }
        else if (!/^[a-zA-Z]+$/.test(this.User_details._company_name.replace(/\s/g, ""))) {
            this.validated.message = "Company Name Field is Invalid Only Characters Are Acceptable";
            return false;
        }
        else {
            return true;
        }

    }
    validatecurrency(): boolean {
        // this.User_details._currency=this.User_details._currency.trim();

        if (this.User_details._currency == null || this.User_details._currency == "") {
            this.validated.message = "Please Choose Currency";
            return false;
        }
        else {
            return true;
        }
        // else if(this.User_details._currency.length<2 || this.User_details._currency.length>20){
        //     this.validated.message="Currency Field Length Should be between and 2 and 20";
        //     return false;
        // }
        // else{
        //     if (!/^[a-zA-Z]+$/.test(this.User_details._currency.replace(/\s/g, ""))){
        //         this.validated.message="Currecny Field Is Invalid";
        //         return false;
        //     }
        //     else{
        //         return true;
        //     }

        // }


    }
    checkUsername() {
        this.User_details._user_name = this.User_details._user_name.trim();
        if (this.User_details._user_name == "" || this.User_details._user_name == null) {
            toastr.warning("<h5>UserId Cannot be Empty<h5>");
            return false;
        }
        this._appService.post_method(this._userService.check_username, JSON.stringify({ 'user_name': this.User_details._user_name })).subscribe(
            data => {
                //  console.log(data);
                if (data.status) {
                    toastr.info("User Id " + this.User_details._user_name + " is Available");
                    return true;
                }
                else {
                    toastr.warning("User Id " + this.User_details._user_name + " is Not Available Please Choose Another UserId");
                    return false;
                }

            },
            error => {
                toastr.warning("Error Checking UserId Availability")
                return false;
            }
        );
    }
    validateuserid(): boolean {
        this.User_details._user_name = this.User_details._user_name.trim();
        if (this.User_details._user_name == "" || this.User_details._user_name == null) {
            this.validated.message = "UserId Field is Required";
            return false;
        }
        else if (this.User_details._user_name.length < 4) {
            this.validated.message = "UserId Field is too Small Minimum Characters Should be 4";
            return false;
        }
        else if (this.User_details._user_name.length > 20) {
            this.validated.message = "UserId Field is too big Maximum Characters Should be 20";
            return false;
        }
        else {

            if (!/^[a-zA-Z]{1,}\w{1,}$/.test(this.User_details._user_name)) {
                this.validated.message = "Invalid UserId:UserId Should start with Alphabet followed by Numbers or Alphabet";
                return false;
            }
            else {
                return true;
            }

        }


    }
    validatemobile(): boolean {
        if (this.User_details._mobile_no != null && this.User_details._mobile_no != "") {
            if (this.User_details.mobile_no < 5 || this.User_details.mobile_no > 15) {
                this.validated.message = "Mobile No length Should be between 5 to 15";
                return false;
            }
            if (!/^\d{5,15}$/.test(this.User_details._mobile_no)) {
                this.validated.message = "Mobile No Field is Invalid";
                return false;

            }

            else {
                return true;
            }
        }
        else {
            this.validated.message = "Mobile No Field is Required";
            return false;
        }
    }
    validateNewPassword(): boolean {
        this.User_details._user_psw = this.User_details._user_psw.trim();
        if (this.User_details._user_psw == "") {
            this.validated.message = "Password Field is Required";
            return false;
        }
        else {
            if (/^(?=.*\d{1,})(?=.*\W{1,})(?=.*\w{1,}).*$/.test(this.User_details._user_psw)) {
                return true;
            }
            else {
                this.validated.message = "Password Format is Invalid,Should Contain Alphabet ,Number and Special Character ";
                return false;
            }
        }

    }
    validateUserRole(): boolean {
        // console.log(this.User_details._user_role);
        if (this.User_details._user_role === null || this.User_details._user_role === "") return false;
        return true;
    }
    validateUserStatus(): boolean {
        // console.log(this.User_details._user_status);
        if (this.User_details._user_status === null || this.User_details._user_status === "") return false;
        return true;
    }
    validateProfile() {


        this.validated.status = false;
        if (!this.validatefullname()) return false;
        if (!this.validateEmail()) return false;
        if (!this.validateDesignation()) return false;
        if (!this.validatemobile()) return false;
        if (!this.edit_account && !this.validateuserid()) return false;
        if (!this.edit_account && !this.validatecurrency()) return false;
        if (!this.validatelanguage()) return false;
        if (!this.validatetimezone()) return false;
        if (!this.validateNewPassword()) return false;
        if (!this.validatecompanyname()) return false;
        if (!this.validateUserStatus()) return this.validated.message = "Select User Status";
        //if(!this.validateOldPassword()) this.validated.message="Old Password is invalid"; return;  
        if (!this.edit_account && !this.validateUserRole()) return this.validated.message = "Select User Role";

        this.validated.status = true;
        this.validated.message = "Success.";
        return this.validated.status;
    }

    add_wallet_bal() {
        if (this.amount_ == "" || this.amount_ == null) return toastr.warning("<h5>Enter Amount<h5>");
        this.show_loading = true;
        //if (this.note_ == "" || this.note_ == null) return  toastr.warning("Enter correct Note");
        let data = {
            'balance': this.amount_,
            'type': 1,
            'remarks': this.note_

        }

        this._appService.patch_method(this._userService.permission_manage_users_permissions + this.w_userid + "/" + "wallet", JSON.stringify(data)).subscribe(
            data => {
                if (!data.status) {
                    this.show_loading = false;
                    return toastr.error("<h5>" + data.message + "<h5>");
                }
                toastr.success("<h6>" + data.message + "<h6>");
                this._appService.get_method(this._userService.permission_manage_users_permissions + this.w_userid + "/" + "wallet-history").subscribe(
                    data => {

                        if (data.status) {
                            this._appService.get_method(this._userService.permission_manage_users_permissions + this.w_userid + "/" + "wallet").subscribe(
                                data => {
                                    this.w_balance = data.data[0].balance;
                                    this.w_balance = parseInt(this.w_balance).toFixed(2);
                                },
                                error => {
                                }
                            );
                            this._walletService.walletbalance();
                            this.wallet_history = data.data.reverse();
                            this.amount_ = "";
                            this.note_ = "";
                            for (let i = 0; i < this.wallet_history.length; i++) {
                                this.wallet_history[i]['date'] = moment(this.wallet_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");
                            }
                            this._appService.get_method(this._userService.manage_user_add_account).subscribe(
                                data => {
                                    if (data.status) {
                                        this.user_list = data.result.reverse();
                                        for (let i = 0; i < this.user_list.length; i++) {
                                            this.user_list[i]['balance'] = this.user_list[i]['balance'].toFixed(2);
                                            let ar = this.user_list[i]['relationship'].split('->');
                                            this.user_list[i]['relationship'] = ar.join('->\n');
                                            let s = this.user_list[i]['relationship'].match(/(.|[\r\n]){1,30}/g);
                                            let s1 = "";
                                            for (let j = 0; j < s.length; j++) {
                                                s1 += s[j];
                                                // s1+="\n";
                                            }
                                            this.user_list[i]['relationship'] = s1;
                                        }
                                    }
                                    this.show_loading = false;
                                },
                                error => {
                                    toastr.error("<h5>Server Error " + error + "");
                                    this.show_loading = false;
                                });
                        }


                        //  this.show_loading=false;

                    },
                    error => {
                        this.show_loading = false;

                        toastr.error("<h6>Server Error " + error + "<h6>");

                    }
                );

            },
            error => {
                this.show_loading = false;

                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }

    deduct_wallet_bal() {
        if (this.amount_ == "" || this.amount_ == null) return toastr.warning("Enter Amount");
        if (parseInt(this.amount_) <= 0) {
            return toastr.warning("Invalid Amount");
        }
        // if (this.note_ == "" || this.note_ == null) return  toastr.warning("Enter correct Note");
        let data = {
            'balance': this.amount_,
            'type': 0,
            'remarks': this.note_

        }
        this.show_loading = true;

        this._appService.patch_method(this._userService.permission_manage_users_permissions + this.w_userid + "/" + "wallet", JSON.stringify(data)).subscribe(
            data => {
                //console.log(data);
                if (data.status) { toastr.success("<h5>" + data.message + "<h5>"); }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
                this._appService.get_method(this._userService.permission_manage_users_permissions + this.w_userid + "/" + "wallet-history").subscribe(
                    data => {

                        if (data.status) {
                            this._appService.get_method(this._userService.permission_manage_users_permissions + this.w_userid + "/" + "wallet").subscribe(
                                data => {
                                    this.w_balance = data.data[0].balance;
                                    this.w_balance = parseInt(this.w_balance).toFixed(2);

                                },
                                error => {
                                }
                            );
                            this._walletService.walletbalance();

                            this.wallet_history = data.data.reverse();
                            this.amount_ = "";
                            this.note_ = "";


                            for (let i = 0; i < this.wallet_history.length; i++) {
                                this.wallet_history[i]['date'] = moment(this.wallet_history[i]['date']).format("DD/MM/YYYY hh:mm:ss A");
                            }
                        }

                        this.show_loading = false;

                    },
                    error => {
                        this.show_loading = false;
                        toastr.error("<h5>Server Error " + error + "<h5>");

                        // console.log(error);
                    }
                );
            },
            error => {
                this.show_loading = false;

                toastr.error("<h5>Server Error " + error + "<h5>");

            }
        );
    }

    add_pricing_new() {
        if (this.add_pricing_gateway == "") return toastr.warning("Select Route");
        if (this.add_pricing_new_price <= 0) return toastr.warning("Invalid Price");
        $('#ind_pricing_modal_add').modal('hide');
        // console.log(this.add_pricing_gateway);
        // console.log(this.add_pricing_new_price);
        let countryData = $("#country").countrySelect("getSelectedCountryData");
        //console.log(countryData);
        let country = countryData.name.split("(");
        let iso2 = countryData.iso2.toUpperCase();
        let c_name = country[0].toUpperCase();
        let c_code = country_code[iso2]['code'];

        // console.log(c_name, c_code, iso2);
        let data = {
            'price': this.add_pricing_new_price.toString(),
            'route_id': this.add_pricing_gateway.toString(),
            'c_name': c_name,
            'c_iso': iso2,
            'c_num_code': c_code
        }
        this._appService.post_method(this._userService.permission_manage_users_permissions + this.p_u_id + "/" + "pricing", JSON.stringify(data)).subscribe(
            data => {
                // console.log(data);
                // $('#ind_pricing_modal_add').modal('toggle');
                if (data.status) { toastr.success("<h5>successfully Created<h5>"); }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
                this._appService.get_method(this._userService.permission_manage_users_permissions + this.p_u_id + "/" + "pricing").subscribe(
                    data => {
                        // this.pricing_permissions = data.data;
                        this.pricing_permissions_global = [];
                        this.pricing_permissions_india = [];
                        this.pricing_permissions = data.data;
                        for (let i = 0; i < this.pricing_permissions.length; i++) {
                            if (this.pricing_permissions[i]['iso2'] == "IN") {
                                this.pricing_permissions_india.push(this.pricing_permissions[i]);

                            }
                            else {
                                this.pricing_permissions_global.push(this.pricing_permissions[i]);
                            }
                        }

                        // console.log(this.pricing_permissions);

                        // console.log("pricing");
                    },
                    error => {
                        toastr.error("<h5>Server Error " + error + "<h5>");

                        //  console.log(error);
                    }
                );

            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");

                //  console.log(error);
            }
        )
    }

    status2_change() {
        this.status2_ = this.option2_value;
        // console.log("status2",this.status1_);
    }
    status1_change() {
        this.status1_ = this.option1_value;
        // console.log("status1",this.status2_);
    }
    tranrangeChanged() {
        if (this.calendar_range_transaction == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar_transaction = true;
        }
        else {
            this.show_calendar_transaction = false;
        }
    }
    transactionSearchData() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range_transaction == "") return;
        else if (this.calendar_range_transaction == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range_transaction == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range_transaction == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range_transaction == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range_transaction == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("Choose Dates");
            // this.from_date=moment(this.from_date,"MM/DD/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"MM/DD/YYYY").format('DD/MM/YYYY');
        }
        this.transaction_history_search(this.from_date, this.to_date);

    }
    branding_clicked() {
        $('#branding_modal').modal('show');
        this.show_loading = true;
        //  console.log(this.logo);
        // console.log(this.favicon);
        this.brand_favicon_ = "Choose file";
        this.brand_logo_ = "Choose file";
        this.brand_website_ = "";
        this.brand_sitename_ = "";
        this.brand_sitetitle_ = "";
        this.brand_wid = "";
        this.brand_color_ = "";
        this._appService.get_method(this._userService.branding_manageusers).subscribe(
            data => {
                // this.branding_=data.result[0];
                if (data.result.length != 0) {
                    this.brand_patch = true;
                    this.brand_website_ = data.result[0]['domain_name'];
                    this.brand_sitetitle_ = data.result[0]['title'];
                    this.brand_sitename_ = data.result[0]['website'];
                    this.brand_wid = data.result[0]['wid'];
                    this.color = data.result[0]['colour'];
                    //    
                    // this.brand_color_=data.result[0]['colour'];
                    if (data.result[0]['logo'] != "" && data.result[0]['logo'] != null) {
                        this.brand_logo_ = "Logo is Present";
                    }
                    if (data.result[0]['favicon'] != "" && data.result[0]['favicon'] != null) {
                        this.brand_favicon_ = "Favicon is Present";
                    }

                }
                this.show_loading = false;
                // console.log(data);
            },
            error => {
                this.show_loading = false;
                toastr.error("<h6>Server Error " + error + "<h6>");
                // console.log(error);
            }
        );
    }
    logo_file(files) {

        let ar = files[0].type.split('/');
        if (ar[0] == 'image') this.logo = files[0];
        else return toastr.warning("Only Image Files are allowed");
        // console.log("logo file",this.logo);

    }
    favicon_file(files) {

        let ar = files[0].type.split('/');
        if (ar[0] == 'image') this.favicon = files[0];
        else return toastr.warning("Only Image Files are allowed");
        //  console.log("favicon file",this.favicon);
    }

    create_branding() {

        // console.log(this.brand_logo_);
        // console.log(this.validateBranding()==);

        if (this.validateBranding() == true) {
            $('#branding_modal').modal('hide');
            this.savingBrand = true;
            if (this.brand_patch) {
                const data = new FormData()
                data.append('domain_name', this.brand_website_);
                data.append('title', this.brand_sitetitle_);
                data.append('website', this.brand_sitename_);
                data.append('logo', this.logo);
                data.append('favicon', this.favicon);
                data.append('colour', $('#colorpicker').css("color"));
                // console.log("in patch branding",this.logo);

                this._appService.patch_file_method(this._userService.branding_manageusers + this.brand_wid, data).subscribe(
                    data => {
                        if (data.status) {
                            toastr.success("<h5>Branding Successfully Updated<h5>");
                        }
                        else {
                            toastr.error("<h5>" + data.message + "<h5>");
                        }

                        this.savingBrand = false;
                    },
                    error => {

                        this.savingBrand = false;
                        toastr.error("<h5>Server Error " + error + "<h5>");
                    }
                );
            }
            else {
                const data = new FormData()
                data.append('domain_name', this.brand_website_);
                data.append('title', this.brand_sitetitle_);
                data.append('website', this.brand_sitename_);
                data.append('logo', this.logo);
                data.append('favicon', this.favicon);
                data.append('colour', $('#colorpicker').css("color"));
                // console.log("in create branding",this.logo);

                this._appService.post_file_method(this._userService.branding_manageusers + this.brand_wid, data).subscribe(
                    data => {
                        if (data.status) {
                            toastr.success("<h5>Branding Successfully Created<h5>");
                        }
                        else {
                            toastr.error("<h5>" + data.message + "<h5>");
                        }
                        this.savingBrand = false;
                    },
                    error => {
                        this.savingBrand = false;
                        toastr.error("<h5>Server Error " + error + "<h5>");
                    }
                );


            }
            return;
        }



    }
    validateBranding() {
        if (!this.validateBrandingWebsite()) return toastr.warning("Invalid Domain Name");
        if (!this.validateBrandingSiteTitle()) return toastr.warning("Invalid Site Title");
        if (!this.validateBrandingSiteName()) return toastr.warning("Invalid Site Name");
        //if(!this.validateBrandColor())return  toastr.warning("please Choose color");
        if (!this.validateBrandIcon()) return toastr.warning("Please Upload Icon");
        if (!this.validateBrandFavicon()) return toastr.warning("Please Upload Favicon");
        return true;
    }
    validateBrandingWebsite() {
        if (this.brand_website_ == "" || !/^(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm.test(this.brand_website_)) {

            return false;
        }
        else {
            return true;
        }

    }
    validateBrandingSiteTitle() {
        if (this.brand_sitetitle_ == "" || !/^[a-zA-Z]+$/.test(this.brand_sitetitle_.replace(/\s/g, ''))) { return false; }
        else {
            let web = this.brand_sitetitle_.split(' ').join("");
            if (/^[a-zA-Z]{4,}$/.test(web)) {
                return true;
            }
            return false;
        }

    }
    validateBrandColor() {
        if (this.brand_color_ == "") { return false; }
        else { return true; }

    }
    validateBrandingSiteName() {
        if (this.brand_sitename_ == "" || !/^[a-zA-Z]+$/.test(this.brand_sitename_.replace(/\s/g, ''))) { return false; }
        else { return true; }


    }
    validateBrandIcon() {
        if (this.brand_patch || this.brand_logo_ == "Logo is Present") {
            return true;
        }

        if (typeof this.logo === "undefined") {
            return false;
        }
        else {
            return true;

        }

    }
    validateBrandFavicon() {
        if (this.brand_patch || this.brand_favicon_ == "Favicon is Present") {
            return true;
        }
        else {
            if (typeof this.favicon === "undefined") { return false; }

        }
        return true;

    }
    validateFile(name: String) {
        let ext = name.substring(name.lastIndexOf('.') + 1);
        if (ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'xls' || ext.toLowerCase() == 'ods') {
            return true;
        }
        else {
            return false;
        }
    }
    importPricingDetails(files, target) {
        //let files=target.value;
        let pids = [];
        let prices = [];
        let file: File = files.item(0);
        if (file == null) {
            target.value = "";
            return;
        }
        let ext = file.name.substring(file.name.lastIndexOf('.') + 1);
        let file_ext = ext.toLowerCase();
        if (!this.validateFile(file.name)) {
            target.value = "";
            return toastr.warning('Selected file format is not supported.\n Only ods,xlxs,xls are supported currently');
        }

        let myReader: FileReader = new FileReader();
        if (file_ext == 'xlsx' || file_ext == 'xls' || file_ext == 'ods') { myReader.readAsArrayBuffer(file); }
        //  else if (file_ext == 'csv' || file_ext == 'txt') { myReader.readAsText(file); }
        else { return toastr.warning("Cant load file"); }
        myReader.onload = (e) => {
            //console.log(file_ext);
            if (file_ext == 'xlsx' || file_ext == 'xls' || file_ext == 'ods') {
                this.import_show = true;
                var arrayB = myReader.result;
                var data = new Uint8Array(arrayB);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");
                let workbook = XLSX.read(bstr, { type: "binary" });
                //console.log(XLSX.utils.sheet_to_csv(worksheet));
                for (let j = 0; j < workbook.SheetNames.length; j++) {
                    var sheet_name = workbook.SheetNames[j];
                    /* Get worksheet */
                    var worksheet = workbook.Sheets[sheet_name];
                    //console.log(XLSX.utils.sheet_to_json(worksheet,{raw:true}));
                    var result = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                    // console.log("result",result);
                    for (var i = 0; i < result.length; i++) {
                        if (result[i]['price'] == "" || !/^[0-9.]+$/.test(result[i]['price']) || result[i]['price'] <= 0) {
                            this.import_show = false;
                            return toastr.warning("<h5>Invalid value for price at Row " + (i + 1) + "<h5>");
                            // pids.push(result[i]['id']);
                            // prices.push("Invalid");

                        }
                        else {
                            pids.push(result[i]['id']);
                            prices.push(result[i]['price'].toString());
                        }



                    }

                }


                this.import_show = false;

            }
            if (pids.length == this.pricing_permissions.length) toastr.success("<h5>File Imported successfully<h5>");

            else { return toastr.warning("<h6>Invalid File (Do not change headers in the files and Do not Add New Rows)<h6>"); }
            //    console.log("File data",pids,prices);

            this._appService.patch_method(this._userService.permission_manage_users_permissions + this.p_u_id + "/pricing-multi", JSON.stringify({ 'prices': prices, 'pids': pids })).subscribe(
                data => {

                    //  console.log(data);
                    if (data.status) {
                        toastr.success("<h5> Pricing Details Updated Successfully <h5>");

                        this.show_loading = true;
                        this._appService.get_method(this._userService.permission_manage_users_permissions + this.p_u_id + "/" + "pricing").subscribe(
                            data => {
                                if (data.status) {
                                    // toastr.success("<h5>"+data.message+"<h5>")
                                }
                                if (!data.status) {
                                    toastr.error("<h5>" + data.message + "<h5>");
                                }
                                this.pricing_permissions_global = [];
                                this.pricing_permissions_india = [];
                                this.pricing_permissions = data.data;
                                for (let i = 0; i < this.pricing_permissions.length; i++) {
                                    if (this.pricing_permissions[i]['iso2'] == "IN") {
                                        this.pricing_permissions_india.push(this.pricing_permissions[i]);

                                    }
                                    else {
                                        this.pricing_permissions_global.push(this.pricing_permissions[i]);
                                    }
                                }

                                this.show_loading = false;
                            },
                            error => {
                                this.show_loading = false;
                                toastr.error("<h5>Server Error " + error + "<h5>");
                                // console.log(error);
                            }
                        );
                    }
                },
                error => {

                    toastr.error("<h5>Server error " + error + "<h5>")
                }
            )

        }




    }
    exportPricingDetails() {
        if (this.pricing_permissions.length == 0) {
            return toastr.warning("<h5>Data is Not Available to Export<h5>");
        }
        this.export_show = true;
        let exportArray = [];
        for (let i = 0; i < this.pricing_permissions.length; i++) {
            let d = {
                'id': this.pricing_permissions[i]['id'],
                'route': this.pricing_permissions[i]['az_rname'],
                'Country': this.pricing_permissions[i]['name'],
                'Country Code': this.pricing_permissions[i]['numcode'],
                'price': this.pricing_permissions[i]['price']
            }
            exportArray.push(d);

        }
        let ws = XLSX.utils.json_to_sheet(exportArray);
        let wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Pricing Details");
        XLSX.writeFile(wb, "Pricing Details.xlsx");
        this.export_show = false;
        return toastr.success("Data Exported Successfully");
    }
    login(user) {
        // console.log(user);
        this._appService.get_method(this._userService.manage_user_add_account + "/" + user.userid + "/login").subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    //  console.log(data);
                    localStorage.setItem('currentUser', JSON.stringify(data['token']));
                    localStorage.setItem('token', data['token']);
                    this._cookieService.set('userrole', data.user_profile['user_role']);
                    this._cookieService.set('userid', data.user_profile['user_id']);
                    this._cookieService.set('country', data.user_profile['country']);
                    this._cookieService.set('parentlogin', (data.user_profile['is_parent_login']).toString());
                    this._cookieService.set('parentid', data.user_profile['parent_id']);
                    toastr.success("<h5>Routing To DashBoard<h5>");
                    setTimeout(() => {
                        // console.log("after login",this._cookieService.get('userrole'),this._cookieService.get('userid'))
                        this._router.navigate(['/dashboard']);
                    }, 1000);
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }


            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }

    addPricing() {
        this.add_pricing_gateway = "";
        this.add_pricing_new_price = 0;
        $('#ind_pricing_modal_add').modal('show');
    }
}
