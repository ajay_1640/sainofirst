import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from './../../../../../app.service';
import { UserService } from '../../../../../auth/_services';
declare let toastr;
declare let moment;
@Component({
    selector: 'app-alerts',
    templateUrl: "./alerts.component.html",
    styles: []
})
export class AlertsComponent implements OnInit {
    send_flash = "";
    amount = "";
    emails = "";
    p = 1;
    show_loading = false;
    notifications = [];
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) {
    }

    ngOnInit() {
        this._script.loadScripts('app-alerts',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        this.getNotifications();

    }
    getNotifications() {
        this.show_loading = true;
        this._appService.get_method(this._userService.notifications).subscribe(
            data => {
                this.notifications = data.data;
                for (let i = 0; i < this.notifications.length; i++) {
                    this.notifications[i]['created_at'] = moment(this.notifications[i]['created_at']).format("DD/MM/YYYY hh:mm A");

                }
                this.show_loading = false;

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }
    updateEmails() {
        if (this.amount == "") {
            return toastr.warning("<h5> Enter Amount <h5>");
        }

        if (parseInt(this.amount, 10) < 0 || !/^[0-9]+$/.test(this.amount)) {
            return toastr.warning("<h5>Invalid Amount<h5>");
        }
        if (this.emails == "") {
            return toastr.warning("<h5>Enter Emails<h5>");
        }
        let ar = this.emails.replace(/\n/g, '').split(',');
        if (ar.length > 3) {
            return toastr.warning("<h5>Only 3 Emails are Allowed<h5>");
        }
        //console.log(ar);
        for (let i = 0; i < ar.length; i++) {

            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(ar[i].trim())) {
                //console.log("valid email");
                return toastr.warning("<h5>Check Emails<h5>");
            }
        }
        let d = {
            'limit': this.amount,
            'emails': this.emails
        }
        this._appService.post_method(this._userService.alert_settings, JSON.stringify(d)).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");

                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );

    }

}
