import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from "@angular/router";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from "rxjs/Rx";
import { CookieService } from 'ngx-cookie-service';
import { AppService } from '../../../../app.service';
import { UserService } from '../../../../auth/_services/user.service';
import { WalletService } from '../../../layouts/header-nav/wallet.service';



// import { ActivatedRoute } from '@angular/router';


@Injectable()
export class Permission implements CanActivate {
    //current_routes=[];
    user_role = "";
    user_id = "";
    live_status = false;
    constructor(private _router: Router, private _walletService: WalletService, private _cookieService: CookieService, private _route: ActivatedRoute, private _appService: AppService, private _userService: UserService) {
        this.user_role = this._cookieService.get('userrole');
        this.user_id = this._cookieService.get('userid');
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        this._walletService.showsettings = true;
        //console.log("in auth has permission");
        //console.log(route);
        //console.log(state);
        let s: any = route.url.toString();
        let s1 = s.lastIndexOf('/');
        let path = s.substring(s1 + 1, s.length);
        //console.log(path);
        if (path == 'manage-customers') {
            if (this.user_role == '4' || this.user_role == '5') { this._router.navigate(['/app/profile']) }
            else { return true }
        }
        else if (path == 'sender-id-approval') {
            if (this.user_role == '4') { this._router.navigate(['/app/profile']) }
            else { return true; }
        }
        else if (path == 'mgmtk') {
            if (this.user_role == '1') {
                return true;
            }
            else { this._router.navigate(['/app/profile']) }
        }
        else if (path == 'smpp') {
            if (this.user_role != '1') {
                this._router.navigate(['/app/profile'])
            }
            else { return true; }
        }
        else if (path == 'live-status') {
            //debugger
            this.user_role = this._cookieService.get('userrole');
            this.user_id = this._cookieService.get('userid');

            return this._appService.get_method(this._userService.permission_manage_users_permissions + "" + this.user_id + "/permissions").map(
                data => {
                    let p_data = data.data[0];
                    // console.log("permissions",p_data);
                    let permission = p_data.permissions;
                    if (permission != null && permission != "") {
                        let arr = permission.split(",");
                        for (let i = 0; i < arr.length; i++) {
                            if (arr[i] == "LiveStatus") { this.live_status = true; }
                        }
                        console.log(this.live_status);
                        console.log((this.live_status && this.user_role != '4') || this.user_role == '1' || this.user_role == '5')
                        if ((this.live_status && this.user_role != '4') || this.user_role == '1' || this.user_role == '5') {
                            // this._router.navigate(['/theme/live-status'])
                            //debugger
                            return true;
                        }

                        else {
                            //debugger
                            this._router.navigate(['/app/profile'])
                        }

                    }
                    else {
                        this._router.navigate(['/app/profile'])

                    }
                },
                error => {
                    this._router.navigate(['/app/profile'])
                }
            );
        }
        else if (path == 'kyc') {
            if (this.user_role == '4') { this._router.navigate(['/app/profile']) }
            else { return true; }
        }


    }
}