import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ComponentFactoryResolver, ViewChild, ViewContainerRef, ViewEncapsulation, } from '@angular/core';
import { AppService } from '../../../../app.service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { timezone } from '../../../pages/default/profile/my-profile/country codes';
import * as XLSX from 'xlsx';
import { UserService } from '../../../../auth/_services/';
import { WalletService } from '../../../../theme/layouts/header-nav/wallet.service';
import { timeout } from 'rxjs/operator/timeout';
declare let toastr: any;
declare let moment: any;

declare let $;
let schedule_date = "";
@Component({
    selector: 'app-customized-sms',
    templateUrl: './customized-sms.component.html',
    styles: []
})
export class CustomizedSmsComponent implements OnInit {

    constructor(private _appService: AppService, private _walletService: WalletService, private _userService: UserService, private _router: Router, private _script: ScriptLoaderService) { }
    routeId = ""
    //	senderId="";
    validated = false;
    hideSenderId = false;
    BulkUploadFile: any;
    selectedRouteName = "";
    selectedSenderIdName = "";
    select_col = '';
    preview_text = '';
    searchText = "";
    onSelectColumn = "";
    messagearr = [];
    tot_characters = "";
    tot_sms = "";
    show_loading = false;
    customize_test_file = "";
    max_msg_count: number = 140;
    route = null;
    senderId = null;
    smsTimezone = "";
    has_sender_id: boolean;
    smsTimezone1 = "";

    routes_list;
    caretposition = null;
    senderid_list;
    route_name = null;

    sender_id_name = null;

    send_flash = false;
    numbers = '';
    numbers_count = 0;
    contentType = 'English';
    template = 'Template';
    message_t_area = '';
    template_list;
    temp_value_selected;
    file_data;
    groups_list;
    preview_selected = false;

    credits_data = {
        "pr_balance": 0,
        "tr_balance": 0
    };
    p = 1;
    arrayBuffer;
    customers = [];
    columns = [];
    kyc_status = 0;
    radio_types = ['English', 'Unicode'];
    timezone = "";
    timezoneDropdown = {};
    timezoneobject = [];

    ngOnInit() {
        this._script.loadScripts('app-customized-sms',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-customized-sms',
            ['assets/demo/demo3/base/toastr.js']);
        this._script.loadScripts('app-customized-sms',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        // this._script.loadScripts('app-customized-sms',
        //     ['assets/demo/demo3/base/dropzone.js']);
        this._script.loadScripts('app-customized-sms',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        // $('customize_schedule').timepicker('refresh');
        let s = this;
        $("#message").bind("keydown click focus", function() {
            s.caretposition = this.selectionStart;
            //  console.log(this.selectionStart);
        });
        let s_send = this;
        let Dropzone = $('#m-dropzone-four').dropzone(
            {
                url: "#",
                paramName: "file",
                maxFiles: 1,
                maxFilesize: 100,
                addRemoveLinks: 1,
                acceptedFiles: ".ods,.csv,.xlsx,.xls",
                //accept: function(e, o) { "justinbieber.jpg" == e.name ? o("Naha, you don't.") : o() },
                maxfilesexceeded: function(file) { this.removeFile(file); },
                removedfile: function(file) {
                    s_send.clearData();
                    $(document).find(file.previewElement).remove();
                },
                complete: function(file) {
                    console.log("comming");
                    if (!file.accepted) this.removeFile(file);
                    // var reader = new FileReader();
                    // reader.onload = function(event) {
                    //    // event.target.result contains base64 encoded image
                    //     console.log(event.target);
                    //     s_send.dataURLtoFile(event.target['result'],'hii.xlsx')

                    console.log("file", file)
                    // };
                    // reader.readAsDataURL(file);
                    let ext = file.name.substring(file.name.lastIndexOf('.') + 1);
                    if (ext.toLowerCase() == 'ods' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'xls') {
                        s_send.sendFileForHeader(file);
                    }
                    else if (ext.toLowerCase() == 'csv') {
                        s_send.readFileCsv(file);
                    }
                    else {
                        this.removeFile(file);
                        toastr.warning("Only CSV,ODS,XLS,XLSX Files are Allowed");
                        return;
                    }
                    // s_send.readFile(file);
                    //s_send.readFile(file);

                },
                error: function(file) {
                    if (!file.accepted) this.removeFile(file);
                    else {

                        //file = file.slice(0, 1024);
                        // console.log("in dropzone else", file)
                        // var myReader = new FileReader();
                        // let rABS = true;
                        // let data;
                        // myReader.addEventListener("loadend", function(event) {
                        //     console.log(event.target['result'])
                        //     let arrayBuffer = new Uint8Array(event.target['result']);
                        //     var binary = "";
                        //     var length = arrayBuffer.byteLength;
                        //     for (var i = 0; i < length; i++) {
                        //       binary += String.fromCharCode(arrayBuffer[i]);
                        //     }
                        //     // call 'xlsx' to read the file
                        //     var workbook = XLSX.read(binary, {type: 'binary', cellDates:true, cellStyles:true});
                        //     //if(!rABS) data = new Uint8Array(arrayBuffer);
                        //     //var workbook = XLSX.read(arrayBuffer, {type: 'array'});
                        //     console.log(workbook);
                        //     for (let j = 0; j < workbook.SheetNames.length; j++) {
                        //         if(j==1)break;
                        //         var sheet_name = workbook.SheetNames[j];
                        //         var worksheet = workbook.Sheets[sheet_name];
                        //         var result = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                        //         console.log( Object.keys(result[0])); 
                        //         s_send.columns = Object.keys(result[0]);       
                        //     }
                        // });
                        // //  reader.readAsText(file);
                        // myReader.readAsArrayBuffer(file)


                        // myReader.addEventListener("loadend", function(event){
                        //     let arrayBuffer = event.target['result'];
                        //     var binary = "";
                        //     console.log(arrayBuffer);
                        //     var data = new Uint8Array(arrayBuffer);
                        //     var length = data.byteLength;
                        //     for (var i = 0; i < length; i++) {
                        //         binary += String.fromCharCode(data[i]);
                        //     }
                        //     let workbook:any;
                        //     try{
                        //         workbook = XLSX.read(binary, { type: "binary" });
                        //     }
                        //     catch(e){
                        //         console.log(e);
                        //     }
                        //     for (let j = 0; j < workbook.SheetNames.length; j++) {
                        //         if(j==1)break;
                        //         var sheet_name = workbook.SheetNames[j];
                        //         var worksheet = workbook.Sheets[sheet_name];
                        //         var result = XLSX.utils.sheet_to_json(worksheet, { raw: true });
                        //         console.log( Object.keys(result[0])); 
                        //     }
                        // });
                    }
                }
            });

        Dropzone.autoDiscover = false;
        this.timezoneDropdown = timezone
        this.timezoneobject = Object.keys(this.timezoneDropdown);
        this.getCredits();
        this.getGroups();
        this.getRoutes();
        this.getSenderId();
        this.getKycStatus();
        this.getUserTimezone();

        // $('#m_modal_schedule').on('shown.bs.modal', function(e) {

        //  //   $('customize_schedule').timepicker('refresh');
        // })
        jQuery(function() {
            var datepicker: any = $('input.datepicker');

            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    schedule_date = datepicker[0].value;
                    (<any>$(this)).datepicker('hide');
                })
            }
        });
        this.customize_test_file = this._userService.files + "/files/testfiles/customize_test_file.csv";

    }
    //     caretPositionChange(target)
    //     {

    //         this.caretposition=target.selectionStart;
    //         console.log(target.selectionStart);
    //         $('#').a
    // //console.log(document.getElementById('message').selectionStart);
    //     }
    clearData() {
        this.BulkUploadFile = null;
        this.message_t_area = '';
        this.columns = [];
        this.messagearr = [];
        this.select_col = "";
        this.onSelectColumn = "";
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
    }
    getKycStatus() {
        this._appService.get_method(this._userService.kyc_status).subscribe(
            data => {
                this.kyc_status = data.status;
                if (this.kyc_status == 0) {
                    this.message_t_area = "Thank you for using Saino First Platform. You will be able to change the content of your message after you complete the KYC in Profile and Add Balance."
                }
            },
            error => {

            }
        );
    }
    dataURLtoFile(dataurl, filename) {
        console.log("dataurl to file")
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        console.log(new File([u8arr], filename, { type: mime }))
        // this.readFile(new File([u8arr], filename, {type:mime}))
    }
    getRoutes() {
        this._appService.get_method(this._userService.get_routes_url).subscribe(
            data => {
                this.routes_list = data.data;
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['selected'] = false;
                }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);
            },
            error => {
            });
    }
    getSenderId() {
        this._appService.get_method(this._userService.get_senderid_url).subscribe(
            data => {
                this.senderid_list = data.sender_information;
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['selected'] = false;
                }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);
            },
            error => {
            });
    }
    getSpecificSenderId(val) {
        this.senderid_list = [];
        for (let j = 0; j < this.routes_list.length; j++) {
            if (this.routes_list[j]['az_rname'] == val) {
                this.route = this.routes_list[j];
                break;
            }
        }
        this.selectedRouteName = this.route.az_rname;
        if (this.route['is_senderid'] == 0) {
            this.hideSenderId = true;
        }
        else {
            this.hideSenderId = false;
        }
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
        this.route_name = this.route.az_gateway_name;
        this._appService.get_method(this._userService.get_senderid_url + '/' + this.route.az_routeid).subscribe(
            data => {
                //  console.log(data);
                this.senderid_list = data.sender_information;
                this.has_sender_id = true;
                if (!this.senderid_list.length) {

                    return this.has_sender_id = false;
                }
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['selected'] = false;
                    if (this.senderid_list[i]['senderid'] == this.selectedSenderIdName) {
                        this.senderid_list[i]['selected'] = true;
                        this.senderId = this.senderid_list[i];
                        this.sender_id_name = this.senderId.senderid;
                    }
                }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);




            },
            error => {

            });

    }
    getSpecificRoute(val) {
        for (let j = 0; j < this.senderid_list.length; j++) {
            if (this.senderid_list[j]['senderid'] == val) {
                this.senderId = this.senderid_list[j];
                break;
            }
        }
        this.selectedSenderIdName = this.senderId.senderid;

        //  console.log("route senderid specific route ",this.selectedRouteName,this.selectedSenderIdName);

        this.sender_id_name = this.senderId.senderid;
        this._appService.get_method(this._userService.get_routes_url + '/' + this.senderId.sid).subscribe(
            data => {
                //  console.log(data);
                this.routes_list = data.data;
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['selected'] = false;
                    if (this.routes_list[i]['az_rname'] == this.selectedRouteName) {
                        this.routes_list[i]['selected'] = true;
                        this.route = this.routes_list[i];
                        this.route_name = this.route.az_gateway_name;
                    }
                }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);
                //  console.log(this.routes_list);
                $('#search_categories').val(this.selectedRouteName);
                //  console.log("route senderid specific route end ",this.selectedRouteName,this.selectedSenderIdName);


                //console.log(JSON.stringify(this.routes_list) + 'sender id data');
            },
            error => {
                // console.log('get error')
            });
    }
    getCredits() {
        this._appService.get_method(this._userService.msg_credits_url).subscribe(
            data => {
                this.credits_data = data.data;
            },
            error => {
            });
    }
    getGroups() {
        this._appService.get_method(this._userService.get_groups_url).subscribe(
            data => {
                this.groups_list = data.groups;

            },
            error => {
            });
    }


    validateFile(name: String) {
        let ext = name.substring(name.lastIndexOf('.') + 1);
        if (ext.toLowerCase() == 'csv' || ext.toLowerCase() == 'ods' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'xls') {
            return true;
        }
        else {
            return false;
        }
    }

    readFileCsv(files: File): void {
        this.caretposition = null;
        let file: any = files;
        this.BulkUploadFile = file;
        file = file.slice(0, 1024 * 10);
        this.columns = [];
        this.customers = [];
        this.preview_text = "";
        this.message_t_area = "";
        this.select_col = "";
        this.messagearr = [];
        this.preview_selected = true;
        var myReader: FileReader = new FileReader();
        myReader.readAsText(file);
        myReader.onload = (e) => {
            let data: any = myReader.result;
            let dataarr = data.split('\n');
            this.tot_sms = dataarr.length.toString();
            this.columns = dataarr[0].trim().split(',');
            if (data != null) {
                let columnvariables = {};
                for (let k = 0; k < this.columns.length; k++) {
                    columnvariables[this.columns[k]] = "";
                }
                for (let i = 1; i < dataarr.length; i++) {
                    if (i == 5) break;
                    let valuearr = dataarr[i].trim().split(",");
                    if (valuearr.length == this.columns.length) {
                        let col_data = columnvariables;
                        for (let val = 0; val < valuearr.length; val++) {
                            col_data[this.columns[val]] = valuearr[val];
                        }
                        this.customers.push(JSON.parse(JSON.stringify(col_data)));
                    }
                }

            }
            setTimeout(() => {
                $('#selectColumns').selectpicker('refresh');
            }, 200)

        }

    }


    onSelect(col) { this.onSelectColumn = col; }
    columnSelected() {
        this.preview_text = "";
        let phone_regex = /^\d{5,15}$/;
        for (let i = 0; i < this.customers.length; i++) {
            if (!phone_regex.test(this.customers[i][this.select_col])) {
                this.select_col = "";
                toastr.warning("<h5>Select Correct Column<h5>");
                break;
            }
        }
    }
    preview() {
        this.messagearr = [];
        if (this.select_col == "") return toastr.warning("<h5>Select Column With No's<h5>");
        this.preview_selected = true;
        let arr = [];
        for (let i = 0; i < this.columns.length; i++) { arr.push("{" + this.columns[i] + "}"); }
        for (let i = 0; i < this.customers.length; i++) {
            let user_phone_no, user_message;
            user_phone_no = this.customers[i][this.select_col];
            user_message = this.message_t_area;
            //to replace {column_values} with values from the input
            for (let j = 0; j < this.columns.length; j++) {
                user_message = user_message.replace(new RegExp(arr[j], 'g'), this.customers[i][this.columns[j]]);
            }
            //push both phone no and message to messagearr
            this.messagearr.push(JSON.parse(JSON.stringify({ 'phoneno': user_phone_no, 'message': user_message })));
            this.preview_text = this.preview_text.concat(user_phone_no + " => " + user_message + "\n");

        }
        this.tot_sms = this.messagearr.length.toString();
        // console.log(this.messagearr);
    }
    insertintoMessagebox() {
        // console.log(this.caretposition);
        if (this.onSelectColumn == null || this.onSelectColumn == '') {
            return toastr.warning("<h5>No Column Choosen<h5>");
        }
        if (this.caretposition == null) {
            this.message_t_area = this.message_t_area + " " + "{" + this.onSelectColumn + "}";
        }
        else {
            this.message_t_area = [this.message_t_area.slice(0, this.caretposition), "{" + this.onSelectColumn + "}", this.message_t_area.slice(this.caretposition)].join(' ');
            // this.caretposition=this.caretposition+this.onSelectColumn.length+4;
        }
    }
    make_unique() {

        // let xs = this.column_t_area.split("\n");
        // xs = xs.toString().split("\,");
        // //console.log(xs+'after split');
        // let unique_array = xs.filter(function(elem, index, self) {
        //     return index == self.indexOf(elem);
        // });
        // // console.log(unique_array+'new msg');
        // let new_msg = unique_array.toString();
        // new_msg = new_msg.replace(/\,/g, '\n');
        // this.column_t_area = new_msg;

    }
    get_numbers() {
        let str = "";
        for (let i = 0; i < this.messagearr.length; i++) {
            str = str.concat(this.messagearr[i]['phoneno'] + ",");
        }


        return str;
    }
    SendNow() {

        let valid_ = this.validateform();
        if (!valid_) return;
        this.smsTimezone1 = this.timezone;
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
        $('#m_modal_send').modal('show');


    }
    calcTime(city, offset, type) {
        let d = new Date();
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        let nd;
        if (type == '+') {
            nd = new Date(utc + (3600000 * offset));

        }
        else {
            nd = new Date(utc - (3600000 * offset));
        }
        return nd.toLocaleString();

    }
    send() {
        //    let schedule_time=moment().format("YYYY-MM-DD HH:mm:ss");
        //     let timezone=this.smsTimezone1.substr(this.smsTimezone1.length-7,6);
        //     let targetCountryTime;
        //     let timediff=timezone.substr(1,5).split(':'); 
        //     let hours=parseInt(timediff[0]);
        //     let  minutes=0;
        //     if(parseInt(timediff[1])!=0){ minutes=parseInt(timediff[1])/60; }
        //     let targetCountryPresentTime;
        //     if(timezone[0]=='+'){ targetCountryPresentTime=this.calcTime("country",hours+minutes,'+');  }
        //    else{ targetCountryPresentTime=this.calcTime("country",hours+minutes,'-'); }           
        //     targetCountryTime=moment(scheduled_time,['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
        //     targetCountryTime=targetCountryTime.concat("GMT"+timezone);
        //     let final_date=moment(new Date(targetCountryTime).toLocaleString("en-US", {timeZone: "Asia/kolkata"}),'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
        //     console.log("final date",final_date);
        //     console.log("targetcountry",targetCountryTime);
        //     console.log("targetcountry present time",targetCountryPresentTime);
        //     if(moment(targetCountryPresentTime,'DD/MM/YYYY, HH:mm:ss').diff(final_date)>0){
        //         return toastr.info(this.smsTimezone1+" Present time is Ahead of Your System Time Please Choose Another Timezone");
        //     }
        // console.log(schedule_time);
        // let s='a';
        // if('b'!=s){
        //     return;
        // }
        toastr.options = {
            "closeButton": true,
            "maxOpened": 1,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"

        };

        let content_num;
        let is_flash = 0;
        if (this.send_flash) is_flash = 1; else is_flash = 0;
        let col_number = this.columns.indexOf(this.select_col);
        if (col_number < 0) {
            return toastr.error("Message Sending Failed No Numbers Found");
        }
        $('#m_modal_send').modal('hide');
        toastr.info("<h5>Processing Data Please wait...<h5>");
        if (!this.has_sender_id) {

            this.senderId = {
                'sid': "",
                'senderid': ""
            };
        }
        if (this.contentType == 'English') content_num = "0"; else content_num = "1";
        var data_send = new FormData();
        data_send.append("numbers", this.BulkUploadFile);
        data_send.append("route_id", this.route.az_routeid);
        data_send.append('sms_count', this.tot_sms);
        data_send.append('sender_id', this.senderId.sid);
        data_send.append('gateway_name', this.route.az_gateway_name);
        data_send.append('senderid_name', this.senderId.senderid);
        data_send.append('message', this.message_t_area);
        data_send.append('sms_type', "Customize Sms");
        data_send.append('unicode_type', content_num);
        data_send.append('is_scheduled', "0");
        data_send.append('schedule_time', "");
        data_send.append('is_flash', is_flash.toString());
        data_send.append('column_number', col_number.toString());
        //    console.log("hii",data_send.get('numbers'));
        //    console.log(data_send.get('route_id'));
        //    console.log(data_send.get('sms_count'));
        //    console.log(data_send.get('sender_id'));
        //    console.log(data_send.get('gateway_name'));
        //    console.log(data_send.get('senderid_name'));
        //    console.log(data_send.get('unicode_type'));
        //    console.log(data_send.get('is_scheduled'));
        //    console.log(data_send.get('schedule_time'));
        //    console.log(data_send.get('is_flash'));
        //    console.log(data_send.get('column_number'));


        //  console.log(data_send.get('numbers'),data_send.get('sender_id'));
        // Display the key/value pairs
        //  console.log(data_send);
        this._appService.post_file_method(this._userService.send_customize_sms_url, data_send).subscribe(
            data => {
                //  console.log(data);
                if (data.status) {
                    this._walletService.walletbalance();

                    // console.log("message sent successfully");
                    toastr.success("Message Sent Successfully!");
                    $("#showtoast").click();
                    // this._walletService.walletbalance();

                }
                else {
                    //  console.log("sending failed");
                    toastr.error("Message Sending Failed! " + data.message);
                    $("#showtoast").click();
                }
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
                // console.log('post error');
            });



        // console.log('route: ' + this.route + ' senderId: ' + this.senderId + ' select_col: ' + this.select_col + ' column: ' + this.column_t_area + ' preview_text: ' + this.preview_text +
        //     ' message: ' + this.message_t_area);
    }
    getUserTimezone() {

        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.timezone = data.profile_details[0]['timezone'];
                // let s=this.timezone.lastIndexOf('/');
                // this.timezone=this.timezone.substring(s+1,this.timezone.length);
                this.smsTimezone = this.timezone;
                this.smsTimezone1 = this.timezone;
                //  console.log("user timezone",this.timezone);              
            },
            error => {
            }
        );


    }
    getTimeAndDate() {
        // console.log("in gettimeanddate");
        let time: any;
        let date: any;
        date = moment(schedule_date, ["DD/MM/YYYY"]).format("YYYY-MM-DD");
        let timepicker: any = $('#customize_schedule');
        time = timepicker[0].value;
        time += " :00";
        time = moment(time, ["h:mm A :ss"]).format("HH:mm:ss A");
        let required_time = date + " " + time;
        required_time = moment(required_time, ["YYYY-MM-DD HH:mm:ss A"]).subtract(5.5, "hours").format("YYYY-MM-DD HH:mm:ss");
        return required_time;

    }
    scheduleNow() {
        let valid_ = this.validateform();
        if (!valid_) return;
        this.smsTimezone = this.timezone;
        //  console.log("smstimezone schedule",this.smsTimezone);
        $('#m_modal_schedule').modal('show');
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
    }

    // handle:any;
    sendFileForHeader(file) {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "maxOpened": 1,
            "autoDismiss": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "0",
            "hideDuration": "0",
            "timeOut": "0",
            "extendedTimeOut": "0",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        //         var handle = toastr.success("Test","Great", {timeout: NEVER}); 
        // //...
        // handle.closeToast();
        toastr.info("<h5>Retrieving Information From File. It May Take a While Please Wait....<h5>");
        this.show_loading = true;
        this.BulkUploadFile = file;
        this.customers = [];
        this.columns = [];
        var data_send = new FormData();
        data_send.append("numbers", this.BulkUploadFile);
        this._appService.post_file_method(this._userService.preview_data_url, data_send).subscribe(
            data => {
                console.log(data);
                if (data.status) {
                    let result = data.data;
                    this.columns = Object.keys(result[0]);
                    for (let i = 0; i < result.length; i++) {
                        if (i == 5) break;
                        this.customers.push(JSON.parse(JSON.stringify(result[i])));
                    }
                    setTimeout(() => { $('#selectColumns').selectpicker('refresh'); }, 200)
                    toastr.clear()
                    setTimeout(() => {
                        toastr.success("<h5>Data Reading Successful<h5>");
                    }, 50)
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        // "maxOpened":1,
                        // "autoDismiss":true,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "0",
                        "hideDuration": "0",
                        "timeOut": "1000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    this.show_loading = false;
                }
                else {
                    toastr.error("<h5>Data Reading Failed<h5>");
                    this.show_loading = false;
                }
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
                this.show_loading = false;
                // console.log('post error');
            });
    }
    ReadCsvFiles(file) {

    }

    schedule() {
        let time: any;
        let date: any;
        date = moment(schedule_date, ["DD/MM/YYYY"]).format("YYYY-MM-DD");
        let timepicker: any = $('#customize_schedule');
        time = timepicker[0].value;
        time += " :00";
        time = moment(time, ["h:mm A :ss"]).format("HH:mm:ss A");
        let required_time = date + " " + time;
        required_time = moment(required_time, ["YYYY-MM-DD HH:mm:ss A"]).format("YYYY-MM-DD HH:mm:ss");
        // let scheduled_time_date = this.getTimeAndDate();
        // scheduled_time_date = scheduled_time_date.toString();
        if (required_time == "Invalid date") {
            return toastr.error("Invalid date");
        }
        // let scheduled_time=new moment();
        // scheduled_time=moment($('input#date1')[0]['value']+" "+$('#m_timepicker_1')[0]['value'],"DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
        let scheduled_time = new moment();
        scheduled_time = required_time;
        let timezone = this.smsTimezone.substr(this.smsTimezone.length - 7, 6);
        let targetCountryTime;
        let timediff = timezone.substr(1, 5).split(':');
        let hours = parseInt(timediff[0]);
        let minutes = 0;
        if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
        let targetCountryPresentTime;
        if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
        else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }
        targetCountryTime = moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
        targetCountryTime = targetCountryTime.concat("GMT" + timezone);
        let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
        // console.log("final date",final_date);
        // console.log("targetcountry",targetCountryTime);
        // console.log("targetcountry present time",targetCountryPresentTime);
        if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
            return toastr.info("Present Time at " + this.smsTimezone + " Ahead of the Selected Time Please Choose Valid Time");
        }
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "maxOpened": 1,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        let content_num;
        let is_flash = 0;

        if (this.send_flash) is_flash = 1; else is_flash = 0;
        let col_number = this.columns.indexOf(this.select_col);
        if (col_number < 0) {
            return toastr.error("Message Sending Failed No Numbers Found");
        }
        $('#m_modal_schedule').modal('hide');
        toastr.info("<h5>Processing Data Please wait...<h5>");
        if (!this.has_sender_id) {

            this.senderId = {
                'sid': "",
                'senderid': ""
            };
        }
        if (this.contentType == 'English') content_num = "0"; else content_num = "1";
        let data_send = new FormData();
        data_send.append("numbers", this.BulkUploadFile);
        data_send.append("route_id", this.route.az_routeid);
        data_send.append('sms_count', this.tot_sms);
        data_send.append('sender_id', this.senderId.sid);
        data_send.append('gateway_name', this.route.az_gateway_name);
        data_send.append('senderid_name', this.senderId.senderid);
        data_send.append('message', this.message_t_area);
        data_send.append('sms_type', "Customize Sms");
        data_send.append('unicode_type', content_num);
        data_send.append('is_scheduled', "1");
        data_send.append('schedule_time', final_date);
        data_send.append('is_flash', is_flash.toString());
        data_send.append('column_number', col_number.toString());

        this._appService.post_file_method(this._userService.send_customize_sms_url, data_send).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    this._walletService.walletbalance();

                    //console.log("message sent successfully");
                    toastr.success("Message Sent Successfully!");
                    $("#showtoast").click();
                }
                else {
                    //  console.log("sending failed");
                    toastr.error("Message Sending Failed!" + " " + data.message);
                    $("#showtoast").click();
                }
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
                //  console.log('post error');
            });




    }
    automate() {

    }
    validateform() {
        this.validated = false;
        //console.log("validation called");
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        // console.log(this.route_name);
        // console.log(this.sender_id_name);
        // console.log(this.message_t_area);
        if (!this.validateRoute()) { toastr.error("Select Route"); return false; }
        if (!this.validateSenderId()) { toastr.error("Select SenderId"); return false; }
        // if (!this.validateNumbers()) { toastr.error("Check the Numbers Entered"); return false; }
        if (!this.validatefile()) { toastr.error("No File Found"); return false; }
        if (!this.validateColumn()) { toastr.error("Select column with Number"); return false; }
        if (!this.validatemessage()) { toastr.error("Check the Message"); return false; }
        this.validated = true;
        return this.validated;
        // console.log(this.validated);
    }
    validatefile() {
        if (this.BulkUploadFile == "" || this.BulkUploadFile == null) return false;
        return true;
    }
    validateColumn() {
        if (this.select_col == "") return false;
        return true;
    }
    validateRoute() {
        if (this.route_name == null) return false;
        return true;
    }
    validateSenderId() {

        if (this.sender_id_name != null) return true;
        if (this.sender_id_name == null && !this.has_sender_id) return true;
        return false;
    }

    validatemessage() {
        if (this.message_t_area != null && this.message_t_area.trim() != "") return true;
        else return false;
    }


    contentTypechanged() {
        // console.log(this.contentType);
        if (this.contentType == "Unicode") {
            this.contentType = "English";
        }
        else {
            this.contentType = "Unicode";
        }
        // console.log(this.send_flash);
        // console.log(this.contentType);
    }

}
