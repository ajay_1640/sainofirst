import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation, } from '@angular/core';
import { AppService } from '../../../../app.service';
import { timezone } from '../../../pages/default/profile/my-profile/country codes';

import * as XLSX from 'xlsx';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { UserService } from '../../../../auth/_services/user.service';
import { WalletService } from '../../../../theme/layouts/header-nav/wallet.service';
// import { DropzService } from './dropz.service';
// import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
// import { DEFAULT_DROPZONE_CONFIG } from './dropz.service';
// import {HeaderNavComponent} from '../../../layouts/header-nav/header-nav.component';
import { User } from '../../../../auth/_models';
import swal from 'sweetalert2';
// import {WalletService} from '../../../layouts/header-nav/wallet.service';

declare let $;
//declare let Dropzone: any;
declare let toastr: any;
let schedule_date = "";
let schedule_time = "";
declare let moment;
export let droppedfiles = [];


@Component({
    selector: 'app-bulksms',
    templateUrl: './bulksms.component.html',
    styleUrls: ['./bulksms.component.css']
})
export class BulksmsComponent implements OnInit {

    //  config: DropzoneConfigInterface = DEFAULT_DROPZONE_CONFIG;
    sender_id_name = null;
    route_name = null;
    route_id = null;
    contentType_ = false;
    send_flash = false;
    show_to_numbers = true;
    sms_count = 0;
    kyc_status = 0;
    selectedSenderIdName = ""
    selectedRouteName = "";

    // get_bulk_sms_url = 'http://35.238.188.102/saino-first/dash_board/get-bulk-sms-details';
    // get_group_details = 'http://35.238.188.102/saino-first/dash_board/get-group-details';
    // get_groups_url = 'http://35.238.188.102/saino-first/api/groups';
    // get_routes_url = 'http://35.238.188.102/saino-first/api/dashboard/routes';
    // get_senderid_url = 'http://35.238.188.102/saino-first/api/dashboard/sender_id';
    // msg_credits_url = "http://35.238.188.102/saino-first/api/dashboard/sms_balance";
    // add_group_nos_url = "http://35.238.188.102/saino-first/api/groups/multiple"
    // send_sms_url = "http://35.238.188.102/saino-first/api/dashboard/send_sms"
    max_msg_count: number = 160;
    route = {
        "az_routeid": null,
        "az_rname": null,
        "az_issenderid": null,
        "az_gateway_name": null
    };
    senderId = null;
    has_sender_id: boolean;
    numbers: String;

    numbers_count = 0;
    show_loading = false;
    validated = false;
    contentType = "English";
    template = null;
    message = '';
    reading_data = false;
    template_list;
    kycmessage = "";
    temp_value_selected;
    file_data;
    groups_list;
    routes_list: any;
    time_ngmodel = "";
    hideSenderId = false;
    date_ngmodel = "";
    senderid_list;
    timezone = "";
    smsTimezone = "";
    smsTimezone1 = "";

    BulkUploadFile: any = null;
    credits_data = {
        "pr_balance": 0,
        "tr_balance": 0
    };
    file_size = 0;

    schedule_sms = false;
    arrayBuffer;
    searchText = "";
    timezoneDropdown = {};
    timezoneobject = [];
    file_ext;
    checkedList: any[] = [];//Just created a list
    constructor(private _script: ScriptLoaderService, private _walletService: WalletService, private _userService: UserService, private _appService: AppService, private _router: Router) {
        // this._walletService.onWalletChangedNew.subscribe((m:any) => {
        //     console.log(m);this._walletService.wallet_balance=m;

        // })
    }


    ngOnInit() {

        this._script.loadScripts('app-bulksms',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-bulksms',
            ['assets/demo/demo3/base/toastr.js']);
        this._script.loadScripts('app-bulksms',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        // this._script.loadScripts('app-bulksms',
        //     ['assets/demo/demo3/base/dropzone.js']);
        this._script.loadScripts('app-bulksms',
            ['assets/build/js/moment-timezone.js']);
        this._script.loadScripts('app-bulksms',
            ['assets/build/js/moment-timezone-with-data.js']);
        let s_send = this;
        let Dropzone = $('#m-dropzone-three').dropzone(
            {
                url: "#",
                paramName: "file",
                maxFiles: 1,
                maxFilesize: 100,
                addRemoveLinks: 1,
                acceptedFiles: ".ods,.csv,.xlsx,.xls,.txt",
                accept: function(e, o) { "justinbieber.jpg" == e.name ? o("Naha, you don't.") : o() },
                maxfilesexceeded: function(file) { this.removeFile(file); },
                removedfile: function(file) {
                    s_send.BulkUploadFile = null;
                    $(document).find(file.previewElement).remove();
                    //console.log("file removed")
                },
                error: function(file) {
                    if (!file.accepted) this.removeFile(file);
                    else {
                        s_send.BulkUploadFile = file;
                        // console.log("file stored",s_send.BulkUploadFile);
                    }
                }

            });

        Dropzone.autoDiscover = false;
        this.timezoneDropdown = timezone
        this.timezoneobject = Object.keys(this.timezoneDropdown);

        this.get_templates();
        this.getCredits();
        this.getKycStatus();
        this.getGroups();
        this.getRoutes();
        this.getSenderId();
        this.getUserTimezone();


        // $('#"m_modal_schedule').on('shown.bs.modal', function (e) {
        //     $('#date1').datepicker('refresh');
        //     $('m_timepicker_1').timepicker('refresh');
        // })
        this._script.loadScripts('app-bulksms',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        jQuery(function() {
            var datepicker: any = $('input.datepicker');

            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    schedule_date = datepicker[0].value;
                    (<any>$(this)).datepicker('hide');

                    //console.log(this.schedule_date);



                })
            }
        });

        $('.selectpicker').selectpicker({ size: 4 });
        // $('#m_modal_schedule').on('shown.bs.modal', function (e) {
        //     var datepicker: any = $('input.datepicker');
        //         datepicker.datepicker('refresh');

        //         // do something...
        //       })
        // console.log("routeslist***" + JSON.stringify(this.routes_list));
    }
    getKycStatus() {
        this._appService.get_method(this._userService.kyc_status).subscribe(
            data => {
                this.kyc_status = data.status;
                if (this.kyc_status == 0) {
                    for (let i = 0; i < this.template_list.length; i++) {
                        if (this.template_list[i]['template_name'] == "SAINODEF") {
                            this.kycmessage = this.template_list[i]['template_data'];
                            this.message = this.template_list[i]['template_data'];
                            break;
                        }
                    }
                }

            },

        );

    }
    update_count() {
        this.numbers_count = this.numbers.split("\n").length;

    }
    getUserTimezone() {

        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.timezone = data.profile_details[0]['timezone'];
                this.smsTimezone = this.timezone;
                this.smsTimezone1 = this.timezone;
                // let s=this.timezone.lastIndexOf('/');
                // this.timezone=this.timezone.substring(s+1,this.timezone.length);

                //    console.log("user timezone",this.timezone);       

            },
            error => {
            }
        );


    }
    get_templates() {
        this._appService.get_method(this._userService.template_url).subscribe(
            data => {
                if (!data.status) return;
                this.template_list = data.templates;
                setTimeout(() => {
                    $('#selectpickertemplate').selectpicker('refresh');
                }, 200)
            },
            error => { });
    }
    getScheduleTime(time, type) {
        // console.log(this.smsTimezone1);
        let scheduled_time = new moment();
        scheduled_time = time;
        let timezone = "";
        if (type == 'schedule') {
            timezone = this.smsTimezone1.substr(this.smsTimezone1.length - 7, 6);
        }
        else {
            timezone = this.smsTimezone.substr(this.smsTimezone.length - 7, 6);
        }
        // console.log(timezone,time);
        let offset: any = new Date().getTimezoneOffset();
        offset = offset.toString();
        if (offset[0] == '-') {
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").add(parseInt(offset), "minutes").format("YYYY-MM-DD HH:mm:ss");
        }
        else {
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").subtract(parseInt(offset), "minutes").format("YYYY-MM-DD HH:mm:ss");
        }
        if (timezone[0] == '+') {
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").add(hours + minutes, "hours").format("YYYY-MM-DD HH:mm:ss");
        }
        else {
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            scheduled_time = moment(scheduled_time, "YYYY-MM-DD HH:mm:ss").subtract(hours + minutes, "hours").format("YYYY-MM-DD HH:mm:ss");
        }
        return scheduled_time;
    }
    getTimeAndDate(type) {

        let time: any;
        let date: any;
        let timepicker: any = $('#bulksms_schedule');
        date = moment(schedule_date, ["DD/MM/YYYY"]).format("YYYY-MM-DD");
        time = timepicker[0].value;
        time += " :00";
        time = moment(time, ["h:mm A :ss"]).format("HH:mm:ss A");
        let required_time = date + " " + time;
        required_time = moment(required_time, ["YYYY-MM-DD HH:mm:ss A"]).format("YYYY-MM-DD HH:mm:ss");
        return required_time;

    }

    getRoutes() {
        this._appService.get_method(this._userService.get_routes_url).subscribe(
            data => {
                if (!data.status) return;
                this.routes_list = data.data;
                // console.log(" routes called routes data",data);
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['selected'] = false;
                }
                setTimeout(() => {
                    $('#selectpicker1').selectpicker('refresh');

                }, 200)
            },
            error => { });
    }

    getSenderId() {
        this._appService.get_method(this._userService.get_senderid_url).subscribe(
            data => {
                if (!data.status) return;
                this.senderid_list = data.sender_information;
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['selected'] = false;
                }
                setTimeout(() => {
                    $('#selectpicker2').selectpicker('refresh');

                }, 200);
                // console.log(JSON.stringify(this.senderid_list) + 'sender id data');
            },
            error => {
                //console.log('get error')
            });
    }

    getSpecificSenderId(val) {
        // this.hideSenderId=false;
        this.senderid_list = [];

        for (let j = 0; j < this.routes_list.length; j++) {
            if (this.routes_list[j]['az_rname'] == val) {
                this.route = this.routes_list[j];
                break;
            }
        }
        //console.log("selected route",this.route);
        if (this.route['is_senderid'] == 0) {
            this.hideSenderId = true;
        }
        else {
            this.hideSenderId = false;
        }
        this.selectedRouteName = this.route.az_rname;
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
        //console.log("route senderid ",this.selectedRouteName,this.selectedSenderIdName);
        this.route_name = this.route.az_gateway_name;
        this._appService.get_method(this._userService.get_senderid_url + '/' + this.route.az_routeid).subscribe(
            data => {
                //console.log(data);
                this.senderid_list = data.sender_information;
                this.has_sender_id = true;
                if (!this.senderid_list.length) {
                    return this.has_sender_id = false;
                }
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['selected'] = false;
                    if (this.senderid_list[i]['senderid'] == this.selectedSenderIdName) {
                        this.senderid_list[i]['selected'] = true;
                        this.senderId = this.senderid_list[i];
                        this.sender_id_name = this.senderId.senderid;
                    }
                }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);

                //console.log(this.senderid_list);

                // console.log("route senderid ",this.selectedRouteName,this.selectedSenderIdName);


            },
            error => {

            });

    }

    getSpecificRoute(val) {
        for (let j = 0; j < this.senderid_list.length; j++) {
            if (this.senderid_list[j]['senderid'] == val) {
                this.senderId = this.senderid_list[j];
                break;
            }
        }
        this.selectedSenderIdName = this.senderId.senderid;

        // console.log("route senderid specific route ",this.selectedRouteName,this.selectedSenderIdName);

        this.sender_id_name = this.senderId.senderid;
        this._appService.get_method(this._userService.get_routes_url + '/' + this.senderId.sid).subscribe(
            data => {
                //  console.log(data);
                this.routes_list = data.data;
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['selected'] = false;
                    if (this.routes_list[i]['az_rname'] == this.selectedRouteName) {
                        this.routes_list[i]['selected'] = true;
                        this.route = this.routes_list[i];
                        this.route_name = this.route.az_gateway_name;
                    }
                }
                // console.log(this.routes_list)
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);
                $('#search_categories').val(this.selectedRouteName);
                // console.log("route senderid specific route end ",this.selectedRouteName,this.selectedSenderIdName);


                //console.log(JSON.stringify(this.routes_list) + 'sender id data');
            },
            error => {
                // console.log('get error')
            });
    }
    onCheckboxChange(option, event) {
        //console.log(JSON.stringify(option) + 'check');
        if (event.target.checked) {
            this.checkedList.push(option.group_id);
        } else {
            for (var i = 0; i < this.groups_list.length; i++) {
                if (this.checkedList[i] == option.group_id) {
                    this.checkedList.splice(i, 1);
                }
            }
        }
        //  console.log(this.checkedList);
        let data_send = JSON.stringify({
            'groups': this.checkedList
        });
        this.numbers = '';
        if (this.checkedList.length != 0) {
            this._appService.post_method(this._userService.add_group_nos_url, data_send).subscribe(
                data => {
                    // console.log('received ' + JSON.stringify(data["contact-information"]));
                    for (var i = 0; i < data.total_contacts; i++) {
                        this.numbers += data["contact-information"][i].contact_number + '\n';
                    }
                    // this.update_count();
                },
                error => {
                    // console.log('post error');
                });
        }

        //  this.update_count();
    }

    make_unique() {
        this.numbers = this.numbers.replace(/[\n]/g, '\,');
        this.numbers = this.numbers.replace(/\,/g, '\n');
        let xs = this.numbers.split("\n");
        xs = xs.toString().split("\,");
        //console.log(xs+'after split');
        let unique_array = xs.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
        // console.log(unique_array+'new msg');
        let new_msg = unique_array.toString();
        new_msg = new_msg.replace(/\,/g, '\n');
        this.numbers = new_msg;
        // console.log("in make unique",this.numbers);
    }
    getCredits() {
        this._appService.get_method(this._userService.msg_credits_url).subscribe(
            data => {
                if (!data.status) return;
                this.credits_data = data.data;
            },
            error => { });
    }
    getGroups() {
        this._appService.get_method(this._userService.get_groups_url).subscribe(
            data => {
                // console.log("groups data",data);
                //if(!data.subscribe)return;
                this.groups_list = data.groups;

            },
            error => { });
    }

    validateFile(name: String) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        this.file_ext = ext.toLowerCase();
        if (ext.toLowerCase() == 'csv' || ext.toLowerCase() == 'ods' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'xls') {
            return true;
        }
        else {
            return false;
        }
    }

    readFile(files: FileList) {
        var file: File = files.item(0);
        //  console.log(file);
        if (!this.validateFile(file.name)) {
            return toastr.warning('Selected file format is not supported.\n Only csv,ods,xlxs,xls are supported currently');
        }
        var myReader: FileReader = new FileReader();
        myReader.onload = (e) => {
            if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods') {
                this.arrayBuffer = myReader.result;
                let data = new Uint8Array(this.arrayBuffer);
                let arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                let bstr = arr.join("");
                let workbook = XLSX.read(bstr, { type: "binary" });
                for (let sheet = 0; sheet < workbook.SheetNames.length; sheet++) {
                    var sheet_name = workbook.SheetNames[sheet];
                    let worksheet = workbook.Sheets[sheet_name];
                    this.numbers = this.numbers.concat(XLSX.utils.sheet_to_csv(worksheet));
                    this.numbers = this.numbers.concat('\n');
                }


            }
            else if (this.file_ext == 'csv') {
                this.file_data = myReader.result;
                this.numbers = this.numbers.concat(this.file_data);
                this.numbers = this.numbers.concat('\n');
                this.show_loading = false;
            }
        }

        if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods')
            myReader.readAsArrayBuffer(file);
        else
            myReader.readAsText(file);


    }
    clear_nos() { this.numbers = ''; }
    select_temp(x) {
        this.message = JSON.stringify(x);
        this.message = this.message.substr(1, this.message.length - 2);
        this.MessageChanged();
    }
    MessageChanged() {
        if (this.contentType == 'English') this.sms_count = Math.ceil(this.message.length / 160);
        else {
            this.sms_count = Math.ceil(this.message.length / 70);
        }

    }
    DropzoneFile(files) { this.numbers = ""; }
    calcTime(city, offset, type) {
        let d = new Date();
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        let nd;
        if (type == '+') {
            nd = new Date(utc + (3600000 * offset));

        }
        else {
            nd = new Date(utc - (3600000 * offset));

        }
        return nd.toLocaleString();

    }
    SendNow() {
        // console.log("route senderid ",this.route,this.senderId);
        this.MessageChanged();
        let valid_ = this.validateform();
        if (!valid_) return;
        $('#m_modal_send').modal('show');
        this.smsTimezone = this.timezone;
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
    }
    changes() {
        // console.log(this.smsTimezone);
    }
    send() {
        // let schedule_time;
        // schedule_time=moment().format("YYYY-MM-DD HH:mm:ss");
        // let timezone=this.smsTimezone.substr(this.smsTimezone.length-7,6);
        // let targetCountryTime;
        // let timediff=timezone.substr(1,5).split(':'); 
        // let hours=parseInt(timediff[0]);
        // let  minutes=0;
        // if(parseInt(timediff[1])!=0){ minutes=parseInt(timediff[1])/60; }
        // let targetCountryPresentTime;
        //     if(timezone[0]=='+'){ targetCountryPresentTime=this.calcTime("country",hours+minutes,'+');  }
        //    else{ targetCountryPresentTime=this.calcTime("country",hours+minutes,'-'); }       
        // targetCountryTime=moment(scheduled_time,['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
        // targetCountryTime=targetCountryTime.concat("GMT"+timezone);
        // let final_date=moment(new Date(targetCountryTime).toLocaleString("en-US", {timeZone: "Asia/kolkata"}),'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
        // console.log("final date",final_date);
        // console.log("targetcountry",targetCountryTime);
        // console.log("targetcountry present time",targetCountryPresentTime);
        // if(moment(targetCountryPresentTime,'DD/MM/YYYY, HH:mm:ss').diff(final_date)>0){
        //     return toastr.info(this.smsTimezone+" Present time is Ahead of the Current Time Please Choose Another Timezone");
        // }  
        // console.log("schedule time",schedule_time,moment());
        // let s='a';
        // if('b'!=s){
        //     return;
        // }
        $('#m_modal_send').modal('hide');
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        let content_num;
        let is_flash = 0;
        let tonumbers = 0;

        //console.log("sender id",this.senderId);
        if (!this.has_sender_id) {

            this.senderId = {
                'sid': "",
                'senderid': ""
            };
        }
        if (this.send_flash) is_flash = 1; else is_flash = 0;
        if (this.contentType == 'English') content_num = "0"; else content_num = "1";
        if (this.show_to_numbers) { tonumbers = 1; }
        else { tonumbers = 0; }
        this.numbers = this.numbers.trim();
        var numbers = this.numbers.replace(/\n/g, ',');
        //    this.numbers=this.numbers.substr(0,this.numbers.length);
        let data_send = JSON.stringify({
            'numbers': numbers,
            'route_id': this.route.az_routeid,
            'sms_count': this.sms_count,
            'sender_id': this.senderId.sid,
            'gateway_name': this.route.az_gateway_name,
            'senderid_name': this.senderId.senderid,
            'message': this.message,
            'sms_type': "Quick Send",
            'unicode_type': content_num,
            'is_scheduled': "0",
            'schedule_time': "",
            'is_flash': is_flash,
            'to_numbers': tonumbers
        });
        let data_send1 = new FormData();
        data_send1.append('numbers', this.BulkUploadFile);
        data_send1.append('route_id', this.route.az_routeid);
        data_send1.append('sms_count', this.sms_count.toString());
        data_send1.append('sender_id', this.senderId.sid);
        data_send1.append('gateway_name', this.route.az_gateway_name);
        data_send1.append('senderid_name', this.senderId.senderid);
        if (this.kyc_status == 1) {
            data_send1.append('message', this.message);
        }
        else {
            data_send1.append('message', this.kycmessage);
        }
        data_send1.append('sms_type', "Quick Send");
        data_send1.append('unicode_type', content_num.toString());
        data_send1.append('is_scheduled', "0");
        data_send1.append('schedule_time', "");
        data_send1.append('is_flash', is_flash.toString());
        data_send1.append('to_numbers', tonumbers.toString());
        // toastr.info("<h5>Processing Data Please wait...<h5>");
        toastr.info("<h5>Processing Data Please wait...<h5>");
        if (this.show_to_numbers) {
            if (this.kyc_status == 0) {
                data_send['message'] = this.kycmessage;
            }
            this._appService.post_method(this._userService.send_sms_url, data_send).subscribe(
                data => {
                    if (data.status) {
                        this._walletService.walletbalance();
                        toastr.success(data.message);
                        $("#showtoast").click();
                    }
                    else {

                        toastr.error("Message Sending Failed!" + " " + data.message);
                        $("#showtoast").click();
                    }
                },
                error => {
                    toastr.error("Message Sending failed:Server Error " + error + "");

                });
        }
        else {
            this._appService.post_file_method(this._userService.send_sms_url, data_send1).subscribe(
                data => {
                    if (data.status) {
                        this._walletService.walletbalance();
                        toastr.success(data.message);
                        $("#showtoast").click();

                    }
                    else {
                        toastr.error("Message Sending Failed!" + " " + data.message);
                        $("#showtoast").click();
                    }
                },
                error => {
                    toastr.error("Message Sending failed:Server Error " + error + "");
                });
        }

    }
    ScheduleNow() {
        //console.log("route senderid ",this.route,this.senderId);
        this.MessageChanged();
        let valid = this.validateform();
        if (!valid) return;
        $('#m_modal_schedule').modal('show');
        this.smsTimezone1 = this.timezone;
        setTimeout(() => {
            $('#selectpicker4').selectpicker('refresh');
        }, 50)

    }
    schedule() {
        let time: any;
        let date: any;
        let timepicker: any = $('#bulksms_schedule');
        date = moment(schedule_date, ["DD/MM/YYYY"]).format("YYYY-MM-DD");
        time = timepicker[0].value;
        time += " :00";
        time = moment(time, ["h:mm A :ss"]).format("HH:mm:ss A");
        let required_time = date + " " + time;
        required_time = moment(required_time, ["YYYY-MM-DD HH:mm:ss A"]).format("YYYY-MM-DD HH:mm:ss");
        if (required_time == "Invalid date") {
            return toastr.error("Invalid date");
        }
        let scheduled_time = new moment();
        scheduled_time = required_time;
        let timezone = this.smsTimezone1.substr(this.smsTimezone1.length - 7, 6);
        let targetCountryTime;
        let timediff = timezone.substr(1, 5).split(':');
        let hours = parseInt(timediff[0]);
        let minutes = 0;
        if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
        let targetCountryPresentTime;
        if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
        else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }
        targetCountryTime = moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
        targetCountryTime = targetCountryTime.concat("GMT" + timezone);
        let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
        // console.log("final date",final_date);
        //  console.log("targetcountry",targetCountryTime);
        //  console.log("targetcountry present time",targetCountryPresentTime);
        if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
            return toastr.info(this.smsTimezone1 + " Present time At Selected Timezone Location is Ahead of the Selected Time Please Choose Valid Time");
        }

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        let content_num;
        let is_flash = 0;
        let tonumbers = 0;

        $('#m_modal_schedule').modal('hide');

        if (!this.has_sender_id) {
            this.senderId = {
                'sid': "",
                'senderid': ""
            };
        }
        if (this.send_flash) is_flash = 1; else is_flash = 0;
        if (this.contentType == 'English') content_num = "0"; else content_num = "1";
        if (this.show_to_numbers) { tonumbers = 1; }
        else { tonumbers = 0; }
        // this.numbers=this.numbers.replace(/\n/g, '\,');
        this.numbers = this.numbers.trim();
        var numbers = this.numbers.replace(/\n/g, ',');
        // this.numbers=this.numbers.substr(1,this.numbers.length-1);
        let data_send = JSON.stringify({
            'numbers': numbers,
            'route_id': this.route.az_routeid,
            'sms_count': this.sms_count,
            'sender_id': this.senderId.sid,
            'gateway_name': this.route.az_gateway_name,
            'senderid_name': this.senderId.senderid,
            'message': this.message,
            'sms_type': "Quick Send",
            'unicode_type': content_num,
            'is_scheduled': "1",
            'schedule_time': final_date,
            'is_flash': is_flash,
            'to_numbers': tonumbers
        });
        let data_send1 = new FormData();
        data_send1.append('numbers', this.BulkUploadFile);
        data_send1.append('route_id', this.route.az_routeid);
        data_send1.append('sms_count', this.sms_count.toString());
        data_send1.append('sender_id', this.senderId.sid);
        data_send1.append('gateway_name', this.route.az_gateway_name);
        data_send1.append('senderid_name', this.senderId.senderid);
        if (this.kyc_status == 1) {
            data_send1.append('message', this.message);
        }
        else {
            data_send1.append('message', this.kycmessage);
        }

        data_send1.append('sms_type', "Quick Send");
        data_send1.append('unicode_type', content_num.toString());
        data_send1.append('is_scheduled', "1");
        data_send1.append('schedule_time', final_date);
        data_send1.append('is_flash', is_flash.toString());
        data_send1.append('to_numbers', tonumbers.toString())
        toastr.info("<h5>Processing Data Please wait...<h5>");

        if (this.show_to_numbers) {

            //  toastr.info("<h5>Sending Message....<h5>");
            if (this.kyc_status == 0) {
                data_send['message'] = this.kycmessage;
            }
            this._appService.post_method(this._userService.send_sms_url, data_send).subscribe(
                data => {
                    // console.log(data);
                    if (data.status) {
                        this._walletService.walletbalance();

                        toastr.success(data.message);
                        $("#showtoast").click();

                    }
                    else {
                        toastr.error("Message Sending Failed!" + " " + data.message);
                        $("#showtoast").click();
                    }
                },
                error => {
                    toastr.error("Message Sending Failed:Server Error " + error);
                });
        }
        else {
            this._appService.post_file_method(this._userService.send_sms_url, data_send1).subscribe(
                data => {
                    //console.log(data);
                    if (data.status) {
                        // console.log("message sent successfully");
                        this._walletService.walletbalance();

                        toastr.success(data.message);
                        $("#showtoast").click();
                        //  this._headerScript.walletbalance();

                    }
                    else {
                        //  console.log("sending failed");
                        toastr.error("Message Sending Failed!" + " " + data.message);
                        $("#showtoast").click();
                    }
                },
                error => {
                    toastr.error("Message Sending Failed:Server Error " + error);

                });

        }





    }

    validateform() {
        this.validated = false;
        // console.log("validation called");
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        if (!this.validateRoute()) { toastr.error("Select Route"); return false; }
        if (!this.validateSenderId()) { toastr.error("Select SenderId"); return false; }
        if (this.show_to_numbers) {
            if (!this.validateNumbers()) { toastr.error("Check the Numbers Entered"); return false; }
        }
        else {
            if (!this.validFile()) { toastr.error("Upload File"); return false; }

        }
        if (!this.validatemessage()) { toastr.error("Check the Message"); return false; }

        this.validated = true;
        return true;

    }
    validFile() {
        if (this.BulkUploadFile == null || typeof this.BulkUploadFile == "undefined") return false;
        return true;
    }
    validateRoute() {
        if (this.route_name == null) return false;
        return true;
    }
    validateSenderId() {

        if (this.sender_id_name == null && !this.has_sender_id) return true;
        if (this.sender_id_name != null) return true;

        return false;
    }
    validateNumbers() {
        this.numbers = this.numbers.split('\n').join(',');
        this.numbers = this.numbers.split(/[.,@#$%^&*()_+{}!~`:;"'><?/]/g).join(',');
        //this.make_unique();
        //alert(this.numbers);
        let numberarray = this.numbers.split(',');
        // console.log("in validation",numberarray);
        //alert(numberarray.length);
        for (let i = 0; i < numberarray.length; i++) {
            if (!(/^\d{5,15}$/.test(numberarray[i]))) {
                return false;
            }
        }
        return true;
    }
    validatemessage() {
        if (this.message != null && this.message.trim() != "") return true;
        else return false;
    }

    automate() {
        //  console.log('Automate SMS ');
    }
    // onFilesChange(fileList: Array<File>) {
    //     if (this.reading_data) { alert("File Reading in progress Upload interupted try again"); return }
    //     else {

    //         let files = fileList;
    //         let file = files[0];
    //         if (file == null) { alert('Selected file format is not supported.\n Only txt,csv,xlxs,xls are supported currently'); }
    //         else {
    //             let file_ext = file.name.substring(file.name.lastIndexOf('.') + 1)
    //             this.reading_data = true;
    //             console.log("New  files read");
    //             console.log(file);
    //             var myReader: FileReader = new FileReader();
    //             //start reading
    //             myReader.onload = (e) => {
    //                 if (file_ext == 'xlsx' || file_ext == 'xls') {

    //                     this.arrayBuffer = myReader.result;
    //                     let data = new Uint8Array(this.arrayBuffer);
    //                     let arr = new Array();
    //                     for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    //                     let bstr = arr.join("");
    //                     let workbook = XLSX.read(bstr, { type: "binary" });
    //                     let first_sheet_name = workbook.SheetNames[0];
    //                     let worksheet = workbook.Sheets[first_sheet_name];
    //                     console.log(XLSX.utils.sheet_to_csv(worksheet));

    //                     this.numbers = this.numbers.concat(XLSX.utils.sheet_to_csv(worksheet));
    //                     this.numbers = this.numbers.concat('\n');
    //                     let cols = '';
    //                     let workbook1 = XLSX.read(data, { type: 'array' }); // parse the file
    //                     let sheet1 = workbook.Sheets[workbook1.SheetNames[0]];
    //                     let range = XLSX.utils.decode_range(sheet1['!ref']);
    //                     for (let R = range.s.r; R <= range.e.r; ++R) {
    //                         for (let C = range.s.c; C <= range.e.c; ++C) {

    //                             let cellref = XLSX.utils.encode_cell({ c: C, r: R }); // construct A1 reference for cell
    //                             if (!sheet1[cellref]) continue; // if cell doesn't exist, move on
    //                             let cell = sheet1[cellref];

    //                         }
    //                     }
    //                 }
    //                 else if (file_ext == 'txt' || file_ext == 'csv') {
    //                     this.file_data = myReader.result;
    //                     this.numbers = this.numbers.concat(this.file_data);
    //                     this.numbers = this.numbers.concat('\n');
    //                 }
    //                 else {
    //                     alert('Selected file format is not supported.\n Only txt,csv,xlxs,xls are supported currently');
    //                 }
    //                 this.update_count();
    //                 this.make_unique();
    //                 this.reading_data = false;

    //             }
    //             //End of reading

    //             if (file_ext == 'xlsx' || this.file_ext == 'xls')
    //                 myReader.readAsArrayBuffer(file);
    //             else
    //                 myReader.readAsText(file);
    //         }


    //     }


    // }

    upload_fail() {
        //  console.log("Upload failed");
    }

    onFileSelected(ljsdf) {
        //  console.log("files changed");
    }
    show_numbers() {
        // console.log("show numbers clicked");


        this.show_to_numbers = true;
    }
    contentTypeChanged() {
        // console.log(this.contentType);
        if (this.contentType == "Unicode") {
            this.contentType = "English";
        }
        else {
            this.contentType = "Unicode";
        }
        this.MessageChanged();
        // console.log(this.contentType);

    }

    show_dropzone() {
        // console.log("show dropzone clicked");
        this.numbers = "";
        this.show_to_numbers = false;
    }
}