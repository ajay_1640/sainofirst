
import { Directive, HostListener, HostBinding, EventEmitter, Output, Input } from '@angular/core';


@Directive({
    selector: '[appDnd]'
})
export class DndDirective {
    //@Input() private allowed_extensions: Array<string> = [];
    @Output() private filesChangeEmiter: EventEmitter<File[]> = new EventEmitter();
    // @Output() private filesInvalidEmiter: EventEmitter<File[]> = new EventEmitter();
    @HostBinding('style.background') private background = '#eee';

    constructor() { }

    @HostListener('dragover', ['$event'])
    public onDragOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#999';
    }

    @HostListener('dragleave', ['$event'])
    public onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#eee'
    }

    @HostListener('drop', ['$event'])
    public onDrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.background = '#eee';
        let files = evt.dataTransfer.files;
        let valid_files: Array<File> = [];
        for (let i = 0; i < files.length; i++) {
            let ext = files[i].name.substring(files[i].name.lastIndexOf('.') + 1);
            if (ext.toLowerCase() == 'txt' || ext.toLowerCase() == 'xls' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'csv') {
                valid_files.push(files[i]);

            }
        }



        if (files.length > 0) {

            this.filesChangeEmiter.emit(valid_files);

        }
    }

}