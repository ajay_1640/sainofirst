import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BulksmsComponent } from './bulksms.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { FormsModule } from "@angular/forms";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { DndDirective } from './dnd.directive';
// import { DropzoneModule } from 'ngx-dropzone-wrapper';
// import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
// import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
// import { DEFAULT_DROPZONE_CONFIG, DropzService } from './dropz.service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { WalletService } from '../../../layouts/header-nav/wallet.service';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BulksmsComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes),
        LayoutModule, FormsModule, Ng2SearchPipeModule, DragAndDropModule.forRoot(),
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        })

    ], exports: [
        RouterModule
    ], declarations: [
        BulksmsComponent,
        DndDirective,

    ],
    providers: [
        //  DropzService,
        WalletService
    ]

})
export class BulkSmsModule {




}