import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import * as XLSX from 'xlsx';
declare let toastr;
declare let $;
@Component({
    selector: 'app-block-number',
    templateUrl: './block-number.component.html',
    styles: []
})
export class BlockNumberComponent implements OnInit {

    constructor(private _appService: AppService, private _script: ScriptLoaderService, private _userService: UserService) { }
    p: number = 1;
    searchText = "";
    show_loading = false;
    block_list = [{ 'numbers': "No data", 'added_date': "No data" }];
    multiple_nos = '';

    arrayBuffer;
    file_ext;
    file_data;
    block_selected;
    ngOnInit() {
        this._script.loadScripts('app-block-number',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        this.show_loading = true;
        this.getBlockNumbers();
        $('#m_modal_3').on('hide', function(e) {
            if (!confirm('You want to close me?'))
                e.preventDefault();
        });

    }

    validateFile(name: String) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        this.file_ext = ext.toLowerCase();
        if (ext.toLowerCase() == 'csv' || ext.toLowerCase() == 'txt' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'xls' || ext.toLowerCase() == 'ods') {
            return true;
        }
        else {
            return false;
        }
    }

    readFile(files: FileList, val): void {
        var file: File = files.item(0);

        if (!this.validateFile(file.name)) {
            val.value = "";
            return toastr.warning('Selected file format is not supported.\n Only txt,csv,ods,xlxs,xls,ods are supported currently');
        }
        val.value = "";
        // console.log(this.file_ext);
        var myReader: FileReader = new FileReader();
        myReader.onload = (e) => {
            if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods') {
                this.arrayBuffer = myReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                //console.log(XLSX.utils.sheet_to_csv(worksheet));
                this.multiple_nos = this.multiple_nos.concat(XLSX.utils.sheet_to_csv(worksheet));
                this.multiple_nos = this.multiple_nos.trim();
                $('#nav-home-tab').removeClass('active');
                $('#nav-contact-tab').removeClass('active');
                $('#nav-home-tab').addClass('active');
                $('#nav-home').removeClass('show active');
                $('#nav-contact').removeClass('show active');
                $('#nav-home').addClass('show active');
            }
            else {
                this.file_data = myReader.result;
                //console.log(this.file_data);
                this.multiple_nos = this.multiple_nos.concat(this.file_data);
                this.multiple_nos = this.multiple_nos.trim();
                $('#nav-home-tab').removeClass('active');
                $('#nav-contact-tab').removeClass('active');
                $('#nav-home-tab').addClass('active');
                $('#nav-home').removeClass('show active');
                $('#nav-contact').removeClass('show active');
                $('#nav-home').addClass('show active');

            }
        }
        if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods')
            myReader.readAsArrayBuffer(file);
        else
            myReader.readAsText(file);
    }



    getBlockNumbers() {
        this._appService.get_method(this._userService.block_list_url).subscribe(
            data => {

                this.block_list = [];

                this.block_list = data.result;
                // console.log(this.block_list);
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    delete_num(val) {

        this._appService.delete_method(this._userService.block_list_url + val.id).subscribe(
            data => {
                if (data.status) {
                    this.show_loading = true;
                    this.getBlockNumbers();
                    toastr.success("<h4>Number Deleted From Blocked List<h4>");
                }
                else {
                    toastr.error("<h4>" + data.message + "<h4>");
                }
            },
            error => {
                toastr.error("<h4>Server Error  " + error + "<h4>");
            });
    }
    add_multiple() {
        if (this.multiple_nos == "") return toastr.warning("<h5>No Numbers Found<h5>");
        this.multiple_nos = this.multiple_nos.replace(/\n/g, ',');
        let ar = this.multiple_nos.split(',');
        for (let i = 0; i < ar.length; i++) {
            if (!/^[0-9]{5,15}$/.test(ar[i])) {
                // console.log(/^[0-9]{5,15}$/.test(ar[i]))
                return toastr.warning("<h6>Invalid Numbers Found<h6>");
            }
        }
        $('#m_modal_6').modal('hide');
        let data_send = JSON.stringify({
            'numbers': this.multiple_nos
        });
        this._appService.post_method(this._userService.block_list_url + 'multiple/', data_send).subscribe(
            data => {
                if (data.status) {
                    this.multiple_nos = "";
                    toastr.success("<h4>Valid Numbers Inserted Successfully<h4>")
                    this.show_loading = true;
                    this.getBlockNumbers();
                }
                else {
                    toastr.error("<h4>" + data.message + "<h4>");
                }
            },
            error => {
                toastr.error("<h4>Server Error " + error + "<h4>");

            });
        this.multiple_nos = '';
    }

    current_blocknumber(val) {
        $('#m_modal_3').modal('show');
        this.block_selected = val;
        this.block_num_edit = val.numbers;

    }

    addBlockNumber() {
        $('#m_modal_6').modal('show');
        this.multiple_nos = "";
    }
    block_num_edit;
    update_blocknum() {


        if (!/^[0-9]{5,15}$/.test(this.block_num_edit)) return toastr.warning("<h4>Block Number is Invalid<h4>");
        $('#m_modal_3').modal('hide');
        let data_send = JSON.stringify({
            'number': this.block_num_edit,
        });

        this._appService.patch_method(this._userService.block_list_url + this.block_selected.id, data_send).subscribe(
            data => {

                if (data.status) {
                    toastr.success("<h4>Blocked Number Updated Successfully<h4>");
                    this.block_num_edit = "";
                    this.show_loading = true;
                    this.getBlockNumbers();
                }
                else {
                    toastr.error("<h4>" + data.message + "<h4>")
                }

            },
            error => {
                toastr.error("<h4> Server Error " + error + "");

            });
    }
}
