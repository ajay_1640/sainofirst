import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ManageComponent } from './manage.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { FormsModule } from "@angular/forms";
import { BlockNumberComponent } from './block-number/block-number.component';
import { SenderidComponent } from './senderid/senderid.component';
import { TemplatesComponent } from './templates/templates.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ManageComponent
            },
            {
                "path": "block-number",
                "component": BlockNumberComponent
            },
            {
                "path": "templates",
                "component": TemplatesComponent
            },
            {
                "path": "sender-id",
                "component": SenderidComponent
            }

        ]
    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        }),
        Ng2SearchPipeModule
    ], exports: [
        RouterModule
    ], declarations: [
        ManageComponent,
        BlockNumberComponent,
        SenderidComponent,
        TemplatesComponent
    ]
})
export class ManageModule {



}