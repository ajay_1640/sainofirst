import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
declare let toastr;
import swal from 'sweetalert2';
declare let $;
declare let moment;

@Component({
    selector: 'app-senderid',
    templateUrl: './senderid.component.html',
    styles: []
})
export class SenderidComponent implements OnInit {

    constructor(private _appService: AppService, private _script: ScriptLoaderService, private _userService: UserService) { }
    p: number = 1;
    sender_id_name = "";
    show_loading = false;
    check = "";
    transact = "";
    kyc_status = 0;
    senderid_list = [];
    routes_list = [];
    routes_selected = [];
    // senderid_url = 'http://35.238.188.102/saino-first/api/sender/';
    searchText = "";
    ngOnInit() {
        this._script.loadScripts('app-senderid',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.show_loading = true;
        this.getKycStatus();
        this.getSenderId();
        this.getRoutes();
    }
    getKycStatus() {
        this._appService.get_method(this._userService.kyc_status).subscribe(
            data => {
                this.kyc_status = data.status;
            }
        );
    }
    getSenderId() {
        this.show_loading = true;
        this._appService.get_method(this._userService.senderid_url).subscribe(
            data => {
                this.senderid_list = [];
                this.senderid_list = data["sender_information"];
                for (let i = 0; i < this.senderid_list.length; i++) {
                    let routes = this.senderid_list[i]['route_names'];
                    let routes_string = "";
                    for (let j = 1; j <= routes.length; j++) {
                        routes_string = routes_string.concat(routes[j - 1]);
                        routes_string = routes_string.concat(",");
                        if (j % 3 == 0) { routes_string = routes_string.concat("\n"); }
                    }
                    //  console.log(routes_string);
                    this.senderid_list[i]['route_names'] = routes_string.substr(0, routes_string.length - 1);
                    this.senderid_list[i]['created_at'] = moment(this.senderid_list[i]['created_at']).format("DD-MM-YYYY hh:mm A");

                }
                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }
    getRoutes() {

        this._appService.get_method(this._userService.get_routes_url).subscribe(
            data => {
                this.routes_list = data.data;
                for (let j = 0; j < this.routes_list.length; j++) {
                    this.routes_list[j]['status'] = false;
                }
            },
            error => {
            }
        );

    }

    delete_sender(val) {
        this._appService.delete_method(this._userService.senderid_url + val.sid).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h4>Sender Id Deleted Successfully<h4>");
                    this.getSenderId();
                }
                else {
                    toastr.error("<h4>" + data.message + "<h4>");
                }

            },
            error => {
                toastr.error("<h4>Server Error  " + error + "<h4>");

            });

    }
    checkboxChanged(index) {
        this.routes_list[index]['status'] = !this.routes_list[index]['status'];

    }
    addSenderid() {
        if (this.sender_id_name == "" || !/^[a-zA-Z]+$/.test(this.sender_id_name))
            return toastr.warning("<h5>Invalid Sender Id name<h5>");

        let ar = [];
        for (let i = 0; i < this.routes_list.length; i++) {
            if (this.routes_list[i]['status']) {
                ar.push(this.routes_list[i]['az_routeid']);
            }
        }
        if (ar.length == 0) return toastr.warning("<h5>Please Choose Routes<h5>");

        let d = {
            'sender_id': this.sender_id_name,
            'type': ar
        }

        this._appService.post_method(this._userService.senderid_url, JSON.stringify(d)).subscribe(
            data => {
                if (data.status) {
                    this.sender_id_name = "";
                    for (let i = 0; i < this.routes_list.length; i++) {
                        this.routes_list[i]['status'] = false;
                    }
                    toastr.success("<h5>" + data.message + "<h5>");

                    this.getSenderId();

                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");

            }

        );

    }
    create_senderid() {
        this.sender_id_name = "";
        for (let j = 0; j < this.routes_list.length; j++) {
            this.routes_list[j]['status'] = false;
        }
        $('#m_modal_6').modal('show');

    }

}
