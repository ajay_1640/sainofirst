import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { ShortCodeComponent } from './short-code.component';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
const routes: Routes = [
    {
        "path": "",
        "component": ShortCodeComponent,
        "children": [
            {
                "path": "",
                component: ShortCodeComponent,
            },
            {
                "path": "dashboard",
                "loadChildren": ".\/dashboard\/dashboard.module#DashboardModule"
            },
            {
                "path": "settings",
                "loadChildren": ".\/settings\/settings.module#SettingsModule"
            },
            {
                "path": "reports",
                "loadChildren": ".\/reports\/reports.module#ReportsModule"
            },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule

    ], exports: [
        RouterModule,
    ], declarations: [
        ShortCodeComponent
    ],
})
export class ShortcodeModule {
}