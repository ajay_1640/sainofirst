import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { ShortCodeComponent } from '../short-code.component';
import { DashboardComponent } from './dashboard.component';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

const routes: Routes = [
    {
        'path': '',
        'component': DashboardComponent
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, AmChartsModule
    ], exports: [
        RouterModule,
    ], declarations: [
        DashboardComponent,
    ],
})
export class DashboardModule {
}