import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { ShortCodeComponent } from '../short-code.component';
import { SettingsComponent } from './settings.component';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
const routes: Routes = [
    {
        'path': '',
        'component': SettingsComponent
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule
    ], exports: [
        RouterModule,
    ], declarations: [
        SettingsComponent,
    ],
})
export class SettingsModule {
}