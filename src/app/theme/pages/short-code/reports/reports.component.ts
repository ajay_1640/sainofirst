import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
declare let $;
@Component({
    selector: 'app-short-code-reports',
    templateUrl: "./reports.component.html",
    styles: []
})
export class ReportsComponent implements OnInit {

    constructor(private _script: ScriptLoaderService, ) { }
    dateval = "";
    groups_list = "";
    searchText = "";
    p = "";
    calendar_range = "";
    show_calendar = false;

    ngOnInit() {
        this._script.loadScripts('app-short-code-reports',
            ['assets/demo/demo3/base/bootstrap-select.js']);


        (<any>$('.input-daterange input')).each(function() {
            (<any>$(this)).datepicker({
                todayHighlight: true,
                autoclose: true,
                format: "dd/mm/yyyy",
            });
        });

        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
    }
    back() {

    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }
    }
}
