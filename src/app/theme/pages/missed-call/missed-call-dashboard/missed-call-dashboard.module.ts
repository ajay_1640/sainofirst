import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { MissedCallDashboardComponent } from './missed-call-dashboard.component';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { FormsModule } from "@angular/forms";


const routes: Routes = [
    {
        'path': '',
        'component': MissedCallDashboardComponent
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule,FormsModule, AmChartsModule
    ], exports: [
        RouterModule,
    ], declarations: [
        MissedCallDashboardComponent,
    ],
})
export class MissedCallDashboardModule {
}
