import { Component, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AppService } from '../../../../app.service';
import { UserService } from '../../../../auth/_services/user.service';
import 'rxjs/Rx';
declare var $, jQuery: any,moment,toastr;
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
@Component({
    selector: 'app-missed-call-dashboard',
    templateUrl: "./missed-call-dashboard.component.html",
    styles: []
})
export class MissedCallDashboardComponent implements OnInit {

    private chart: AmChart;
    subscribedNumbers=[];
    selectedMobileNumber="";
    constructor(private _script: ScriptLoaderService, private AmCharts: AmChartsService,private _appService:AppService,private _userService:UserService) { }

    ngOnInit() {
        this.getSubscribedNumbers();
        this._script.loadScripts('app-missed-call-dashboard',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-missed-call-dashboard',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-missed-call-dashboard',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        this._script.loadScripts('app-missed-call-dashboard',
            ['assets/demo/demo3/base/toastr.js']);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        this._script.loadScripts('head', [
            'https://www.amcharts.com/lib/3/amcharts.js',
            'https://www.amcharts.com/lib/3/serial.js',
            'https://www.amcharts.com/lib/3/plugins/export/export.min.js',
            'https://www.amcharts.com/lib/3/themes/light.js',], true).then(() => {
                Helpers.loadStyles('head', 'https://www.amcharts.com/lib/3/plugins/export/export.css');

                this.chart = this.AmCharts.makeChart("chartdiv", {
                    "theme": "light",
                    "type": "serial",
                    "hideCredits": true,
                    "startDuration": 2,
                    "dataProvider": [{
                        "country": "USA",
                        "visits": 4025,
                        "color": "#FF0F00"
                    }, {
                        "country": "China",
                        "visits": 1882,
                        "color": "#FF6600"
                    }, {
                        "country": "Japan",
                        "visits": 1809,
                        "color": "#FF9E01"
                    }, {
                        "country": "Germany",
                        "visits": 1322,
                        "color": "#FCD202"
                    }, {
                        "country": "UK",
                        "visits": 1122,
                        "color": "#F8FF01"
                    }, {
                        "country": "France",
                        "visits": 1114,
                        "color": "#B0DE09"
                    }, {
                        "country": "India",
                        "visits": 984,
                        "color": "#04D215"
                    }, {
                        "country": "Spain",
                        "visits": 711,
                        "color": "#0D8ECF"
                    }, {
                        "country": "Netherlands",
                        "visits": 665,
                        "color": "#0D52D1"
                    }, {
                        "country": "Russia",
                        "visits": 580,
                        "color": "#2A0CD0"
                    }, {
                        "country": "South Korea",
                        "visits": 443,
                        "color": "#8A0CCF"
                    }, {
                        "country": "Canada",
                        "visits": 441,
                        "color": "#CD0D74"
                    }, {
                        "country": "Brazil",
                        "visits": 395,
                        "color": "#754DEB"
                    }, {
                        "country": "Italy",
                        "visits": 386,
                        "color": "#DDDDDD"
                    }, {
                        "country": "Australia",
                        "visits": 384,
                        "color": "#999999"
                    }, {
                        "country": "Taiwan",
                        "visits": 338,
                        "color": "#333333"
                    }, {
                        "country": "Poland",
                        "visits": 328,
                        "color": "#000000"
                    }],
                    "valueAxes": [{
                        "position": "left",
                        "title": "Visitors"
                    }],
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "fillColorsField": "color",
                        "fillAlphas": 1,
                        "lineAlpha": 0.1,
                        "type": "column",
                        "valueField": "visits"
                    }],
                    "depth3D": 20,
                    "angle": 30,
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "country",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "labelRotation": 90
                    },
                    "export": {
                        "enabled": true
                    }

                });
            });
    }


    ngOnDestroy() {
      try{  this.AmCharts.destroyChart(this.chart);}
      catch(e){}
    }
   
    getSubscribedNumbers(){
        this.subscribedNumbers=[];
        this.selectedMobileNumber=""
        this._appService.get_method(this._userService.missedCallSubscribedNumbers).subscribe(
            data=>{
                if(data.status){
                    this.subscribedNumbers=data.data;
                    setTimeout(()=>{
                        $('.selectpicker').selectpicker('refresh');
                    },1)
                    console.log(data)
                 }
            },
            error=>{ }
        );
    }
    mobileNumberChanged(){
        let startdate= moment().startOf('day').subtract(8, 'days').valueOf();
        let enddate=moment().endOf('day').valueOf();
        startdate=startdate.toString();
        enddate=enddate.toString();
        startdate=startdate.slice(0,-3);
        enddate=enddate.slice(0,-3);
       for(let i=0;i<this.subscribedNumbers.length;i++){
           if(this.subscribedNumbers[i]['virtual_number_id']==this.selectedMobileNumber){
               if(parseInt(startdate)<this.subscribedNumbers[i]['subscription_start_date']){
                   startdate=this.subscribedNumbers[i]['subscription_start_date']
               }
               if(parseInt(enddate)>this.subscribedNumbers[i]['subscription_end_date']){
                   enddate=this.subscribedNumbers[i]['subscription_end_date'];
               }
               this._appService.get_method(this._userService.missedCallReports+this.selectedMobileNumber+"/"+startdate+"/"+enddate).subscribe(
                data=>{
                    console.log(data);
                },
                error=>{
                    console.log(error)
                }
            );
            break;
           }
       }
        
  
    }
}
