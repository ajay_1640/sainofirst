import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AppService } from '../../../../app.service';
import { UserService } from '../../../../auth/_services/user.service';
import * as XLSX from 'xlsx';
declare let toastr;
declare var $, jQuery: any;
declare var moment: any;

@Component({
    selector: 'app-missed-call-reports',
    templateUrl: "./missed-call-reports.component.html",
    styles: []
})
export class MissedCallReportsComponent implements OnInit {

    searchText = "";
    calendar_range = "";
    show_calendar = false;
    subscribedNumbers=[];
    selectedMobileNumber="";
    missedReports=[];
    show_loading=false;

    constructor(private _script: ScriptLoaderService, private _appService: AppService,private _userService:UserService) { }
    p: number = 1;

    ngOnInit() {
        this.getSubscribedNumbers();
        this._script.loadScripts('app-missed-call-reports',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-missed-call-reports',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        this._script.loadScripts('app-missed-call-reports',
            ['assets/demo/demo3/base/toastr.js']);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        
    }

    getReports(){

    }
    from_date1="";
    to_date1="";

    search() {
        
        this.from_date1 = "";
        this.to_date1 = "";
        if (this.calendar_range == "") return toastr.warning("<h5>Choose Dates<h5>");
        else if (this.calendar_range == "today") {
            this.from_date1 = moment().startOf('day');
            this.to_date1 = moment().endOf('day');
        }
        else if (this.calendar_range == "7days") {
            this.from_date1 = moment().startOf('day').subtract(6, 'days');
            this.to_date1 = moment().endOf('day');
        }
        else if (this.calendar_range == "15days") {
            this.from_date1 = moment().startOf('day').subtract(14, 'days');
            this.to_date1 = moment().endOf('day');
        }
        else if (this.calendar_range == "month") {
            this.from_date1 = moment().startOf('month').subtract(1, 'month');
            this.to_date1 = moment().endOf('month').subtract(1, 'month');
        }
        else if (this.calendar_range == "custom") {
            let s = $('#m_datepicker_1')[0].value;
            let e = $('#m_datepicker_2')[0].value;
            this.from_date1=moment(s,"MM/DD/YYYY");
            this.to_date1=moment(e,"MM/DD/YYYY");
            if (s == "" || e == "") return toastr.warning("<h5>Choose Dates<h5>");
        } 
        if(this.selectedMobileNumber=="" || this.selectedMobileNumber==null){
            return toastr.warning("<h5>Select Mobile Number<h5>");
        }
        let startdate=this.from_date1.valueOf();
        let enddate=this.to_date1.valueOf();
        startdate=startdate.toString();
        enddate=enddate.toString();
        startdate=startdate.slice(0,-3);
        enddate=enddate.slice(0,-3);
        console.log("startdate enddate in moment",this.from_date1,this.to_date1);
        console.log("startdate and enddate in epoch",startdate,enddate);
        this.searchMissedReports(startdate,enddate);
    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }
    }
    getSubscribedNumbers(){
        this.subscribedNumbers=[];
        this.selectedMobileNumber=""
        this._appService.get_method(this._userService.missedCallSubscribedNumbers).subscribe(
            data=>{
                if(data.status){
                    this.subscribedNumbers=data.data;
                    setTimeout(()=>{
                        $('.selectpicker').selectpicker('refresh');
                    },1)
                    console.log(data)
                 }
            },
            error=>{ }
        );
    }
    searchMissedReports(starttime,endtime){
        this.missedReports=[];
        this._appService.get_method(this._userService.missedCallReports+this.selectedMobileNumber).subscribe(
            data=>{
                if(data.status){
                   for(let i=0;i<data.data.length;i++){
                       if(data.data[i]['date_received']>=parseInt(starttime) && data.data[i]['date_received']<=parseInt(endtime)){
                          data.data[i]['receiveddate']=moment.unix(data.data[i]['date_received']).format("DD/MM/YYYY hh:mm:ss A");
                          this.missedReports.push(data.data[i]);                           
                       }
                   }
                }
                else{
                    toastr.error("<h5>"+data.message+"<h5>")
                }
            },
            error=>{
               toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );
    }
    mobileNumberChanged(){
       
        this.missedReports=[];
         this._appService.get_method(this._userService.missedCallReports+this.selectedMobileNumber).subscribe(
             data=>{
                 if(data.status){
                    this.missedReports=data.data;
                    for(let i=0;i<this.missedReports.length;i++){
                      this.missedReports[i]['receiveddate']=moment.unix(this.missedReports[i]['date_received']).format("DD/MM/YYYY hh:mm:ss A");
                    }
                 }
                 else{
                     toastr.error("<h5>"+data.message+"<h5>")
                 }
             },
             error=>{
                toastr.error("<h5>Server Error: "+error+"<h5>");
             }
         );
    }
    downloadFile(){
        if(this.missedReports.length==0){
            return toastr.error("<h5>No Data Found to Download<h5>");
        }
        else{
            var arr=[];
            for(let i=0;i<this.missedReports.length;i++){
                let d={
                    'Caller Number':this.missedReports[i]['caller_number'],
                    'Operator':this.missedReports[i]['operator'],
                    'Received Date':this.missedReports[i]['receiveddate'],
                }
                arr.push(d)
                if(i==this.missedReports.length-1){
                     let ws = XLSX.utils.json_to_sheet(arr);
                    let wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "Reports");
                    XLSX.writeFile(wb, "Reports.xlsx");
                }
            }

        }
    }


}
