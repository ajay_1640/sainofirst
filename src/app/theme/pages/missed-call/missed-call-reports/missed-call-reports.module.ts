import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { MissedCallReportsComponent } from './missed-call-reports.component';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const routes: Routes = [
    {
        'path': '',
        'component': MissedCallReportsComponent
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, Ng2SearchPipeModule, NgxPaginationModule
    ], exports: [
        RouterModule,
    ], declarations: [
        MissedCallReportsComponent,
    ],
})
export class MissedCallReportsModule {
}