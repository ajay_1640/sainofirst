import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { ScriptLoaderService } from '../../../_services/script-loader.service';

@Component({
    selector: "missed-call",
    templateUrl: "./missed-call.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class MissedCallComponent implements OnInit, AfterViewInit {


    constructor(private _script: ScriptLoaderService) {

    }
    ngOnInit() {

    }
    ngAfterViewInit() {

    }
}