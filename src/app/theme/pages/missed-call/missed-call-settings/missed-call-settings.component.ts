import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../app.service';
import { UserService } from '../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
declare let $,toastr,moment;
@Component({
    selector: 'app-missed-call-settings',
    templateUrl: "./missed-call-settings.component.html",
    styles: []
})
export class MissedCallSettingsComponent implements OnInit {
    searchText = "";
    groups_list = "";
    p =1;
    autoReplySmsEdit="";
    urlpostpage = "";
    urlforwardEdit="";
    subscribedNumbers:any=[];
    selectedNumber:any={};
    autoReplySmsActive=false;
    urlForwardingActive=false;
    constructor(private _appService:AppService,private _userService:UserService,private _script:ScriptLoaderService) { }
    ngOnInit() {
        this.getSubscribedNumbers();
        this._script.loadScripts('app-missed-call-settings',
        ['assets/demo/demo3/base/toastr.js']);
        this._script.loadScripts('app-missed-call-settings',
        ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    }
    autoReplySave() {
      
        this.autoReplySmsEdit=this.autoReplySmsEdit.trim();
        if(this.autoReplySmsEdit==""){
            return toastr.warning("<h5>Reply SMS cannot be Empty<h5>");
        }
        $('#auto_reply').modal('hide');
        let d={
            "auto_reply_enabled": this.autoReplySmsActive,
            "auto_reply_text": this.autoReplySmsEdit
        }
        this._appService.post_method(this._userService.missedCallUrlForward+this.selectedNumber['virtual_number_id']+"/auto-reply",JSON.stringify({d})).subscribe(
            data=>{
                if(data.status){
                    this.getSubscribedNumbers();
                    return toastr.success("<h5>"+data.message+"<h5>")
                }
                else{
                    return toastr.error("<h5>"+data.message+"<h5>");
                }
            },
            error=>{
                return toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );

    }
    openAutoReplySms(number) {
        this.autoReplySmsActive=false;
        this.autoReplySmsEdit="";
        console.log(number)
        this.selectedNumber=number;
        this.autoReplySmsEdit=this.selectedNumber['auto_reply_text'];
        if(this.selectedNumber['auto_reply_enabled']==1 || this.selectedNumber['auto_reply_enabled']=='1'){
            this.autoReplySmsActive=true;
        }        
        $('#auto_reply').modal('show');
    }
    urlOpen(number) {
        this.urlforwardEdit="";
        this.urlForwardingActive=false;
        this.selectedNumber=number;
        this.urlforwardEdit=this.selectedNumber['forwarding_url'];
        if(this.selectedNumber['url_forwarding_enabled']==1 || this.selectedNumber['url_forwarding_enabled']=='1'){
            this.urlForwardingActive=true;
        }       
        $('#url').modal('show');

    }
    urlClose() {
        this.urlforwardEdit=this.urlforwardEdit.trim();
        if(this.urlforwardEdit==""){
            return toastr.warning("<h5>Url Post Page Cannot be Empty<h5>");
        }
        $('#url').modal('hide');
        let d={
            "url_forwarding_enabled":this.urlForwardingActive ,
            "forwarding_url": this.urlforwardEdit
        }
        this._appService.post_method(this._userService.missedCallUrlForward+this.selectedNumber['virtual_number_id']+"/url",JSON.stringify({d})).subscribe(
            data=>{
                if(data.status){
                    this.getSubscribedNumbers();
                    return toastr.success("<h5>"+data.message+"<h5>")
                }
                else{
                    return toastr.error("<h5>"+data.message+"<h5>");
                }
            },
            error=>{
                return toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );

    }
    getSubscribedNumbers(){
        this.subscribedNumbers=[];
        this._appService.get_method(this._userService.missedCallSubscribedNumbers).subscribe(
            data=>{
                if(data.status){
                    this.subscribedNumbers=data.data;
                    for(let i=0;i<this.subscribedNumbers.length;i++){
                        this.subscribedNumbers[i]['subscription_start_date']=moment.unix(this.subscribedNumbers[i]['subscription_start_date']).format("DD/MM/YYYY hh:mm:ss A");
                        this.subscribedNumbers[i]['subscription_end_date']=moment.unix(this.subscribedNumbers[i]['subscription_end_date']).format("DD/MM/YYYY hh:mm:ss A");

                    }
                    // app_user_id
                    // :
                    // 1
                    // auto_reply_enabled
                    // :
                    // 0
                    // auto_reply_text
                    // :
                    // ""
                    // auto_reply_units
                    // :
                    // 5000
                    // forwarding_url
                    // :
                    // ""
                    // id
                    // :
                    // 1
                    // missed_call_units
                    // :
                    // 5000
                    // subscription_end_date
                    // :
                    // 1552468073
                    // subscription_start_date
                    // :
                    // 1544692073
                    // subscription_status
                    // :
                    // 1
                    // url_forwarding_enabled
                    // :
                    // 0
                    // virtual_number_id
                    // :
                    // 1
                }
                else{
                    return toastr.error("<h5>Error Retrieving Data<h5>");
                    
                }
                console.log("subscribed data",data);},
            error=>{
                return toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );
    }
  

}
