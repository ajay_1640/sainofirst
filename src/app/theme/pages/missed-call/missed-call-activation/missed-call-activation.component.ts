import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { UserService } from '../../../../auth/_services/user.service';
import { AppService } from '../../../../app.service';
declare let $,toastr;
@Component({
    selector: 'app-missed-call-activation',
    templateUrl: "./missed-call-activation.component.html",
    styles: []
})
export class MissedCallActivationComponent implements OnInit {
    wallet_balance: any = 0;
    groups_list = "";
    Selected_validity: string="";
    Selected_Call_unit: string="";
    Selected_AutoReplySms: string="";
    total_cost;
    p = 1;
    searchText = "";
    newNumbersArray:any=[];
    regularNumbersArray:any=[];
    specialNumbersArray:any=[];
    additional={};
    callUnits=[];
    autoReplySmsUnits=[];
    discountOnTotal=[];
    regularNumber:boolean=false;
    specialNumber:boolean=false;
    autoReplySms=[];
    SelectedActivationNumber={}
    selectPrice="";
    selectNumber="";
    selectTotalCost:any;
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this.getNewNumbers();
        this.getUnitPrices();
        this._script.loadScripts('app-missed-call-activation',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);

        this._script.loadScripts('app-missed-call-activation',
            ['assets/demo/demo3/base/bootstrap-select.js']);
            this._script.loadScripts('app-missed-call-activation',
            ['assets/demo/demo3/base/toastr.js']);
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        this._appService.get_method(this._userService.wallet_url).subscribe(
            data => {
            this.wallet_balance = data.data.balance;
                this.wallet_balance = this.wallet_balance.toFixed(2)
            },
            error => {
                // console.log("Cant receive any wallet balance") 
            }
        );
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
    }
    Validity_changed() {
        if(this.Selected_AutoReplySms!="" && this.Selected_Call_unit!="" && this.Selected_validity!=""){
            console.log(this.Selected_AutoReplySms,this.Selected_Call_unit,this.Selected_validity)
        }
        else{
            return;
        }
    }
    activate() {
        console.log(this.Selected_AutoReplySms);
        console.log(this.autoReplySmsUnits)
            if(this.Selected_validity!="" && this.Selected_Call_unit=="" && this.Selected_AutoReplySms==""){
               return toastr.warning("<h5>Please Choose Additional Call Units or Additional AutoReplySms<h5>")
          }
          let data={
                "virtual_number_id": this.SelectedActivationNumber['id'],
                "virtual_number": this.SelectedActivationNumber['number'],
                "months": null,
                "missed_call_Units_id": null,
                "auto_reply_sms_Units_id": null
            }
          if(this.Selected_validity!=""){
              data['months']=parseInt(this.Selected_validity)
              if(this.Selected_Call_unit!=""){
                  for(let i=0;i<this.callUnits.length;i++){
                      if(this.callUnits[i]['units']==parseInt(this.Selected_Call_unit)){
                        data['missed_call_Units_id']=i;
                        break;
                      }
                  }
                  
              }
              if(this.Selected_AutoReplySms!=""){
                for(let j=0;j<this.autoReplySmsUnits.length;j++){
                    if(this.autoReplySmsUnits[j]['units']==parseInt(this.Selected_AutoReplySms)){
                      data['auto_reply_sms_Units_id']=j;
                      break;
                    }
                }
              }
          }
        
       
        console.log(data);
//  this._appService.post_method(this._userService.missedCallNumberSubscribe,JSON.stringify({})).subscribe(
//      data=>{
//          console.log("hiii")
//      },
//      error=>{}
//  );
}
    
    openAutoReply(number){
        this.SelectedActivationNumber=number;
        console.log(this.SelectedActivationNumber)
        this.Selected_AutoReplySms="";
        this.Selected_Call_unit="";
        this.Selected_validity="";
        this.selectNumber=this.SelectedActivationNumber['number'];
        this.selectPrice=this.SelectedActivationNumber['price'];
        $('#auto_reply').modal('show');
        setTimeout(()=>{
            $('.selectpicker').selectpicker('refresh');
        },20)

    }
    regularNumberTypeSelected(){
        if(this.regularNumber){
            this.regularNumber=false;
            this.newNumbersArray=this.specialNumbersArray;
        }
        else{
            this.regularNumber=true;
            this.newNumbersArray=this.regularNumbersArray;
        }
        console.log("regular 1 regular number",this.regularNumber)
    }
    specialNumberTypeSelected(){  
        this.regularNumber=false;  
        this.newNumbersArray=this.specialNumbersArray;  
        console.log("special regular number",this.regularNumber)

    }
    getNewNumbers(){
        this.newNumbersArray=[];
        this.regularNumbersArray=[];
        this.specialNumbersArray=[];
        this._appService.get_method(this._userService.missedCallnewNumbers).subscribe(
            data=>{
                if(data.status){
                    this.newNumbersArray=data.data;
                    for(let i=0;i<this.newNumbersArray.length;i++){
                        if(this.newNumbersArray[i]['is_special_number']!=1 && this.newNumbersArray[i]['is_special_number']!='1'){
                            this.regularNumbersArray.push(this.newNumbersArray[i]);
                        }
                        else{
                            this.specialNumbersArray.push(this.newNumbersArray[i]);
                        }
                    }

                }
                else{
                    return toastr.error("<h5>Error Retrieving Data<h5>");
                    
                }
                console.log(data);
            },
            error=>{
                return toastr.error("<h5>Server Error: "+error+"<h5>");
            }
        );
    }
    getUnitPrices(){
        this.discountOnTotal=[];
        this._appService.get_method(this._userService.missedCallUnitPrices).subscribe(
            data=>{
                console.log(data);
                if(data.status){
                    this.additional=data.data['additional'];
                  //  this.discountOnTotal=data.data['discountOnTotal'];
                    this.autoReplySmsUnits=data.data['additional']['autoReply'];
                    this.callUnits=data.data['additional']['callUnits'];
                    for(var key in data.data['discountOnTotal']){
                        this.discountOnTotal.push({'month':key,'discount':data.data['discountOnTotal'][key]})
                    }
                    console.log("auto reply",this.autoReplySmsUnits);
                    console.log("call units",this.callUnits)
                }
            },
            error=>{
            }
        );
    }

}
