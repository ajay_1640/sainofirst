import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { MissedCallComponent } from './missed-call.component';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MissedCallSettingsComponent } from './missed-call-settings/missed-call-settings.component';
import { MissedCallReportsComponent } from './missed-call-reports/missed-call-reports.component';
import { MissedCallActivationComponent } from './missed-call-activation/missed-call-activation.component';
const routes: Routes = [
    {
        "path": "",
        "component": MissedCallComponent,
        "children": [
            {
                "path": "",
                component: MissedCallComponent,
            },
            {
                "path": "dashboard",
                "loadChildren": ".\/missed-call-dashboard\/missed-call-dashboard.module#MissedCallDashboardModule"
            },
            {
                "path": "reports",
                "loadChildren": ".\/missed-call-reports\/missed-call-reports.module#MissedCallReportsModule"
            },
            {
                "path": "settings",
                "loadChildren": ".\/missed-call-settings\/missed-call-settings.module#MissedCallSettingsModule"
            },
            {
                "path": "activation",
                "loadChildren": ".\/missed-call-activation\/missed-call-activation.module#MissedCallActivationModule"
            },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule

    ], exports: [
        RouterModule,
    ], declarations: [
        MissedCallComponent,
    ],
})
export class MissedCallModule {
}