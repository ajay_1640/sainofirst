import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { AppService } from '../../../../../app.service';
declare let $;
@Component({
    selector: 'app-ivr-activation-component',
    templateUrl: './ivr-activation-component.component.html',
    styles: []
})
export class IvrActivationComponentComponent implements OnInit {
    wallet_balance: any = 0;
    groups_list = "";
    Selected_validity: string;
    Selected_Call_unit: string;
    Selected_AutoReplySms: string;
    total_cost;
    p = 1;
    searchText = "";
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this._script.loadScripts('app-ivr-activation-component',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);

        this._script.loadScripts('app-ivr-activation-component',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._appService.get_method(this._userService.wallet_url).subscribe(
            data => {
            this.wallet_balance = data.data.balance;
                this.wallet_balance = this.wallet_balance.toFixed(2)
            },
            error => {
                // console.log("Cant receive any wallet balance") 
            }
        );
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
    }
    Validity_changed() {
        this.total_cost = 10 + this.Selected_validity;
    }
    activate() {

    }

}
