import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IvrComponent } from './ivr/ivr.component';

import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes: Routes = [
    {
        "path": "",
        "component": IvrComponent,
        "children": [
            {
                "path": "",
                component: IvrComponent,
            },
            {
                "path": "dashboard",
                "loadChildren": ".\/ivr-dashboard\/ivr-dashboard.module#IvrDashboardModule"
            },
            {
                "path": "calllogs",
                "loadChildren": ".\/ivr-calllogs\/ivr-calllogs.module#IvrCalllogsModule"
            },
            {
                "path": "reports",
                "loadChildren": ".\/ivr-reports\/ivr-reports.module#IvrReportsModule"
            },
            {
                "path": "contacts",
                "loadChildren": ".\/ivr-contacts\/ivr-contacts.module#IvrContactsModule"
            },
            {
                "path": "settings",
                "loadChildren": ".\/ivr-settings\/ivr-settings.module#IvrSettingsModule"
            },
            {
                "path": "activation",
                "loadChildren": ".\/ivr-activation\/ivr-activation.module#IvrActivationModule"
            }

        ]
    }
];
@NgModule({

    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule

    ], exports: [
        RouterModule,
    ],
    declarations: [IvrComponent]
})
export class IvrModule { }



