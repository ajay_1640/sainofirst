import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
declare let $;
@Component({
    selector: 'app-ivr-reports-component',
    templateUrl: './ivr-reports-component.component.html',
    styles: []
})
export class IvrReportsComponentComponent implements OnInit {
    calllogs_p = 1;
    show_calendar = false;
    show_calendar1 = false;
    calendar_range = "";
    calendar_range1 = "";
    constructor(private _script: ScriptLoaderService) { }

    ngOnInit() {
        this._script.loadScripts('app-ivr-reports-component',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
    }

    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }
    rangeChanged1() {
        if (this.calendar_range1 == "custom") {
            $('#m_datepicker_11')[0].value = "";
            $('#m_datepicker_21')[0].value = "";
            this.show_calendar1 = true;
        }
        else {
            this.show_calendar1 = false;
        }
    }
}
