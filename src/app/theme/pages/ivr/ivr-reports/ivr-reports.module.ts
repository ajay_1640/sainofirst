
import { IvrReportsComponentComponent } from './ivr-reports-component/ivr-reports-component.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const routes: Routes = [
    {
        'path': '',
        'component': IvrReportsComponentComponent
    },
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule
    ], exports: [
        RouterModule,
    ], declarations: [
        IvrReportsComponentComponent,
    ],
})
export class IvrReportsModule { }
