import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { languages_ } from '../../../voicecall/sendvoicecall/sendvoice/language';

import swal from 'sweetalert2';
declare let moment;
declare let toastr;
declare let selectpicket;
declare let $;
@Component({
    selector: 'app-ivr-settings-component',
    templateUrl: './ivr-settings-component.component.html',
    styleUrls: ['./ivr-settings.css']
})
export class IvrSettingsComponentComponent implements OnInit {
    agent_list = [{
        name: 'agent name',
        email_id: 'agent emailid',
        login_access: 'login access',
        status: 'status',
        username: 'username'

    }
    ];
    p = 1;
    smsp = 1;
    emailp=1;
    calllogs_p = 1;
    textTospeechtext = "";
    showTextToSpeech = true;
    multimedia = [];
    flowp = 1;
    addedAgents = [];
    flowSearchText = "";
    name = "";
    access = false;
    status = 0;
    kyc_status = 0;
    email = "";
    number = "";
    index = 0;
    userid = "";
    password = "";
    list = ['Inactive', 'Active'];
    fromDate = '';
    toDate = '';
    timeSlotActivate = false;
    dateRange;
    ar: any;
    fromDate_2 = '';
    toDate_2 = '';
    dateRange_2;
    ar_2: any;
    call_searchText = "";
    agentnames = "";
    numbers = "";
    extension = "";
    temp_list = [{ 'created_date': "No data", 'template_data': 'No data', 'template_id': 'No data', 'template_name': 'No data', 'template_type': 'No data' }];
    temp_name;
    message = '';
    sms_count = 0;
    sms_count_edit = 0;
    message_updated = '';
    addedspan = [];
    show_loading = false;
    activateAgent = false;
    languageDropdown = [];
    temp_type = "";
    language = ""
    temp_selected;
    temp_type_updated = "";

    audioPlayer = $('.green-audio-player');
    playPause = $('#playPause');
    playpauseBtn = $('.play-pause-btn');
    loading = $('.loading');
    progress = $('.progress');
    sliders = $('.slider');
    volumeBtn = $('.volume-btn');
    volumeControls = $('.volume-controls');
    volumeProgress = $('.slider .progress');
    player = $('audio');
    currentTime = $('.current-time');
    totalTime = $('.total-time');
    speaker = $('#speaker');

    draggableClasses = ['pin'];
    currentlyDragged = null;
    responsecodes=[];
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) {

    }

    ngOnInit() {
        this.languageDropdown = languages_;
        this._script.loadScripts('app-ivr-settings-component',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-ivr-settings-component',
            ['assets/demo/demo3/base/toastr.js']);
        this._script.loadScripts('app-ivr-settings-component',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-ivr-settings-component',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.show_loading = true;
        this.getKycDetails();
        this.getTemplates();
        // this._script.loadScripts('app-inner',
        //     ['assets/app/js/dashboard.js']);
        $('.selectpicker').selectpicker({
            style: 'btn-info',
            size: 4
        });
        let settings = this;
        let s=this;
        $("#responsecodesdiv").on("click", "span.spanclose", (e) => {
            $('div#' + $(e.target).attr('id')).remove();
            for (let i = 0; i < s.responsecodes.length; i++) {
                if (s.responsecodes[i] == $(e.target).attr('id')) {
                    s.responsecodes.splice(i, 1);
                    break;
                }
            }
            // console.log("after deleting",s.responsecodes)
            // console.log($(e.target).attr('id'));
        })
        $("#responsecodesdiv").on("click", "input.selectaction", (e) => {
            // if ('Call Transfer' == e.target.value) {
            //     let s = $(e.target).attr('id').split('_');
            //     $('#mobile_' + s[1]).show();
            // }
            // else {
            //     let s = $(e.target).attr('id').split('_');
            // }
            console.log($(e.target).prop("checked") )
             console.log(e);
             let s = $(e.target).attr('id').split('_');
            console.log($('#mobile_' + s[1]))
            if($(e.target).prop("checked")){
                $('#mobile_' + s[1]).parent().show();
                $('#mobile_'+s[1]).selectpicker('refresh');
            }
            else{
                $('#mobile_' + s[1]).parent().hide();

            }


            //  $('#mobile_' + s[1]).attr('hidden','false')
            // console.log($(e.target).attr('id'));

        })
        // $('.volume-btn').on('click', () => {
        //     console.log("volume button pressed")
        //     $('.volume-btn').toggle('open');
        //     $('.volume-controls').toggle('hidden');
        //   })

        //   $('#mySelect').on('loaded.bs.select', function (e) {
        //     // do something...
        //     console.log("select loaded");
        //   });
        $('#add_agent').on('shown.bs.modal', function(e) {
            $('#select1').selectpicker('refresh');

        })

        // $("#addagentdiv").on("click","span.spanclose",(e)=>{
        //     $('div#'+$(e.target).attr('id')).remove();
        //     // for(let i=0;i<s.responsecodes.length;i++){
        //     //     if(s.responsecodes[i]==$(e.target).attr('id')){
        //     //         s.responsecodes.splice(i,1);
        //     //         break;
        //     //     }
        //     // }
        //     // console.log("after deleting",s.responsecodes)
        //     // console.log($(e.target).attr('id'));
        // })
        $("#addagentdiv").on("change", "input.activatespan", (e) => {
            console.log(this.addedspan)
            $('.selectpicker').selectpicker('refresh')
            // if(false==e.target.value){
            //     let s=$(e.target).attr('id').split('_');
            //     $('#mobile_'+s[1]).show();
            //     console.log("false");
            // }
            // else{
            // let s=$(e.target).attr('id').split('_');
            // $('#mobile_'+s[1]).hide();
            // console.log("true")
            // }
            // console.log(e.target.value);
            // console.log($(e.target).attr('id'));

        })
        // $(function() {

        //     var start = moment();
        //     var end = moment();

        //     function cb(start, end) {
        //         $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        //     }

        //     $('#reportrange').daterangepicker({
        //         startDate: start,
        //         endDate: end,
        //         ranges: {
        //             'Today': [moment(), moment()],
        //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //             'This Month': [moment().startOf('month'), moment().endOf('month')],
        //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //         }
        //     }, cb);

        //     cb(start, end);

        // });
        // $(function() {

        //     var start_2 = moment();
        //     var end_2 = moment();

        //     function cb(start, end) {
        //         $('#reportrange_2 span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        //     }

        //     $('#reportrange_2').daterangepicker({
        //         startDate_2: start_2,
        //         endDate_2: end_2,
        //         ranges: {
        //             'Today': [moment(), moment()],
        //             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        //             'This Month': [moment().startOf('month'), moment().endOf('month')],
        //             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //         }
        //     }, cb);

        //     cb(start_2, end_2);

        // });

        jQuery(function() {
            var datepicker: any = $('input#date2');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');

                })
            }
        });
        // this.dateRange_2 = $('#reportrange span').text();
        // this.ar_2 = this.dateRange.split(' - ');
        // this.fromDate_2 = this.ar[0];
        // this.toDate_2 = this.ar[1];

        // this.dateRange = $('#reportrange span').text();
        // this.ar = this.dateRange.split(' - ');
        // this.fromDate = this.ar[0];
        // this.toDate = this.ar[1];

        $('#musicpopup').on('hidden.bs.modal', (e) => {
            $('#musicplayer').get(0).pause();
            $('#musicplayer').currentTime = 0;

        })

    }
    flowSettings() {
        $('#m_modal_flow_settings').modal('show')
    }


    access_changed() {
        this.access = !this.access;
        // $('.selectpicker').selectpicker('refresh');
    }
    add_agent_clicked() {
        $('#add_agent').modal('show')
        this.name = "";
        this.status = 0;
        this.access = false;
        this.email = "";
        this.number = "";
        this.userid = "";
        this.password = "";


    }
    // <input  class='form-control m-input' type='text' placeholder='Mobile No'  [ngModelOptions]='{standalone: true}'>
    addResponseCode() {
        this.getResponseCodeDetails();
        this.index++;
        this.responsecodes.push('codediv_' + this.index);
        let d = 'selectpicker';
        // <input class='form-control m-input' type='text' placeholder='Code' id='code_`+ this.index + `' [ngModelOptions]='{standalone: true}'>

        //  let code="responsecodes["+(i-1)+"].code";
        //  let action="responsecodes["+(i-1)+"].action";
        //  let mobile="responsecodes["+(i-1)+"].mobile";
        //  let time="responsecodes["+(i-1)+"].time"; <input class='form-control m-input' type='text' placeholder='Code' id='action_`+this.index+`' [ngModelOptions]='{standalone: true}'>
        let s = `
 <div  id='codediv_`+ this.index + `' class='row col-lg-12' style='margin-bottom:5px;padding:0px'>
  <div class='col-lg-2'>
  <select hidden id='code_`+ this.index + `'  class=" selectpicker  form-control form-contm-bootstrap-select m_selectpicker " data-dropup-auto="false" data-size="5" data-live-search="true">
  <option  [selected]="true" [disabled]='false'>Select Action</option>
  <option value='repeatMessage'>Repeat Message</option>
  <option>Get Responses</option>
  <option>Call Transfer</option>
  </select>
 </div>
 <div class='col-lg-3'>

 <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
 <label>
             <input  id="select_`+ this.index + `" type="checkbox" class="selectaction" >
             <span></span>
             </label>
         </span>
 </div>
 <div class='col-lg-3'>
  <select hidden id='mobile_`+ this.index + `'  class=" selectpicker  form-control form-contm-bootstrap-select m_selectpicker " data-dropup-auto="false" data-size="5" data-live-search="true">
  <option  [selected]="true" [disabled]='false'>Select Action</option>
  <option value='repeatMessage'>Repeat Message</option>
  <option>Get Responses</option>
  <option>Call Transfer</option>
  </select>

 </div>
 <div class='col-lg-1'>
 <span  class="spanclose" style="float: left;padding-top: 5px;">
 <img id="codediv_`+ this.index + `" src="./assets/app/media/img/icons/close.png"  style="width: 14px;cursor: pointer;">
</span>
 </div> 
 </div>`
        $('#responsecodesdiv').append(s);
        $('#code_'+this.index).selectpicker('render');

        // $('#mobile_' + this.index).hide();
        // $("#select_" + this.index + " option:contains('Select Action')").attr('disabled', 'disabled')
        // $('.selectpicker').selectpicker('render');
        //  console.log(this.responsecodes);
    }
    new_agent() {

    }
    saveagents() {

    }
    edit_agent_clicked(agent_index) {
        //  console.log(this.agent_list[agent_index]);

    }
    musicPopUp() {
        $('#musicpopup').modal('show')
    }
    addTemp() {
        $('#m_modal_6').modal('show');
        this.temp_type = '';
        this.message = "";
        this.temp_name = "";
        this.sms_count = 0;
    }
    getKycDetails() {
        this._appService.get_method(this._userService.kyc_status).subscribe(
            data => { this.kyc_status = data.status; }
        );
    }
    MessageChanged() {
        //console.log("message");
        if (this.temp_type == 'English') this.sms_count = Math.ceil(this.message.length / 160);
        else if (this.temp_type == 'Unicode') {
            this.sms_count = Math.ceil(this.message.length / 70);
        }
        else {
            this.sms_count = 0;
        }

    }
    getResponseCodeDetails() {
        // console.log("getting all the details");
        let s1 = [];
        for (let i = 0; i < this.responsecodes.length; i++) {
            let s = this.responsecodes[i].split('_');
            // console.log("s1",s[1]);
            let data = {
                'code': $('#responsecodesdiv input#code_' + s[1]).val(),
                'mobile': $('#responsecodesdiv input#mobile_' + s[1]).val(),
                'time': $('#responsecodesdiv input#time_' + s[1]).val(),
                'select': $('#responsecodesdiv select#select_' + s[1]).val()
            }
            s1.push(data);
        }
        console.log("all data",s1)
    }
    englishSelected() {
        this.temp_type = 'English';
        this.MessageChanged();
    }
    unicodeSelected() {
        this.temp_type = 'Unicode';
        this.MessageChanged();

    }
    MessageChangedEdit() {

        if (this.temp_type_updated == 'English') this.sms_count_edit = Math.ceil(this.message_updated.length / 160);
        else if (this.temp_type_updated == 'Unicode') {
            this.sms_count_edit = Math.ceil(this.message_updated.length / 70);
        }
        else {
            this.sms_count_edit = 0;
        }

    }
    englishSelectedeEdit() {
        this.temp_type_updated = 'English';
        // console.log(this.temp_type_updated);

        this.MessageChangedEdit();
    }
    unicodeSelectedEdit() {
        this.temp_type_updated = 'Unicode';
        // console.log(this.temp_type_updated);

        this.MessageChangedEdit();

    }
    addAgent() {

        this.addedspan.push({ 'activate': false })
        let s = `<div class="row col-lg-12" id="addedagent_` + this.index + `" style="margin-top: 10px;padding: 0px;margin: 0px;">
                <div class="row col-lg-6" style="margin: 0px 0px;display: flex;align-items: center;">
                  <div class="col-lg-5">
                      Agent
                  </div>
                  <div class="col-lg-7" style="padding:0px">
                   <select id="addedagent_agent`+ this.index + `" class="form-control selectpicker m-bootstrap-select m_selectpicker">
                     <option>
                      DTMF
                     </option>
                     <option > 
                       Phone Call
                     </option>
                   </select>
                  </div>
               </div>
               <div class="row col-lg-6" style="margin: 0px 0px;display: flex;align-items: center;">
                <div class="col-lg-5">
                    <span  class="m-switch m-switch--outline m-switch--icon m-switch--success">
                        <label >
                                     <input class="activatespan" id="addedagent_activate`+ this.index + `" [(ngModel)]="addedspan[` + this.index + `]['activate']" type="checkbox" name="">
                                    <span></span>
                                    </label>
                                </span>
                </div>
                <div [hidden]="addedspan[`+ this.index + `]['activate']"  class="col-lg-5" style="padding:0px">
                    <select id="addedagent_extension`+ this.index + `t" data-size="5" class="form-control selectpicker m-bootstrap-select m_selectpicker">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                      </select>
                </div>
                <div class="col-lg-2" style="padding:0px">
                     <img (click)="deleteAgent(`+this.index+`)" src="./assets/app/media/img/icons/close.png"  style="width: 14px;cursor: pointer;">
                </div>
             </div>
               </div>`;
        $('#addagentdiv').append(s)
        $('.selectpicker').selectpicker('render')
        this.index++;


    }
    deleteAgent(ind){
        this.addedspan.splice(ind,1);
        console.log(this.addedspan);

    }
    getTemplates() {
        this.show_loading = true;
        this._appService.get_method(this._userService.template_url).subscribe(
            data => {
                this.temp_list = data["templates"].reverse();

                for (let i = 0; i < this.temp_list.length; i++) {

                    let s = this.temp_list[i]['template_data'].replace(/\n/g, ' ').match(/(.|[\r\n]){1,40}/g);
                    let s1 = "";
                    for (let j = 0; j < s.length; j++) {
                        s1 += s[j];
                        s1 += "\n";
                    }
                    this.temp_list[i]['template_data'] = s1;
                }

                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    delete_temp(val) {
        this.show_loading = true;
        this._appService.delete_method(this._userService.template_url + "/" + val.template_id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Template Deleted Successfully<h5>");

                    this.getTemplates();
                }

                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    add_temp() {
        this.temp_name = this.temp_name.trim();
        if (this.temp_name == "" || !/^[a-zA-Z]+$/.test(this.temp_name.replace(/\s/g, ''))) return toastr.warning("<h5>Template name is Invalid<h5>");

        for (let j = 0; j < this.temp_list.length; j++) {
            if (this.temp_list[j]['template_name'] == this.temp_name) {
                return toastr.error("<h5>Template with the given name exist<h5>");
            }
        }
        if (this.temp_type == "") return toastr.warning("<h5>Choose Template Type<h5>");

        if (this.message == "") return toastr.warning("<h5>Message is Invalid<h5>");
        $('#m_modal_6').modal('hide');
        let data_send = JSON.stringify({
            'template_name': this.temp_name,
            'template_text': this.message,
            'template_type': this.temp_type,
        });
        this.show_loading = true;
        this._appService.post_method(this._userService.template_url, data_send).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Template Created Successfully<h5>");
                    this.temp_name = "";
                    this.temp_type = "";
                    this.message = "";

                    this.getTemplates();
                }
                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }


            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    current_temp(val) {
        $('#m_modal_5').modal('show');
        this.temp_selected = val;
        this.message_updated = val.template_data;
        this.temp_type_updated = val.template_type;
        this.MessageChangedEdit();
        this.temp_name = val.template_name;
    }
    update_temp() {
        if (this.message_updated == "") return toastr.warning("Message is Invalid");
        $('#m_modal_5').modal('hide');
        let data_send = JSON.stringify({
            'template_name': this.temp_name,
            'template_text': this.message_updated,
            'template_type': this.temp_type_updated,
        });
        this.show_loading = true;
        this._appService.patch_method(this._userService.template_url + "/" + this.temp_selected.template_id, data_send).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Template Updated Successfully<h5>");

                    this.getTemplates();
                }
                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error  " + error + "<h5>");
            });
    }
    multimediaFile(target) {

        if (target.files == null || target.files == '') {
            return;
        }
        if (this.multimedia.length == 1) {
            toastr.info("<h5>Maximum 1 File Can be uploaded<h5>");
            return
        }
        else {
            this.multimedia.push(target.files[0]);
            target.value = "";
        }
        // const file = target.files[0];

        // let size=file['size'];
        // size=size/1024;
        // size=size/1024;
        // console.log(size);
        // if(size>1.1){
        //     console.log("File is Larger Than 1MB");
        //     return;        
        // }
        // else{
        //     console.log("file is ok");
        // }
        // console.log(target.files[0])
        // const uploads = []
        // const blob = file.slice(0, 4);
        // const filereader = new FileReader()
        // filereader.readAsArrayBuffer(blob);
        // filereader.onloadend = function(evt) {       
        //         const uint = new Uint8Array(filereader.result);
        //         let bytes = []          
        //         uint.forEach((byte) => {
        //             bytes.push(byte.toString(16))
        //         })
        //         const hex = bytes.join('').toUpperCase()
        //         uploads.push({
        //             filename: file.name,
        //             filetype: file.type ? file.type : 'Unknown/Extension missing',
        //             binaryFileType: getMimetype(hex),
        //             hex: hex
        //         })

        //     console.timeEnd('FileOpen')
        // }
        // const getMimetype = (signature) => {
        // if(/^424D/.test(signature)){
        //     return 'image/bmp';
        // }
        // if(/^494433/.test(signature)){
        //     return 'audio/mp3'
        // }
        // if(/^000{1}[0-9a-zA-Z]{2}/.test(signature)){
        //     return 'video/mp4'
        // }
        //     switch (signature) {
        //         //img
        //         case '89504E47':
        //             return 'image/png'
        //         case '47494638':
        //             return 'image/gif'
        //         case 'FFD8FFDB':
        //         case 'FFD8FFE0':
        //         case 'FFD8FFE1':
        //             return 'image/jpeg'
        //         case '49492A00':
        //         case '4D4D002A':
        //             return 'image/tiff'
        //         case '38425053':
        //             return 'image/psd'
        //         //pdf
        //         case '25504446':
        //             return 'application/pdf'

        //         //audio
        //         case '52494646':
        //              return 'audio/wav'


        //         case '1A45DFA3':
        //              return 'video/x-matroska'
        //         default:
        //             return 'Unknown filetype'
        //     }

        // }


    }
    removeFile(index) {
        this.multimedia.splice(index, 1);
    }
    addSound() {
        $('#add_sound').modal('show')
    }
    recordChanged() {
        this.showTextToSpeech = false;

    }
    textChanged() {
        this.showTextToSpeech = true;
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh')
        }, 250)
        this.textTospeechtext = "";

    }

    // window.addEventListener('mousedown', function(event) {

    //   if(!isDraggable(event.target)) return false;

    //   currentlyDragged = event.target;
    //   let handleMethod = currentlyDragged.dataset.method;

    //   this.addEventListener('mousemove', window[handleMethod], false);

    //   window.addEventListener('mouseup', () => {
    //     currentlyDragged = false;
    //     window.removeEventListener('mousemove', window[handleMethod], false);
    //   }, false);  
    // });

    // this.playpauseBtn.addEventListener('click', togglePlay);
    // player.addEventListener('timeupdate', updateProgress);
    // player.addEventListener('volumechange', updateVolume);
    // player.addEventListener('loadedmetadata', () => {
    //   totalTime.textContent = formatTime(player.duration);
    // });
    // player.addEventListener('canplay', makePlay);
    // player.addEventListener('ended', function(){
    //   playPause.attributes.d.value = "M18 12L0 24V0";
    //   player.currentTime = 0;
    // });



    // window.addEventListener('resize', directionAware);

    // sliders.forEach(slider => {
    //   let pin = slider.querySelector('.pin');
    //   slider.addEventListener('click', window[pin.dataset.method]);
    // });

    // directionAware();

    // function isDraggable(el) {
    //   let canDrag = false;
    //   let classes = Array.from(el.classList);
    //   draggableClasses.forEach(draggable => {
    //     if(classes.indexOf(draggable) !== -1)
    //       canDrag = true;
    //   })
    //   return canDrag;
    // }

    // function inRange(event) {
    //   let rangeBox = getRangeBox(event);
    //   let rect = rangeBox.getBoundingClientRect();
    //   let direction = rangeBox.dataset.direction;
    //   if(direction == 'horizontal') {
    //     var min = rangeBox.offsetLeft;
    //     var max = min + rangeBox.offsetWidth;   
    //     if(event.clientX < min || event.clientX > max) return false;
    //   } else {
    //     var min = rect.top;
    //     var max = min + rangeBox.offsetHeight; 
    //     if(event.clientY < min || event.clientY > max) return false;  
    //   }
    //   return true;
    // }

    // function updateProgress() {
    //   var current = player.currentTime;
    //   var percent = (current / player.duration) * 100;
    //   progress.style.width = percent + '%';

    //   currentTime.textContent = formatTime(current);
    // }

    // function updateVolume() {
    //   volumeProgress.style.height = player.volume * 100 + '%';
    //   if(player.volume >= 0.5) {
    //     speaker.attributes.d.value = 'M14.667 0v2.747c3.853 1.146 6.666 4.72 6.666 8.946 0 4.227-2.813 7.787-6.666 8.934v2.76C20 22.173 24 17.4 24 11.693 24 5.987 20 1.213 14.667 0zM18 11.693c0-2.36-1.333-4.386-3.333-5.373v10.707c2-.947 3.333-2.987 3.333-5.334zm-18-4v8h5.333L12 22.36V1.027L5.333 7.693H0z';  
    //   } else if(player.volume < 0.5 && player.volume > 0.05) {
    //     speaker.attributes.d.value = 'M0 7.667v8h5.333L12 22.333V1L5.333 7.667M17.333 11.373C17.333 9.013 16 6.987 14 6v10.707c2-.947 3.333-2.987 3.333-5.334z';
    //   } else if(player.volume <= 0.05) {
    //     speaker.attributes.d.value = 'M0 7.667v8h5.333L12 22.333V1L5.333 7.667';
    //   }
    // }

    // function getRangeBox(event) {
    //   let rangeBox = event.target;
    //   let el = currentlyDragged;
    //   if(event.type == 'click' && isDraggable(event.target)) {
    //     rangeBox = event.target.parentElement.parentElement;
    //   }
    //   if(event.type == 'mousemove') {
    //     rangeBox = el.parentElement.parentElement;
    //   }
    //   return rangeBox;
    // }

    // function getCoefficient(event) {
    //   let slider = getRangeBox(event);
    //   let rect = slider.getBoundingClientRect();
    //   let K = 0;
    //   if(slider.dataset.direction == 'horizontal') {

    //     let offsetX = event.clientX - slider.offsetLeft;
    //     let width = slider.clientWidth;
    //     K = offsetX / width;    

    //   } else if(slider.dataset.direction == 'vertical') {

    //     let height = slider.clientHeight;
    //     var offsetY = event.clientY - rect.top;
    //     K = 1 - offsetY / height;

    //   }
    //   return K;
    // }

    // function rewind(event) {
    //   if(inRange(event)) {
    //     player.currentTime = player.duration * getCoefficient(event);
    //   }
    // }

    // function changeVolume(event) {
    //   if(inRange(event)) {
    //     player.volume = getCoefficient(event);
    //   }
    // }

    // function formatTime(time) {
    //   var min = Math.floor(time / 60);
    //   var sec = Math.floor(time % 60);
    //   return min + ':' + ((sec<10) ? ('0' + sec) : sec);
    // }

    // function togglePlay() {
    //   if(player.paused) {
    //     playPause.attributes.d.value = "M0 0h6v24H0zM12 0h6v24h-6z";
    //     player.play();
    //   } else {
    //     playPause.attributes.d.value = "M18 12L0 24V0";
    //     player.pause();
    //   }  
    // }

    // function makePlay() {
    //   playpauseBtn.style.display = 'block';
    //   loading.style.display = 'none';
    // }

    //  directionAware() {
    //   if(window.innerHeight < 250) {
    //     volumeControls.style.bottom = '-54px';
    //     volumeControls.style.left = '54px';
    //   } else if(audioPlayer.offsetTop < 154) {
    //     volumeControls.style.bottom = '-164px';
    //     volumeControls.style.left = '-3px';
    //   } else {
    //     volumeControls.style.bottom = '52px';
    //     volumeControls.style.left = '-3px';
    //   }
    // }
}

