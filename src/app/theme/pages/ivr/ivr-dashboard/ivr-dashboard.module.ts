import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IvrDashboardComponentComponent } from './ivr-dashboard-component/ivr-dashboard-component.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AmChartsModule } from "@amcharts/amcharts3-angular";


const routes: Routes = [
    {
        'path': '',
        'component': IvrDashboardComponentComponent
    },
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule, AmChartsModule,
        Ng2SearchPipeModule
    ], exports: [
        RouterModule,
    ], declarations: [
        IvrDashboardComponentComponent,
    ],
})
export class IvrDashboardModule { }


