import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IvrCalllogsComponentComponent } from './ivr-calllogs-component/ivr-calllogs-component.component';

import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
const routes: Routes = [
    {
        'path': '',
        'component': IvrCalllogsComponentComponent
    },
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule
    ], exports: [
        RouterModule,
    ], declarations: [
        IvrCalllogsComponentComponent,
    ],
})
export class IvrCalllogsModule { }
