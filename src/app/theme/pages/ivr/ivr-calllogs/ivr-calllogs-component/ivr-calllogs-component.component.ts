import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import swal from 'sweetalert2';
declare let moment;
declare let $;
@Component({
    selector: 'app-ivr-calllogs-component',
    templateUrl: './ivr-calllogs-component.html',
    styles: []
})
export class IvrCalllogsComponentComponent implements OnInit {
    call_searchText = "";
    numbers = "";
    agentnames = "";
    show_calendar = false;
    extension = "";
    agent_list = [{
        name: 'agent name',
        email_id: 'agent emailid',
        login_access: 'login access',
        status: 'status',
        username: 'username'

    }
    ];
    calllogs_p = 1;
    calendar_range = "";

    // p = 1;

    // name = "";
    // access = false;
    // status = 0;
    // email = "";
    // number = "";
    // userid = "";
    // password = "";
    // list = ['Inactive', 'Active'];
    // fromDate = '';
    // toDate = '';
    // dateRange;
    // ar: any;
    // fromDate_2 = '';
    // toDate_2 = '';
    // dateRange_2;
    // ar_2: any;


    constructor(private _script: ScriptLoaderService) { }

    ngOnInit() {
        this._script.loadScripts('app-ivr-calllogs-component',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-ivr-calllogs-component',
            ['assets/demo/demo3/base/toastr.js']);
        this._script.loadScripts('app-ivr-calllogs-component',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }
    }
    musicPopUp() {
        $('#musicpopup').modal('show')
    }

}
