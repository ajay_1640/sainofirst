import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { CookieService } from 'ngx-cookie-service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
declare let toastr;
@Component({
    selector: 'app-voice-price',
    templateUrl: './voice-price.component.html',
    styles: []
})
export class VoicePriceComponent implements OnInit {
    p: number = 1;
    searchText = "";
    pricing_p = 1;
    pricing_permissions = [];
    pricing_permissions_india = [];
    pricing_permissions_global = [];
    country = "";
    show_loading = false
    constructor(private _appService: AppService, private _script: ScriptLoaderService, private _cookieService: CookieService, private _userService: UserService) { }

    ngOnInit() {
        // console.log("price called");
        this.country = this._cookieService.get('country');
        //    console.log("country",this.country);
        this._script.loadScripts('app-voice-price',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.show_pricing_menu();
    }
    show_pricing_menu() {
        this.show_loading = true;
        this.show_loading = true;
        let userid = this._cookieService.get('userid');
        this._appService.get_method(this._userService.permission_manage_users_permissions + userid + "/" + "pricing").subscribe(
            data => {
                this.pricing_permissions_global = [];
                this.pricing_permissions_india = [];
                this.pricing_permissions = data.data;
                for (let i = 0; i < this.pricing_permissions.length; i++) {
                    if (this.pricing_permissions[i]['iso2'] == "IN") {
                        this.pricing_permissions_india.push(this.pricing_permissions[i]);
                    }
                    else {
                        this.pricing_permissions_global.push(this.pricing_permissions[i]);

                    }
                }
                //  console.log(this.pricing_permissions);

                // console.log("pricing");
                this.show_loading = false;
                if (!data.status) return toastr.error("<h5>" + data.message + "<h5>");
            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
                this.show_loading = false;


            }
        );

    }

}
