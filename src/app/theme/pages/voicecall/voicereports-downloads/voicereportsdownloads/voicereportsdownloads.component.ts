import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { saveAs } from 'file-saver/FileSaver';
import * as JSZip from 'jszip';
import * as XLSX from 'xlsx';


declare var moment: any;
declare let toastr;
declare var $, jQuery: any;
@Component({
    selector: 'app-voicereportsdownloads',
    templateUrl: './downloads.html',
    styles: []
})
export class VoicereportsdownloadsComponent implements OnInit {
    download_list = [{
        'fromdate': 'Not Done',
        'todate': 'Not Done',
        'requested': 'Not Done',
        'prepared': 'Not Done',
        'status': 'Not Done'

    }];
    p = 1;
    dateRange: any;
    ar: any;
    from_date = "";
    arr: any;
    zip: any
    to_date = "";
    show_status = false;
    toDate: any;
    fromDate: any;
    calendar_range = "";
    show_calendar = false;
    requested_fromdate = "";
    requested_todate = "";
    requested = "";
    ready = "";
    readystatus = 0;
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this._script.loadScripts('app-voicereportsdownloads',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-voicereportsdownloads',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);

        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        this._script.loadScripts('app-voicereportsdownloads',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };


    }
    rangeChanged() {
        if (this.calendar_range == "custom") {
            $('#m_datepicker_1')[0].value = "";
            $('#m_datepicker_2')[0].value = "";
            this.show_calendar = true;
        }
        else {
            this.show_calendar = false;
        }

    }
    search_custom_date() {
        this.from_date = "";
        this.to_date = "";
        if (this.calendar_range == "") return;
        else if (this.calendar_range == "today") {
            this.from_date = moment().format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "7days") {
            this.from_date = moment().subtract(6, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "15days") {

            this.from_date = moment().subtract(14, 'days').format('DD/MM/YYYY');
            this.to_date = moment().format('DD/MM/YYYY');

        }
        else if (this.calendar_range == "month") {
            this.from_date = moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY');
            this.to_date = moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY');
        }
        else if (this.calendar_range == "custom") {

            this.from_date = $('#m_datepicker_1')[0].value;
            this.to_date = $('#m_datepicker_2')[0].value;
            if (this.from_date == "" || this.to_date == "") return toastr.warning("<h5>Choose Dates<h5>");
            // console.log(this.from_date,this.to_date);
            // this.from_date=moment(this.from_date,"DD/MM/YYYY").format('DD/MM/YYYY');
            // this.to_date=moment(this.to_date,"DD/MM/YYYY").format('DD/MM/YYYY');
        }
        this.show_status = false;
        this.downloadReport(this.from_date, this.to_date);

        // console.log(this.from_date,this.to_date);

    }
    download() {
        this.zip.generateAsync({ type: "blob" })
            .then(function(content) {
                saveAs(content, "DownloadReport.zip");

            });
    }
    downloadReport(starttime, endtime) {

        this.requested_fromdate = starttime;
        this.requested_todate = endtime;
        this.requested = moment().format("DD/MM/YYYY hh:mm:ss A");
        this.ready = "";
        this.readystatus = 0;
        this.show_status = true;


        this._appService.post_method(this._userService.downlaod_url, JSON.stringify({ 'start_time': starttime, 'end_time': endtime })).subscribe(
            data => {
                if (!data.status) return toastr.error("<h5>" + data.message + "<h5>");
                // this.show_status=false;
                // this.show_status=true;
                let result = data.result;
                if (result.length == 0) {
                    this.show_status = false;
                    return toastr.info("<h6>No Data Found For the Given Interval<h6>");
                    //this.show_status=false;
                }
                this.zip = new JSZip();
                let limit = 1000000;
                let noOfFiles = Math.ceil(result.length / limit);
                this.arr = new Array(noOfFiles);
                for (let j = 0; j < this.arr.length; j++) {
                    this.arr[j] = [];
                }

                let file_name = 1;
                for (let i = 1; i <= result.length; i++) {
                    result[i - 1]['created_at'] = moment(result[i - 1]['created_at']).format("DD/MM/YYYY hh:mm A");
                    result[i - 1]['delivered_date'] = moment(result[i - 1]['delivered_date']).format("DD/MM/YYYY hh:mm A");
                    let d = {
                        'Created Date': result[i - 1]['created_at'],
                        'Delivered Date': result[i - 1]['delivered_date'],
                        'Mobile Number': result[i - 1]['mobile_number'],
                        'Message': result[i - 1]['msgdata'],
                        'Message Credit': result[i - 1]['msgcredit'],
                        'Sender Id': result[i - 1]['senderid'],
                        'Service Id': result[i - 1]['service_id'],
                        'Location': result[i - 1]['location'],
                        'Status': result[i - 1]['status']

                    }
                    this.arr[file_name - 1].push(d);

                    if (i % limit == 0) {
                        file_name++
                    }


                }
                for (let j = 0; j < this.arr.length; j++) {
                    let ws = XLSX.utils.json_to_sheet(this.arr[j]);
                    let wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "DReport_" + starttime.replace(/\//g, ' ') + "_" + endtime.replace(/\//g, ' '));
                    this.zip.file("Report_" + j + ".xlsx", XLSX.write(wb, { type: 'binary', compression: true }), { binary: true });
                }
                this.ready = moment().format("DD/MM/YYYY hh:mm:ss A");
                this.readystatus = 1;

            },
            error => {
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );
    }

}
