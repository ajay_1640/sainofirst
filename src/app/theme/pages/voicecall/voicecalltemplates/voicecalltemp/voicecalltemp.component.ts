import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
declare let toastr;
declare let $;
@Component({
    selector: 'app-voicecalltemp',
    templateUrl: './voicecalltemp.component.html',
    styles: []
})
export class VoicecalltempComponent implements OnInit {

    constructor(private _appService: AppService, private _script: ScriptLoaderService, private _userService: UserService) { }
    p: number = 1;
    kyc_status = 0;
    temp_list = [{ 'created_date': "No data", 'template_data': 'No data', 'template_id': 'No data', 'template_name': 'No data', 'template_type': 'No data' }];
    temp_name;
    message = '';
    sms_count = 0;
    sms_count_edit = 0;
    message_updated = '';
    show_loading = false;
    temp_type = "";
    temp_selected;
    temp_type_updated = "";
    ngOnInit() {
        this._script.loadScripts('app-voicecalltemp',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        this.show_loading = true;
        this.getKycDetails();
        this.getTemplates();
    }
    addTemp() {
        $('#m_modal_6').modal('show');
        this.temp_type = '';
        this.message = "";
        this.temp_name = "";
        this.sms_count = 0;
    }
    getKycDetails() {
        this._appService.get_method(this._userService.kyc_status).subscribe(
            data => { this.kyc_status = data.status; }
        );
    }
    MessageChanged() {
        //console.log("message");
        if (this.temp_type == 'English') this.sms_count = Math.ceil(this.message.length / 160);
        else if (this.temp_type == 'Unicode') {
            this.sms_count = Math.ceil(this.message.length / 70);
        }
        else {
            this.sms_count = 0;
        }

    }
    englishSelected() {
        this.temp_type = 'English';
        this.MessageChanged();
    }
    unicodeSelected() {
        this.temp_type = 'Unicode';
        this.MessageChanged();

    }
    MessageChangedEdit() {

        if (this.temp_type_updated == 'English') this.sms_count_edit = Math.ceil(this.message_updated.length / 160);
        else if (this.temp_type_updated == 'Unicode') {
            this.sms_count_edit = Math.ceil(this.message_updated.length / 70);
        }
        else {
            this.sms_count_edit = 0;
        }

    }
    englishSelectedeEdit() {
        this.temp_type_updated = 'English';
        // console.log(this.temp_type_updated);

        this.MessageChangedEdit();
    }
    unicodeSelectedEdit() {
        this.temp_type_updated = 'Unicode';
        // console.log(this.temp_type_updated);

        this.MessageChangedEdit();

    }
    getTemplates() {
        this.show_loading = true;
        this._appService.get_method(this._userService.template_url).subscribe(
            data => {
                this.temp_list = data["templates"].reverse();

                for (let i = 0; i < this.temp_list.length; i++) {

                    let s = this.temp_list[i]['template_data'].replace(/\n/g, ' ').match(/(.|[\r\n]){1,40}/g);
                    let s1 = "";
                    for (let j = 0; j < s.length; j++) {
                        s1 += s[j];
                        s1 += "\n";
                    }
                    this.temp_list[i]['template_data'] = s1;
                }

                this.show_loading = false;
            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    delete_temp(val) {
        this.show_loading = true;
        this._appService.delete_method(this._userService.template_url + "/" + val.template_id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Template Deleted Successfully<h5>");

                    this.getTemplates();
                }

                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    add_temp() {
        this.temp_name = this.temp_name.trim();
        if (this.temp_name == "" || !/^[a-zA-Z]+$/.test(this.temp_name.replace(/\s/g, ''))) return toastr.warning("<h5>Template name is Invalid<h5>");

        for (let j = 0; j < this.temp_list.length; j++) {
            if (this.temp_list[j]['template_name'] == this.temp_name) {
                return toastr.error("<h5>Template with the given name exist<h5>");
            }
        }
        if (this.temp_type == "") return toastr.warning("<h5>Choose Template Type<h5>");

        if (this.message == "") return toastr.warning("<h5>Message is Invalid<h5>");
        $('#m_modal_6').modal('hide');
        let data_send = JSON.stringify({
            'template_name': this.temp_name,
            'template_text': this.message,
            'template_type': this.temp_type,
        });
        this.show_loading = true;
        this._appService.post_method(this._userService.template_url, data_send).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Template Created Successfully<h5>");
                    this.temp_name = "";
                    this.temp_type = "";
                    this.message = "";

                    this.getTemplates();
                }
                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }


            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            });
    }

    current_temp(val) {
        $('#m_modal_5').modal('show');
        this.temp_selected = val;
        this.message_updated = val.template_data;
        this.temp_type_updated = val.template_type;
        this.MessageChangedEdit();
        this.temp_name = val.template_name;
    }
    update_temp() {
        if (this.message_updated == "") return toastr.warning("Message is Invalid");
        $('#m_modal_5').modal('hide');
        let data_send = JSON.stringify({
            'template_name': this.temp_name,
            'template_text': this.message_updated,
            'template_type': this.temp_type_updated,
        });
        this.show_loading = true;
        this._appService.patch_method(this._userService.template_url + "/" + this.temp_selected.template_id, data_send).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>Template Updated Successfully<h5>");

                    this.getTemplates();
                }
                else {
                    this.show_loading = false;
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                this.show_loading = false;
                toastr.error("<h5>Server Error  " + error + "<h5>");
            });
    }
}
