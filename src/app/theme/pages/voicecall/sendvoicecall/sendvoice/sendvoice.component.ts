import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { languages_ } from './language';
import { timezone } from '../../../../pages/default/profile/my-profile/country codes';


declare let $
declare let toastr;
declare let moment;
@Component({
    selector: 'app-sendvoice',
    templateUrl: './sendvoice.component.html',
    styles: []
})
export class SendvoiceComponent implements OnInit {

    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }

    sender_id_name = null;
    route_name = null;
    selectedSenderIdName = ""
    selectedRouteName = "";
    languageDropdown = [];
    // get_bulk_sms_url = 'http://35.238.188.102/saino-first/dash_board/get-bulk-sms-details';
    // get_group_details = 'http://35.238.188.102/saino-first/dash_board/get-group-details';
    // get_groups_url = 'http://35.238.188.102/saino-first/api/groups';
    // get_routes_url = 'http://35.238.188.102/saino-first/api/dashboard/routes';
    // get_senderid_url = 'http://35.238.188.102/saino-first/api/dashboard/sender_id';
    // msg_credits_url = "http://35.238.188.102/saino-first/api/dashboard/sms_balance";
    // add_group_nos_url = "http://35.238.188.102/saino-first/api/groups/multiple"
    // send_sms_url = "http://35.238.188.102/saino-first/api/dashboard/send_sms"
    max_msg_count: number = 160;
    route = {
        "az_routeid": null,
        "az_rname": null,
        "az_issenderid": null,
        "az_gateway_name": null
    };
    senderId = null;
    responsecodes = [];
    has_sender_id: boolean;
    numbers_count = 0;
    show_loading = false;
    validated = false;
    contentType = "English";
    reading_data = false;
    template_list;
    kycmessage = "";
    temp_value_selected;
    file_data;
    groups_list;
    routes_list: any;
    time_ngmodel = "";
    hideSenderId = false;
    date_ngmodel = "";
    senderid_list;
    timezone = "";
    smsTimezone = "";
    showTextToSpeech = true;

    smsTimezone1 = "";
    credits_data = {
        "pr_balance": 0,
        "tr_balance": 0
    };
    file_size = 0;

    schedule_sms = false;
    arrayBuffer;
    timezoneDropdown = {};
    timezoneobject = [];
    file_ext;
    checkedList: any[] = [];//Just created a list

    index = 0;

    show_to_numbers = true;
    numbers = "";
    textTospeechtext = "";
    message = "";
    template = "";
    searchText = "";
    scrolled = false;
    responsecode = false;
    r_schedule = false;
    BulkUploadFile: any = null;
    language = 'en-in';
    speechrate: any = 1;
    multimedia = [];
    voicetimezone = ""
    multiupload = [];
    speechrateintervals = [0.5, 0.75, 1, 1.25, 1.50, 1.75, 2];

    ngOnInit() {
        this.getUserTimezone();
        this._script.loadScripts('app-sendvoice',
            ['assets/demo/demo3/base/form-control.js']);
        this._script.loadScripts('app-sendvoice',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-sendvoice',
            ['assets/demo/demo3/base/dropzone.js']);
        this._script.loadScripts('app-sendvoice',
            ['assets/build/js/moment-timezone.js']);
        this._script.loadScripts('app-sendvoice',
            ['assets/build/js/moment-timezone-with-data.js']);
        this._script.loadScripts('app-sendvoice',
            ['assets/demo/demo3/base/bootstrap-datetimepicker.js']);
        this._script.loadScripts('app-sendvoice',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        // this._script.loadScripts('app-sendvoice',
        // ['./slider/js/bootstrap-slider.js']);
        // $('.add-one').click(function(){
        //     $('.dynamic-element').first().clone().appendTo('.dynamic-stuff').show();
        //     attach_delete();
        //   });


        //Attach functionality to delete buttons
        //   function attach_delete(){
        //     $('.delete').off();
        //     $('.delete').click(function(){
        //       console.log("click");
        //       $(this).closest('.form-group').remove();
        //     });
        //   }
        this.timezoneDropdown = timezone
        this.timezoneobject = Object.keys(this.timezoneDropdown);
        jQuery(function() {
            var datepicker: any = $('input.datepicker');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');

                })
            }
        });
        let s = this;
        $("#responsecodesdiv").on("click", "span.spanclose", (e) => {
            $('div#' + $(e.target).attr('id')).remove();
            for (let i = 0; i < s.responsecodes.length; i++) {
                if (s.responsecodes[i] == $(e.target).attr('id')) {
                    s.responsecodes.splice(i, 1);
                    break;
                }
            }
            // console.log("after deleting",s.responsecodes)
            // console.log($(e.target).attr('id'));
        })
        $("#responsecodesdiv").on("change", "select.selectaction", (e) => {
            if ('Call Transfer' == e.target.value) {
                let s = $(e.target).attr('id').split('_');
                $('#mobile_' + s[1]).show();
            }
            else {
                let s = $(e.target).attr('id').split('_');
                $('#mobile_' + s[1]).hide();
            }
            // console.log(e.target.value);
            // console.log($(e.target).attr('id'));

        })

        this.languageDropdown = languages_;
        // $('.selectpicker').selectpicker('refresh');
        this._script.loadScripts('app-sendvoice',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        let s_send = this;
        let Dropzone = $('#m-dropzone-three').dropzone(
            {
                url: "#",
                paramName: "file",
                maxFiles: 1,
                maxFilesize: 100,
                addRemoveLinks: 1,
                acceptedFiles: ".ods,.csv,.xlsx,.xls",
                accept: function(e, o) { "justinbieber.jpg" == e.name ? o("Naha, you don't.") : o() },
                maxfilesexceeded: function(file) { this.removeFile(file); },
                removedfile: function(file) {
                    s_send.BulkUploadFile = null;
                    $(document).find(file.previewElement).remove();
                    //console.log("file removed")
                },
                error: function(file) {
                    if (!file.accepted) this.removeFile(file);
                    else {
                        s_send.BulkUploadFile = file;
                        //   console.log("file stored",s_send.BulkUploadFile);
                    }
                }

            });

        Dropzone.autoDiscover = false;
        this.multimedia = [];
        //  this.getRoutes();
        //  this.getSenderId();
        this.getGroups();
        this.get_templates();

    }
    getUserTimezone() {
        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.timezone = data.profile_details[0]['timezone'];
                this.voicetimezone = this.timezone;
                //    console.log("user timezone",this.timezone);   
                $('.selectpicker').selectpicker('refresh')

            },
            error => {
            }
        );
    }
    calcTime(city, offset, type) {
        let d = new Date();
        let utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        let nd;
        if (type == '+') {
            nd = new Date(utc + (3600000 * offset));
        }
        else {
            nd = new Date(utc - (3600000 * offset));
        }
        return nd.toLocaleString();

    }
    sendNow() {
        if (this.r_schedule) {
            let scheduled_time = new moment();
            scheduled_time = moment($('input#date1')[0]['value'] + " " + $('#m_timepicker_1')[0]['value'], "DD/MM/YYYY hh:mm a").format("YYYY-MM-DD HH:mm:ss");
            let timezone = this.voicetimezone.substr(this.voicetimezone.length - 7, 6);
            let targetCountryTime;
            let timediff = timezone.substr(1, 5).split(':');
            let hours = parseInt(timediff[0]);
            let minutes = 0;
            if (parseInt(timediff[1]) != 0) { minutes = parseInt(timediff[1]) / 60; }
            let targetCountryPresentTime;
            if (timezone[0] == '+') { targetCountryPresentTime = this.calcTime("country", hours + minutes, '+'); }
            else { targetCountryPresentTime = this.calcTime("country", hours + minutes, '-'); }
            targetCountryTime = moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss']).format('MMMM DD, YYYY HH:mm:ss ');
            targetCountryTime = targetCountryTime.concat("GMT" + timezone);

            let final_date = moment(new Date(targetCountryTime).toLocaleString("en-US", { timeZone: "Asia/kolkata" }), 'MM/DD/YYYY, h:mm:ss A').format('YYYY-MM-DD HH:mm:ss');
            // console.log("final date",final_date);
            // console.log("targetcountry",targetCountryTime);
            // console.log("targetcountry present time",targetCountryPresentTime);
            if (moment(targetCountryPresentTime, 'DD/MM/YYYY, HH:mm:ss').diff(moment(scheduled_time, ['YYYY-MM-DD HH:mm:ss'])) > 0) {
                return toastr.info(this.voicetimezone + " Present time is Ahead of the Selected Time Please Choose Valid Time");
            }
        }

    }
    timezonechanged() {
        //   console.log("timezone",this.voicetimezone)
    }
    getGroups() {
        this._appService.get_method(this._userService.get_groups_url).subscribe(
            data => {
                // console.log("groups data",data);
                //if(!data.subscribe)return;
                this.groups_list = data.groups;

            },
            error => { });
    }
    getRoutes() {
        this._appService.get_method(this._userService.get_routes_url).subscribe(
            data => {
                if (!data.status) return;
                this.routes_list = data.data;
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['selected'] = false;
                }
                setTimeout(() => {
                    $('#selectpicker1').selectpicker('refresh');

                }, 200)
            },
            error => { });
    }

    getSenderId() {
        this._appService.get_method(this._userService.get_senderid_url).subscribe(
            data => {
                if (!data.status) return;
                this.senderid_list = data.sender_information;
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['selected'] = false;
                }
                setTimeout(() => {
                    $('#selectpicker2').selectpicker('refresh');

                }, 200);
                // console.log(JSON.stringify(this.senderid_list) + 'sender id data');
            },
            error => {
                //console.log('get error')
            });
    }

    getSpecificSenderId(val) {
        // this.hideSenderId=false;
        this.senderid_list = [];

        for (let j = 0; j < this.routes_list.length; j++) {
            if (this.routes_list[j]['az_rname'] == val) {
                this.route = this.routes_list[j];
                break;
            }
        }
        // console.log("selected route",this.route);
        if (this.route['is_senderid'] == 0) {
            this.hideSenderId = true;
        }
        else {
            this.hideSenderId = false;
        }
        this.selectedRouteName = this.route.az_rname;
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 50)
        //console.log("route senderid ",this.selectedRouteName,this.selectedSenderIdName);
        this.route_name = this.route.az_gateway_name;
        this._appService.get_method(this._userService.get_senderid_url + '/' + this.route.az_routeid).subscribe(
            data => {
                // console.log(data);
                this.senderid_list = data.sender_information;
                this.has_sender_id = true;
                if (!this.senderid_list.length) {
                    return this.has_sender_id = false;
                }
                for (let i = 0; i < this.senderid_list.length; i++) {
                    this.senderid_list[i]['selected'] = false;
                    if (this.senderid_list[i]['senderid'] == this.selectedSenderIdName) {
                        this.senderid_list[i]['selected'] = true;
                        this.senderId = this.senderid_list[i];
                        this.sender_id_name = this.senderId.senderid;
                    }
                }
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);

                //console.log(this.senderid_list);

                // console.log("route senderid ",this.selectedRouteName,this.selectedSenderIdName);


            },
            error => {

            });

    }

    getSpecificRoute(val) {
        for (let j = 0; j < this.senderid_list.length; j++) {
            if (this.senderid_list[j]['senderid'] == val) {
                this.senderId = this.senderid_list[j];
                break;
            }
        }
        this.selectedSenderIdName = this.senderId.senderid;

        // console.log("route senderid specific route ",this.selectedRouteName,this.selectedSenderIdName);

        this.sender_id_name = this.senderId.senderid;
        this._appService.get_method(this._userService.get_routes_url + '/' + this.senderId.sid).subscribe(
            data => {
                // console.log(data);
                this.routes_list = data.data;
                for (let i = 0; i < this.routes_list.length; i++) {
                    this.routes_list[i]['selected'] = false;
                    if (this.routes_list[i]['az_rname'] == this.selectedRouteName) {
                        this.routes_list[i]['selected'] = true;
                        this.route = this.routes_list[i];
                        this.route_name = this.route.az_gateway_name;
                    }
                }
                // console.log(this.routes_list)
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');

                }, 200);
                $('#search_categories').val(this.selectedRouteName);
                // console.log("route senderid specific route end ",this.selectedRouteName,this.selectedSenderIdName);


                //console.log(JSON.stringify(this.routes_list) + 'sender id data');
            },
            error => {
                // console.log('get error')
            });
    }
    scroll() {
        this.scrolled = !this.scrolled;
        if (this.scrolled) {
            $('#scroll').attr('src', './assets/app/media/img/icons/arrow-up.png');
            this.fileUploaded();

        }
        else {
            $('#mediaattach').css('height', '90px');
            $('#scroll').attr('src', './assets/app/media/img/icons/arrow-down.png');
            $('#scrolldiv').css({ 'margin-top': '6px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });
            // $('#mediaattach').animate( { height: '90px' }, { queue:false, duration:250 }).promise().done(function () {
            //     setTimeout(()=>{

            //     },350)

            // });
            // $('#mediaattach').css('height','90px');
            //  $('#scrolldiv').animate( { height: '90px' }, { queue:false, duration:250 })
        }

    }
    fileUploaded() {
        if (this.multimedia.length == 0) {
            $('#mediaattach').css('height', '120px');
            // $('#scroll').css('margin','0px 0px 0px 0px');
            $('#scrolldiv').css({ 'margin-top': '-4px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });

        }
        else if (this.multimedia.length >= 3) {
            $('#mediaattach').css('height', '165px');
            //  $('#scroll').css('margin','0px');
            $('#scrolldiv').css({ 'margin-top': '42px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });

        }
        else if (this.multimedia.length >= 1) {
            $('#mediaattach').css('height', '135px');
            // $('#scroll').css('margin','0px'); 
            $('#scrolldiv').css({ 'margin-top': '10px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });

        }
    }
    show_numbers() {
        this.show_to_numbers = true;
    }
    rScheduleChanged() {
        $('input.datepicker')[0]['value'] = "";
        $('#m_timepicker_1')[0]['value'] = this.getCurrentTime(new Date());
    }
    responsecodeChanged() {
        $('#responsecodesdiv').empty();
        this.responsecodes = [];
    }
    getCurrentTime(date) {
        var hours = date.getHours(),
            minutes = date.getMinutes(),
            ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return hours + ':' + minutes + ' ' + ampm;
    }
    recordChanged() {
        this.showTextToSpeech = false;

    }
    textChanged() {
        this.showTextToSpeech = true;
        this.speechrate = "";
        this.language = "";

        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh')
        }, 250)
        this.textTospeechtext = "";

    }
    update_count() {
        this.numbers_count = this.numbers.split("\n").length;    }
    show_dropzone() {
        this.numbers = "";
        this.show_to_numbers = false;
    }
    clear_nos() { this.numbers = ""; }
    make_unique() {
        this.numbers = this.numbers.replace(/[\n]/g, '\,');
        this.numbers = this.numbers.replace(/\,/g, '\n');
        let xs = this.numbers.split("\n");
        xs = xs.toString().split("\,");
        //console.log(xs+'after split');
        let unique_array = xs.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
        // console.log(unique_array+'new msg');
        let new_msg = unique_array.toString();
        new_msg = new_msg.replace(/\,/g, '\n');
        this.numbers = new_msg;
        // console.log("in make unique",this.numbers);
    }
    removeFile(index) {
        this.multimedia.splice(index, 1);
    }
    multimediaFile(target) {

        if (target.files == null || target.files == '') {
            return;
        }
        if (this.multimedia.length == 1) {
            toastr.info("<h5>Maximum 1 File Can be uploaded<h5>");
            return
        }
        else {
            this.multimedia.push(target.files[0]);
            target.value = "";
        }
        // const file = target.files[0];

        // let size=file['size'];
        // size=size/1024;
        // size=size/1024;
        // console.log(size);
        // if(size>1.1){
        //     console.log("File is Larger Than 1MB");
        //     return;        
        // }
        // else{
        //     console.log("file is ok");
        // }
        // console.log(target.files[0])
        // const uploads = []
        // const blob = file.slice(0, 4);
        // const filereader = new FileReader()
        // filereader.readAsArrayBuffer(blob);
        // filereader.onloadend = function(evt) {       
        //         const uint = new Uint8Array(filereader.result);
        //         let bytes = []          
        //         uint.forEach((byte) => {
        //             bytes.push(byte.toString(16))
        //         })
        //         const hex = bytes.join('').toUpperCase()
        //         uploads.push({
        //             filename: file.name,
        //             filetype: file.type ? file.type : 'Unknown/Extension missing',
        //             binaryFileType: getMimetype(hex),
        //             hex: hex
        //         })

        //     console.timeEnd('FileOpen')
        // }
        // const getMimetype = (signature) => {
        // if(/^424D/.test(signature)){
        //     return 'image/bmp';
        // }
        // if(/^494433/.test(signature)){
        //     return 'audio/mp3'
        // }
        // if(/^000{1}[0-9a-zA-Z]{2}/.test(signature)){
        //     return 'video/mp4'
        // }
        //     switch (signature) {
        //         //img
        //         case '89504E47':
        //             return 'image/png'
        //         case '47494638':
        //             return 'image/gif'
        //         case 'FFD8FFDB':
        //         case 'FFD8FFE0':
        //         case 'FFD8FFE1':
        //             return 'image/jpeg'
        //         case '49492A00':
        //         case '4D4D002A':
        //             return 'image/tiff'
        //         case '38425053':
        //             return 'image/psd'
        //         //pdf
        //         case '25504446':
        //             return 'application/pdf'

        //         //audio
        //         case '52494646':
        //              return 'audio/wav'


        //         case '1A45DFA3':
        //              return 'video/x-matroska'
        //         default:
        //             return 'Unknown filetype'
        //     }

        // }


    }
    get_templates() {
        this._appService.get_method(this._userService.template_url).subscribe(
            data => {
                if (!data.status) return;
                this.template_list = data.templates;
                setTimeout(() => {
                    $('#selectpickertemplate').selectpicker('refresh');
                }, 200)
            },
            error => { });
    }
    addResponseCode() {
        this.index++;
        this.responsecodes.push('codediv_' + this.index);
        let d = 'selectpicker';
        //  let code="responsecodes["+(i-1)+"].code";
        //  let action="responsecodes["+(i-1)+"].action";
        //  let mobile="responsecodes["+(i-1)+"].mobile";
        //  let time="responsecodes["+(i-1)+"].time"; <input class='form-control m-input' type='text' placeholder='Code' id='action_`+this.index+`' [ngModelOptions]='{standalone: true}'>
        let s = `
 <div  id='codediv_`+ this.index + `' class='row col-lg-12' style='margin-bottom:5px'>
  <div class='col-lg-2'>
  <input class='form-control m-input' type='text' placeholder='Code' id='code_`+ this.index + `' [ngModelOptions]='{standalone: true}'>
 </div>
 <div class='col-lg-3'>
 <select id="select_`+ this.index + `" class="selectaction selectpicker  form-control form-contm-bootstrap-select m_selectpicker " data-dropup-auto="false" data-size="5" data-live-search="true">
 <option  [selected]="true" [disabled]='false'>Select Action</option>
 <option value='repeatMessage'>Repeat Message</option>
 <option>Get Responses</option>
 <option>Call Transfer</option>
 </select>	
 </div>
 <div class='col-lg-3'>
 <input  class='form-control m-input' type='text' placeholder='WaitTime(Seconds)' id='time_`+ this.index + `' [ngModelOptions]='{standalone: true}'>
 </div>
 <div class='col-lg-3'>
 <input  class='form-control m-input' type='text' placeholder='Mobile No' [hidden]=true id='mobile_`+ this.index + `' [ngModelOptions]='{standalone: true}'>
 </div>
 <div class='col-lg-1'>
 <span  class="spanclose" style="float: left;padding-top: 5px;">
 <img id="codediv_`+ this.index + `" src="./assets/app/media/img/icons/close.png"  style="width: 14px;cursor: pointer;">
</span>
 </div> 
 </div>`
        $('.selectpicker').selectpicker('refresh');
        $('#responsecodesdiv').append(s);
        $('#mobile_' + this.index).hide();
        $("#select_" + this.index + " option:contains('Select Action')").attr('disabled', 'disabled')
        $('.selectpicker').selectpicker('render');
        //  console.log(this.responsecodes);
    }
    getResponseCodeDetails() {
        // console.log("getting all the details");
        let s1 = [];
        for (let i = 0; i < this.responsecodes.length; i++) {
            let s = this.responsecodes[i].split('_');
            // console.log("s1",s[1]);
            let data = {
                'code': $('#responsecodesdiv input#code_' + s[1]).val(),
                'mobile': $('#responsecodesdiv input#mobile_' + s[1]).val(),
                'time': $('#responsecodesdiv input#time_' + s[1]).val(),
                'select': $('#responsecodesdiv select#select_' + s[1]).val()
            }
            s1.push(data);
        }
        // console.log("all data",s1)
    }
    removeResponseCode() {
        // console.log("working");
        // console.log($().attr('id'));
    }
    select_temp(x) {
        this.textTospeechtext = JSON.stringify(x);
        this.textTospeechtext = this.textTospeechtext.substr(1, this.textTospeechtext.length - 2);
    }
    onCheckboxChange(option, event) {
        //console.log(JSON.stringify(option) + 'check');
        if (event.target.checked) {
            this.checkedList.push(option.group_id);
        } else {
            for (var i = 0; i < this.groups_list.length; i++) {
                if (this.checkedList[i] == option.group_id) {
                    this.checkedList.splice(i, 1);
                }
            }
        }
        //  console.log(this.checkedList);
        let data_send = JSON.stringify({
            'groups': this.checkedList
        });
        this.numbers = '';
        if (this.checkedList.length != 0) {
            this._appService.post_method(this._userService.add_group_nos_url, data_send).subscribe(
                data => {
                    // console.log('received ' + JSON.stringify(data["contact-information"]));
                    for (var i = 0; i < data.total_contacts; i++) {
                        this.numbers += data["contact-information"][i].contact_number + '\n';
                    }
                     this.update_count();
                },
                error => {
                    // console.log('post error');
                });
        }

        //  this.update_count();
    }
    //Clone the hidden element and shows it

}



