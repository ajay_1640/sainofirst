import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoicecallComponent } from './voicecall/voicecall.component';


import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes: Routes = [
    {
        "path": "",
        "component": VoicecallComponent,
        "children": [
            {
                "path": "",
                component: VoicecallComponent,
            },
            {
                "path": "send",
                "loadChildren": ".\/sendvoicecall\/sendvoicecall.module#SendvoicecallModule"
            },
            {
                "path": "contacts",
                "loadChildren": ".\/voicecallcontacts\/voicecallcontacts.module#VoicecallcontactsModule"
            },

            {
                "path": "pricing",
                "loadChildren": ".\/voice-pricing\/voice-pricing.module#VoicePricingModule"
            },
            {
                "path": "analytics",
                "loadChildren": ".\/voicereports-analytics\/voicereports-analytics.module#VoicereportsAnalyticsModule"
            },
            {
                "path": "sent-report",
                "loadChildren": ".\/voicereports-sentreport\/voicereports-sentreport.module#VoicereportsSentreportModule"
            },
            {
                "path": "mis-report",
                "loadChildren": ".\/voicereports-mis\/voicereports-mis.module#VoicereportsMisModule"
            },
            {
                "path": "download-report",
                "loadChildren": ".\/voicereports-downloads\/voicereports-downloads.module#VoicereportsDownloadsModule"
            },
            {
                "path": "templates",
                "loadChildren": ".\/voicecalltemplates\/voicecalltemplates.module#VoicecalltemplatesModule"
            },

        ]
    }
];
@NgModule({

    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule

    ], exports: [
        RouterModule,
    ],
    declarations: [VoicecallComponent]
})

export class VoicecallModule { }
