import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhatsapptempcomponentComponent } from './whatsapptempcomponent/whatsapptempcomponent.component';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

const routes: Routes = [
    {
        'path': '',
        'component': WhatsapptempcomponentComponent
    },
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, AmChartsModule, NgxPaginationModule,
        Ng2SearchPipeModule, SweetAlert2Module.forRoot({
            buttonsStyling: false,
            customClass: 'modal-content',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn'
        })
    ], exports: [
        RouterModule,
    ], declarations: [
        WhatsapptempcomponentComponent,
    ],
})
export class WhatsapptempModule { }
