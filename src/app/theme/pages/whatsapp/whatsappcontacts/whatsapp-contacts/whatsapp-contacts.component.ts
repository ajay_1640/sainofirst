import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../../app.service';
import { AlertService } from '../../../../../auth/_services/alert.service';
import { UserService } from '../../../../../auth/_services/user.service';
import * as XLSX from 'xlsx';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';

import 'rxjs/Rx';
import swal from 'sweetalert2';
import { toBase64String } from '@angular/compiler/src/output/source_map';
declare let toastr;
declare let $;
@Component({
    selector: 'app-whatsapp-contacts',
    templateUrl: './whatsapp-contacts.html',
    styles: []
})
export class WhatsappContactsComponent implements OnInit {

    constructor(private _appService: AppService, private _userService: UserService, private _script: ScriptLoaderService, private _alertService: AlertService) { }
    p: number = 1;


    groups_list = [];
    selectedid = "";
    selectedGroup = "";
    name_single; email_single; number_single;
    multiple_nos = '';
    arrayBuffer;
    file_ext;
    file_data;
    new_group_name = "";
    new_group_desc = "";
    new_group_name_edit = "";
    new_group_desc_edit = "";
    contact = false;
    show_loading = false;
    disableAddContact = false;
    ngOnInit() {
        this.show_loading = true;
        // this.disableAddContact=true;
        this.getGroups();
        this._script.loadScripts('app-whatsapp-contacts',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        let s = this;
        $('#m_modal_6').on('shown.bs.modal', function(e) {
            if (s.groups_list.length == 0) {
                s.disableAddContact = true;
                s._appService.get_method(s._userService.groups).subscribe(
                    data => {
                        s.groups_list = data.groups.reverse();
                        s.disableAddContact = false;
                        $('#group_name_select').selectpicker('refresh');
                    },
                    error => {
                        s.disableAddContact = false;
                        $('#group_name_select').selectpicker('refresh');
                    });
            }
            if (s.contact) {
                s.selectedGroup = s.group_selected.group_id;
                //s.selectedid=s.group_selected.group_id;
                setTimeout(() => {

                    $('#group_name_select').selectpicker('refresh');
                    // console.log("hii",s.selectedGroup);
                }, 500)
            }
            else {
                $('#group_name_select').selectpicker('refresh');
                // s.selectedid="";
                s.selectedGroup = "";
            }
            //    console.log(s.contact,s.selectedGroup);

        })
    }

    validateFile(name: String) {
        var ext = name.substring(name.lastIndexOf('.') + 1);
        this.file_ext = ext.toLowerCase();
        if (ext.toLowerCase() == 'csv' || ext.toLowerCase() == 'txt' || ext.toLowerCase() == 'xlsx' || ext.toLowerCase() == 'ods' || ext.toLowerCase() == 'xls') {
            return true;
        }
        else {
            return false;
        }
    }
    groupChanged(target) {
        this.selectedGroup = target.value;
        // console.log(target.value);
    }
    addcontact() {
        $('#m_modal_6').modal('show');
        this.selectedGroup = "";
        this.name_single = "";
        this.email_single = "";
        this.number_single = "";
        this.multiple_nos = "";
    }
    addgroup() {
        $('#m_modal_5').modal('show');
        this.new_group_name = "";
        this.new_group_desc = "";

    }
    readFile(files: FileList, val): void {
        var file: File = files.item(0);

        if (!this.validateFile(file.name)) {
            val.value = "";
            return toastr.warning('Selected file format is not supported.\n Only ods,txt,csv,xlxs,xls are supported currently');
        }
        val.value = "";

        //console.log(this.file_ext);
        var myReader: FileReader = new FileReader();
        myReader.onload = (e) => {
            if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods') {
                this.arrayBuffer = myReader.result;
                var data = new Uint8Array(this.arrayBuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");
                var workbook = XLSX.read(bstr, { type: "binary" });
                var first_sheet_name = workbook.SheetNames[0];
                var worksheet = workbook.Sheets[first_sheet_name];
                // console.log(XLSX.utils.sheet_to_csv(worksheet));
                this.multiple_nos = this.multiple_nos.concat(XLSX.utils.sheet_to_csv(worksheet));
                this.multiple_nos = this.multiple_nos.trim()
                toastr.info("<h6>File Reading Successful<h6>");
                $('#nav-home-tab').removeClass('active');
                $('#nav-profile-tab').removeClass('active');
                $('#nav-contact-tab').removeClass('active');
                $('#nav-profile-tab').addClass('active');
                $('#nav-home').removeClass('show active');
                $('#nav-profile').removeClass('show active');
                $('#nav-contact').removeClass('show active');
                $('#nav-profile').addClass('show active');
            }
            else {
                this.file_data = myReader.result;
                //console.log(this.file_data);
                this.multiple_nos = this.multiple_nos.concat(this.file_data);
                this.multiple_nos = this.multiple_nos.trim()
                toastr.info("<h6>File Reading Successful<h6>");
                $('#nav-home-tab').removeClass('active');
                $('#nav-profile-tab').removeClass('active');
                $('#nav-contact-tab').removeClass('active');
                $('#nav-profile-tab').addClass('active');
                $('#nav-home').removeClass('show active');
                $('#nav-profile').removeClass('show active');
                $('#nav-contact').removeClass('show active');
                $('#nav-profile').addClass('show active');

            }
        }
        if (this.file_ext == 'xlsx' || this.file_ext == 'xls' || this.file_ext == 'ods')
            myReader.readAsArrayBuffer(file);
        else
            myReader.readAsText(file);
    }


    getGroups() {
        this._appService.get_method(this._userService.groups).subscribe(
            data => {
                //  console.log(data);
                this.groups_list = data.groups.reverse();
                // for(let i=0;i<this.groups_list.length;i++){
                //     let s=this.groups_list[i]['group_desc'];
                //     let s1=this.groups_list[i]['group_desc'].match(/{1,30}/g);
                //     this.groups_list[i]['group_desc']=s1.join("\r\n");
                //     console.log(this.groups_list[i]);
                // }              
                this.show_loading = false;
            },
            error => {
                this.disableAddContact = false;
                this.show_loading = false;
            });
    }

    create_new_group() {
        if (this.new_group_name == "") return toastr.warning("<h5>Name is Invalid<h5>");
        // if(this.new_group_desc=="" || !/^[a-zA-Z]+$/.test(this.new_group_desc.replace(/\s/g,'')))return toastr.warning("<h5>Description is Invalid<h5>");
        $('#m_modal_5').modal('hide');
        let data_send = JSON.stringify({
            'group_name': this.new_group_name.trim(),
            'group_desc': this.new_group_desc.trim(),
        });
        this._appService.post_method(this._userService.groups, data_send).subscribe(
            data => {

                if (data.status) {

                    toastr.success("<h5>" + data.message + "<h5>");
                    this.new_group_name = "";
                    this.new_group_desc = "";
                    this.show_loading = true;
                    this.getGroups();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {

                toastr.error("<h5>Server Error " + error + "<h5>");

            });
    }

    add_single_contact() {
        if (this.selectedGroup == "") return toastr.warning("<h4>Group is Not Selected<h4>");

        if (this.name_single == "" || !/^[a-zA-Z]+$/.test(this.name_single.replace(/\s/g, ''))) return toastr.warning("<h5>Invalid Name<h5>")
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email_single)) {
            return toastr.warning("<h5>Invalid Email<h5>");
        }
        if (this.number_single == "" || !/^[0-9]{5,15}$/.test(this.number_single)) return toastr.warning("<h5>Invalid Number<h5>");
        $('#m_modal_6').modal('hide');
        let data_send = JSON.stringify({
            'contact_name': this.name_single,
            'number': this.number_single.toString(),
            'mail_id': this.email_single
        });
        this._appService.post_method(this._userService.groups + this.selectedGroup + '/contacts', data_send).subscribe(
            data => {
                //console.log(data);
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.name_single = "";
                    this.selectedGroup = "";
                    this.number_single = "";
                    this.email_single = "";
                    this.show_loading = true;
                    this.getGroups();
                    if (this.contact) this.show_contacts(this.group_selected);
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {

                toastr.error("<h5>Server Error " + error + "<h5>");
            });


    }

    add_multiple_contacts() {
        if (this.selectedGroup == "") return toastr.warning("<h4>Group is Not Selected<h4>");
        if (this.multiple_nos == "") return toastr.warning("<h5>No Numbers Found<h5>");

        this.multiple_nos = this.multiple_nos.replace(/\n/g, ',');
        let ar = this.multiple_nos.split(',');
        for (let i = 0; i < ar.length; i++) {
            if (!/^[0-9]{5,15}$/.test(ar[i])) {
                return toastr.warning("<h6>Invalid Numbers Found<h6>");
            }
        }
        $('#m_modal_6').modal('hide');
        let data_send = JSON.stringify({
            'numbers': this.multiple_nos,
        });

        this._appService.post_method(this._userService.groups + this.selectedGroup + '/contacts/multiple', data_send).subscribe(
            data => {

                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.selectedGroup = "";
                    this.multiple_nos = "";

                    this.show_loading = true;
                    this.getGroups();
                    if (this.contact) this.show_contacts(this.group_selected);
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {

                toastr.error("<h5>Server Error " + error + "<h5>");
            });
        this.multiple_nos = '';
    }

    delete_group(val) {
        this._appService.delete_method(this._userService.groups + val.group_id).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.show_loading = true;
                    this.getGroups();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {

                toastr.error("<h4>Server Error " + error + "<h4>");
            });
    }

    edit_group() {

        if (this.new_group_name_edit == "") return toastr.warning("<h5>Name is Invalid<h5>");
        // if(this.new_group_desc_edit=="" || !/^[a-zA-Z]+$/.test(this.new_group_desc_edit.replace(/\s/g,'')))return toastr.warning("<h5>Description is Invalid<h5>");
        $('#m_modal_4').modal('hide');
        let data_send = JSON.stringify({
            'group_name': this.new_group_name_edit.trim(),
            'group_desc': this.new_group_desc_edit.trim()
        });
        this._appService.patch_method(this._userService.groups + this.group_selected.group_id, data_send).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    toastr.success("<h5>Successfully Updated<h5>");
                    this.new_group_name_edit = "";
                    this.new_group_desc_edit = "";
                    this.show_loading = true;
                    this.getGroups();
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {

                toastr.error("<h5>Server Error " + error + "<h5>");

            });
    }
    group_selected;
    current_group(val) {
        $('#m_modal_4').modal('show');
        this.new_group_name_edit = val.group_name;
        this.new_group_desc_edit = val.group_desc;
        this.group_selected = val;
    }
    contact_selected;
    current_contact(val) {
        $('#m_modal_3').modal('show');
        this.new_contact_name_edit = val.contact_name;
        this.new_contact_num_edit = val.contact_number;
        this.contact_selected = val;

    }

    group_contacts_list;
    show_contacts(val) {
        this.show_loading = true;

        this.group_selected = val;
        this.contact = true;
        let _url = this._userService.groups + '/' + val.group_id + '/contacts/';

        this._appService.get_method(_url).subscribe(
            data => {
                //  console.log(data);
                this.show_loading = false;
                this.group_contacts_list = data["contact-information"];
            },
            error => {
            });
    }


    delete_contact(val) {
        var delete_url = this._userService.groups + this.group_selected.group_id + '/contacts/' + val.c_id + '/';
        this._appService.delete_method(delete_url).subscribe(
            data => {
                if (data.status) {
                    toastr.success("<h5>" + data.message + "<h5>");

                    this.show_contacts(this.group_selected);
                }

                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }
            },
            error => {
                toastr.error("<h5>" + error + "<h5>");
            });
    }

    back() {
        this.contact = false;
        this.show_loading = true;
        this.getGroups();
    }

    new_contact_name_edit;
    new_contact_num_edit;
    edit_contact() {
        if (this.new_contact_name_edit == "" || !/^[a-zA-Z]+$/.test(this.new_contact_name_edit.trim().replace(/s/g, ''))) return toastr.warning("<h5>Name is Empty<h5>");
        if (this.new_contact_num_edit == "") return toastr.warning("<h5>Number is Empty<h5>")
        if (!/^[0-9]{5,15}$/.test(this.new_contact_num_edit)) return toastr.warning("<h5>Number is Invalid<h5>");
        $('#m_modal_3').modal('hide');
        let data_send = JSON.stringify({
            'contact_name': this.new_contact_name_edit + '',
            'contact_number': this.new_contact_num_edit + '',
            'mail_id': this.contact_selected.mail_id
        });
        this._appService.patch_method(this._userService.groups + this.group_selected.group_id + '/contacts/' + this.contact_selected.c_id, data_send).subscribe(
            data => {
                if (data.status) {
                    this.new_contact_name_edit = "";
                    this.new_contact_num_edit = "";
                    toastr.success("<h5>" + data.message + "<h5>");
                    this.show_contacts(this.group_selected);
                }
                else {
                    toastr.error("<h5>" + data.message + "<h5>");
                }

            },
            error => {
                toastr.error("<h5> Server Error " + error + "<h5>");
            });
    }
    download(val) {
        let group_name = val.group_name;
        let _url = this._userService.groups + '/' + val.group_id + '/contacts/';
        let d = [];
        toastr.info("<h6>Preparing Data to Download<h6>");

        this._appService.get_method(_url).subscribe(
            data => {
                if (data.status) {
                    let da = data["contact-information"];
                    for (let i = 0; i < da.length; i++) {
                        let contact = {
                            'Name': da[i]['contact_name'],
                            'Number': da[i]['contact_number'],
                            'Mail id': da[i]['mail_id']
                        }
                        d.push(contact);
                    }
                    let ws = XLSX.utils.json_to_sheet(d);
                    let wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, group_name);
                    XLSX.writeFile(wb, group_name + ".xlsx");
                }
                else { toastr.error("<h6>" + data.message + "<h6>"); }
            },
            error => {
                toastr.error("<h6>Server Error " + error + "<h6>");
            });




        // console.log(val);

    }


    saveFile(name, type, data) {
        if (data != null && navigator.msSaveBlob)
            return navigator.msSaveBlob(new Blob([data], { type: type }), name);
        var a = $("<a style='display: none;'/>");
        var url = window.URL.createObjectURL(new Blob([data], { type: type }));
        a.attr("href", url);
        a.attr("download", name);
        $("body").append(a);
        a[0].click();
        window.URL.revokeObjectURL(url);
        a.remove();
    }

}
