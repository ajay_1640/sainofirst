
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';
import 'rxjs/Rx';
declare var $, jQuery: any;
declare let toastr;
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
declare var moment;
@Component({
    selector: 'app-whatsanalysis',
    templateUrl: './whatsanalysis.component.html',
    styles: []
})
export class WhatsanalysisComponent implements OnInit {
    private chart1: any;
    private chart2: any;
    private chart3: any;
    NineDayData = [];
    submittedAndSent = [];
    chart1Loader = false;
    chart2Loader = false;
    chart3Loader = false;
    bullet = "\u2022";
    totalSmsSent;
    total;
    calendar_range = "today";
    pending;
    DNDFailed: number = 0;
    Delivered: number = 0;
    Other: number = 0;
    Submitted: number = 0;
    numbers_count: number = 0;
    DNDFailed_percentage: number;
    Delivered_percentage: number;
    Other_percentage: number;
    Submitted_percentage: number;
    show_loading = false;
    show_calendar = false;
    from_date = "";
    to_date = "";
    searchdata = false;
    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService, private AmCharts: AmChartsService) { }
    titles = "Sent SMS Total Analysis";
    ngOnInit() {
        this._script.loadScripts('app-whatsanalysis',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-whatsanalysis',
            ['assets/demo/demo3/base/toastr.js']);
        (<any>$('#sandbox-container .input-daterange')).datepicker({
            todayHighlight: true,
            autoclose: true,
            format: "dd/mm/yyyy",
        });
        $('.input-daterange input').each(function() {
            $(this).datepicker('clearDates');
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        this._script.loadScripts('app-whatsanalysis',
            ['https://cdn.jsdelivr.net/momentjs/latest/moment.min.js']);
        this.chart1Loader = true;
        this.chart2Loader = true;
        this.chart3Loader = true;
        this.show_loading = true;
        // console.log(moment().format('DD/MM/YYYY'))
        this.getLastNineDaysData();
        // this.getSubmittedDeliveredData();
        // this.searchSmsAnalysis(moment().format('DD/MM/YYYY'),moment().format('DD/MM/YYYY'));
        // this.getAllData();
        setTimeout(() => {
            this.LoadScripts();
            this.chart1Loader = false;
            this.chart2Loader = false;
            this.chart3Loader = false;
            this.show_loading = false;
        }, 4000);


    }


    getLastNineDaysData() {
        this.NineDayData = new Array(9);
        this._appService.get_method(this._userService.analytics_report).subscribe(
            data => {
                let first_date = new moment(moment().format("YYYY-MM-DD"));
                let first_date_ = first_date.format("YYYY-MM-DD");
                if (data.result.length == 0) {
                    for (let i = 0; i < this.NineDayData.length; i++) {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'submitted': 0,
                            'delivered': 0

                        };
                        this.NineDayData[8 - i] = d;
                    }
                    return;
                }
                for (let i = 0; i < data.result.length; i++) {
                    let ar = data.result[i]['date'].split('T');
                    data.result[i]['date'] = ar[0];
                    let second_date = new moment(ar[0]);
                    let diff = first_date.diff(second_date, "days");
                    this.NineDayData[8 - diff] = data.result[i];

                }
                for (let i = 0; i < this.NineDayData.length; i++) {
                    if (typeof this.NineDayData[8 - i] == "undefined") {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'delivered': 0,
                            'submitted': 0
                        };
                        this.NineDayData[8 - i] = d;
                    }
                }
            },
            error => {
                // console.log(error);
            }
        );
    }
    LoadScripts() {
        this._script.loadScripts('head', [
            'https://www.amcharts.com/lib/3/amcharts.js',
            'https://www.amcharts.com/lib/3/pie.js',
            'https://www.amcharts.com/lib/3/radar.js',
            'https://www.amcharts.com/lib/3/plugins/animate/animate.min.js',
            'https://www.amcharts.com/lib/3/serial.js',
            'https://www.amcharts.com/lib/3/plugins/export/export.min.js',
            'https://www.amcharts.com/lib/3/themes/light.js'], true).then(() => {
                Helpers.loadStyles('head', 'https://www.amcharts.com/lib/3/plugins/export/export.css');
                this.chart1 = this.AmCharts.makeChart("m_amcharts_4", {
                    "theme": "light",
                    "type": "serial",
                    "hideCredits": true,
                    "dataProvider": this.NineDayData,
                    "valueAxes": [{
                        "stackType": "3d",
                        "unit": "%",
                        "position": "left",
                        "title": "Last 9 Day Data",
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "Delivered: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2004",
                        "type": "column",
                        "valueField": "delivered"
                    }, {
                        "balloonText": "Submitted: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2005",
                        "type": "column",
                        "valueField": "submitted"
                    }],
                    "plotAreaFillAlphas": 0.1,
                    "depth3D": 60,
                    "angle": 30,
                    "categoryField": "date",
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "export": {
                        "enabled": true
                    }
                });
                jQuery('.chart-input').off().on('input change', function() {
                    var property = jQuery(this).data('property');
                    var target = this.chart1;
                    this.chart1.startDuration = 0;

                    if (property == 'topRadius') {
                        target = this.chart1.graphs[0];
                        if (this.value == 0) {
                            this.value = undefined;
                        }
                    }

                    target[property] = this.value;
                    this.chart1.validateNow();
                });

            });
    }
    ngOnDestroy() {
        // this.AmCharts.destroyChart(this.chart1);
        // this.AmCharts.destroyChart(this.chart2);
        // this.AmCharts.destroyChart(this.chart3);
    }

    ninedayData() {
        this.chart1Loader = true;
        this.NineDayData = new Array(9);
        this._appService.get_method(this._userService.analytics_report).subscribe(
            data => {
                let first_date = new moment(moment().format("YYYY-MM-DD"));
                let first_date_ = first_date.format("YYYY-MM-DD");
                if (data.result.length == 0) {
                    for (let i = 0; i < this.NineDayData.length; i++) {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'submitted': 0,
                            'delivered': 0

                        };
                        this.NineDayData[8 - i] = d;
                    }
                    return;
                }
                for (let i = 0; i < data.result.length; i++) {
                    let ar = data.result[i]['date'].split('T');
                    data.result[i]['date'] = ar[0];
                    let second_date = new moment(ar[0]);
                    let diff = first_date.diff(second_date, "days");
                    this.NineDayData[8 - diff] = data.result[i];

                }
                for (let i = 0; i < this.NineDayData.length; i++) {
                    if (typeof this.NineDayData[8 - i] == "undefined") {
                        let d = {
                            'date': moment(first_date_, ["YYYY-MM-DD"]).subtract(i, "days").format("YYYY-MM-DD"),
                            'delivered': 0,
                            'submitted': 0
                        };
                        this.NineDayData[8 - i] = d;
                    }
                }
                this.chart1 = this.AmCharts.makeChart("m_amcharts_4", {
                    "theme": "light",
                    "type": "serial",
                    "hideCredits": true,
                    "dataProvider": this.NineDayData,
                    "valueAxes": [{
                        "stackType": "3d",
                        "unit": "%",
                        "position": "left",
                        "title": "Last 9 Day Data",
                    }],
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "Delivered: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2004",
                        "type": "column",
                        "valueField": "delivered"
                    }, {
                        "balloonText": "Submitted: <b>[[value]]</b>",
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "title": "2005",
                        "type": "column",
                        "valueField": "submitted"
                    }],
                    "plotAreaFillAlphas": 0.1,
                    "depth3D": 60,
                    "angle": 30,
                    "categoryField": "date",
                    "categoryAxis": {
                        "gridPosition": "start"
                    },
                    "export": {
                        "enabled": true
                    }
                });
                jQuery('.chart-input').off().on('input change', function() {
                    var property = jQuery(this).data('property');
                    var target = this.chart1;
                    this.chart1.startDuration = 0;

                    if (property == 'topRadius') {
                        target = this.chart1.graphs[0];
                        if (this.value == 0) {
                            this.value = undefined;
                        }
                    }

                    target[property] = this.value;
                    this.chart1.validateNow();
                });
                this.chart1Loader = false;
                //console.log("refresh");
            },
            error => {
                this.chart1Loader = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
                // console.log(error);
            }
        );

    }

}
