import { Component, OnInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { timezone } from '../../../../pages/default/profile/my-profile/country codes';
import { AppService } from '../../../../../app.service';
import { UserService } from '../../../../../auth/_services/user.service';

declare let $

@Component({
    selector: 'app-sendsms',
    templateUrl: './sendsms.component.html',
    styles: []
})
export class SendsmsComponent implements OnInit {

    constructor(private _script: ScriptLoaderService, private _appService: AppService, private _userService: UserService) { }
    show_to_numbers = true;
    numbers = "";
    message = "";
    template = "";
    searchText = "";
    scrolled = false;
    r_schedule = false;
    checkedList: any[] = [];//Just created a list
    vCard = false;
    template_list = []
    dp = false;
    BulkUploadFile: any = null;
    dpFile: any = null;
    multimedia = [];
    groups_list = [];
    multiupload = [];
    timezoneDropdown = {}
    timezoneobject = []
    ngOnInit() {
        this._script.loadScripts('app-sendsms',
            ['assets/demo/demo3/base/bootstrap-select.js']);
        this._script.loadScripts('app-sendsms',
            ['assets/demo/demo3/base/dropzone.js']);
        this._script.loadScripts('app-sendsms',
            ['assets/demo/demo3/base/bootstrap-datetimepicker.js']);
        this._script.loadScripts('app-sendsms',
            ['assets/demo/demo3/base/bootstrap-timepicker.js']);
        this.getUserTimezone();
        jQuery(function() {
            var datepicker: any = $('input.datepicker');
            if (datepicker.length > 0) {
                datepicker.datepicker({
                    format: "dd/mm/yyyy",
                    startDate: new Date(),
                }).on('changeDate', function(ev) {
                    (<any>$(this)).datepicker('hide');

                })
            }
        });
        this.timezoneDropdown = timezone
        this.timezoneobject = Object.keys(this.timezoneDropdown);
        this.getGroups();
        this.get_templates();
        let s_send = this;
        let Dropzone = $('#m-dropzone-three').dropzone(
            {
                url: "#",
                paramName: "file",
                maxFiles: 1,
                maxFilesize: 100,
                addRemoveLinks: 1,
                acceptedFiles: ".ods,.csv,.xlsx,.xls",
                accept: function(e, o) { "justinbieber.jpg" == e.name ? o("Naha, you don't.") : o() },
                maxfilesexceeded: function(file) { this.removeFile(file); },
                removedfile: function(file) {
                    s_send.BulkUploadFile = null;
                    $(document).find(file.previewElement).remove();
                    //console.log("file removed")
                },
                error: function(file) {
                    if (!file.accepted) this.removeFile(file);
                    else {
                        s_send.BulkUploadFile = file;
                        //   console.log("file stored",s_send.BulkUploadFile);
                    }
                }

            });

        Dropzone.autoDiscover = false;
        this.multimedia = [];

    }
    timezone = "";
    timezone1 = "";
    getUserTimezone() {

        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.timezone = data.profile_details[0]['timezone'];
                this.timezone1 = this.timezone;
                setTimeout(() => {
                    $('.selectpicker').selectpicker('refresh');
                }, 200)
                //    console.log("user timezone",this.timezone);       

            },
            error => {
            }
        );


    }
    select_temp(x) {
        this.message = JSON.stringify(x);
        this.message = this.message.substr(1, this.message.length - 2);
    }

    get_templates() {
        this._appService.get_method(this._userService.template_url).subscribe(
            data => {
                if (!data.status) return;
                this.template_list = data.templates;
                setTimeout(() => {
                    $('#selectpickertemplate').selectpicker('refresh');
                }, 200)
            },
            error => { });
    }
    getGroups() {
        this._appService.get_method(this._userService.get_groups_url).subscribe(
            data => {
                // console.log("groups data",data);
                //if(!data.subscribe)return;
                this.groups_list = data.groups;

            },
            error => { });
    }
    scroll() {
        this.scrolled = !this.scrolled;
        if (this.scrolled) {
            $('#scroll').attr('src', './assets/app/media/img/icons/arrow-up.png');
            this.fileUploaded();

        }
        else {
            $('#mediaattach').css('height', '90px');
            $('#scroll').attr('src', './assets/app/media/img/icons/arrow-down.png');
            $('#scrolldiv').css({ 'margin-top': '6px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });
            // $('#mediaattach').animate( { height: '90px' }, { queue:false, duration:250 }).promise().done(function () {
            //     setTimeout(()=>{

            //     },350)

            // });
            // $('#mediaattach').css('height','90px');
            //  $('#scrolldiv').animate( { height: '90px' }, { queue:false, duration:250 })
        }

    }
    fileUploaded() {
        if (this.multimedia.length == 0) {
            $('#mediaattach').css('height', '120px');
            // $('#scroll').css('margin','0px 0px 0px 0px');
            $('#scrolldiv').css({ 'margin-top': '-4px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });

        }
        else if (this.multimedia.length >= 3) {
            $('#mediaattach').css('height', '165px');
            //  $('#scroll').css('margin','0px');
            $('#scrolldiv').css({ 'margin-top': '42px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });

        }
        else if (this.multimedia.length >= 1) {
            $('#mediaattach').css('height', '135px');
            // $('#scroll').css('margin','0px'); 
            $('#scrolldiv').css({ 'margin-top': '10px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });

        }
    }
    show_numbers() {
        this.show_to_numbers = true;
    }
    rScheduleChanged() {
        $('input.datepicker')[0]['value'] = "";
        $('#m_timepicker_1')[0]['value'] = this.getCurrentTime(new Date());
        this.timezone1 = this.timezone;
        // console.log(this.timezone1);
        setTimeout(() => {
            $('.selectpicker').selectpicker('refresh');
        }, 250)
    }

    getCurrentTime(date) {
        var hours = date.getHours(),
            minutes = date.getMinutes(),
            ampm = hours >= 12 ? 'pm' : 'am';

        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return hours + ':' + minutes + ' ' + ampm;
    }

    show_dropzone() {
        this.numbers = "";
        this.show_to_numbers = false;
    }
    clear_nos() { this.numbers = ""; }
    make_unique() {
        this.numbers = this.numbers.replace(/[\n]/g, '\,');
        this.numbers = this.numbers.replace(/\,/g, '\n');
        let xs = this.numbers.split("\n");
        xs = xs.toString().split("\,");
        //console.log(xs+'after split');
        let unique_array = xs.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        });
        // console.log(unique_array+'new msg');
        let new_msg = unique_array.toString();
        new_msg = new_msg.replace(/\,/g, '\n');
        this.numbers = new_msg;
        // console.log("in make unique",this.numbers);
    }
    removeFile(index) {
        this.multimedia.splice(index, 1);
        if (this.scrolled) {
            this.fileUploaded();
        }
    }
    readDp(target) {
        this.dpFile = target.files.item(0)
        // console.log(target.files);
    }
    dpChanged() {
        this.dpFile = null;
    }
    onCheckboxChange(option, event) {
        //console.log(JSON.stringify(option) + 'check');
        if (event.target.checked) {
            this.checkedList.push(option.group_id);
        } else {
            for (var i = 0; i < this.groups_list.length; i++) {
                if (this.checkedList[i] == option.group_id) {
                    this.checkedList.splice(i, 1);
                }
            }
        }
        //  console.log(this.checkedList);
        let data_send = JSON.stringify({
            'groups': this.checkedList
        });
        this.numbers = '';
        if (this.checkedList.length != 0) {
            this._appService.post_method(this._userService.add_group_nos_url, data_send).subscribe(
                data => {
                    // console.log('received ' + JSON.stringify(data["contact-information"]));
                    for (var i = 0; i < data.total_contacts; i++) {
                        this.numbers += data["contact-information"][i].contact_number + '\n';
                    }
                    // this.update_count();
                },
                error => {
                    // console.log('post error');
                });
        }

        //  this.update_count();
    }
    multimediaFile(target) {
        if (target.files == null || target.files == '') {
            return;
        }
        if (this.multimedia.length == 4) {
            // console.log("Maximum 4 Files Can be uploaded");
            return
        }
        else {
            this.multimedia.push(target.files[0]);
            if (this.scrolled) {
                this.fileUploaded();
            }
            if (!this.scrolled && this.multimedia.length >= 1 && this.multimedia.length <= 2) {
                this.scrolled = true;
                $('#mediaattach').css('height', '135px');
                // $('#scroll').css('margin','0px'); 
                $('#scrolldiv').css({ 'margin-top': '10px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });
                $('#scroll').attr('src', './assets/app/media/img/icons/arrow-up.png');

            }
            if (!this.scrolled && this.multimedia.length >= 3 && this.multimedia.length <= 4) {
                this.scrolled = true;
                $('#mediaattach').css('height', '165px');
                //  $('#scroll').css('margin','0px');
                $('#scrolldiv').css({ 'margin-top': '42px' }, { 'margin-left': '46%' }, { 'margin-right': '45%' });
                $('#scroll').attr('src', './assets/app/media/img/icons/arrow-up.png');

            }

        }
        const file = target.files[0];

        let size = file['size'];
        size = size / 1024;
        size = size / 1024;
        // console.log(size);
        if (size > 1.1) {
            // console.log("File is Larger Than 1MB");
            return;
        }
        else {
            // console.log("file is ok");
        }
        // console.log(target.files[0])
        const uploads = []
        const blob = file.slice(0, 4);
        const filereader = new FileReader()
        filereader.readAsArrayBuffer(blob);
        filereader.onloadend = function(evt) {
            const uint = new Uint8Array(filereader.result);
            let bytes = []
            uint.forEach((byte) => {
                bytes.push(byte.toString(16))
            })
            const hex = bytes.join('').toUpperCase()
            uploads.push({
                filename: file.name,
                filetype: file.type ? file.type : 'Unknown/Extension missing',
                binaryFileType: getMimetype(hex),
                hex: hex
            })

            // console.timeEnd('FileOpen')
        }
        const getMimetype = (signature) => {
            if (/^424D/.test(signature)) {
                return 'image/bmp';
            }
            if (/^494433/.test(signature)) {
                return 'audio/mp3'
            }
            if (/^000{1}[0-9a-zA-Z]{2}/.test(signature)) {
                return 'video/mp4'
            }
            switch (signature) {
                //img
                case '89504E47':
                    return 'image/png'
                case '47494638':
                    return 'image/gif'
                case 'FFD8FFDB':
                case 'FFD8FFE0':
                case 'FFD8FFE1':
                    return 'image/jpeg'
                case '49492A00':
                case '4D4D002A':
                    return 'image/tiff'
                case '38425053':
                    return 'image/psd'
                //pdf
                case '25504446':
                    return 'application/pdf'

                //audio
                case '52494646':
                    return 'audio/wav'


                case '1A45DFA3':
                    return 'video/x-matroska'
                default:
                    return 'Unknown filetype'
            }

        }


    }

}
