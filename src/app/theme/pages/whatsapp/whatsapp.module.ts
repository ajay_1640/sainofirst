import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhatsappComponent } from './whatsapp/whatsapp.component';

import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../layouts/layout.module';
import { FormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes: Routes = [
    {
        "path": "",
        "component": WhatsappComponent,
        "children": [
            {
                "path": "",
                component: WhatsappComponent,
            },
            {
                "path": "send",
                "loadChildren": ".\/send\/send.module#SendModule"
            },
            {
                "path": "contacts",
                "loadChildren": ".\/whatsappcontacts\/whatsappcontacts.module#WhatsappcontactsModule"
            },
            {
                "path": "reports",
                "loadChildren": ".\/whatsappreports\/whatsappreports.module#WhatsappreportsModule"
            },
            {
                "path": "analytics",
                "loadChildren": ".\/analysis\/analysis.module#AnalysisModule"
            },
            {
                "path": "templates",
                "loadChildren": ".\/whatsapptemp\/whatsapptemp.module#WhatsapptempModule"
            },
             {
            "path": "mis",
            "loadChildren": ".\/whatsappmis\/whatsappmis.module#WhatsappmisModule"
             },
             {
              "path": "download",
              "loadChildren": ".\/whatsappdownload\/whatsappdownload.module#WhatsappdownloadModule"
               }

        ]
    } 
];
@NgModule({

    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, NgxPaginationModule,
        Ng2SearchPipeModule

    ], exports: [
        RouterModule,
    ],
    declarations: [WhatsappComponent]
})
export class WhatsappModule { }
