import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";

const routes: Routes = [
    {
        "path": "app",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        //'canActivateChild':[AuthGuard],
        "children": [
            // {
            //     "path": "index",
            //     "loadChildren": ".\/pages\/aside\/index\/index.module#IndexModule",
            //     // "canActivate": [AuthGuard]
            // },
            {
                "path": "Ivr",
                "loadChildren": ".\/pages\/ivr\/ivr.module#IvrModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "profile",
                "loadChildren": ".\/pages\/default\/profile\/profile.module#ProfileModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "bulk-sms",
                "loadChildren": ".\/pages\/default\/bulksms\/bulksms.module#BulkSmsModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "customized-sms",
                "loadChildren": ".\/pages\/default\/customized-sms\/customized-sms.module#customizedSmsModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "contact-list",
                "loadChildren": ".\/pages\/default\/contactlist\/contactlist.module#ContactListModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "manage",
                "loadChildren": ".\/pages\/default\/manage\/manage.module#ManageModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "shortlink",
                "loadChildren": ".\/pages\/default\/pri\/pricing.module#ShortlinkModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "reports",
                "loadChildren": ".\/pages\/default\/reports\/reports.module#ReportsModule",
                "canActivate": [AuthGuard]
            },

            {
                "path": "pricing",
                "loadChildren": ".\/pages\/default\/pricing\/pricing.module#PricingModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "short-code",
                "loadChildren": ".\/pages\/short-code\/short-code.module#ShortcodeModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "missed-call",
                "loadChildren": ".\/pages\/missed-call\/missed-call.module#MissedCallModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "whatsapp",
                "loadChildren": ".\/pages\/whatsapp\/whatsapp.module#WhatsappModule",
                "canActivate": [AuthGuard]
            },
            {
                "path": "voice-call",
                "loadChildren": ".\/pages\/voicecall\/voicecall.module#VoicecallModule",
                "canActivate": [AuthGuard]
            },
            // {
            //     "path": "404",
            //     "loadChildren": ".\/pages\/default\/not-found\/not-found.module#NotFoundModule",
            //     // "canActivate": [AuthGuard]
            // },
            // {
            //     "path": "",
            //     "redirectTo": "index",
            //     "pathMatch": "full",
            //     // "canActivate": [AuthGuard]
            // },
            // {
            //     path: '**',
            //     // redirectTo: "/404",
            //     "loadChildren": "..\/pages\/default\/not-found\/not-found.module#NotFoundModule",

            //     pathMatch: "full",
            //     // "canActivate": [AuthGuard]
            // }

        ]
    },
    {
        path: '**',
        redirectTo: "/404",
        // "loadChildren": "../error/error.module#ErrorModule",

        pathMatch: "full",
        "canActivate": [AuthGuard]
    },
    {
        "path": "404",
        "loadChildren": "..\/error\/error.module#ErrorModule",
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThemeRoutingModule { }