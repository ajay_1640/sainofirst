import { Injectable, Output, EventEmitter } from '@angular/core';
import { AppService } from '../../../app.service';
import { UserService } from '../../../auth/_services/user.service';
import { User } from '../../../auth/_models';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class WalletService {
    onWalletChangedNew: Subject<any> = new Subject();
    onAppsChangedNew: Subject<any> = new Subject();
    onSettingsChanged: Subject<any> = new Subject();
    onProfileDropdown: Subject<any> = new Subject();
    @Output() fire: EventEmitter<any> = new EventEmitter();

    // listen(): Observable<any> {
    //   return this.walletbal.asObservable();
    // }
    walletbal: any;
    showsettings: boolean = false;
    constructor(private _appService: AppService, private _userService: UserService) { }


    walletbalance() {

        this._appService.get_method(this._userService.wallet_url).subscribe(
            data => {
                // console.log("walletbalance",data);
                //this.fire.emit();
                this.walletbal = data.data.balance;
                this.onWalletChangedNew.next(this.walletbal);
                this.fire.emit(this.walletbal);
                return this.fire;
                // this.onWalletChangedNew.next(this.walletbal);
            },
            error => {

                //  console.log("Cant receive any wallet balance") 
            }
        )


    }
    appPermissionsChanged() {
        this._appService.get_method(this._userService.apps).subscribe(
            data => {
                this.onAppsChangedNew.next(data.apps.split(","));
            },
            error => {

            }
        );
    }
    settingsChanged() {
        this.showsettings = true;
        // console.log("in wallet service");
        this.onSettingsChanged.next(true);
    }
    settingsChanged_() {
        this.showsettings = false;
        // console.log("in wallet service");
        this.onSettingsChanged.next(false);
    }
    profiledropdown(path) {
        this.onProfileDropdown.next(path);

    }
}
