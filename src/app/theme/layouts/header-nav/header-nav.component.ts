import { Component, OnInit, ViewEncapsulation, AfterViewInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Helpers } from '../../../helpers';
import { AppService } from '../../../app.service';
import { UserService } from '../../../auth/_services/user.service';
import { WalletService } from './wallet.service';
import { CookieService } from 'ngx-cookie-service';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { ScriptLoaderService } from '../../../_services/script-loader.service';
import { PreviousrouteService } from '../../pages/default/profile/api/previousroute.service';

declare let toastr;
declare let mLayout: any;
declare let moment;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    profile_name = "";
    profile_email = "";
    import_show = false;
    parentlogin = 'false';
    wallet_balance: any;
    userapps = [];
    showWallet = true;
    notifications = [];
    notification_number: number = 0;
    showSettings: boolean = false;
    hidden = true;
    parentid = '0';
    userrole = "";
    constructor(private _appService: AppService, private prevRouteService: PreviousrouteService, private _router: Router, private _script: ScriptLoaderService, private _cookieService: CookieService, private _userService: UserService, private _walletService: WalletService) {
        this._walletService.onWalletChangedNew.subscribe((m: any) => {
            // console.log(m);
            this.wallet_balance = m;
            this.wallet_balance = this.wallet_balance.toFixed(2);

        })
        this._walletService.onAppsChangedNew.subscribe((m: any) => {
            this.userapps = m;
        })
        this.showSettings = false;

    }
    ngOnInit() {
        if (window.location.hostname != 'app.sainofirst.com') {
            this.showWallet = false;
        }
        // this.parentlogin=false;
        this.userrole = this._cookieService.get('userrole');
        if (window.location.hostname != 'app.sainofirst.com') {
            try {
                var link = document.getElementById("logo_");
                link['src'] = '';
            }
            catch (e) { }

        }
        this._appService.post_method(this._userService.domain, JSON.stringify({ 'domain': window.location.hostname })).subscribe(
            data => {
                try {
                    var link = document.getElementById("logo_");
                    link['src'] = this._userService.files + data.result[0]['logo'];
                }
                catch (e) { }
            },

        );
        // console.log("route url",this._router.url);
        this._script.loadScripts('app-header-nav',
            ['assets/demo/demo3/base/toastr.js']);
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "100",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        this.parentlogin = this._cookieService.get('parentlogin');
        this.parentid = this._cookieService.get('parentid');
        // console.log("parent login in header",this.parentlogin,this._cookieService.get('parentid'))
        this._walletService.walletbalance();
        // console.log(this.wallet_balance);

        this.profileDetails();
        this.appDetails();
        this.getNotifications();

    }
    logoutFromUserAccount() {
        this.import_show = true;
        let d = {
            'token': localStorage.getItem('token')
        }
        this._appService.post_method(this._userService.parent_login, JSON.stringify(d)).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    localStorage.setItem('currentUser', JSON.stringify(data['token']));
                    localStorage.setItem('token', data['token']);
                    this._cookieService.set('userrole', data.user_profile['user_role']);
                    this._cookieService.set('userid', data.user_profile['user_id']);
                    this._cookieService.set('country', data.user_profile['country']);

                    // this._cookieService.set('parentlogin','false');
                    // this._cookieService.set('parentid','0');
                    toastr.success("<h5>Routing to Manage Customers<h5>");
                    this._cookieService.set('parentlogin', 'false');
                    this._cookieService.set('parentid', '0');
                    // this._cookieService.set('parentlogin','false');
                    // this._cookieService.set('parentid','0');
                    // console.log("before logout",this._cookieService.get('parentlogin'),this._cookieService.get('parentid'),this._cookieService.get('userrole'),this._cookieService.get('parentlogin'));
                    setTimeout(() => {
                        let s = window.location.pathname.lastIndexOf('/');
                        let path = window.location.pathname.substring(s, window.location.pathname.length);
                        if (path == "/app/profile/manage-customers") {

                            // console.log("after logout",this._cookieService.get('parentlogin'),this._cookieService.get('parentid'),this._cookieService.get('userrole'),this._cookieService.get('parentlogin'));

                            window.location.reload();
                        }

                        else {

                            // console.log("after logout",this._cookieService.get('parentlogin'),this._cookieService.get('parentid'),this._cookieService.get('userrole'),this._cookieService.get('parentlogin'));

                            this._router.navigate(['/app/profile/manage-customers']);
                        }

                    }, 1000);
                }
                else {
                    this.import_show = false;
                    toastr.error("<h5>Error Logging Out<h5>");
                }
            },
            error => {
                this.import_show = false;
                toastr.error("<h5>Server Error " + error + "<h5>");
            }
        );

    }
    notificationsRead() {
        this._appService.post_method(this._userService.notifications, JSON.stringify({ '': '' })).subscribe(
            data => {
                if (data.status) {
                    setTimeout(() => {
                        this.notification_number = 0;
                    }, 200);
                }
            },
            error => { }
        );
    }
    routetoWallet() {
        this._walletService.settingsChanged_();
        this._router.navigate(['/app/profile/wallet']);
        this._walletService.profiledropdown('wallet');
    }
    getNotifications() {
        this._appService.get_method(this._userService.notifications).subscribe(
            data => {
                this.notifications = data.data;
                // console.log(data);
                // console.log(this.notifications);
                let startTime = moment();
                for (let i = 0; i < this.notifications.length; i++) {
                    if (this.notifications[i]['status'] == '0') {
                        this.notification_number++;
                    }
                    let endTime = moment(this.notifications[i]['created_at']);
                    let duration = moment.duration(startTime.diff(endTime));
                    let hours = parseInt(duration.asHours());
                    let minutes = parseInt(duration.asMinutes());
                    if (hours > 0) {
                        if (hours > 24) {
                            let s = hours / 24;
                            this.notifications[i]['time'] = Math.floor(s) + " days";
                        }
                        else {

                            this.notifications[i]['time'] = Math.floor(hours) + " hr";
                        }

                    }
                    else if (minutes > 0) {

                        if (minutes >= 60) {
                            let s = minutes / 60;

                            this.notifications[i]['time'] = Math.floor(s) + " hr";
                        }
                        else {
                            this.notifications[i]['time'] = Math.floor(minutes) + " mins";

                        }
                    }
                }
                //  console.log("notifications ",this.notifications);
            },
            error => {

            }
        );
    }
    profileDetails() {
        this._appService.get_method(this._userService.profile_url).subscribe(
            data => {
                this.profile_name = data.profile_details[0].client_name;
                this.profile_email = data.profile_details[0].email_id;

            },
            error => { }
        )
    }
    appDetails() {
        this._appService.get_method(this._userService.apps).subscribe(
            data => {
                //console.log("app data",data);
                this.userapps = data.apps.split(",");
            },
            error => {
                // console.log(error);
            }
        )
    }
    walletbalance() {
        this._appService.get_method(this._userService.wallet_url).subscribe(
            data => {
                this.wallet_balance = data.data.balance;
                this.wallet_balance = this.wallet_balance.toFixed(2);
            },
            error => {
                // console.log("Cant receive any wallet balance")
            }
        )
    }
    isHidden() {
        this.hidden = !this.hidden;
    }
    ngAfterViewInit() {

        mLayout.initHeader();

    }
    adminClicked() {
        //  console.log("settings pressed");  
        //this._router.navigate(['/app/profile']);
        //  this.prevRouteService.getPreviousUrl();
        this._walletService.settingsChanged();
        let s = this._router.url.lastIndexOf('/');
        let str = this._router.url.substring(s + 1, this._router.url.length);
        //  console.log("admin settings pressed",str);
        if (str == 'manage-customers') {
            this._walletService.profiledropdown('manage-customers');

        }
        else if (str == 'sender-id-approval') {
            this._walletService.profiledropdown('sender-id-approval');

        }
        else if (str == 'mgmtk') {
            this._walletService.profiledropdown('mgmtk');

        }
        else if (str == 'live-status') {
            this._walletService.profiledropdown('live-status');

        }
        else if (str == 'kyc') {
            this._walletService.profiledropdown('kyc');

        }
        else {
            this._router.navigate(['/app/profile/manage-customers']);
            this._walletService.profiledropdown('manage-customers');
            // this._walletService.profiledropdown('profile');
        }

    }

    myprofileClicked() {
        // this.prevRouteService.getPreviousUrl();
        this._walletService.profiledropdown('profile');
        this._walletService.settingsChanged_();

    }
    apiClicked() {
        //    this.prevRouteService.getPreviousUrl();
        this._walletService.profiledropdown('api');
        this._walletService.settingsChanged_();
    }
    walletClicked() {
        // this.prevRouteService.getPreviousUrl();
        this._walletService.profiledropdown('wallet');
        this._walletService.settingsChanged_();

    }
    tranClicked() {
        // this.prevRouteService.getPreviousUrl();
        this._walletService.profiledropdown('transaction-history');
        this._walletService.settingsChanged_();

    }
    alertClicked() {
        // this.prevRouteService.getPreviousUrl();
        this._walletService.profiledropdown('alerts');
        // this._walletService.showsettings=false;
        this._walletService.settingsChanged_();
    }

}