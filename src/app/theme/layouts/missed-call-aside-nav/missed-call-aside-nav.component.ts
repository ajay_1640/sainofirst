import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { UserService } from '../../../auth/_services/user.service';
import { WalletService } from '../header-nav/wallet.service';
import { AppService } from '../../../app.service';

declare let mLayout: any;

@Component({
    selector: 'app-missed-call-aside-nav',
    templateUrl: "./missed-call-aside-nav.component.html",
    encapsulation: ViewEncapsulation.None,
    styles: []
})
export class MissedCallAsideNavComponent implements OnInit, AfterViewInit {

    constructor(private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this._appService.post_method(this._userService.domain, JSON.stringify({ 'domain': window.location.hostname })).subscribe(
            data => {
                //console.log(data);
                try {
                    $('#m_aside_left').css('background-color', data.result[0]['colour']);
                    // var link = document.getElementById("logo_");
                    // link['src'] = this._userService.files+data.result[0]['logo'];
                }
                catch (e) { }
            },

        );
    }

    ngAfterViewInit() {

        mLayout.initAside();

    }
}

