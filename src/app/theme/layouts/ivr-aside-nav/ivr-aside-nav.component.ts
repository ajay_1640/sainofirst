import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { UserService } from '../../../auth/_services/user.service';
import { WalletService } from '../header-nav/wallet.service';
import { AppService } from '../../../app.service';
@Component({
    selector: 'app-ivr-aside-nav',
    templateUrl: './ivr-aside-nav.component.html',
    encapsulation: ViewEncapsulation.None,
    styles: []
})
export class IvrAsideNavComponent implements OnInit {

    constructor(private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this._appService.post_method(this._userService.domain, JSON.stringify({ 'domain': window.location.hostname })).subscribe(
            data => {
                //console.log(data);
                try {
                    $('#m_aside_left').css('background-color', data.result[0]['colour']);
                    // var link = document.getElementById("logo_");
                    // link['src'] = this._userService.files+data.result[0]['logo'];
                }
                catch (e) { }
            },

        );
    }

}
