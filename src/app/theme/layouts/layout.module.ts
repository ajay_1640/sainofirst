import { NgModule } from '@angular/core';
import { DefaultComponent } from '../pages/default/default.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { AsideComponent } from '../pages/aside/aside.component';
import { AsideNavComponent } from './aside-nav/aside-nav.component';
import { FooterComponent } from './footer/footer.component';
import { QuickSidebarComponent } from './quick-sidebar/quick-sidebar.component';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { TooltipsComponent } from './tooltips/tooltips.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HrefPreventDefaultDirective } from '../../_directives/href-prevent-default.directive';
import { UnwrapTagDirective } from '../../_directives/unwrap-tag.directive';
import { ShortCodeAsideNavComponent } from './short-code-aside-nav/short-code-aside-nav.component';
import { MissedCallAsideNavComponent } from './missed-call-aside-nav/missed-call-aside-nav.component';
import { IvrAsideNavComponent } from './ivr-aside-nav/ivr-aside-nav.component';
import { WhatsappNavComponent } from './whatsapp-nav/whatsapp-nav.component';
import { VoicecallnavComponent } from './voicecallnav/voicecallnav.component';


@NgModule({
    declarations: [
        DefaultComponent,
        HeaderNavComponent,
        AsideComponent,
        AsideNavComponent,
        FooterComponent,
        QuickSidebarComponent,
        ScrollTopComponent,
        TooltipsComponent,
        HrefPreventDefaultDirective,
        UnwrapTagDirective,
        ShortCodeAsideNavComponent,
        MissedCallAsideNavComponent,
        IvrAsideNavComponent,
        WhatsappNavComponent,
        VoicecallnavComponent,
    ],
    exports: [
        DefaultComponent,
        HeaderNavComponent,
        VoicecallnavComponent,
        MissedCallAsideNavComponent,
        IvrAsideNavComponent,
        ShortCodeAsideNavComponent,
        WhatsappNavComponent,
        AsideComponent,
        AsideNavComponent,
        FooterComponent,
        QuickSidebarComponent,
        ScrollTopComponent,
        TooltipsComponent,
        HrefPreventDefaultDirective,
    ],
    imports: [
        CommonModule,
        RouterModule,
    ]
})
export class LayoutModule {
}