import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { UserService } from '../../../auth/_services/user.service';
import { WalletService } from '../header-nav/wallet.service';
import { AppService } from '../../../app.service';
declare let mLayout: any;
declare let $;
@Component({
    selector: 'app-voicecallnav',
    templateUrl: './voicecallnav.component.html',
    encapsulation: ViewEncapsulation.None,

})
export class VoicecallnavComponent implements OnInit, AfterViewInit {

    constructor(private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {
        this._appService.post_method(this._userService.domain, JSON.stringify({ 'domain': window.location.hostname })).subscribe(
            data => {
                //console.log(data);
                try {
                    $('#m_aside_left').css('background-color', data.result[0]['colour']);
                    // var link = document.getElementById("logo_");
                    // link['src'] = this._userService.files+data.result[0]['logo'];
                }
                catch (e) { }
            },

        );
    }
    ngAfterViewInit() {

        mLayout.initAside();

    }

}
// import { Helpers } from '../../../helpers';
// import {CookieService} from 'ngx-cookie-service';
// import { UserService } from '../../../auth/_services/user.service';
// import { WalletService } from '../header-nav/wallet.service';
// import { AppService } from '../../../app.service';

// @Component({
//     selector: "app-aside-nav",
//     templateUrl: "./aside-nav.component.html",
// })
// export class AsideNavComponent implements OnInit, AfterViewInit {
//  userid="";
//     constructor(private _cookieService:CookieService,private _userService:UserService,private _appService:AppService) {


//     }
//     ngOnInit() {
//         this.userid=this._cookieService.get('userrole');
//         this._appService.post_method(this._userService.domain,JSON.stringify({'domain':window.location.hostname})).subscribe(
//             data=>{
//                 //console.log(data);
//                 try{
//                     $('#m_aside_left').css('background-color',data.result[0]['colour']);
//                 // var link = document.getElementById("logo_");
//                 // link['src'] = this._userService.files+data.result[0]['logo'];
//                 }
//                 catch(e){}
//          },

//         );
//     }


// }