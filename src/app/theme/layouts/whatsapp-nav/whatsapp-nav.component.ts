import { Component, OnInit ,ViewEncapsulation,AfterViewInit} from '@angular/core';
declare let mLayout: any;
declare let $;
@Component({
    selector: 'app-whatsapp-nav',
    templateUrl: './whatsapp-nav.component.html',
    encapsulation: ViewEncapsulation.None,

})
export class WhatsappNavComponent implements OnInit,AfterViewInit {

    constructor() { }

    ngOnInit() {
    }
    ngAfterViewInit() {

        mLayout.initAside();

    }
}


