import { Component, OnInit } from '@angular/core';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { Observable } from "rxjs/Observable";
import { AppService } from '../app.service';
@Component({
    selector: 'app-ocr',
    templateUrl: './ocr.component.html',
    styles: []
})
export class OcrComponent implements OnInit {

    image;
    imgSrc = 'http://placehold.it/180';
    raw_data;
    data_received = '';
    data_ = '';
    constructor(private _appService: AppService) { }

    ngOnInit() {
    }
    fileToUpload: File = null;
    handleFileInput(files: FileList) {
        this.readThis(files);
    }

    _url = 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyACVUCekk3d4boTxgszeARzf8PcERmm91E';
    readThis(inputValue: any): void {
        var file: File = inputValue.item(0);

        var myReader: FileReader = new FileReader();

        myReader.onloadend = (e) => {
            this.image = myReader.result;
            this.imgSrc = myReader.result;
            //console.log(this.image);
            var re = "jpeg";
            if (this.image.search(re) != -1)
                this.raw_data = this.image.replace('data\:image\/jpeg\;base64\,', "");
            re = "data:image/png;base64,";
            if (this.image.search(re) != -1)
                this.raw_data = this.image.replace('data\:image\/png\;base64\,', "");
            re = "data:image/jpg;base64,";
            if (this.image.search(re) != -1)
                this.raw_data = this.image.replace('data\:image\/jpg\;base64\,', "");

            //this.raw_data=this.image.replace('data\:image\/png\;base64\,',"");
            // console.log(this.raw_data);


            this._appService.post_method(this._url,
                JSON.stringify(
                    {
                        "requests": [
                            {
                                "image": {
                                    "content": this.raw_data
                                },
                                "features": [
                                    {
                                        "type": "TEXT_DETECTION"
                                    }
                                ]
                            }
                        ]
                    })).subscribe(
                data => {
                    //console.log(JSON.stringify(data.responses[0].textAnnotations[0].description));
                    this.data_ = JSON.stringify(data.responses[0].textAnnotations[0].description);
                    this.data_received = this.data_.replace('\\n', '\<br\/\>');
                    while (this.data_ != this.data_received) {
                        this.data_ = this.data_received;
                        this.data_received = this.data_received.replace('\\n', '\<br\/\>');
                    }
                    this.data_received = this.data_received.replace('\\n', '\<br\/\>');
                    // console.log('recvd' + this.data_received);
                },
                error => {
                    // console.log('post error')
                });;

        }
        myReader.readAsDataURL(file);
    }

}
