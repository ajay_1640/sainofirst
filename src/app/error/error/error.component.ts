import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-error',
    templateUrl: './error.html',
    // styleUrls:['./error.css']
    encapsulation: ViewEncapsulation.None,

})
export class ErrorComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
