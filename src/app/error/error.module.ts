import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorComponent } from './error/error.component';

import { CommonModule } from '@angular/common';
const routes: Routes = [
    { path: '', component: ErrorComponent },


];
@NgModule({
    imports: [RouterModule.forChild(routes), CommonModule],
    exports: [RouterModule],
    declarations: [ErrorComponent]

})

export class ErrorModule { }


