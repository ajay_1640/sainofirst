import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/merge';
import "rxjs/add/operator/map";
import { ActivatedRoute, Router } from '@angular/router';


@Injectable()
export class FileuploadService {

    constructor(private http: HttpClient, private _router: Router) { }
    file_patch_method(url: string, data: any) {

        return this.http.patch(url, data).map((res: Response) => JSON.parse(JSON.stringify(res)));
    }
    getImage(imageUrl: string) {
        return this.http.get(JSON.stringify(imageUrl), { observe: 'response', responseType: 'blob' })
            .map((res) => {
                return new Blob([res.body], { type: res.headers.get('Content-Type') });
            })
    }

}
