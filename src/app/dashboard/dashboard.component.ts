import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { AppService } from '../app.service';
import { UserService } from '../auth/_services/user.service';
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./css/demo.css', './css/style.css']
})
export class DashboardComponent implements OnInit {
    apps = [];
    constructor(private _script: ScriptLoaderService, private _router: Router, private _appService: AppService, private _userService: UserService) { }

    ngOnInit() {

        this._script.loadScripts('body', ['assets/vendors/base/vendors.bundle.js', 'assets/demo/demo3/base/scripts.bundle.js'], true)
            .then(result => {
                //Helpers.setLoading(false);
                // optional js to be loaded once
                this._script.loadScripts('head', ['assets/vendors/custom/fullcalendar/fullcalendar.bundle.js'], true);
            });
        this.appDetails();

    }
    appDetails() {
        this._appService.get_method(this._userService.apps).subscribe(
            data => {
                //console.log("app data",data);

                let apppermissions = data.apps.split(",");
                // console.log("app permissions",this.apppermissions)
                if (apppermissions.includes('bulk_sms')) {
                    this.apps.push("bulk_sms");
                }
                if (apppermissions.includes('bulk_voice')) {
                    this.apps.push("bulk_voice");
                }
                if (apppermissions.includes('ivr_toll')) {
                    this.apps.push("ivr_toll");
                }
                if (apppermissions.includes('missed_call')) {
                    this.apps.push("missed_call");
                }
                if (apppermissions.includes('short_code')) {
                    this.apps.push("short_code");
                }
                if (apppermissions.includes('whatsapp')) {
                    this.apps.push("whatsapp");
                }
            },
            error => {
                // console.log(error);
            }
        )
    }


}
