import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Helpers } from "./helpers";
import { AppService } from './app.service';
import { UserService } from './auth/_services/user.service';
import { HeaderNavComponent } from './theme/layouts/header-nav/header-nav.component';

@Component({
    selector: 'body',
    templateUrl: './app.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
    @ViewChild('HeaderNavComponent')
    private headerNavComponent: HeaderNavComponent;
    title = 'app';
    sitename = '';
    globalBodyClass = 'm-page--loading-non-block m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';

    constructor(private _router: Router, private _appService: AppService, private _userService: UserService, private activatedRoute: ActivatedRoute) {

    }

    ngOnInit() {

        this._router.events.subscribe((route) => {
            if (route instanceof NavigationStart) {
                Helpers.setLoading(true);
                Helpers.bodyClass(this.globalBodyClass);
            }
            if (route instanceof NavigationEnd) {
                Helpers.setLoading(false);
            }
        });
        // this._appService.post_method(this._userService.domain,JSON.stringify({'domain':window.location.hostname})).subscribe(
        //     data=>{
        //         try{
        //         var link = document.querySelector("link[rel*='icon']");
        //          link['rel'] = 'shortcut icon';
        //          link['href'] = this._userService.files+data.result[0]['favicon'];
        //          document.getElementsByTagName('head')[0].appendChild(link);
        //          document.title=data.result[0]['title'];
        //         }
        //         catch(e){}
        //    },

        // );
    }
}





