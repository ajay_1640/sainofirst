import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from "@angular/router";
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from "../_services/authentication.service";
import { Observable } from "rxjs/Rx";
import { PreviousrouteService } from '../../theme/pages/default/profile/api/previousroute.service';
// import { ActivatedRoute } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {
    //current_routes=[];
    constructor(private _auth: AuthenticationService, private previousRouteService: PreviousrouteService, private _router: Router, private _route: ActivatedRoute) {


    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        // this.current_routes=[];
        // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        //  console.log("in auth current url");
        // this.current_routes.push(this._router.url);

        // console.log(this.current_routes[0]);
        // console.log(this._auth.checkloginstatus() + 'guard called');
        // //  console.log(this.current_routes[0]);
        // // this.current_routes.push(this._router.url);
        // console.log(state.url);
        let val = this._auth.checkloginstatus();

        if (!val) {
            localStorage.removeItem('token');
            if (this._router.url != '/login') {
                window.location.href = "/login?c_url=" + this._router.url;
            }
            // window.location.href="/login?c_url="+this._router.url;

            //  this._router.navigate(['/login'],{queryParams:{c_url:state.url}});
        }
        // this.previousRouteService.getPreviousUrl();
        // this.current_routes=[];
        return val;
    }
    // canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    //     // this.current_routes=[];
    //     // let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //     //  console.log("in auth current url");
    //     // this.current_routes.push(this._router.url);

    //     // console.log(this.current_routes[0]);
    //     // console.log(this._auth.checkloginstatus() + 'guard called');
    //     // //  console.log(this.current_routes[0]);
    //     // // this.current_routes.push(this._router.url);
    //     // console.log(state.url);
    //     let val = this._auth.checkloginstatus();

    //     if (!val) {
    //         localStorage.removeItem('token');
    //         if (this._router.url == '/login') {
    //             window.location.href = "/login"
    //         }
    //         else {
    //             window.location.href = "/login?c_url=" + this._router.url;
    //         }
    //         // window.location.href="/login?c_url="+this._router.url;

    //         //  this._router.navigate(['/login'],{queryParams:{c_url:state.url}});
    //     }
    //     // this.previousRouteService.getPreviousUrl();
    //     // this.current_routes=[];
    //     return val;
    // }
    hasPermission(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        // console.log("in auth has permission");
        // console.log(route);
        // console.log(state);
        return
    }
}