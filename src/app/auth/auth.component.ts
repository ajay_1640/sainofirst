import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef, ViewEncapsulation, Injectable, Input, } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { AuthenticationService } from './_services/authentication.service';
import { AlertService } from './_services/alert.service';
import { WalletService } from '../theme/layouts/header-nav/wallet.service';

import { UserService } from './_services/user.service';
import { AlertComponent } from './_directives/alert.component';
import { AppService } from '../app.service';
import { CookieService } from 'ngx-cookie-service';

import { Helpers } from '../helpers';
import { parse, format, AsYouType, isValidNumber } from 'libphonenumber-js';
import 'rxjs/add/operator/pairwise';

import { co } from './countries';

declare let $: any;
declare let mUtil: any;
declare let intlTelInput: any;

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './templates/login-1.component.html',
    styleUrls: ['./bootstrap-pincode-input.css'],
    encapsulation: ViewEncapsulation.None,
})

export class AuthComponent implements OnInit {
    apps = "";
    rememberMe = true;
    verifiedUsernamestatus = false;
    verifiedUsername = "";
    disable = false;
    disable_verify = false;
    model: any = {};
    validPhn = false;
    checkNumber = "";
    verifiedNumber = "";
    verificationStatus = false;
    verifiedCountry: any;
    signin_username = "";
    signin_password = "";
    loading = false;
    signup_fullname = "";
    signup_username = "";
    signup_email = "";
    signup_phoneno = "";
    appPermissions = ['bulk_sms', 'bulk_voice', 'ivr_toll', 'missed_call', 'short_code', 'whatsapp']
    signup_otp = "";
    signup_country = "";
    signup_companyname = "";
    signup_password = "";
    signup_rpassword = "";
    signup_agree = true;
    dummySignupDetails = { 'mobile_no': "" };
    signupotp = "";
    forgot_username = "";
    forgot_email = "";
    showOtpField = false;
    loading_signup = false;
    returnUrl: string;
    countrydata: any;
    c_route = "";
    parse: any;
    otp_mble = "";
    otp_email = "";
    showBanner = true;
    co = [];
    otp: any = {
        'mble': '',
        'mail': '',
        'u__id': '',
        'otp': ''
    }
    Verify_otp = "";

    @ViewChild('alertSignin',
        { read: ViewContainerRef }) alertSignin: ViewContainerRef;
    @ViewChild('alertSignup',
        { read: ViewContainerRef }) alertSignup: ViewContainerRef;
    @ViewChild('alertForgotPass',
        { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;
    @ViewChild('alertOtp',
        { read: ViewContainerRef }) alertOtp: ViewContainerRef;
    @ViewChild('alertsignupotp',
        { read: ViewContainerRef }) alertsignupotp: ViewContainerRef;
    constructor(
        private _cookieService: CookieService,
        private _appService: AppService,
        private _router: Router,
        private _script: ScriptLoaderService,
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _authService: AuthenticationService,
        private _alertService: AlertService,
        private cfr: ComponentFactoryResolver,
        private _walletService: WalletService) {

        // this._route.queryParams
        //     .filter(params => params.order)
        //     .subscribe(params => {
        //       console.log("in auth module");
        //       console.log(params); // {order: "popular"}

        //       //this.order = params.order;
        //      // console.log(this.order); // popular
        //     });
        // this._router.events.pairwise().subscribe((e) => {
        //     console.log("in autho moduleo alsdjfas");
        //     this.current_routes.push(e);

        // }
        //     //console.log(this.current_routes[0].)
        //     this.current_route=[];
        this._route.queryParams.subscribe((params: Params) => {
            this.c_route = params.c_url;
        });

        // this._route
        //     .params
        //     .subscribe(params => {
        //         console.log('Regular Params:', params);
        //     });
    }




    ngOnInit() {
        if (window.location.hostname != 'app.sainofirst.com') {
            $('#m_login').css('justify-content', 'center');
            try {
                var link = document.getElementById("logo_");
                link['src'] = '';
            }
            catch (e) { }
            this.showBanner = false;
        }
        this._appService.post_method(this._userService.domain, JSON.stringify({ 'domain': window.location.hostname })).subscribe(
            data => {
                try {
                    var link = document.getElementById("logo_");
                    link['src'] = this._userService.files + data.result[0]['logo'];

                }
                catch (e) { }
            },

        );

        // this._cookieService.set('remember',"true");

        if (this._cookieService.get('remember') == "true") {
            this.rememberMe = true;
            this.signin_username = this._cookieService.get('username');
            this.signin_password = this._cookieService.get('password');
        }
        else {
            this.rememberMe = false;
            this.signin_password = "";
            this.signin_username = "";
        }


        // console.log("lasjdlfjas",this._router.url);
        if (localStorage.getItem('token') != null) {
            // this.appdetails()
            //  console.log("in auth component",this.apps);
            this._cookieService.set('parentlogin', 'false');
            this._cookieService.set('parentid', '0');
            this._authService.appDetails().subscribe(
                data => {
                    let ar = data.apps.split(',');
                    // console.log("app array ",ar.length);
                    if (ar.length == '1') {
                        // let ind=this.appPermissions.indexOf(ar[0]);
                        // bulk_sms','bulk_voice','ivr_toll','missed_call','short_code','whatsapp'

                        try {
                            if (ar.includes('bulk_sms')) {
                                this._router.navigate(['/app/bulk-sms']);
                            }
                            else if (ar.includes('bulk_voice')) {
                                this._router.navigate(['/app/voice-call/send']);

                            }
                            else if (ar.includes('ivr_toll')) {
                                this._router.navigate(['/app/Ivr/dashboard']);

                            }
                            else if (ar.includes('missed_call')) {
                                this._router.navigate(['/app/missed-call/dashboard']);

                            }
                            else if (ar.includes('short_code')) {
                                this._router.navigate(['/app/short-code/dashboard']);

                            }
                            else if (ar.includes('whatsapp')) {
                                this._router.navigate(['/app/whatsapp/send']);

                            }
                            else if (ar[0] == '') {
                                this._walletService.settingsChanged();
                                this._walletService.profiledropdown('manage-customers');
                                this._router.navigate(['/app/profile/manage-customers'])
                            }
                        }
                        catch (e) {
                            localStorage.removeItem('token')
                            this._router.navigate(['/logout']);
                            alert("Unauthorized Access No App Permissions");
                        }
                    }

                    else {
                        this._router.navigate(['/dashboard']);

                    }
                },
                error => {
                    localStorage.removeItem('token')
                    this._router.navigate(['/logout']);


                }
            );


        }


        // let countryData = $("#phn_no").intlTelInput("getSelectedCountryData");
        // console.log("lhsdfoasjdlfjasdf",countryData);

        // this._script.loadScripts('.m-grid.m-grid--hor.m-grid--root.m-page',
        // ['assets/demo/demo3/base/bootstrap-select.js']);


        // $('.selectpicker').selectpicker({
        //     style: 'btn-info',
        //     size: 4

        // });

        // if (this._authService.checkloginstatus())
        // this._router.navigate(['/dashboard']);
        //this._router.navigateByUrl('/dashboard');
        this.model.remember = true;
        this.co = co;
        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/demo3/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                this.handleFormSwitch();
                this.handleSignInFormSubmit();
                this.handleSignUpFormSubmit();
                this.handleForgetPasswordFormSubmit();
            });
        this._script.loadScripts('.m-grid.m-grid--hor.m-grid--root.m-page',
            ['assets/demo/demo3/base/input-mask.js']);
        this._script.loadScripts('.m-grid.m-grid--hor.m-grid--root.m-page',
            ['assets/build/js/intlTelInput.js']);


        $("#phone").intlTelInput({
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: "body",
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            // geoIpLookup: function(callback) {
            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
            //     var countryCode = (resp && resp.country) ? resp.country : "";
            //     callback(countryCode);
            //   });
            // },
            // hiddenInput: "full_number",
            initialCountry: "in",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            separateDialCode: true,
            utilsScript: "assets/build/js/utils.js"
        });
        let auth = this;
        this.countrydata = $('#phone').intlTelInput("getSelectedCountryData");
        // console.log(this.countrydata);
        auth.signup_country = "India";
        $("#phone").on("countrychange", function(e, countryData) {

            auth.signup_otp = "";
            // console.log("Clicked",countryData);
            auth.countrydata = countryData;
            auth.signup_country = countryData.name.split('(')[0];
            auth.phonenoEvent($('#pno')[0]);
        });
        // console.log("model",this.model);

    }
    appdetails() {
        this.apps = "";
        this._authService.appDetails().subscribe(
            data => {

                let ar = data.apps.split(',');
                if (ar.length == 1) {
                    this.apps = ar[0];

                }
                return true;

            },
            error => {
                return false;

            }
        );
    }
    rem(val) {
        //     console.log(this.rememberMe);
        //    console.log(val);
    }
    displayDetails(mobile_no, email_id) {
        this.otp_mble = mobile_no;
        this.otp_email = email_id;
        this.otp_mble = "*******" + this.otp_mble.slice(this.otp_mble.length - 3, this.otp_mble.length);
        let mail = "";
        let s = this.otp_email.substr(this.otp_email.lastIndexOf('@'), this.otp_email.length);
        let s1 = this.otp_email.length - s.length;
        mail = mail + this.otp_email.substr(0, s1 / 2);
        for (let i = 0; i < s1 / 2; i++) mail = mail + "*";
        mail = mail + s;
        this.otp_email = mail;

    }

    signin() {

        this.loading = true;
        this.otp['mble'] = "";
        this.otp['mail'] = "";
        this.otp['u__id'] = "";
        this.Verify_otp = "";
        // console.log('signin called');

        this._authService.login(
            this.signin_username, this.signin_password).subscribe(
            data => {
                // console.log('data' + JSON.stringify(data));
                if (JSON.parse(JSON.stringify(data)).status == true) {
                    if (data.hasOwnProperty('is_two_factor') && data.is_two_factor) {
                        this.loading = false;
                        // console.log(data);
                        this.displayDetails(data.mobile_no, data.email_id);
                        this.otp['mble'] = data.mobile_no;
                        this.otp['mail'] = data.email_id;
                        this.otp['u__id'] = data.userid;
                        this.Verify_otp = "";
                        if (this.rememberMe) {
                            this._cookieService.set('remember', "true");
                            this._cookieService.set('username', this.signin_username);
                            this._cookieService.set('password', this.signin_password);
                        }
                        else {
                            try {
                                this._cookieService.set('remember', "false");
                                this._cookieService.delete('username');
                                this._cookieService.delete('password');
                            }
                            catch (e) { }
                        }
                        this.displayotpForm();
                        this.model = {};

                    }
                    else {
                        localStorage.setItem('currentUser', JSON.stringify(data.token));
                        //  console.log("profile data",data)
                        localStorage.setItem('token', data.token);
                        this._cookieService.set('userrole', data.user_profile.user_role);
                        this._cookieService.set('country', data.user_profile.country);
                        this._cookieService.set('parentlogin', 'false');
                        this._cookieService.set('parentid', '0');
                        // localStorage.setItem('userrole', data.user_profile.user_role);
                        this._cookieService.set('userid', data.user_profile.user_id);
                        if (this.rememberMe) {
                            this._cookieService.set('remember', "true");
                            this._cookieService.set('username', this.signin_username);
                            this._cookieService.set('password', this.signin_password);
                        }
                        else {
                            try {
                                this._cookieService.set('remember', "false");
                                this._cookieService.delete('username');
                                this._cookieService.delete('password');
                            }
                            catch (e) { }
                        }
                        this.model = {};
                        //  localStorage.setItem('userid',data.user_profile.user_id);
                        // console.log("user_role"+localStorage.getItem('userrole'));
                        //     console.log("c_route", this.c_route);

                        if (typeof this.c_route === "undefined") {

                            this._authService.appDetails().subscribe(
                                data => {
                                    let ar = data.apps.split(',');
                                    // console.log("app array ",ar.length);
                                    this.loading = false;

                                    // if(ar.length=='1'){
                                    //      this._router.navigate(['/app/bulk-sms']);
                                    //     }
                                    //     else{
                                    //         this._router.navigate(['/dashboard']);

                                    //     }

                                    if (ar.length == '1') {
                                        // let ind=this.appPermissions.indexOf(ar[0]);
                                        // bulk_sms','bulk_voice','ivr_toll','missed_call','short_code','whatsapp'

                                        try {
                                            if (ar.includes('bulk_sms')) {
                                                this._router.navigate(['/app/bulk-sms']);
                                            }
                                            else if (ar.includes('bulk_voice')) {
                                                this._router.navigate(['/app/voice-call/send']);

                                            }
                                            else if (ar.includes('ivr_toll')) {
                                                this._router.navigate(['/app/Ivr/dashboard']);

                                            }
                                            else if (ar.includes('missed_call')) {
                                                this._router.navigate(['/app/missed-call/dashboard']);

                                            }
                                            else if (ar.includes('short_code')) {
                                                this._router.navigate(['/app/short-code/dashboard']);

                                            }
                                            else if (ar.includes('whatsapp')) {
                                                this._router.navigate(['/app/whatsapp/send']);

                                            }
                                            else if (ar[0] == '') {
                                                this._walletService.settingsChanged();
                                                this._walletService.profiledropdown('manage-customers');
                                                this._router.navigate(['/app/profile/manage-customers'])
                                            }
                                        }
                                        catch (e) {
                                            localStorage.removeItem('token')
                                            this._router.navigate(['/logout']);
                                            alert("Unauthorized Access No App Permissions");
                                        }
                                    }

                                    else {
                                        this._router.navigate(['/dashboard']);

                                    }
                                },
                                error => {
                                    localStorage.removeItem('token')
                                    this._router.navigate(['/logout']);


                                }
                            );


                        }
                        else {
                            this.loading = false;
                            this._router.navigate([this.c_route]);
                        }

                    }


                    // this._route
                    // .queryParams
                    // .subscribe(queryParams => {

                    //     console.log("queryparams",queryParams.current_route);
                    //     // console.log("fdghdfgh",this.state.url)
                    // });

                    //     this._router.navigate([queryParams.current_route]);
                    //     this.current_route.push(queryParams.current_route);


                    // });




                    //   if(this.current_route.length>=1){

                    //     let url=this.current_route[0];
                    //     console.log("in navigation",this.current_route[0]);
                    //     this._router.navigate([url]);
                    // }

                }
                else {
                    this.model = {};
                    this.showAlert('alertSignin');
                    this._alertService.error('Invalid login Details', true);

                    localStorage.removeItem('currentUser');
                    localStorage.removeItem('token');
                    this._router.navigate(['/login']);
                    this.loading = false;
                }
            },
            error => {
                this.showAlert('alertSignin');

                this._alertService.error(error);
                this.loading = false;
            });

    }


    signup() {
        this.loading = true;
        this.signupotp = "";

        if (this.validateSignUp()) {
            //  console.log("working");
            // this.loading_signup=true;
            let s = this.signup_phoneno;

            let d = {
                'user_name': this.signup_username,
                'password': this.signup_password,
                'email': this.signup_email,
                'phone': s.toString(),
                'company_name': this.signup_companyname,
                'country': this.countrydata.name.split('(')[0].trim(),
                'numcode': this.countrydata.dialCode.toString(),
                'iso2': this.countrydata.iso2.toUpperCase(),
                'name': this.signup_fullname
            }
            if (this.verifiedUsernamestatus) {
                this._appService.post_method(this._userService.register, JSON.stringify(d)).subscribe(
                    data => {
                        if (data.status) {
                            this.loading = false;
                            this.dummySignupDetails = data.data;
                            // console.log("dummy details",this.dummySignupDetails);
                            this.displaySignupOtp();
                            // this._alertService.success("OTP has Been Sent To the Number "+data.data.mobile_no);


                            // this._appService.post_method(this._userService.register_otp,JSON.stringify({'number':s})).subscribe(
                            //     data=>{
                            //         if(data.status)
                            //         {   
                            //             this.loading=false;
                            //             this.displaySignupOtp();
                            //             this.showAlert('alertsignupotp'); 
                            //             this._alertService.success("OTP has Been Sent To the Number "+s);

                            //         }
                            //         else{
                            //             this.loading=false;
                            //             this.displaySignInForm();
                            //             this.showAlert('alertSignin');
                            //             this._alertService.error("OTP Sending Failed Login Using Your Credentials");
                            //         }


                            //     },
                            //     error=>{
                            //         this.loading=false;
                            //         this.displaySignInForm();
                            //         this.showAlert('alertSignin');
                            //         this._alertService.error("OTP Sending Failed Login Using Your Credentials");

                            //     }
                            // )
                        }
                        else {
                            this.loading = false;
                            this.showAlert('alertSignup');
                            this._alertService.error("Error Registering User");
                        }
                    }, error => {
                        this.loading = false;

                        this.showAlert('alertSignup');
                        this._alertService.error("Server Error:Error Registering User");
                    }
                );

            }
            else {
                this.loading = false;
                this.showAlert('alertSignup');
                this._alertService.error("User name is Not Available Please Choose Another Username");
                return;

            }
            //  this.loading=false;
            // else{
            //     this.loading=false;
            //     this.showAlert('alertSignup');
            //     this._alertService.error("Username is Not Available Please Choose Another Username");
            // }




        }
        else {
            this.loading = false;

        }















        // signup_email
        // let valid=this.validateSignUp();
        //console.log(valid);
        //    if(this.verifiedUsername!=this.signup_username || !this.verifiedUsernamestatus){
        //     this.showAlert('alertSignup');
        //     this._alertService.error("Username is Not Available Please Choose Another Username");
        //     return;
        // }
        //    if(this.validateSignUp()){
        //     this.loading = true;

        //     let d={
        //         'user_name':this.signup_username,
        //         'password':this.signup_password,
        //         'email':this.signup_email,
        //         'phone':this.verifiedNumber.toString(),
        //         'company_name':this.signup_companyname,
        //         'country':this.countrydata.name.split('(')[0].trim(),
        //         'numcode':this.countrydata.dialCode.toString(),
        //         'iso2':this.countrydata.iso2.toUpperCase(),
        //         'name':this.signup_fullname
        //     }
        //   //  console.log(d);
        //     this._appService.post_method(this._userService.register,JSON.stringify(d)).subscribe(
        //         data=>{
        //            // console.log(data);
        //             if(data.status){
        //                 this.displaySignInForm();
        //                 this.showAlert('alertSignin');
        //                 this._alertService.success(data.message);
        //             }
        //             else{
        //                 this.showAlert('alertSignup');
        //                 this._alertService.error(data.message);
        //             }
        //             this.loading=false;
        //         },
        //         error=>{
        //             this.loading=false;
        //             this.showAlert('alertSignup');
        //             this._alertService.error(error);
        //         }
        //     );

        //    }




        // this._authService.signup(
        //     this.model.fullname, this.model.email, this.model.username, this.model.companyname, this.model.phone, this.model.password).subscribe(
        //     data => {
        //         console.log(JSON.stringify(data));
        //         this._router.navigate([this.returnUrl]);
        //     },
        //     error => {
        //         this.showAlert('alertSignin');
        //         this._alertService.error(error);
        //         this.loading = false;
        //     });
        // this.showAlert('alertSignin');
        // this._alertService.success(
        //     'Thank you. To complete your registration please check your email.',
        //     true);
        // this.loading = false;
        // this.displaySignInForm();
        // this.model = {};
    }
    signupOtpVerify() {

        this.loading = true;
        let data = {}
        data = this.dummySignupDetails;
        data['otp'] = this.signupotp;
        this._appService.post_method(this._userService.signup_otp_url, JSON.stringify(data)).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    this.signupotp = "";
                    this.loading = false;
                    let login = document.getElementById('m_login');
                    mUtil.removeClass(login, 'm-login--signup--otp');
                    mUtil.addClass(login, 'm-login--signin');
                    mUtil.animateClass(login.getElementsByClassName('m-login__signin')[0], 'flipInX animated');
                    this.showAlert('alertSignin');
                    this._alertService.success(data.message);
                }
                else {
                    this.loading = false;
                    this.showAlert('alertsignupotp');
                    this._alertService.error(data.message);
                }
            },
            error => {
                this.loading = false;
                this.showAlert('alertsignupotp');
                this._alertService.error(error);
            }

        );
    }
    signupresendOtp() {
        this.loading = true;
        let data = this.dummySignupDetails;
        this._appService.post_method(this._userService.signup_resendotp_url, JSON.stringify(data)).subscribe(
            data => {

                if (data.status) {
                    this.loading = false;
                    this.showAlert('alertsignupotp');
                    this._alertService.success(data.message);
                }
                else {
                    this.loading = false;
                    this.showAlert('alertsignupotp');
                    this._alertService.error(data.message);
                }
            },
            error => {
                this.loading = false;
                this.showAlert('alertsignupotp');
                this._alertService.error(error);
            }
        );
    }
    forgotPass() {
        // console.log('forgotPass in component ');
        // console.log(this.forgot_email);
        // console.log(this.forgot_username);
        this.loading = true;
        this._authService.forgotPassword(this.forgot_username, this.forgot_email).subscribe(
            data => {
                // console.log(data);
                this.showAlert('alertSignin');
                if (data.status) {
                    this._alertService.success(
                        'Cool! Password recovery instruction has been sent to your email.',
                        true);
                }
                else {
                    this._alertService.success(
                        'User with this Email-Id or UserName does not exist',
                        true);
                }
                this.loading = false;
                this.displaySignInForm();
                this.forgot_email = "";
                this.forgot_username = "";
            },
            error => {
                this.showAlert('alertForgotPass');
                this._alertService.error('error');
                this.loading = false;
            });
    }
    resendOtp() {
        this.loading = true;
        // console.log(this.otp['mble']);
        // console.log(this.otp['mail']);
        // console.log(this.Verify_otp);
        let data = {
            'mobile_no': this.otp['mble'],
            'email_id': this.otp['mail'],
            'user_id': this.otp['u__id']
        }

        this._authService.resend_Otp(JSON.stringify(data)).subscribe(
            data => {
                this.showAlert('alertOtp')
                if (data.is_two_factor) {

                    this._alertService.success("OTP Sent Successfully.", true);
                }
                else {
                    this._alertService.error("OTP Sending Failed", true);
                }
                this.loading = false;
            },
            error => {
                this.showAlert('alertOtp');
                this._alertService.error("OTP Sending Failed", true);
                this.loading = false;
            }
        );

    }
    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }

    handleSignInFormSubmit() {
        $('#m_login_signin_submit').click((e) => {
            let form = $(e.target).closest('form');
            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                    },
                },
            });
            if (!form.valid()) {
                e.preventDefault();
                return;
            }
        });
    }
    displaySignupOtp() {
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--signup');
        mUtil.addClass(login, 'm-login--signup--otp');
        mUtil.animateClass(login.getElementsByClassName('m-login__signup-otp')[0], 'flipInX animated');

    }
    signupOtpCancel() {
        this.signup_companyname = "";
        this.signup_country = "";
        this.signup_email = "";
        this.signup_fullname = "";
        this.signup_password = "";
        this.signup_username = "";
        this.signup_phoneno = "";
        this.showAlert('alertSignup');
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--signup--otp');
        mUtil.addClass(login, 'm-login--signup');
        mUtil.animateClass(login.getElementsByClassName('m-login__signup')[0], 'flipInX animated');
    }
    displaySignUpForm() {
        this.showAlert('alertSignup');
        this.signup_companyname = "";
        this.signup_country = "India";
        this.signup_email = "";
        this.signup_fullname = "";
        this.signup_otp = "";
        this.signup_password = "";
        this.signup_phoneno = "";
        this.signup_rpassword = "";
        this.signup_username = "";
        this.validPhn = false;
        this.showOtpField = false;
        this.verifiedNumber = "";
        this.verificationStatus = false;
        $('.selectpicker').selectpicker('refresh');
        //  console.log("in auth component ts");
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--forget-password');
        mUtil.removeClass(login, 'm-login--signin');

        mUtil.addClass(login, 'm-login--signup');
        mUtil.animateClass(login.getElementsByClassName('m-login__signup')[0], 'flipInX animated');
        this._script.loadScripts('.m-grid.m-grid--hor.m-grid--root.m-page',
            ['assets/demo/demo3/base/intlTelInput.js']);

    }
    displayotpForm() {
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--forget-password');
        mUtil.removeClass(login, 'm-login--signin');

        mUtil.addClass(login, 'm-login--otp');
        mUtil.animateClass(login.getElementsByClassName('m-login__forget-otp')[0], 'flipInX animated');
        // $('#pincode-input1').pincodeInput({inputs:4});
    }

    displaySignInForm() {
        this.signin_password = "";
        this.signin_username = "";
        this.rememberMe = false;
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--forget-password');
        mUtil.removeClass(login, 'm-login--signup');
        try {
            $('form').data('validator').resetForm();
        } catch (e) {
        }

        mUtil.addClass(login, 'm-login--signin');
        mUtil.animateClass(login.getElementsByClassName('m-login__signin')[0], 'flipInX animated');
    }

    displayForgetPasswordForm() {
        this.forgot_username = "";
        this.forgot_email = "";
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--signin');
        mUtil.removeClass(login, 'm-login--signup');

        mUtil.addClass(login, 'm-login--forget-password');
        mUtil.animateClass(login.getElementsByClassName('m-login__forget-password')[0], 'flipInX animated');
    }
    otpCancel() {

        this.signin_username = "";
        this.signin_password = "";
        this.rememberMe = false;
        let login = document.getElementById('m_login');
        mUtil.removeClass(login, 'm-login--otp');
        // try {
        //     $('form').data('validator').resetForm();
        // } catch (e) {
        // }

        mUtil.addClass(login, 'm-login--signin');
        mUtil.animateClass(login.getElementsByClassName('m-login__signin')[0], 'flipInX animated');
    }

    submitOtp() {
        this.showAlert("alertOtp");
        this.loading = true;
        // console.log(this.Verify_otp);
        let v = /^[0-9]{4}$/;
        if (this.Verify_otp == "" || !v.test(this.Verify_otp)) {

            setTimeout(() => {
                this.showAlert('alertOtp');
                this._alertService.error("Invalid Otp");
                this.Verify_otp = "";
                this.loading = false
            }, 2000);
            return;
        }
        let data = {
            'otp': this.Verify_otp,
            'user_id': this.otp['u__id'],
            'mobile_no': this.otp['mble']
        }

        this._authService.verifyOtp(data).subscribe(
            data => {
                // console.log(data);
                if (data.status) {
                    this.loading = false;
                    localStorage.setItem('currentUser', JSON.stringify(data.response.token));
                    localStorage.setItem('token', data.response.token);
                    this._cookieService.set('userrole', data.response.user_profile.user_role);
                    this._cookieService.set('userid', data.response.user_profile.user_id);
                    this._cookieService.set('country', data.response.user_profile.country);
                    this._cookieService.set('parentlogin', 'false');
                    this._cookieService.set('parentid', '0');
                    // console.log("c_route", this.c_route);

                    if (typeof this.c_route === "undefined") {
                        // this.appdetails();
                        //         if(this.apps!=""){
                        //            // console.log("in auth component",this.apps);
                        //             this._router.navigate(['/app/bulk-sms']);
                        //    }
                        //         this._router.navigate(['/dashboard']);
                        this._authService.appDetails().subscribe(
                            data => {
                                let ar = data.apps.split(',');
                                // console.log("app array ",ar.length);
                                // if(ar.length=='1'){
                                //      this._router.navigate(['/app/bulk-sms']);
                                //     }
                                //     else{
                                //         this._router.navigate(['/dashboard']);

                                //     }
                                if (ar.length == '1') {
                                    // let ind=this.appPermissions.indexOf(ar[0]);
                                    // bulk_sms','bulk_voice','ivr_toll','missed_call','short_code','whatsapp'

                                    try {
                                        if (ar.includes('bulk_sms')) {
                                            this._router.navigate(['/app/bulk-sms']);
                                        }
                                        else if (ar.includes('bulk_voice')) {
                                            this._router.navigate(['/app/voice-call/send']);

                                        }
                                        else if (ar.includes('ivr_toll')) {
                                            this._router.navigate(['/app/Ivr/dashboard']);

                                        }
                                        else if (ar.includes('missed_call')) {
                                            this._router.navigate(['/app/missed-call/dashboard']);

                                        }
                                        else if (ar.includes('short_code')) {
                                            this._router.navigate(['/app/short-code/dashboard']);

                                        }
                                        else if (ar.includes('whatsapp')) {
                                            this._router.navigate(['/app/whatsapp/send']);

                                        }
                                        else if (ar[0] == '') {
                                            this._walletService.settingsChanged();
                                            this._walletService.profiledropdown('manage-customers');
                                            this._router.navigate(['/app/profile/manage-customers'])
                                        }
                                    }
                                    catch (e) {
                                        localStorage.removeItem('token')
                                        this._router.navigate(['/logout']);
                                        alert("Unauthorized Access No App Permissions");
                                    }
                                }

                                else {
                                    this._router.navigate(['/dashboard']);

                                }
                            },

                            error => {
                                localStorage.removeItem('token')
                                this._router.navigate(['/logout']);
                            }
                        );


                    }
                    else {
                        let url = this.c_route;
                        this._router.navigate([url]);
                    }
                }
                else {

                    //  setTimeout(()=>{
                    this.showAlert('alertOtp');
                    this._alertService.error("OTP Verification Failed Invalid OTP");
                    this.Verify_otp = "";
                    this.loading = false
                    //},2000);
                }
            },
            error => {
                this.showAlert('alertOtp');
                this._alertService.error("OTP Verification Failed Kindly Login Again");
                this.Verify_otp = "";
                //  console.log(data);
            }
        );




    }
    phonenoEvent(val) {
        // console.log(this.countrydata);
        this.validPhn = false;
        this.showOtpField = false;


        let v = /^[0-9]{5,15}$/
        // let country_info = $("#phone").intlTelInput("getSelectedCountryData")[0]['countrydata'];
        // console.log("country info",country_info);
        try {
            this.parse = (parse(this.signup_phoneno.toString(), this.countrydata.iso2.toUpperCase()));

        }
        catch{
            this.parse = (parse("0", this.countrydata.iso2.toUpperCase()));

        }

        if (!Object.keys(this.parse).length || !v.test(this.signup_phoneno.toString())) {
            //  val.style.width="100%";
            //  console.log("wrong number");
            this.signup_otp = "";
        }
        else {
            if (isValidNumber(this.parse)) {
                // console.log("valid phone no",this.signup_phoneno); 

                this.validPhn = true;
                this.signup_otp = "";
                //  val.style.width="69%";
                if (this.verifiedNumber != "" && this.verifiedNumber == this.signup_phoneno) {
                    this.showOtpField = true;
                }
                // console.log("correct number");
            }

        }


    }
    sendOtp() {
        // this.disable=true;
        // this.checkNumber=this.signup_phoneno;
        this.verifiedNumber = this.signup_phoneno;
        this.verificationStatus = false;
        this._appService.post_method(this._userService.register_otp, JSON.stringify({ 'number': this.signup_phoneno })).subscribe(
            data => {
                //this.showAlert('alertSignup');

                if (data.status) {

                    //this.signup_otp="";
                    //this.showOtpField=true;

                    //this._alertService.success("OTP has Been sent to the Phone no "+this.signup_phoneno+"");

                }
                else {
                    this.verifiedNumber = "";
                    //this._alertService.error("Error Sending OTP to Please Try with another Valid Number "+this.signup_phoneno+"");

                }
                this.disable = false;
            },
            error => {
                this.verifiedNumber = "";
                //this.showAlert('alertSignup');
                this.disable = false;
                //this._alertService.error("Error Sending OTP to Please Try with another Valid Number "+this.signup_phoneno+"");


            }
        )
    }
    verifyOtp() {
        this.disable_verify = true;

        if (this.signup_otp == "" || !/^[0-9]{4}$/.test(this.signup_otp)) {
            this.showAlert('alertSignup');
            this.disable_verify = false;
            this._alertService.error("Invalid OTP Enter 4 Digit Number");
            return
        }

        this._appService.post_method(this._userService.verify_register_otp, JSON.stringify({ 'number': this.verifiedNumber, 'otp': this.signup_otp })).subscribe(
            data => {

                if (data.status) {
                    this.showAlert('alertSignup');
                    this.verificationStatus = true;
                    this.showAlert('alertSignup');
                    this._alertService.success("OTP verification Successful For Number " + this.verifiedNumber);

                }
                else {
                    this.showAlert('alertSignup');
                    this._alertService.error("OTP Verification Failed For Number " + this.verifiedNumber);
                }
                this.disable_verify = false;
            },
            error => {
                this.disable_verify = false;
                this.showAlert('alertSignup');
                this._alertService.error("OTP verification Failed");
            }
        )
    }
    handleFormSwitch() {
        try {
            document.getElementById('m_login_forget_password').addEventListener('click', (e) => {
                e.preventDefault();
                this.displayForgetPasswordForm();
            });

            document.getElementById('m_login_forget_password_cancel').addEventListener('click', (e) => {
                this.model.username = "";
                this.model.email = "";
                e.preventDefault();
                this.displaySignInForm();
            });

            document.getElementById('m_login_signup').addEventListener('click', (e) => {
                e.preventDefault();
                this.displaySignUpForm();
            });

            document.getElementById('m_login_signup_cancel').addEventListener('click', (e) => {
                e.preventDefault();
                this.displaySignInForm();
            });
            // document.getElementById('m_login_otp_cancel ').addEventListener('click', (e) => {
            //     e.preventDefault();
            //     console.log("in handleform switch");
            //     this.otpCancel();
            // });
        }

        catch (e) {

        }
    }

    handleSignUpFormSubmit() {
        document.getElementById('m_login_signup_submit').addEventListener('click', (e) => {
            let btn = $(e.target);
            let form = $(e.target).closest('form');
            form.validate({
                rules: {
                    fullname: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                    },
                    password: {
                        required: true,
                    },
                    rpassword: {
                        required: true,
                    },
                    agree: {
                        required: true,
                    },
                },
            });
            if (!form.valid()) {
                e.preventDefault();
                return;
            }
        });
    }

    handleForgetPasswordFormSubmit() {
        document.getElementById('m_login_forget_password_submit').addEventListener('click', (e) => {
            let btn = $(e.target);
            let form = $(e.target).closest('form');
            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                    },
                },
            });
            if (!form.valid()) {
                e.preventDefault();
                return;
            }
        });
    }
    validatefullname() {

    }
    validateSignUp() {
        this.showAlert('alertSignup');

        // if(this.signup_password!=this.signup_rpassword){this._alertService.error("Passwords Doesn't Match"); return false;}
        if (!this.validPhn) { this._alertService.error("Enter Valid Phone No without Country Code"); return false; }
        this.verifiedNumber = this.signup_phoneno;
        // if(this.verifiedNumber=="") {this._alertService.error("Mobile Number Verification is required To proceed");return false;}
        // if(!this.verificationStatus){this._alertService.error("Verify OTP");return false;}
        //  if(this.verifiedNumber!=this.signup_phoneno) {this._alertService.error("Mobile Number Changed Verification is required To proceed");return false;}
        return true;



    }
    checkUsername() {
        this.showAlert('alertSignup');
        this._alertService.info("Checking User Name Availability");

        this.verifiedUsernamestatus = false;
        let username = this.signup_username;
        this._appService.post_method(this._userService.check_username, JSON.stringify({ 'user_name': this.signup_username })).subscribe(
            data => {
                //  console.log(data);
                if (data.status) {
                    this.showAlert('alertSignup');

                    this.verifiedUsername = username;
                    this.verifiedUsernamestatus = true;
                    //  console.log(this.verifiedUsername,username,this.verifiedUsernamestatus);
                    this._alertService.success("User name " + username + " is Available");

                    return true;
                }
                else {

                    this.showAlert('alertSignup');
                    this._alertService.error("User name " + username + " is Not Available Please Choose Another Username");
                    return false;
                }

            },
            error => {

                this.showAlert('alertSignup');
                this._alertService.error("Error Checking UserName Availability");
                return false;
            }
        );
    }


}