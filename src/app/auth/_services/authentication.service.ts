import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/merge';
import "rxjs/add/operator/map";
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from './alert.service';
import { AppService } from '../../app.service';
import { UserService } from '../_services/user.service';

@Injectable()
export class AuthenticationService {

    constructor(private _router: Router, private _appService: AppService, private _userService: UserService) {
    }


    login(user_name: string, password: string) {
        //debugger
        let data: string = JSON.stringify({ 'user_name': user_name, 'password': password });
        /*
        this._appService.get_method('http://35.238.188.102/saino-first/api/auth/check').subscribe(
            data => {
                console.log('printing get data '+ JSON.stringify(data));
            }
            error => {}
            );
            */
        return this._appService.post_method(this._userService.signin_url, data)
    }
    appDetails() {
        return this._appService.get_method(this._userService.apps);

    }
    resend_Otp(data) {
        return this._appService.post_method(this._userService.resend_otp, data);

    }
    checkloginstatus() {
        console.log(JSON.stringify(localStorage, null, 2));
        let currentUser = localStorage.getItem('currentUser');
        if (currentUser) return true;
        else return false;
    }

    signup(fullname: string, email: string, username: string, company_name: string, phone_num: number, password: string) {
        let data1: string = JSON.stringify({ 'fullname': fullname, 'email': email, 'username': username, 'company_name': company_name, 'phone_num': phone_num, 'password': password });
        // console.log("signup called" + data1);
        return this._appService.post_method(this._userService.signup_url, data1);
    }
    verifyOtp(data: any) {
        return this._appService.post_method(this._userService.otp_two_way, JSON.stringify(data));
    }

    forgotPassword(username: string, email: string) {
        let data: string = JSON.stringify({ 'email': email, 'user_name': username });
        // console.log("forgot password called" + data);
        return this._appService.post_method(this._userService.forgotPass_url, data);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
    }
}