import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import { User } from '../_models/index';

@Injectable()
export class UserService {

    // base_url='http://35.238.188.102/saino-first';
    // server_log= 'http://35.238.188.102/status.html?password=saniofrst@3579';
    // base_url='http://159.69.120.60/saino-first';
    // server_log= 'http://159.69.120.60:12003/status.html?password=saniofrst@3579';
    
    base_url = 'http://159.69.120.60/saino-first';
    server_log = 'http://159.69.120.60:12003/status.html?password=saniofrst@3579';
    missedCallnewNumbers=this.base_url+"/api/missed-call/newNumbers";
    missedCallSubscribedNumbers=this.base_url+"/api/missed-call/"
    missedCallUnitPrices=this.base_url+"/api/missed-call/unitPrices";
    missedCallNumberSubscribe=this.base_url+"/api/missed-call/subscribe";
    missedCallReports=this.base_url+"/api/missed-call/reports/"
    missedCallUrlForward=this.base_url+"/api/missed-call/";
    groups = this.base_url + "/api/groups/";
    change_pwd_url = this.base_url + '/api/user/profile/update-pwd';
    login_history_url = this.base_url + '/api/user/profile/login-history'
    twofa_url = this.base_url + '/api/user/two-factor';
    shortLink = this.base_url + '/api/short-url/'
    profile_url = this.base_url + '/api/user/profile';
    wallet_url = this.base_url + '/api/user/wallet';
    domain = this.base_url + '/api/auth/domain';
    signin_url = this.base_url + '/api/auth/login';
    manage_user_kyc = this.base_url + '/api/manage-users/kyc';
    manage_user_kyc_profile = this.base_url + '/api/manage-users/';
    parent_login = this.base_url + '/api/auth/parent-login';
    signup_url = this.base_url + '/api/auth/signup';
    signup_otp_url = this.base_url + '/api/auth/signup-otp';
    email_otp = this.base_url + '/api/user/profile/email-otp';
    verify_email_otp = this.base_url + '/api/user/profile/verify-email';
    signup_resendotp_url = this.base_url + '/api/auth/signup-resend-otp';
    check_username = this.base_url + "/api/auth/check-username";
    schedule_url = this.base_url + '/api/reports/schedule';
    schedule_count_url = this.base_url + '/api/reports/schedule-count';
    downlaod_url = this.base_url + '/api/reports/download';
    alert_settings = this.base_url + '/api/user/alert-settings';
    forgotPass_url = this.base_url + '/api/auth/forget-password';
    notifications = this.base_url + '/api/user/notifications';
    get_bulk_sms_url = this.base_url + '/dashboard/get-bulk-sms-details';//changed
    get_group_details = this.base_url + '/dashboard/get-group-details';//changed
    get_groups_url = this.base_url + '/api/groups';
    get_routes_url = this.base_url + '/api/dashboard/routes';
    get_senderid_url = this.base_url + '/api/dashboard/sender-id';
    msg_credits_url = this.base_url + '/api/dashboard/sms-balance';
    add_group_nos_url = this.base_url + '/api/groups/multiple';
    send_sms_url = this.base_url + '/api/dashboard/send-sms';
    send_customize_sms_url = this.base_url + '/api/dashboard/send-customize-sms';
    preview_data_url = this.base_url + '/api/dashboard/preview-data';
    template_url = this.base_url + '/api/templates';
    manage_user_add_account = this.base_url + '/api/manage-users/users';
    manage_user_currency = this.base_url + '/api/manage-users/currency';
    routes_url = this.base_url + '/api/admin/routes';
    user_routes_url = this.base_url + '/api/manage-users/routes';
    admin_gateway_names_url = this.base_url + '/api/admin/gateways-names';
    admin_gateway_url = this.base_url + '/api/admin/gateways';
    admin_routes_url = this.base_url + '/api/admin/routes';
    admin_sender_id_url = this.base_url + '/api/admin/sender-id';
    admin_users_list = this.base_url + '/api/admin/users';
    url_sent_report: string = this.base_url + '/api/reports/sent';
    url_sent_count = this.base_url + '/api/reports/sent-count'
    url_dlr_sent_report: string = this.base_url + '/api/reports/sent/'
    mis_url = this.base_url + '/api/reports/mis';
    admin_kannel = this.base_url + '/api/admin/kannel/'; 
    senderid_url = this.base_url + '/api/sender/';
    permission_manage_users_routes = this.base_url + '/api/manage-users/users/';
    permission_manage_users_permissions = this.base_url + '/api/manage-users/users/'
    admin_block_content = this.base_url + '/api/admin/block-content'
    profile_live_status = this.base_url + '/api/reports/live-status';
    profile_transaction_history = this.base_url + '/api/user/wallet-history';
    admin_err_codes = this.base_url + '/api/admin/err-codes';
    sms_traffic_kannel_stop = this.base_url + '/api/admin/sms-traffic/kannel-stop';
    sms_traffic_kannel_start = this.base_url + '/api/admin/sms-traffic/kannel-start';
    sms_traffic_clear_sent = this.base_url + '/api/admin/sms-traffic/clear-sent';
    sms_traffic_clear_dlr = this.base_url + '/api/admin/sms-traffic/clear-dlr';
    sms_traffic_clear_store = this.base_url + '/api/admin/sms-traffic/clear-store';
    admin_block_content_routes = this.base_url + '/api/admin/block-content-routes';
    admin_block_content_exclude_users = this.base_url + '/api/admin/block-content-exclude-users';
    admin_block_numbers_routes = this.base_url + '/api/admin/block-numbers-routes';
    admin_block_numbers_exclude_users = this.base_url + '/api/admin/block-numbers-exclude-users';
    apps = this.base_url + "/api/user/apps"
    admin_kyc = this.base_url + '/api/admin/kyc';
    campaign_count = this.base_url + '/api/reports/campaign-count/';
    register_otp = this.base_url + '/api/auth/register-otp';
    verify_register_otp = this.base_url + '/api/auth/register-verify-otp';
    profile_kyc = this.base_url + '/api/user/profile/kyc';
    sending_report = this.base_url + '/api/reports/sending-report'
    kyc_status = this.base_url + "/api/user/kyc-status";
    register = this.base_url + "/api/auth/register";
    //sms traffic
    sent_update = this.base_url + '/api/reports/sent-update/';
    sent_repush = this.base_url + '/api/reports/sent-repush/';
    block_list_url = this.base_url + '/api/groups/block_numbers/';
    admin_block_numbers = this.base_url + '/api/admin/block-numbers'
    // user_routes_url=this.base_url+'/api/manage-users/users';
    branding = this.base_url + '/api/admin/branding/';
    branding_manageusers = this.base_url + '/api/manage-users/branding/';
    analytics_report = this.base_url + '/api/reports/analytics';
    analytics_lastmsg_report = this.base_url + '/api/reports/analytics-lastmsg';
    analytics_total_report = this.base_url + '/api/reports/analytics-total';
    otp_two_way = this.base_url + '/api/auth/login/otp-two-way';
    paymentUrl = this.base_url + '/api/user/create-payment-request';
    manage_users_senderid = this.base_url + '/api/manage-users/sender-ids/';

    files = this.base_url;
    // 'http://35.238.188.102/saino-first'
    wallet_stats = this.base_url + '/api/admin/wallet-stats';
    update_password = this.base_url + '/api/auth/update-password';
    resend_otp = this.base_url + '/api/auth/login/resend-otp';
    report_resend = this.base_url + "/api/reports/resend/";
    apiKeys = this.base_url + "/api/user/api-keys";
    DownloadShorlinkReports='http://159.69.120.60/sf/download/report/short/';
    constructor(private http: Http) {
    }

    verify() {
        return true;
    }

    forgotPassword(email: string) {
        return this.http.post('/api/forgot-password', JSON.stringify({ email }), this.jwt()).map((response: Response) => response.json());
    }

    getAll() {
        return this.http.get('/api/users', this.jwt()).map((response: Response) => response.json());
    }

    getById(id: number) {
        return this.http.get('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    create(user: User) {
        return this.http.post('/api/users', user, this.jwt()).map((response: Response) => response.json());
    }

    update(user: User) {
        return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }

    // private helper methods

    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}