import { HttpEvent, HttpHandler, HttpInterceptor, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { ActivatedRoute } from '@angular/router';
import { ContactlistComponent } from './theme/pages/default/contactlist/contactlist.component';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    current_routes = [];
    constructor(private _router: Router, private _route: ActivatedRoute) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.current_routes = [];
        req = req.clone({
            setHeaders: {
                // 'Content-Type': 'application/json; charset=utf-8',
                'Accept': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
        });

        return next.handle(req)
            .map((event: HttpEvent<any>) => {
                //no need to do anything when the response is success.just forward the response
                //debugger
                return event;
            })
            .catch((err: any, caught) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        localStorage.removeItem('token');
                        if (this._router.url != '/login') {
                            // window.location.href = "/login"
                            window.location.href = "/login?c_url=" + this._router.url;
                        }
                        //this._router.navigate(['/login'],{queryParams:{c_url:this._router.url}});

                    }
                    //console.log(err.status);

                    return Observable.throw(err);
                }
            });
    }
}






// import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/do';
// import {tap} from 'rxjs/operators';
// import 'rxjs/add/operator/catch';

// @Injectable()
// export class AuthInterceptor implements HttpInterceptor {

//    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//        req = req.clone({
//            setHeaders: {
//                'Content-Type': 'application/json; charset=utf-8',
//                'Accept': 'application/json',
//                'Authorization': `Bearer ${localStorage.getItem('tokenUser')}`,
//            },
//        });

//        return next.handle(req)
//            .map((event: HttpEvent<any>) => {
//                //no need to do anything when the response is success.just forward the response
//                return event;
//            })
//            .catch((err: any, caught) => {
//                if (err instanceof HttpErrorResponse) {
//                    if (err.status === 401) {
//                        localStorage.removeItem('tokenUser');
//                        window.location.reload();
//                    }
//                    return Observable.throw(err);
//                }
//            });
//    }
// }