import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from "./auth/logout/logout.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ThemeComponent } from "./theme/theme.component";
import { AuthGuard } from "./auth/_guards/auth.guard";
import { ErrorComponent } from '../app/error/error/error.component';
const routes: Routes = [
    { path: 'login', loadChildren: './auth/auth.module#AuthModule' },
    { path: 'logout', component: LogoutComponent },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'dashboard', canActivate: [AuthGuard], component: DashboardComponent },
    { path: 'app', canActivate: [AuthGuard], component: ThemeComponent },
    //    {path:'**',component:ErrorComponent},
    //    {path: '404', component: ErrorComponent},
    //   {path: '**', redirectTo: '404'}
    // { path: '**', loadChildren: './error/error.module#ErrorModule' },
    // {
    //     path: '**',
    //     // redirectTo: "/404",
    //     "loadChildren": './error/error.module#ErrorModule',

    //     // pathMatch: "full",
    //     // "canActivate": [AuthGuard]
    // }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }