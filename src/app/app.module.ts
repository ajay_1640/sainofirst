import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { DashboardComponent } from './dashboard/dashboard.component';
import { OcrComponent } from './ocr/ocr.component';
import { ocrModule } from './ocr/ocr.module';
import { FormsModule } from "@angular/forms";
import { AuthInterceptor } from './auth.interceptor';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { WalletService } from './theme/layouts/header-nav/wallet.service';
import { FileuploadService } from './fileupload.service';
import { FileUploadInterceptor } from './fileupload.interceptor';
import { ErrorComponent } from './error/error/error.component';


// import { CookieService } from 'angular2-cookie/services/cookies.service';

export let InjectorInstance: Injector;

//import {MatSelectModule} from '@angular/material/select';
@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent,
        DashboardComponent,
        OcrComponent,
        // ErrorComponent,
    ],
    imports: [
        LayoutModule,

        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        ocrModule,
        AuthModule,
        FormsModule,
        Ng2SearchPipeModule, NgxPaginationModule,
        NgMultiSelectDropDownModule.forRoot()
    ],
    providers: [
        WalletService,
        CookieService,
        ScriptLoaderService,
        AppService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        FileuploadService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: FileUploadInterceptor,
            multi: true
        },
        // { provide: CookieOptions, useValue: {} }

    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private injector: Injector) {
        InjectorInstance = this.injector;
    }

}